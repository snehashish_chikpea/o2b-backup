@isTest
public class TestNewPackageExtension1
{
    @isTest
    static void testMethod1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false);
        insert(o2bset);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Package__c pkg = new ChikPeaO2B__Package__c();
        
        
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(pkg);
        NewPackageExtension1 newpkgext = new NewPackageExtension1(stdc);
        PageReference pageRef = Page.NewPackage1;
        Test.setCurrentPage(pageRef);
        pkg.name='1st package';
        pkg.Package_Type__c='Recurring';
        pkg.Bill_Cycle__c='Monthly';
        pkg.Price_Book__c=pb.id;
        pkg.Bill_On_Package__c=true;
        newpkgext.PackageNew=pkg;
        newpkgext.savePackage();
        newpkgext.editpackage();
        newpkgext.UpdatePkg();
        newpkgext.cancel();
        
        newpkgext.UnitPriceCalculation(12,'Annual','Daily');
        newpkgext.UnitPriceCalculation(12,'Annual','Monthly');
        newpkgext.UnitPriceCalculation(12,'Annual','Quarterly');
        newpkgext.UnitPriceCalculation(12,'Annual','Weekly');
        newpkgext.UnitPriceCalculation(12,'Annual','Half Yearly');
        
        newpkgext.UnitPriceCalculation(12,'Daily','Annual');
        newpkgext.UnitPriceCalculation(12,'Daily','Monthly');
        newpkgext.UnitPriceCalculation(12,'Daily','Quarterly');
        newpkgext.UnitPriceCalculation(12,'Daily','Weekly');
        newpkgext.UnitPriceCalculation(12,'Daily','Half Yearly');
        
        newpkgext.UnitPriceCalculation(12,'Monthly','Annual');
        newpkgext.UnitPriceCalculation(12,'Monthly','Daily');
        newpkgext.UnitPriceCalculation(12,'Monthly','Quarterly');
        newpkgext.UnitPriceCalculation(12,'Monthly','Weekly');
        newpkgext.UnitPriceCalculation(12,'Monthly','Half Yearly');
        
        newpkgext.UnitPriceCalculation(12,'Quarterly','Annual');
        newpkgext.UnitPriceCalculation(12,'Quarterly','Monthly');
        newpkgext.UnitPriceCalculation(12,'Quarterly','Daily');
        newpkgext.UnitPriceCalculation(12,'Quarterly','Weekly');
        newpkgext.UnitPriceCalculation(12,'Quarterly','Half Yearly');
        
        newpkgext.UnitPriceCalculation(12,'Weekly','Annual');
        newpkgext.UnitPriceCalculation(12,'Weekly','Monthly');
        newpkgext.UnitPriceCalculation(12,'Weekly','Daily');
        newpkgext.UnitPriceCalculation(12,'Weekly','Quarterly');
        newpkgext.UnitPriceCalculation(12,'Weekly','Half Yearly');
        
        newpkgext.UnitPriceCalculation(12,'Half Yearly','Annual');
        newpkgext.UnitPriceCalculation(12,'Half Yearly','Monthly');
        newpkgext.UnitPriceCalculation(12,'Half Yearly','Daily');
        newpkgext.UnitPriceCalculation(12,'Half Yearly','Quarterly');
        newpkgext.UnitPriceCalculation(12,'Half Yearly','Weekly');
        
        newpkgext.UnitPriceCalculation(12,'2','Annual');
        newpkgext.UnitPriceCalculation(12,'2','Monthly');
        newpkgext.UnitPriceCalculation(12,'2','Daily');
        newpkgext.UnitPriceCalculation(12,'2','Quarterly');
        newpkgext.UnitPriceCalculation(12,'2','Weekly');
        newpkgext.UnitPriceCalculation(12,'2','Half Yearly');
        
    }
    @isTest
    static void testMethod2()
    {
        List<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        List<ChikPeaO2B__Tiered_Pricing__c>tieredlist=new List<ChikPeaO2B__Tiered_Pricing__c>();
        List<ChikPeaO2B__Volume_Pricing__c>volumelist=new list<ChikPeaO2B__Volume_Pricing__c>();
    
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false);
        insert(o2bset);
        
        Account acc=new Account(name='acc1');
        insert(acc);
    
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c rec1=new ChikPeaO2B__Item__c(name='rec1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec1);
        ChikPeaO2B__Item__c recV=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(recV);
        ChikPeaO2B__Item__c rec_1=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec_1);
        ChikPeaO2B__Item__c rec1_1=new ChikPeaO2B__Item__c(name='rec1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec1_1);
        ChikPeaO2B__Item__c recV_1=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(recV_1);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        itemlist.add(pre);
        ChikPeaO2B__Item__c oneoffT=new ChikPeaO2B__Item__c(name='oneoffT',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoffT);
        ChikPeaO2B__Item__c oneoffV=new ChikPeaO2B__Item__c(name='oneoffV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoffV);
        ChikPeaO2B__Item__c oneofford=new ChikPeaO2B__Item__c(name='oneofford',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneofford);        
        ChikPeaO2B__Item__c record=new ChikPeaO2B__Item__c(name='record',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(record);
        ChikPeaO2B__Item__c postord=new ChikPeaO2B__Item__c(name='postord',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(postord);
        ChikPeaO2B__Item__c preord=new ChikPeaO2B__Item__c(name='preord',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        itemlist.add(preord);
        ChikPeaO2B__Item__c itm1=new ChikPeaO2B__Item__c(name='Item1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(itm1);
        ChikPeaO2B__Item__c itmx=new ChikPeaO2B__Item__c(name='Itemx',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(itmx);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp4);
        ChikPeaO2B__Rate_Plan__c rp5=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp5);
        ChikPeaO2B__Rate_Plan__c rp6=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='GB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp6);
        ChikPeaO2B__Rate_Plan__c rp7=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Non_Recurring_Charge__c=35,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='KB',ChikPeaO2B__Usage_Rate__c=1.5);
        RPlist.add(rp7);
        ChikPeaO2B__Rate_Plan__c rp8=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp8);
        ChikPeaO2B__Rate_Plan__c rp9=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneofford.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp9);
        ChikPeaO2B__Rate_Plan__c rp10=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=record.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp10);
        ChikPeaO2B__Rate_Plan__c rp11=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=postord.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp11);
        ChikPeaO2B__Rate_Plan__c rp12=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=preord.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp12);
        ChikPeaO2B__Rate_Plan__c rpp=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rpp);
        ChikPeaO2B__Rate_Plan__c rppx=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itmx.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rppx);
        
        ChikPeaO2B__Rate_Plan__c rp2_1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=rec1_1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2_1);
        ChikPeaO2B__Rate_Plan__c rp3_1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=rec_1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp3_1);
        ChikPeaO2B__Rate_Plan__c rp8_1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=recV_1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp8_1);
        insert(RPlist);
             
        ChikPeaO2B__Tiered_Pricing__c tr1=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr1);
        ChikPeaO2B__Tiered_Pricing__c tr2=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr2);
        ChikPeaO2B__Tiered_Pricing__c tr3=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr3);
        ChikPeaO2B__Tiered_Pricing__c tr4=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=200,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr4);
        ChikPeaO2B__Tiered_Pricing__c tr5=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=201,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=11,ChikPeaO2B__Usage_Rate__c=4);
        tieredlist.add(tr5);
        ChikPeaO2B__Tiered_Pricing__c tr6=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Non_Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr6);
        ChikPeaO2B__Tiered_Pricing__c tr7=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Non_Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr7);
        ChikPeaO2B__Tiered_Pricing__c tr1_1=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec_1.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp3_1.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr1_1);
        ChikPeaO2B__Tiered_Pricing__c tr2_1=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec_1.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp3_1.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr2_1);
        insert(tieredlist);
        
        ChikPeaO2B__Volume_Pricing__c vp1=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp1);
        ChikPeaO2B__Volume_Pricing__c vp2=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Non_Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp2);
        ChikPeaO2B__Volume_Pricing__c vp3=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp3);
        ChikPeaO2B__Volume_Pricing__c vp4=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Non_Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp4);
        ChikPeaO2B__Volume_Pricing__c vp5=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp8.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp5);
        ChikPeaO2B__Volume_Pricing__c vp6=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp8.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp6);
        ChikPeaO2B__Volume_Pricing__c vp5_1=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV_1.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp8_1.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp5_1);
        ChikPeaO2B__Volume_Pricing__c vp6_1=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV_1.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp8_1.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp6_1);
        insert(volumelist);
        
        ChikPeaO2B__Package__c pkg = new ChikPeaO2B__Package__c(name='pkg1',ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Package_Type__c='Post-Paid Usage',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Bill_On_Package__c=false);
        insert(pkg);
        ChikPeaO2B__Bundled_Item__c bi=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rpp.id,ChikPeaO2B__Usage_Rate__c=1.2);
        insert(bi);
        ChikPeaO2B__Item__c itm2=new ChikPeaO2B__Item__c(name='pkg1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Is_package_Item__c=true);    
        insert(itm2);
        ChikPeaO2B__Rate_Plan__c rpp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=itm2.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rpp1);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(pkg);
        NewPackageExtension1 newpkgext = new NewPackageExtension1(stdc);
        PageReference pageRef = Page.NewPackage1;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',pkg.id);
        NewPackageExtension1 newpkgext1 = new NewPackageExtension1(stdc);
        newpkgext.cancelPkgeditcreate();
        newpkgext.PackageNew.package_item__c=itm2.id;
        newpkgext.updatepkg();
         //========================= one off [START] =============================//
        list<ListOfItem.Item>OneList=newpkgext1.getOneOffItems();
        newpkgext1.oneoffgetmethod();
        for(ListOfItem.Item item : OneList)
        {
            if(item.itm.name!='oneofford')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)//only one rp
                {
                    item.selectedRP.put(rp.id,true);
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)//only one rp
                {
                    item.orderedRP.put(rp.id,true);
                }
            }
        }
        list<ListOfItem.Item>OneList1=newpkgext1.getOneOffItems();
        //========================= one off [END] =============================//
        
        //========================= PrePaid [START] =============================//
        list<ListOfItem.Item>PreList=newpkgext1.getPreItems();
        newpkgext1.pregetmethod();
        list<ListOfItem.Item>PreList1=newpkgext1.getPreItems();
        newpkgext1.pregetmethod();
        for(ListOfItem.Item item : PreList1)
        {
            if(item.itm.name!='preord')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.selectedRP.put(rp.id,true);
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.orderedRP.put(rp.id,true);
                }
            }
        }
        list<ListOfItem.Item>PreList2=newpkgext1.getPreItems();
        //========================= PrePaid [END] =============================//
        
        //========================= Recurring [START] =============================//
        list<ListOfItem.Item>RecList=newpkgext1.getRecItems();
        newpkgext1.recgetmethod();
        list<ListOfItem.Item>RecList1=newpkgext1.getRecItems();
        newpkgext1.recgetmethod();
        for(ListOfItem.Item item : RecList1)
        {
            if(item.itm.name!='record')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.selectedRP.put(rp.id,true);
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.orderedRP.put(rp.id,true);
                }
            }
        }
        list<ListOfItem.Item>RecList2=newpkgext1.getRecItems();
        //========================= Recurring [END] =============================//
        
        //========================= Postpaid [START] =============================//
        list<ListOfItem.Item>PostList=newpkgext1.getPostItems();
        newpkgext1.postgetmethod();
        list<ListOfItem.Item>PostList1=newpkgext1.getPostItems();
        newpkgext1.postgetmethod();
        for(ListOfItem.Item item : PostList1)
        {
            if(item.itm.name!='postord')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.selectedRP.put(rp.id,true); 
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.orderedRP.put(rp.id,true); 
                }
            }
        }
        list<ListOfItem.Item>PostList2=newpkgext1.getPostItems();
        //========================= Postpaid [END] =============================//
        
        newpkgext1.insertBundledItem();
        newpkgext1.FinalPriceCalculation();
        
        ChikPeaO2B__Package__c pkg1 = new ChikPeaO2B__Package__c(name='pkg1',ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Package_Type__c='Post-Paid Usage',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Bill_On_Package__c=true);
        insert(pkg1);
        ChikPeaO2B__Bundled_Item__c bi1=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rpp.id,ChikPeaO2B__Usage_Rate__c=1.2);
        insert(bi1);
        ChikPeaO2B__Item__c itm3=new ChikPeaO2B__Item__c(name='pkg1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Is_package_Item__c=true);    
        insert(itm3);
        ChikPeaO2B__Rate_Plan__c rpp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=itm3.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rpp2);
        Apexpages.Standardcontroller stdc1 = new Apexpages.Standardcontroller(pkg1);
        PageReference pageRef1 = Page.NewPackage1;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('id',pkg1.id);
        NewPackageExtension1 newpkgext2 = new NewPackageExtension1(stdc1);
        newpkgext2.cancelPkgeditcreate();
        newpkgext2.PackageNew.Package_Item__c=null;
        newpkgext2.UpdatePkg();
        newpkgext2.getSeachOptions();
        newpkgext2.SelectedSearch='Category__c';
        newpkgext2.searchVal='software';
        newpkgext2.searchitem();
        
        newpkgext2.SelectedSearch='Name';
        newpkgext2.searchVal='pkg1';
        newpkgext2.searchitem();
        
        newpkgext2.ClickedRP=rp4.id;
        newpkgext2.showPopup();
        newpkgext2.closePopup();
        
        boolean onehn=newpkgext2.OneOffhasNext;
        boolean onehp=newpkgext2.OneOffhasPrevious;
        integer onepgn = newpkgext2.OneOffpageNumber;
        integer onetotpg = newpkgext2.Oneoffpages;
        newpkgext2.oneoffnxt();
        newpkgext2.oneoffprv();
        newpkgext2.Oneofffirst();
        newpkgext2.Oneofflast();
        newpkgext2.OneOffRPSelPriceChangeMethod();
        newpkgext2.OneOffQtyChangeMethod();
        
        boolean prehn=newpkgext2.PrehasNext;
        boolean prehp=newpkgext2.PrehasPrevious;
        integer prepgn = newpkgext2.prepageNumber;
        integer pretotpg = newpkgext2.prepages;
        newpkgext2.prenxt();
        newpkgext2.preprv();
        newpkgext2.Preffirst();
        newpkgext2.prelast();
        newpkgext2.preRPSelPriceChangeMethod();
        
        boolean rechn=newpkgext2.RechasNext;
        boolean rechp=newpkgext2.RechasPrevious;
        integer recpgn = newpkgext2.recpageNumber;
        integer rectotpg = newpkgext2.recpages;
        newpkgext2.recnxt();
        newpkgext2.recprv();
        newpkgext2.recffirst();
        newpkgext2.reclast();
        newpkgext2.recRPSelPriceChangeMethod();
        newpkgext2.PreQtyChangeMethod();
        
        boolean posthn=newpkgext2.PosthasNext;
        boolean posthp=newpkgext2.PosthasPrevious;
        integer postpgn = newpkgext2.postpageNumber;
        integer posttotpg = newpkgext2.postpages;
        newpkgext2.postnxt();
        newpkgext2.postprv();
        newpkgext2.postffirst();
        newpkgext2.postlast();
        newpkgext2.postRPSelPriceChangeMethod();
        newpkgext2.RecQtyChangeMethod();
        newpkgext2.FinalPriceCalculation();
        newpkgext2.PostQtyChangeMethod();
    }
}