public  class PG_Vantiv implements PG_PaymentGateway{
	
	public static final Boolean TOKENIZATION_SUPPORT = false;
    //
    public static final String X_METHOD_ECHECK = 'ECHECK';
    public static final String ECHECK_BANK_ACC_TYPE_BUSINESSCHECKING = 'BusinessChecking';
	public static final String ECHECK_BANK_ACC_TYPE_CHECKING = 'Checking';
	public static final String ECHECK_BANK_ACC_TYPE_SAVINGS = 'Savings';
	
    String gatewayName;
    String bin;
    String industryType;
    Boolean primarySiteAvailable;
    PG_CCDetails details;
    PG_BADetails acDetails;
    PG_Config config;   
    PG_Response pgResponse;
    PG_VantivXmlParser vantiv_xml_parser=new PG_VantivXmlParser();
    
    public Integer R_TIMEOUT_MS{get; set;}
    public String R_ENDPOINT{get; set;}
    public String TerminalID{get; set;}//Required
    public String Amount{get; set;}//Required
    public String CVV{get; set;}
    public String OrderID{get; set;}//Required
    public String Comments{get; set;}
    public String TxRefNum{get; set;}//for Refund, MFC or void
    public String TxRefIdx{get; set;}
    public String PriorAuthCd{get; set;}//for TrxType = FC
	
	public static Map<String, String> bankAccountType;
	public String TransactionRef{get; set;}
	public String InvoiceRef{get; set;}
	public String CustomerRef{get; set;}
	public String EcheckType{get; set;}
	public String firstName{get; set;}
	public String lastName{get; set;}
	public String addressLine1{get; set;}
	public String city{get; set;}
	public String state{get; set;}
	public String zip{get; set;}
	public String country{get; set;}
	public String email{get; set;}
	public String phone{get; set;}
    
    
    
    public void init(PG_CCDetails details, PG_Config config, Map<String, String> transactionInfo)//inject variying gateway parameters
    {
        //gatewayName = 'ChasePaymentech:Orbital';
        System.debug('---details='+details+', config='+config+', transactionInfo='+transactionInfo);
        
        if(details.creditCardType == null){
        	throw new PG_Exception('Gateway Exception::Card type missing');
        }
        
        
        gatewayName = config.paymentGatewayName;
        //bin = PG_Litle.O_BIN_SALEM;//TODO: make it configuarable
        //industryType = PG_Litle.O_INDUSTRY_TYPE_MAIL_ORDER;//TODO: make it configuarable
        primarySiteAvailable = config.primarySiteAvailable;
        this.details = details;
        this.config = config;
        pgResponse = new PG_Response();
        pgResponse.paymentGatewayName = gatewayName;
        
        this.Amount = transactionInfo.get('Amount')!=null?
            transactionInfo.get('Amount'):'';//implied decimal, $100 as Amount = 10000
        this.CVV = transactionInfo.get('CVV')!=null?
            transactionInfo.get('CVV'):'';
        if(Amount != '')
        {
	        try
	        {
	        	Decimal amnt = Decimal.valueOf(Amount);
	        	Amount = String.valueOf(Integer.valueOf(amnt*100));
	        }
	        catch(TypeException ex)
	        {
	        	throw new PG_Exception('Gateway Exception::Invalid amount - '+Amount);
	        }
        }
        this.TerminalID = transactionInfo.get('terminalID')!=null?
            transactionInfo.get('terminalID'):'';
        this.OrderID = transactionInfo.get('orderID')!=null?
            transactionInfo.get('orderID'):'';
        this.Comments = transactionInfo.get('comments')!=null?
            transactionInfo.get('comments'):'';
        this.TxRefNum = transactionInfo.get('TransactionRef')!=null?
            transactionInfo.get('TransactionRef'):'';
        System.debug('&&&&&&&&'+TxRefNum);
        this.TxRefIdx = transactionInfo.get('TxRefIdx')!=null?
            transactionInfo.get('TxRefIdx'):'';
        this.PriorAuthCd = transactionInfo.get('priorAuthCd')!=null?
            transactionInfo.get('priorAuthCd'):'';
        
        R_TIMEOUT_MS = config.timeoutLimit*1000;
        R_ENDPOINT = config.testMode?
            (primarySiteAvailable?config.gatewayUrlTest:config.gatewayUrlTestFallback):
            (primarySiteAvailable?config.gatewayUrlProd:config.gatewayUrlProdFallback);
            
        //paymentechGateway = new PG_Vantiv_Operation.PaymentechGateway(R_ENDPOINT,  R_TIMEOUT_MS);
        
        System.debug('---init():PG name='+gatewayName+', CC details='+details.debug()+', PG config='+config);
        //+', transactionInfo='+transactionInfo);
    }
    public void processTransaction(PG.TransactionType transactionType){//@throws PG_Exception
        
        System.debug('***>>');
        if(config.tokenization && !TOKENIZATION_SUPPORT)
    		throw new PG_Exception('Gateway Exception::Tokenization not supported for '+gatewayName);
        
        String resBody = '';
        
		/**if(PG.TransactionType.VERIFY == transactionType){
			pgResponse.transactionType = PG.TransactionType.VERIFY.name();
			resBody = verify();//Verify
		}else */
		if(PG.TransactionType.AUTHORIZATION == transactionType){
			pgResponse.transactionType = PG.TransactionType.AUTHORIZATION.name();
			resBody = authorization();//Auth
		}else if(PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE == transactionType){
			pgResponse.transactionType = PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE.name();
			resBody = markForCapture();//Delayed Capture
		}else if(PG.TransactionType.AUTHORIZATION_CAPTURE == transactionType){
			pgResponse.transactionType = PG.TransactionType.AUTHORIZATION_CAPTURE.name();
			resBody = authorizationCapture();//Sale			
		}else if(PG.TransactionType.REVERSEAL == transactionType){
			pgResponse.transactionType = PG.TransactionType.REVERSEAL.name();
			resBody = reversal();//Void
		}else if(PG.TransactionType.REFUND == transactionType){
			pgResponse.transactionType = PG.TransactionType.REFUND.name();
			resBody = refund();//Credit
		}else{
			throw new PG_Exception('Gateway Exception::transaction '+transactionType.name()+
				' not supported for '+gatewayName);
		}
		
		populateResponse(vantiv_xml_parser.processXML(resBody));
        
    }
    
   public PG_Response getPGResponse()
   {
        return pgResponse;
   }
   public String authorization()//@throws PG_Exception
   {
        System.debug('AUTHORIZATION PROCESS INVOKED************');
        String resBody='';
        String reqBody='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
                     '<litleOnlineRequest version="8.10" xmlns="http://www.litle.com/schema" merchantId="default">'+
                       '<authentication>'+
                          '<user>'+config.loginId+'</user>'+
                          '<password>'+config.password+'</password>'+
                       '</authentication>'+
                          '<authorization reportGroup="Default Report Group">'+
                          '<orderId>'+OrderID+'</orderId>'+
                          '<amount>'+amount+'</amount>'+
                          '<orderSource>ecommerce</orderSource>'+
                          '<billToAddress>'+
                              '<name>'+details.ccFirstName+'</name>'+
                              '<addressLine1>'+details.ccStreet1+'</addressLine1>'+
                              '<city>'+details.ccCity+'</city>'+
                              '<state>'+details.ccState+'</state>'+
                              '<zip>'+details.ccPostalCode+'</zip>'+
                              '<country>'+details.ccCountry+'</country>'+
                          '</billToAddress>'+
                          '<card>'+
                              '<type>'+getCreditCardType(details.creditCardType)+'</type>'+
                              '<number>'+details.creditCardNumber+'</number>'+
                              '<expDate>'+getExpDate(details.expiryMonth,details.expiryYear)+'</expDate>'+
                            //'<cardValidationNum>''</cardValidationNum>'
                          '</card>'+
                       '</authorization>'+
                    '</litleOnlineRequest>';
                    
        
        resBody = submitCall(reqBody).replaceAll('\'','\"');
        if(Test.isRunningTest()){
			resBody = '<litleOnlineResponse version=\'8.25\' response=\'0\' message=\'Valid Format\' xmlns=\'http://www.litle.com/schema\'><authorizationResponse id=\'\' reportGroup=\'Default Report Group\' customerId=\'\'><litleTxnId>107857590806525000</litleTxnId><orderId>auth1</orderId><response>000</response><responseTime>2015-01-20T02:55:05</responseTime><message>Approved</message><authCode>24319</authCode></authorizationResponse></litleOnlineResponse>';
		}
        return resBody;
    }
    
    public String markForCapture()//@throws PG_Exception
    {
        System.debug('CAPTURE PROCESS INVOKED************');
        String resBody='';
        String reqBody='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
                         '<litleOnlineRequest merchantId="" merchantSdk="Java;8.25.2" version="8.25" xmlns="http://www.litle.com/schema">'+
                              '<authentication>'+
                                 '<user>'+config.loginId+'</user>'+
                                 '<password>'+config.password+'</password>'+
                              '</authentication>'+
                              '<capture reportGroup="Default Report Group">'+
                                 '<litleTxnId>'+TxRefNum+'</litleTxnId>'+
                             '</capture>'+
                          '</litleOnlineRequest>';
        
        resBody = submitCall(reqBody).replaceAll('\'','\"');
        if(Test.isRunningTest()){
			resBody = ' <litleOnlineResponse version=\'8.25\' response=\'0\' message=\'Valid Format\' xmlns=\'http://www.litle.com/schema\'><captureResponse id=\'\' reportGroup=\'Default Report Group\' customerId=\'\'><litleTxnId>768469232119275000</litleTxnId><response>000</response><responseTime>2015-01-20T03:22:01</responseTime><message>Approved</message></captureResponse></litleOnlineResponse>';
		}
        return resBody;
    }
   /** public String partialCapture()//@throws PG_Exception
    {
        System.debug('PARTIAL PROCESS INVOKED************');
        String resBody='';
        String reqBody='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
                         '<litleOnlineRequest merchantId="" merchantSdk="Java;8.25.2" version="8.25" xmlns="http://www.litle.com/schema">'+
                              '<authentication>'+
                                 '<user>'+config.loginId+'</user>'+
                                 '<password>'+config.password+'</password>'+
                              '</authentication>'+
                              '<capture reportGroup="Default Report Group">'+
                                 '<litleTxnId>'+TxRefNum+'</litleTxnId>'+
                                 '<amount>'+amount+'</amount>'+
                             '</capture>'+
                          '</litleOnlineRequest>';
        
         resBody = submitCall(reqBody).replaceAll('\'','\"');
         if(Test.isRunningTest()){
         	resBody='<litleOnlineResponse version=\'8.25\' response=\'0\' message=\'Valid Format\' xmlns=\'http://www.litle.com/schema\'><captureResponse id=\'\' reportGroup=\'Default Report Group\' customerId=\'\'><litleTxnId>837226653257794000</litleTxnId><response>000</response><responseTime>2015-01-20T03:30:35</responseTime><message>Approved</message></captureResponse></litleOnlineResponse>';
		}
         return resBody;
    }**/
   
    public String refund()//@throws PG_Exception
    {
        System.debug('REFUND OR CREDIT PROCESS INVOKED************');
        String resBody='';
        String reqBody='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
                          '<litleOnlineRequest merchantId="" merchantSdk="Java;8.25.2" version="8.25" xmlns="http://www.litle.com/schema">'+
                             '<authentication>'+
                                 '<user>'+config.loginId+'</user>'+
                                 '<password>'+config.password+'</password>'+
                             '</authentication>'+
                             '<credit reportGroup="Default Report Group">'+
                                 '<litleTxnId>'+TxRefNum+'</litleTxnId>'+
                             '</credit>'+
                          '</litleOnlineRequest>';
        resBody = submitCall(reqBody).replaceAll('\'','\"');
        if(Test.isRunningTest()){
        	resBody='<litleOnlineResponse version=\'8.25\' response=\'0\' message=\'Valid Format\' xmlns=\'http://www.litle.com/schema\'><creditResponse id=\'\' reportGroup=\'Default Report Group\' customerId=\'\'><litleTxnId>896481106525218000</litleTxnId><response>000</response><responseTime>2015-01-20T03:25:43</responseTime><message>Approved</message></creditResponse></litleOnlineResponse>';
		}
        return resBody;
    }
    
    public String authorizationCapture()//@throws PG_Exception
    {
        System.debug('SALE PROCESS INVOKED************');
        String resBody='';
        String reqBody='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
                      '<litleOnlineRequest version="8.10" xmlns="http://www.litle.com/schema" merchantId="default">'+
                         '<authentication>'+
                            '<user>'+config.loginId+'</user>'+
                            '<password>'+config.password+'</password>'+
                         '</authentication>'+
                         '<sale reportGroup="Default Report Group">'+
                          '<orderId>'+OrderID+'</orderId>'+
                            '<amount>'+Amount+'</amount>'+
                            '<orderSource>ecommerce</orderSource>'+
                            '<billToAddress>'+
                                '<name>'+details.ccFirstName+'</name>'+
                                '<addressLine1>'+details.ccStreet1+'</addressLine1>'+
                                '<city>'+details.ccCity+'</city>'+
                                '<state>'+details.ccState+'</state>'+
                                '<zip>'+details.ccPostalCode+'</zip>'+
                                '<country>'+details.ccCountry+'</country>'+
                           '</billToAddress>'+
                           '<card>'+
                              '<type>'+getCreditCardType(details.creditCardType)+'</type>'+
                              '<number>'+details.creditCardNumber+'</number>'+
                              '<expDate>'+getExpDate(details.expiryMonth,details.expiryYear)+'</expDate>'+
                            //'<cardValidationNum>''</cardValidationNum>'
                           '</card>'+
                         '</sale>'+
                    '</litleOnlineRequest>';
        resBody = submitCall(reqBody).replaceAll('\'','\"');
        if(Test.isRunningTest()){
        	resBody='<litleOnlineResponse version=\'8.25\' response=\'0\' message=\'Valid Format\' xmlns=\'http://www.litle.com/schema\'><saleResponse id=\'\' reportGroup=\'Default Report Group\' customerId=\'\'><litleTxnId>509187710415384000</litleTxnId><orderId>1</orderId><response>000</response><responseTime>2015-01-20T03:32:33</responseTime><message>Approved</message><authCode>74688</authCode></saleResponse></litleOnlineResponse>';
		}
        return resBody;
    }
    public String reversal()//@throws PG_Exception
    {
        System.debug('VOID or CANCEL PROCESS INVOKED************');
        String resBody='';
        String reqBody='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
                         '<litleOnlineRequest merchantId="" merchantSdk="Java;8.25.2" version="8.25" xmlns="http://www.litle.com/schema">'+
                             '<authentication>'+
                                 '<user>'+config.loginId+'</user>'+
                                 '<password>'+config.password+'</password>'+
                             '</authentication>'+
                             '<void reportGroup="Default Report Group">'+
                                 '<litleTxnId>'+TxRefNum+'</litleTxnId>'+
                             '</void>'+
                          '</litleOnlineRequest>';
        resBody = submitCall(reqBody).replaceAll('\'','\"');
        if(Test.isRunningTest()){
			resBody = '<litleOnlineResponse version=\'9.3\' response=\'0\' message=\'Valid Format\' xmlns=\'http://www.litle.com/schema\'><voidResponse id=\'\' reportGroup=\'Default Report Group\' customerId=\'\'><litleTxnId>233319267838709000</litleTxnId><response>000</response><responseTime>2015-02-16T02:47:23</responseTime><message>Approved</message></voidResponse></litleOnlineResponse>';
		}
        return resBody;
    }
    
    
    public void populateResponse(Map<String,String> vantiv_response)
    {
    	System.debug('populateResponse called--process>>'+vantiv_response.get('process'));
    	if(vantiv_response.get('process').equals('Y'))
    	{
    	  pgResponse.processStatus = vantiv_response.get('response') == '000'?
          PG.Status.SUCCESS.name():PG.Status.FAILURE.name();
          pgResponse.transactionId = vantiv_response.get('litleTxnId');
          pgResponse.transactionTime =getTimeStamp(vantiv_response.get('responseTime')) ;
          pgResponse.responseMessage = vantiv_response.get('message');
          pgResponse.responseCode = vantiv_response.get('response');
          //pgResponse.authcode=vantiv_response.get('authCode');
          pgResponse.isException = false;
          pgResponse.approvalStatus = vantiv_response.get('message');
        }
    	else if(vantiv_response.get('process').equals('N'))
    	{
    	   pgResponse.isException = true;
    	   pgResponse.exceptionMessage =vantiv_response.get('error_message') ;
    	   pgResponse.responseMessage =vantiv_response.get('error_message');
    	}
        System.debug('pgResponse---'+pgResponse);
    }
    
    private Datetime getTimeStamp(String d)
    {
        
        string year1 = d.substring(0, 4);
        string month1 = d.substring(5, 7);
        string day1 = d.substring(8, 10);
        string hour1 = d.substring(11, 13);
        string minute1 = d.substring(14, 16);
        string second1 = d.substring(17);

        Integer year, month, day, hour, minute, second;
        year = Integer.valueOf(year1);
        month = Integer.valueOf(month1);
        day = Integer.valueOf(day1);
        hour = Integer.valueOf(hour1);
        minute = Integer.valueOf(minute1);
        second = Integer.valueOf(second1);

        Datetime t1 = Datetime.newInstance(year, month, day, hour, minute, second);
        
        return t1;
    }
    private String getCreditCardType(String creditCard)
    {  
      String ccType;
      
      if(creditCard.equals('Visa'))
           ccType='VI';
      else if(creditCard.equals('Master'))
          ccType='MC';
      else if(creditCard.equals('Discover'))
          ccType='DI';
      else if(creditCard.equals('American Express'))
         ccType='AX';
      
      return ccType;
    }
    private String getExpDate(String month,String year)
    {  
      String exp_date;
      
      if(year.length()==4)
           year=year.substring(2,4);
           
           
        exp_date=month+year;
      return exp_date;
    }
    private String submitCall(String reqBody){//@throws PG_Exception
		String resBody = '';
		//pgResponse.reqLog = reqBody;
		Map<String, String> vantiv_httpHeader=new Map<String, String>();
        vantiv_httpHeader.put('Content-Type','text/xml');
		PG_Caller caller = new PG_Caller(
		    vantiv_httpHeader,
			PG.HTTP_METHOD_POST,
			R_ENDPOINT,
			reqBody,
			R_TIMEOUT_MS
		);	
		resBody = caller.call();
		System.debug('resBody---->'+resBody);
		pgResponse.resLog = resBody;	
		return resBody;
	}

    
	

}