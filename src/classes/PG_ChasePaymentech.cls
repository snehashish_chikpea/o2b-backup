/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-10-08
@see 
---------------------------------------------------------------------------------------------------
*/
public class PG_ChasePaymentech implements PG_PaymentGateway{
    
    public static final Boolean TOKENIZATION_SUPPORT = false;
    
    public static final String O_NEW_ORDER_TRANSACTION_TYPE_AUTHORIZATION = 'A';
    public static final String O_NEW_ORDER_TRANSACTION_TYPE_AUTHORIZATION_CAPTURE = 'AC';
    public static final String O_NEW_ORDER_TRANSACTION_TYPE_FORCE_CAPTURE = 'FC';
    public static final String O_NEW_ORDER_TRANSACTION_TYPE_REFUND = 'R';
    
    public static final String O_BIN_SALEM = '000001';//Salem, NH Transaction Routing dest
    public static final String O_BIN_PNS = '000002'; //PNS or Tampa Transaction Routing dest
    //@depricated
    //public static final String O_MERCHANT_ID_VALUE_NETWORK_BEYOND_SAVINGS = '206734';
    //public static final String O_MERCHANT_ID_VALUE_NETWORK_SIMPLE_SHOPPING = '206736';    
    public static final String O_TERMINAL_ID = '001';
    
    public static final String O_INDUSTRY_TYPE_MAIL_ORDER = 'MO';
    public static final String O_INDUSTRY_TYPE_RECURRING_PAYMENT = 'RC';
    public static final String O_INDUSTRY_TYPE_ECOMMERSE = 'EC';
    public static final String O_INDUSTRY_TYPE_IVR = 'IV';
    public static final String O_INDUSTRY_TYPE_INSTALLMENT = 'IN';  
    
    public static final String O_WSDL_VERSION = '2.7';  
    
    public static final String O_APPROVAL_STATUS_DECLINED = '0';    
    public static final String O_APPROVAL_STATUS_APPROVED = '1';    
    public static final String O_APPROVAL_STATUS_MESSAGE_OR_SYSTEM_ERROR = '2'; 
    
    public static final String O_CARD_VERIFY_PRESENCE_INDICATOR_VALUE_PRESENT = '1';    
    public static final String O_CARD_VERIFY_PRESENCE_INDICATOR_VALUE_PRESENT_BUT_ILLEGIBLE = '2';  
    public static final String O_CARD_VERIFY_PRESENCE_INDICATOR_STATES_DATA_UNAVAILABLE = '9';  
    
    String gatewayName;
    String bin;
    String industryType;
    Boolean primarySiteAvailable;
    PG_CCDetails details;
    PG_Config config;   
    PG_Response pgResponse;
    PG_WSOribitalOperations.PaymentechGateway paymentechGateway;
    
    public String Amount{get; set;}//Required
    //public String MerchantID{get; set;}//Required
    //public String TerminalID{get; set;}//Required
    public String OrderID{get; set;}//Required
    public String Comments{get; set;}
    public String TxRefNum{get; set;}//for Refund, MFC or void
    public String TxRefIdx{get; set;}
    public String PriorAuthCd{get; set;}//for TrxType = FC
    public String CVV{get; set;}//for cardtype Visa, Discover when
    public String CCCardVerifyPresenceInd{get; set;}//for cardtype Visa, Discover when
                // collecting ccCardVerifyNum
        
    public class Fault {
        public String code;
        public String message;
        
        Fault(){
            code = '';
            message = '';
        }       
    }
    
    
    public Integer R_TIMEOUT_MS{get; set;}
    public String R_ENDPOINT{get; set;}

    public void init(PG_CCDetails details, 
        PG_Config config, 
        Map<String, String> transactionInfo//inject variying gateway parameters
    ){
        //gatewayName = 'ChasePaymentech:Orbital';
        //System.debug('---details='+details+', config='+config+', transactionInfo='+transactionInfo);
        gatewayName = config.paymentGatewayName;
        bin = PG_ChasePaymentech.O_BIN_SALEM;//TODO: make it configuarable
        industryType = PG_ChasePaymentech.O_INDUSTRY_TYPE_MAIL_ORDER;//TODO: make it configuarable
        primarySiteAvailable = config.primarySiteAvailable;
        this.details = details;
        this.config = config;
        
        pgResponse = new PG_Response();
        pgResponse.paymentGatewayName = gatewayName;
        
        this.Amount = transactionInfo.get('Amount')!=null?
            transactionInfo.get('Amount'):'';
        //implied decimal, $5.99 as Amount = 599
        if(Amount != ''){
            try{
                Decimal amnt = Decimal.valueOf(Amount);
                Amount = String.valueOf(Integer.valueOf(amnt*100));
            }catch(TypeException ex){
                throw new PG_Exception('Gateway Exception::Invalid amount - '+Amount);
            }
        }
        this.CVV = transactionInfo.get('CVV')!=null?
            transactionInfo.get('CVV'):'';
        //this.MerchantID = transactionInfo.get(//'MerchantID')!=null?
        //    transactionInfo.get(//'MerchantID'):'';
        //this.TerminalID = transactionInfo.get('TerminalID')!=null?
        //    transactionInfo.get('TerminalID'):'';
        this.OrderID = transactionInfo.get('OrderID')!=null?
            transactionInfo.get('OrderID'):'';
        this.Comments = transactionInfo.get('Comment')!=null?
            transactionInfo.get('Comment'):'';
        this.TxRefNum = transactionInfo.get('TransactionRef')!=null?
            transactionInfo.get('TransactionRef'):'';
        this.TxRefIdx = transactionInfo.get('TxRefIdx')!=null?
            transactionInfo.get('TxRefIdx'):'';
        this.PriorAuthCd = transactionInfo.get('PriorAuthCd')!=null?
            transactionInfo.get('PriorAuthCd'):'';
        this.CVV = transactionInfo.get('CVV')!=null?
            transactionInfo.get('CVV'):'';
        this.ccCardVerifyPresenceInd = transactionInfo.get('CardVerifyPresenceInd')!=null?
            transactionInfo.get('CardVerifyPresenceInd'):'';
        
        R_TIMEOUT_MS = config.timeoutLimit*1000;
        R_ENDPOINT = config.testMode?
            (primarySiteAvailable?config.gatewayUrlTest:config.gatewayUrlTestFallback):
            (primarySiteAvailable?config.gatewayUrlProd:config.gatewayUrlProdFallback);
        if(R_ENDPOINT == null || R_ENDPOINT == '') 
            throw new PG_Exception('Gateway Exception::invalid gateway endpoint url.');    
        paymentechGateway = new PG_WSOribitalOperations.PaymentechGateway(
            R_ENDPOINT, 
            R_TIMEOUT_MS
        );
        
        System.debug('---init():PG name='+gatewayName+', CC details='+details.debug()+', PG config=' +config);
        //+', transactionInfo='+transactionInfo);
    }
    
    public void processTransaction(PG.TransactionType transactionType){//@throws PG_Exception
        
        if(config.tokenization && !TOKENIZATION_SUPPORT)
            throw new PG_Exception('Gateway Exception::Tokenization not supported for '+gatewayName);
        
        if(PG.TransactionType.AUTHORIZATION == transactionType){//suthorization
            pgResponse.transactionType = PG.TransactionType.AUTHORIZATION.name();
            populateResponse(authorization());          
        }else if(PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE == transactionType){//capture
            pgResponse.transactionType = PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE.name();
            populateResponse(markForCapture());
        }else if(PG.TransactionType.AUTHORIZATION_CAPTURE == transactionType){//auth capture
            pgResponse.transactionType = PG.TransactionType.AUTHORIZATION_CAPTURE.name();
            populateResponse(authorizationCapture());//force capture            
        }else if(PG.TransactionType.FORCE_CAPTURE == transactionType){
            pgResponse.transactionType = PG.TransactionType.FORCE_CAPTURE.name();
            populateResponse(forceCapture());           
        }else if(PG.TransactionType.REFUND == transactionType){//return/refund/credit
            pgResponse.transactionType = PG.TransactionType.REFUND.name();
            populateResponse(refund());
        }else if(PG.TransactionType.REVERSEAL == transactionType){//void/reversal
            pgResponse.transactionType = PG.TransactionType.REVERSEAL.name();
            populateResponse(reversal());
        }else if(PG.TransactionType.AUTHORIZATION_REVERSEAL == transactionType){//auth reversal
            pgResponse.transactionType = PG.TransactionType.AUTHORIZATION_REVERSEAL.name();
            populateResponse(authorizationReversal());          
        }else{
            throw new PG_Exception('Gateway Exception::transaction '+transactionType.name()+
                ' not supported for '+gatewayName);
        }
    }
    
    public PG_Response getPGResponse(){
        return pgResponse;
    }
    
    public PG_WSOribitalElements.NewOrderResponseElement authorization(){//@throws PG_Exception
        return newOrder(PG_ChasePaymentech.O_NEW_ORDER_TRANSACTION_TYPE_AUTHORIZATION);
    }
    
    public PG_WSOribitalElements.NewOrderResponseElement authorizationCapture(){
        //@throws PG_Exception
        return newOrder(PG_ChasePaymentech.O_NEW_ORDER_TRANSACTION_TYPE_AUTHORIZATION_CAPTURE);
    }
    
    public PG_WSOribitalElements.NewOrderResponseElement forceCapture(){//@throws PG_Exception
        return newOrder(PG_ChasePaymentech.O_NEW_ORDER_TRANSACTION_TYPE_FORCE_CAPTURE);
    }
    
    public PG_WSOribitalElements.NewOrderResponseElement refund(){//@throws PG_Exception
        return newOrder(PG_ChasePaymentech.O_NEW_ORDER_TRANSACTION_TYPE_REFUND);
    }
    
    public PG_WSOribitalElements.NewOrderResponseElement newOrder(String transactionType){
        //@throws PG_Exception
        PG_WSOribitalElements.NewOrderRequestElement newOrderRequest = null;
        PG_WSOribitalElements.NewOrderResponseElement newOrderResponse = null;      
        try{
            newOrderRequest = new PG_WSOribitalElements.NewOrderRequestElement();
            
            newOrderRequest.orbitalConnectionUsername = config.loginId;
            newOrderRequest.orbitalConnectionPassword = config.password;
            newOrderRequest.version = O_WSDL_VERSION;               
            newOrderRequest.transType = transactionType;
            newOrderRequest.bin = bin;
            newOrderRequest.merchantID = config.merchantReference;
            //newOrderRequest.merchantID = '123456789';//negative testing
            newOrderRequest.terminalID = config.terminalId;
            newOrderRequest.amount = Amount;            
            //newOrderRequest.amount = '1000000000';//negative testing          
            newOrderRequest.ccAccountNum = details.creditCardNumber;
            //newOrderRequest.ccAccountNum = 'badcard';//negative testing
            newOrderRequest.orderID = OrderID;
            if(transactionType == PG_ChasePaymentech.O_NEW_ORDER_TRANSACTION_TYPE_REFUND
                && TxRefNum != ''
            ){
                newOrderRequest.txRefNum = TxRefNum;/*The only time this field is used in a New 
                Order is to complete a Return (Refund, Credit) transaction on the card used in 
                the original transaction from which the TxRefNum was issued. If this field is 
                submitted with any other type of New Order transaction, it is ignored.*/
            }
            newOrderRequest.industryType = industryType;
            if(Comments != '') newOrderRequest.comments = Comments;
            //Address Verification
            if(config.addressVerification){
                newOrderRequest.avsZip = details.ccPostalCode;
                if(!config.addressVerificationZipOnly){
                    if(details.ccStreet1 != null && details.ccStreet2 != null){
                        newOrderRequest.avsAddress1 = details.ccStreet1;//apartment/ suite/ house
                        newOrderRequest.avsAddress2 = details.ccStreet2;//Street
                    } else if(details.ccStreet2 != null){
                        newOrderRequest.avsAddress1 = details.ccStreet2;
                    }
                    newOrderRequest.avsCity = details.ccCity;
                    newOrderRequest.avsState = details.ccState;
                    newOrderRequest.avsName = details.ccFirstName + ' ' + details.ccLastName;
                    newOrderRequest.avsCountryCode = getCountryCode(details.ccCountry);
                //newOrderRequest.avsPhone = '';//for Bill me later, AAAEEENNNNXXXX
                }
            }
            newOrderRequest.ccExp = details.expiryYear + details.expiryMonth;//YYYYMM
            //card security             
            if(config.cardCodeVerification//Card verifiction is on (Default)
                && (transactionType == PG_ChasePaymentech.O_NEW_ORDER_TRANSACTION_TYPE_AUTHORIZATION 
                    || transactionType == PG_ChasePaymentech.O_NEW_ORDER_TRANSACTION_TYPE_AUTHORIZATION_CAPTURE
                )
            ){
                if(details.creditCardType == PG.CARD_TYPE_VISA
                    || details.creditCardType == PG.CARD_TYPE_DISCOVER
                ){
                    if(CCCardVerifyPresenceInd != ''){//Must set it for Visa and Discover
                        //newOrderRequest.ccCardVerifyNum = details.ccSecurityCode;
                        newOrderRequest.ccCardVerifyNum = CVV;
                        newOrderRequest.ccCardVerifyPresenceInd = CCCardVerifyPresenceInd;//1 Value is Present
                        //2 Value on card but illegible, 9 Cardholder states data not available
                    }
                }else{
                    //newOrderRequest.ccCardVerifyNum = details.ccSecurityCode;
                    newOrderRequest.ccCardVerifyNum = CVV;
                }
            }
            //Partial Authorization
            if(config.partialAuthorization){
                newOrderRequest.partialAuthInd = 'Y';
            }else{
                newOrderRequest.partialAuthInd = 'N';
            }
            Map<String, String> reqMapMasked = newOrderRequest.asMap();
            reqMapMasked.put('orbitalConnectionUsername', PG.maskWord(reqMapMasked.get('orbitalConnectionUsername'), '', 4, false));
            reqMapMasked.put('orbitalConnectionPassword', PG.maskWord(reqMapMasked.get('orbitalConnectionPassword'), '', 4, false));
            reqMapMasked.put('merchantID', PG.maskWord(reqMapMasked.get('merchantID'), '', -2, false));
            reqMapMasked.put('ccAccountNum', PG.maskWord(reqMapMasked.get('ccAccountNum'), '', -4, true));
            reqMapMasked.put('ccCardVerifyNum', PG.maskWord(reqMapMasked.get('ccCardVerifyNum'), '', 0, false));
            pgResponse.transactionRequestMap = reqMapMasked;
            pgResponse.reqLog = PG.mapAsString(reqMapMasked);
            newOrderResponse = paymentechGateway.NewOrder(newOrderRequest);
            pgResponse.resLog = PG.mapAsString(newOrderResponse.asMap());
        }catch(CalloutException ex){
            PG_Exception px = new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
            if(ex.getMessage().contains('faultcode=SOAP-ENV:Server')){
                Fault f = getFaultCodeAndMessage(ex.getMessage());
                px.setErrorType('SOAPFault');
                px.setErrorCode(f.code);
                px.setErrorMesssage(f.message);
            }       
            throw px;
        }catch(SecurityException ex){
            throw new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
        }catch(Exception ex){
            throw new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
        }
        return newOrderResponse;
    }
    
    public PG_WSOribitalElements.MFCResponseElement markForCapture(){
        //@throws PG_Exception
        PG_WSOribitalElements.MFCElement markForCaptureRequest = null;
        PG_WSOribitalElements.MFCResponseElement markForCaptureResponse = null;     
        try{
            markForCaptureRequest = new PG_WSOribitalElements.MFCElement();
            
            markForCaptureRequest.orbitalConnectionUsername = config.loginId;
            markForCaptureRequest.orbitalConnectionPassword = config.password;
            markForCaptureRequest.version = O_WSDL_VERSION;
            markForCaptureRequest.bin = bin;
            markForCaptureRequest.merchantID = config.merchantReference;
            markForCaptureRequest.terminalID = config.terminalId;
            markForCaptureRequest.amount = Amount;
            markForCaptureRequest.orderID = OrderID;
            /*Must match the orderID of the original transaction being marked for capture.*/
            markForCaptureRequest.txRefNum = TxRefNum;                      
            /*Must match the txRefNum of the original transaction being marked for capture.*/
                        
            Map<String, String> reqMapMasked = markForCaptureRequest.asMap();
            reqMapMasked.put('orbitalConnectionUsername', PG.maskWord(reqMapMasked.get('orbitalConnectionUsername'), '', 4, false));
            reqMapMasked.put('orbitalConnectionPassword', PG.maskWord(reqMapMasked.get('orbitalConnectionPassword'), '', 4, false));
            reqMapMasked.put('merchantID', PG.maskWord(reqMapMasked.get('merchantID'), '', -2, false));
            pgResponse.transactionRequestMap = reqMapMasked;
            pgResponse.reqLog = PG.mapAsString(reqMapMasked);
            markForCaptureResponse = paymentechGateway.MFC(markForCaptureRequest);
            pgResponse.resLog = PG.mapAsString(markForCaptureResponse.asMap());
        }catch(CalloutException ex){
            PG_Exception px = new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
            if(ex.getMessage().contains('faultcode=SOAP-ENV:Server')){
                Fault f = getFaultCodeAndMessage(ex.getMessage());
                px.setErrorType('SOAPFault');
                px.setErrorCode(f.code);
                px.setErrorMesssage(f.message);
            }       
            throw px;
        }catch(SecurityException ex){
            throw new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
        }catch(Exception ex){
            throw new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
        }
        return markForCaptureResponse;
    }
    
    public PG_WSOribitalElements.ReversalResponseElement reversal(){//@throws PG_Exception
        return void('N');
    }
    
    public PG_WSOribitalElements.ReversalResponseElement authorizationReversal(){
        //@throws PG_Exception
        return void('Y');
    }
    
    public PG_WSOribitalElements.ReversalResponseElement void(String authReversalIndicator){
        //@throws PG_Exception
        PG_WSOribitalElements.ReversalElement reversalRequest = null;
        PG_WSOribitalElements.ReversalResponseElement reversalResponse = null;      
        try{
            reversalRequest = new PG_WSOribitalElements.ReversalElement();
            
            reversalRequest.orbitalConnectionUsername = config.loginId;
            reversalRequest.orbitalConnectionPassword = config.password;
            reversalRequest.bin = bin;
            reversalRequest.merchantID = config.merchantReference;
            reversalRequest.terminalID = config.terminalId;
            reversalRequest.txRefNum = TxRefNum;
            /*Must match the txRefNum of the original transaction being marked for capture.*/
            reversalRequest.txRefIdx = TxRefIdx;
            reversalRequest.orderID = OrderID;
            /*Must match the orderID of the original transaction being marked for capture.*/
            reversalRequest.onlineReversalInd = authReversalIndicator;
            
            Map<String, String> reqMapMasked = reversalRequest.asMap();
            reqMapMasked.put('orbitalConnectionUsername', PG.maskWord(reqMapMasked.get('orbitalConnectionUsername'), '', 4, false));
            reqMapMasked.put('orbitalConnectionPassword', PG.maskWord(reqMapMasked.get('orbitalConnectionPassword'), '', 4, false));
            reqMapMasked.put('merchantID', PG.maskWord(reqMapMasked.get('merchantID'), '', -2, false));
            pgResponse.transactionRequestMap = reqMapMasked;
            pgResponse.reqLog = PG.mapAsString(reqMapMasked);
            reversalResponse = paymentechGateway.Reversal(reversalRequest);
            pgResponse.resLog = PG.mapAsString(reversalResponse.asMap());
        }catch(CalloutException ex){
            PG_Exception px = new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
            if(ex.getMessage().contains('faultcode=SOAP-ENV:Server')){
                Fault f = getFaultCodeAndMessage(ex.getMessage());
                px.setErrorType('SOAPFault');
                px.setErrorCode(f.code);
                px.setErrorMesssage(f.message);
            }       
            throw px;
        }catch(SecurityException ex){
            throw new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
        }catch(Exception ex){
            throw new PG_Exception('Gateway Exception::'+ex.getMessage(), ex);
        }
        return reversalResponse;
    }
    
    private String getCountryCode(String ccCountryName){
        String ccCountryCode = '';
        if(ccCountryName == 'United States'
            || ccCountryName == 'USA'
            || ccCountryName == 'US'
        )ccCountryCode = 'US';
        if(ccCountryName == 'Canada' || ccCountryName == 'CA')ccCountryCode = 'CA';
        if(ccCountryName == 'Great Britain' || ccCountryName == 'CA')ccCountryCode = 'GB';
        if(ccCountryName == 'United Kingdom' || ccCountryName == 'UK')ccCountryCode = 'UK';
        return ccCountryCode;
    }
    
    private void populateResponse(PG_WSOribitalElements.NewOrderResponseElement res){
        pgResponse.transactionResponseMap = res.asMap();
        pgResponse.processStatus = res.procStatus == '0'?
                PG.Status.SUCCESS.name():PG.Status.FAILURE.name();
        pgResponse.approvalStatus = res.approvalStatus == '1'?
                PG.Status.APPROVED.name():res.approvalStatus == '0'?
                PG.Status.DECLINED.name():PG.Status.ERROR.name();
        pgResponse.responseCode = res.respCode;
        pgResponse.responseMessage = res.procStatusMessage;
        pgResponse.transactionId = res.txRefNum;
        pgResponse.transactionTime = getTimeStamp(res.respDateTime);
        pgResponse.isException = false;
        pgResponse.exceptionMessage = '';
    }
    
    private void populateResponse(PG_WSOribitalElements.ReversalResponseElement res){
        pgResponse.transactionResponseMap = res.asMap();
        pgResponse.processStatus = res.procStatus == '0'?
                PG.Status.SUCCESS.name():PG.Status.FAILURE.name();
        pgResponse.approvalStatus = res.approvalStatus == '1'?
                PG.Status.APPROVED.name():res.approvalStatus == '0'?
                PG.Status.DECLINED.name():PG.Status.ERROR.name();
        pgResponse.responseCode = res.respCode;
        pgResponse.responseMessage = res.procStatusMessage;
        pgResponse.transactionId = res.txRefNum;
        pgResponse.transactionTime = getTimeStamp(res.respDateTime);
        pgResponse.isException = false;
        pgResponse.exceptionMessage = '';
    }
    
    private void populateResponse(PG_WSOribitalElements.MFCResponseElement res){
        pgResponse.transactionResponseMap = res.asMap();
        pgResponse.processStatus = res.procStatus == '0'?
                PG.Status.SUCCESS.name():PG.Status.FAILURE.name();
        pgResponse.approvalStatus = res.approvalStatus == '1'?
                PG.Status.APPROVED.name():res.approvalStatus == '0'?
                PG.Status.DECLINED.name():PG.Status.ERROR.name();
        pgResponse.responseCode = res.respCode;
        pgResponse.responseMessage = res.procStatusMessage;
        pgResponse.transactionId = res.txRefNum;
        pgResponse.transactionTime = getTimeStamp(res.respDateTime);
        pgResponse.isException = false;
        pgResponse.exceptionMessage = '';
    }
    
    public Fault getFaultCodeAndMessage(String FaultString){//ad hoc, may require adjustment
        //e.g -
        //Web service callout failed: WebService returned a SOAP Fault: 20412: Precondition Failed: Security Information is missing faultcode=SOAP-ENV:Server faultactor="|0x52d8a810
        //Web service callout failed: WebService returned a SOAP Fault:   885 Error validating amount. Must be numeric, equal to zero or greater [-1] faultcode=SOAP-ENV:Server faultactor=
        Fault f = new Fault();
        String after = 'a SOAP Fault:';
        String before = ' faultcode=S';
        if(FaultString.contains(after) && FaultString.contains(before)){
            String tmp = FaultString.substring(FaultString.indexOf(after)+after.length(), 
                FaultString.indexOf(before)
            );
            f.code = tmp.substring(0, 6).trim();
            f.message = tmp.substring(7).trim();
        }
        return f;
    }
    
    private Datetime getTimeStamp(String d){
        
        string year1 = d.substring(0, 4);
        string month1 = d.substring(4, 6);
        string day1 = d.substring(6, 8);
        string hour1 = d.substring(8, 10);
        string minute1 = d.substring(10, 12);
        string second1 = d.substring(12);

        Integer year, month, day, hour, minute, second;
        year = Integer.valueOf(year1);
        month = Integer.valueOf(month1);
        day = Integer.valueOf(day1);
        hour = Integer.valueOf(hour1);
        minute = Integer.valueOf(minute1);
        second = Integer.valueOf(second1);

        Datetime t1 = Datetime.newInstance(year, month, day, hour, minute, second);
        
        return t1;
    }
    
    //@POC
    //PG_ChasePaymentech.tryIt();
    //public static void tryIt(){
        //authorization - NO
        /*
        String paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        String subsciptionId = 'a0Fi0000005YSMKEA4';
        Map<String, String> tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-3003',
            'Comment'=>'Authorization  - MC',  
            //'CardVerifyPresenceInd'=>'1',//for di and vi only, 1 = card code present
            'Amount'=>'10000'//$10  
        };
        PG_ChasePaymentech cp = new PG_ChasePaymentech();
        PG_CCDetails details = new PG_CCDetails(subsciptionId);
        PG_Config config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res = cp.authorization();
        System.debug('---res='+res);
        */
        //capture - MFC
        /*
        String paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        String subsciptionId = 'a0Fi0000005YSMKEA4';
        Map<String, String> tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-0006',
            'Amount'=>'10000',//$10 
            'TransactionRef'=>'527C8EDCFB165E734526A17F0B7686534D2254C2'  
        };
        PG_ChasePaymentech cp = new PG_ChasePaymentech();
        PG_CCDetails details = new PG_CCDetails(subsciptionId);
        PG_Config config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.MFCResponseElement res = cp.markForCapture();
        System.debug('---res='+res);
        */
        //authorization capture - NO
        /*
        String paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        String subsciptionId = 'a0Fi0000005YSMKEA4';
        Map<String, String> tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-3003',
            'Comment'=>'Auth/Capture Testing -  MC',
            //'CardVerifyPresenceInd'=>'1',//for di and vi only
            'Amount'=>'10000'//$15      
        };
        PG_ChasePaymentech cp = new PG_ChasePaymentech();
        PG_CCDetails details = new PG_CCDetails(subsciptionId);
        PG_Config config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res = cp.authorizationCapture();
        System.debug('---res='+res);
        */
        //refund - NO   
        /*
        String paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        String subsciptionId = 'a0Fi0000005YSMKEA4';
        Map<String, String> tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-0009R',
            //'TransactionRef'=>'527C9764C3E27FF5A7A8C3E4F2128E7492E654AA',
            'Comment'=>'Refund Testing using a Credit Card Number - Vi',
            'Amount'=>'1000'//$4        
        };
        PG_ChasePaymentech cp = new PG_ChasePaymentech();
        PG_CCDetails details = new PG_CCDetails(subsciptionId);
        PG_Config config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res = cp.refund();
        System.debug('---res='+res);
        */
        //Force capture - NO
        /*
        String paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        String subsciptionId = 'a0Fi0000005YSMKEA4';
        Map<String, String> tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'O-104',
            'PriorAuthCd'=>'tst535',
            'Comment'=>'Test Web Service Force Capture Transaction',
            'Amount'=>'500'//$5     
        };
        PG_ChasePaymentech cp = new PG_ChasePaymentech();
        PG_CCDetails details = new PG_CCDetails(subsciptionId);
        PG_Config config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res = cp.forceCapture();
        System.debug('---res='+res);
        */
        //Revrsal - void/ auth reversal
        /*
        String paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        String subsciptionId = 'a0Fi0000005YSMKEA4';
        Map<String, String> tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-3001',
            'TransactionRef'=>'528DD8D97BD111C51F5F481074EBB885C610544E'
            //'TxRefIdx'=>''//Should be numeric, not req for first time void/auth reversal
        };
        PG_ChasePaymentech cp = new PG_ChasePaymentech();
        PG_CCDetails details = new PG_CCDetails(subsciptionId);
        PG_Config config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.ReversalResponseElement res = cp.authorizationReversal();
        //PG_WSOribitalElements.ReversalResponseElement res = cp.reversal();
        System.debug('---res='+res);
        */
    //}

}