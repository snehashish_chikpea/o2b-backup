/*
** Class         :  BatchParentBillScheduler
** Created by    :  Asitm9(chikpea Inc.)
** Description   :   
*/

global with sharing class BatchParentBillScheduler implements Schedulable{
    global void execute(SchedulableContext sc){
        list<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, 
        ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c 
        from ChikPeaO2B__O2B_Setting__c limit 1];
        BatchParentBilling bpp= new BatchParentBilling();
        bpp.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?
        o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
        //bpp.query='SELECT Id FROM Account WHERE ChikPeaO2B__Parent_Billing__c=True AND ParentId=Null';
        bpp.query='SELECT Id FROM Account WHERE ParentId=Null';
        try{
        	ID batchprocessid = Database.executeBatch(bpp,200);
        }catch(Exception e){}
    }
    /*
    global static void execute(){
        list<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, 
        ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c 
        from ChikPeaO2B__O2B_Setting__c limit 1];
        BatchParentBill bpp= new BatchParentBill();
        bpp.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?
        o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
        bpp.query='SELECT Id FROM Account WHERE ParentId=Null';
        ID batchprocessid = Database.executeBatch(bpp,200);
    }
    */
    
}