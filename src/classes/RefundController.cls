/*
** Class         :  RefundController
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  7 Aug 14
** Modified by   :  Spal (chikpea Inc.)
** Reason        :  CURD issue fix
** Description   :   
*/

public with sharing class RefundController{
    public Apexpages.Standardcontroller controller;
    public Decimal RefundAmount{get;set;}
    public ChikPeaO2B__Payment__c pay{get;set;}
    public string selectedoption{get;set;}
    public ChikPeaO2B__Refund__c refund{get;set;}
    public boolean makerefund{get;set;}
    public decimal TotalRefund{get;set;}
     
    private set<id>CCid=new set<id>();
    map<id,ChikPeaO2B__Credit_Card__c>CCmap=new Map<id,ChikPeaO2B__Credit_Card__c>();
    
    public RefundController(ApexPages.StandardController controller)
    {
        makerefund=true;
        TotalRefund=0;
        this.controller=controller;
        refund=new ChikPeaO2B__Refund__c();
        pay=[SELECT id,name,ChikPeaO2B__Account__c,ChikPeaO2B__Account__r.name,ChikPeaO2B__Advance_payment__c,ChikPeaO2B__Gateway_Reference__c,ChikPeaO2B__Advance_payment__r.name,ChikPeaO2B__Credit_Note__c,ChikPeaO2B__Credit_Note__r.name,ChikPeaO2B__Payment_Amount__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Date__c,ChikPeaO2B__Has_Processed__c,ChikPeaO2B__Status__c,ChikPeaO2B__Invoice__c,ChikPeaO2B__Invoice__r.name,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Credit_Card__c,ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c,(SELECT id,name,ChikPeaO2B__Refund_Amount__c from Refunds__r where ChikPeaO2B__Has_Processed__c=true) from ChikPeaO2B__Payment__c where id=:controller.getid()];
        if(pay.ChikPeaO2B__Status__c!='Processed' || !pay.ChikPeaO2B__Has_Processed__c)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'This payment can not be refunded'));
            makerefund=false;
        }
        else
        {
            for(ChikPeaO2B__Refund__c rf : pay.Refunds__r)
            {
                TotalRefund+=(rf.ChikPeaO2B__Refund_Amount__c!=null?rf.ChikPeaO2B__Refund_Amount__c:0);
            }
        }
        if(pay.ChikPeaO2B__Credit_Card__c!=null)
            CCid.add(pay.ChikPeaO2B__Credit_Card__c);
        if(pay.ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c!=null)
            CCid.add(pay.ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c);    
        for(ChikPeaO2B__Credit_Card__c cc : [SELECT id,name,ChikPeaO2B__Account__c,ChikPeaO2B__CC_Street1__c,ChikPeaO2B__CC_City__c,ChikPeaO2B__CC_Country__c,ChikPeaO2B__CC_First_Name__c,ChikPeaO2B__CC_Last_Name__c,ChikPeaO2B__CC_Postal_Code__c,ChikPeaO2B__CC_State__c,ChikPeaO2B__CC_Street2__c,ChikPeaO2B__Credit_Card_Number__c,ChikPeaO2B__Credit_Card_Type__c,ChikPeaO2B__Expiry_Month__c,ChikPeaO2B__Expiry_Year__c, ChikPeaO2B__Customer_Profile_ID__c, ChikPeaO2B__Customer_Payment_Profile_Id__c from ChikPeaO2B__Credit_Card__c where id in : CCid])
        {
            CCmap.put(cc.id,cc);
        }
    }
    public List<SelectOption> getRefundMethods()
    {
        List<SelectOption>RefundMethods=new List<SelectOption>();
        RefundMethods.add(new SelectOption('--SELECT--','--SELECT--'));    
        if(pay.ChikPeaO2B__Payment_Method__c=='Credit Card')
        {
            RefundMethods.add(new SelectOption('Credit Card','Credit Card'));
            RefundMethods.add(new SelectOption('Check','Check'));
            RefundMethods.add(new SelectOption('Credit Note','Credit Note'));
        }
        else if(pay.ChikPeaO2B__Payment_Method__c=='Check')
        {
            RefundMethods.add(new SelectOption('Check','Check'));
            RefundMethods.add(new SelectOption('Credit Note','Credit Note'));
        }
        else if(pay.ChikPeaO2B__Payment_Method__c=='Paypal')
        {
            RefundMethods.add(new SelectOption('Paypal','Paypal'));
        }
        else if(pay.ChikPeaO2B__Payment_Method__c=='Other' && pay.ChikPeaO2B__Credit_Note__c!=null)
        {
            RefundMethods.add(new SelectOption('Credit Note','Credit Note'));
        }
        else if(pay.ChikPeaO2B__Payment_Method__c=='Other' && pay.ChikPeaO2B__Advance_payment__c!=null)
        {
            RefundMethods.add(new SelectOption('Advance Payment','Advance Payment'));
        }
        
        return RefundMethods;
    }
    public pagereference cancel()
    {
        return new pagereference('/'+pay.id);
    }
    public pagereference ProcessRefund()
    {
        List<ChikPeaO2B__O2B_Setting__c>o2bset=[SELECT id,name,ChikPeaO2B__Credit_Note_Default_Validity__c,ChikPeaO2B__Refund_Any_Amount__c  from ChikPeaO2B__O2B_Setting__c limit 1];
        Boolean hasErr = false;
        if(RefundAmount <= 0 )    // checks the refund amount in valid range
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please provide a valid amount.'));
            hasErr = true;
        }
        if((o2bset==null || o2bset.size()==0 || !o2bset[0].ChikPeaO2B__Refund_Any_Amount__c) && (pay.ChikPeaO2B__Payment_Amount__c<(TotalRefund+RefundAmount)))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Refund amount can not be more than payment amount.'));
            hasErr = true;
        }
        try
        {
            if(selectedoption.trim().equals('')|| selectedoption.trim().equalsignorecase('--SELECT--'))    // checking whether refund method is chosen.
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select a refund method.'));
                hasErr = true;
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select a refund method.'));
            hasErr = true;
        }
        if(hasErr)
           return null;   
                
        //Savepoint sp = Database.setSavepoint();// save point
        Savepoint sp = null;
        
        if(selectedoption=='Advance Payment')
        {
          sp = Database.setSavepoint();
            refund.ChikPeaO2B__Account__c=pay.ChikPeaO2B__Account__c;
            refund.ChikPeaO2B__Invoice_Number__c=pay.ChikPeaO2B__Invoice__c;
            refund.ChikPeaO2B__Refund_Amount__c=RefundAmount;
            refund.ChikPeaO2B__Refund_Method__c=selectedoption;
            refund.ChikPeaO2B__Payment__c=pay.id;
            refund.ChikPeaO2B__Status__c='Processed';

            //#### create New Advance payment ####//
            ChikPeaO2B__Advance_payment__c adv=new ChikPeaO2B__Advance_payment__c();
            adv.ChikPeaO2B__Account__c=pay.ChikPeaO2B__Account__c;
            adv.ChikPeaO2B__Advanced_Payment__c=RefundAmount;
            adv.ChikPeaO2B__Comment__c='refund';
            adv.ChikPeaO2B__Payment_Date__c=date.today();
            adv.ChikPeaO2B__Payment_Method__c='Other';
            adv.ChikPeaO2B__Status__c='Processed';
            //=== CURD check [START] ===//
            Map<string,string>advpayInsertResultMap=new map<string,string>();
            advpayInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Advance_payment__c',FieldPermissionSet.AdvancepaymentInsert,'insert');
            if(advpayInsertResultMap.get('AllowDML')!=null && advpayInsertResultMap.get('AllowDML').equalsignorecase('true'))
                insert(adv);
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,advpayInsertResultMap.get('Message')));        
                return null;
            }
            //=== CURD check [END] ===//
            refund.ChikPeaO2B__Advance_payment__c=adv.id;
            //=== CURD check [START] ===//
            Map<string,string>refundInsertResultMap=new map<string,string>();
            refundInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Refund__c',FieldPermissionSet.RefundInsert,'insert');
            if(refundInsertResultMap.get('AllowDML')!=null && refundInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                insert(refund);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Refund successful.'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,refundInsertResultMap.get('Message')));        
                Database.rollback(sp);
                return null;
            }
            //=== CURD check [END] ===//
        }
        else if(selectedoption=='Credit Note')
        {
          sp = Database.setSavepoint();
            refund.ChikPeaO2B__Account__c=pay.ChikPeaO2B__Account__c;
            refund.ChikPeaO2B__Invoice_Number__c=pay.ChikPeaO2B__Invoice__c;
            refund.ChikPeaO2B__Refund_Amount__c=RefundAmount;
            refund.ChikPeaO2B__Refund_Method__c=selectedoption;
            refund.ChikPeaO2B__Payment__c=pay.id;
            refund.ChikPeaO2B__Status__c='Processed';
            
            //#### create credit note ####//
            ChikPeaO2B__Credit_Note__c newcn=new ChikPeaO2B__Credit_Note__c();
            newcn.ChikPeaO2B__Account__c=pay.ChikPeaO2B__Account__c;
            newcn.ChikPeaO2B__Amount__c=RefundAmount;
            //newcn.ChikPeaO2B__Carry_Forward_Credit_Note__c=;
            newcn.ChikPeaO2B__Credit_Date__c=date.today();
            newcn.ChikPeaO2B__Validity__c =o2bset[0].ChikPeaO2B__Credit_Note_Default_Validity__c;
            newcn.ChikPeaO2B__Type__c='Refund';
            newcn.ChikPeaO2B__Used_Amount__c=0;
            //=== CURD check [START] ===//
            Map<string,string>creditnoteInsertResultMap=new map<string,string>();
            creditnoteInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Credit_Note__c',FieldPermissionSet.CreditNoteInsert,'insert');
            if(creditnoteInsertResultMap.get('AllowDML')!=null && creditnoteInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                insert(newcn);
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,creditnoteInsertResultMap.get('Message')));        
                return null;
            }
            //=== CURD check [END] ===//
            refund.ChikPeaO2B__Credit_Note__c=newcn.id;
            //=== CURD check [START] ===//
            Map<string,string>refund1InsertResultMap=new map<string,string>();
            refund1InsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Refund__c',FieldPermissionSet.RefundInsert,'insert');
            if(refund1InsertResultMap.get('AllowDML')!=null && refund1InsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                insert(refund);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Refund successful.'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,refund1InsertResultMap.get('Message')));        
                Database.rollback(sp);
                return null;
            }
            //=== CURD check [END] ===//
        }
        else if(selectedoption=='Check')
        {
            if(refund.ChikPeaO2B__Check_Date__c !=null && refund.ChikPeaO2B__Check_Number__c!=null && refund.ChikPeaO2B__Check_Number__c!='')
            {
                refund.ChikPeaO2B__Account__c=pay.ChikPeaO2B__Account__c;
                refund.ChikPeaO2B__Invoice_Number__c=pay.ChikPeaO2B__Invoice__c;
                refund.ChikPeaO2B__Refund_Amount__c=RefundAmount;
                refund.ChikPeaO2B__Refund_Method__c=selectedoption;
                refund.ChikPeaO2B__Payment__c=pay.id;
                refund.ChikPeaO2B__Status__c='Processed';
                //=== CURD check [START] ===//
                Map<string,string>refund2InsertResultMap=new map<string,string>();
                refund2InsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Refund__c',FieldPermissionSet.RefundInsert,'insert');
                if(refund2InsertResultMap.get('AllowDML')!=null && refund2InsertResultMap.get('AllowDML').equalsignorecase('true'))
                {
                    insert(refund);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Refund successful.'));
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,refund2InsertResultMap.get('Message')));        
                    return null;
                }
                //=== CURD check [END] ===//
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Refund Fail. Provide Valid Check Number and Check Date'));
                return null;
            }
        }
        else if(selectedoption == 'Credit Card')
        {
            try{
                // ############### Gateway Call ###############//
                
                PG_Response pgResponse;
                ChikPeaO2B__Credit_Card__c cc2Bused=(pay.ChikPeaO2B__Credit_Card__c!=null?CCmap.get(pay.ChikPeaO2B__Credit_Card__c):CCmap.get(pay.ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c));
                System.debug('%%%%%'+cc2Bused);
                  if(cc2Bused!=null){
                      map<string,string> ccDetailsMap= new map<string,string>();
                      ccDetailsMap.put('PaymentGatewayName',pay.ChikPeaO2B__Payment_Gateway__c);
                      ccDetailsMap.put('ccCity',cc2Bused.ChikPeaO2B__CC_City__c);
                      ccDetailsMap.put('ccCountry',cc2Bused.ChikPeaO2B__CC_Country__c);
                      ccDetailsMap.put('ccFirstName',cc2Bused.ChikPeaO2B__CC_First_Name__c);
                      ccDetailsMap.put('ccLastName',cc2Bused.ChikPeaO2B__CC_Last_Name__c);
                      ccDetailsMap.put('ccPostalCode',cc2Bused.ChikPeaO2B__CC_Postal_Code__c);
                      ccDetailsMap.put('ccState',cc2Bused.ChikPeaO2B__CC_State__c);
                      ccDetailsMap.put('ccStreet1',cc2Bused.ChikPeaO2B__CC_Street1__c);
                      ccDetailsMap.put('ccStreet2',cc2Bused.ChikPeaO2B__CC_Street2__c);
                      ccDetailsMap.put('ccNumber',cc2Bused.ChikPeaO2B__Credit_Card_Number__c);
                      ccDetailsMap.put('ccType',cc2Bused.ChikPeaO2B__Credit_Card_Type__c);
                      ccDetailsMap.put('ccExpMonth',String.valueof(cc2Bused.ChikPeaO2B__Expiry_Month__c));
                      ccDetailsMap.put('ccExpYear',String.valueof(cc2Bused.ChikPeaO2B__Expiry_Year__c));
            ccDetailsMap.put('customerProfileId',String.valueof(cc2Bused.ChikPeaO2B__Customer_Profile_ID__c));
                      ccDetailsMap.put('customerPaymentProfileId',String.valueof(cc2Bused.ChikPeaO2B__Customer_Payment_Profile_Id__c));
                      ccDetailsMap.put('TransactionType','REFUND');
                      ccDetailsMap.put('Amount',string.valueof(RefundAmount));
                      ccDetailsMap.put('TransactionRef',pay.ChikPeaO2B__Transaction_Id__c);
                      ccDetailsMap.put('InvoiceRef',pay.ChikPeaO2B__Invoice__r.name);
                      ccDetailsMap.put('CustomerRef',pay.ChikPeaO2B__Account__r.name);
                      //ccDetailsMap.put('OrderID',p.ChikPeaO2B__Merchant_Order_Id__c);
                      //ccDetailsMap.put('Comment',payment.Comment__c);
                      pgResponse=PG_PaymentProcessorInvoker.process(ccDetailsMap);
                  }
                  else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Refund Fail. No Credit Cards are associated with this Account'));
                    return null;
                  }
                /*
                if(pay.Payment_Gateway__c=='AuthNet')
                {
                    pgResponse=CCGatewayConnector.invoke('AuthNet',
                                                         (pay.ChikPeaO2B__Credit_Card__c!=null?CCmap.get(pay.ChikPeaO2B__Credit_Card__c):CCmap.get(pay.ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c)), 
                                                          PG.TransactionType.REFUND,
                                                          new Map<string,string>{
                                                            'TransactionRef'=>pay.ChikPeaO2B__Transaction_Id__c,
                                                            'InvoiceRef'=>pay.ChikPeaO2B__Invoice__r.name,
                                                            'CustomerRef'=>pay.ChikPeaO2B__Account__r.name,
                                                            'Amount'=>string.valueof(RefundAmount)        
                                                          }
                                                        );
                }
                else if(pay.Payment_Gateway__c=='ChasePT')
                {
                    pgResponse=CCGatewayConnector.invoke('ChasePT:Orbital',
                                                      (pay.ChikPeaO2B__Credit_Card__c!=null?CCmap.get(pay.ChikPeaO2B__Credit_Card__c):CCmap.get(pay.ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c)), 
                                                       PG.TransactionType.REFUND,//modified
                                                       new Map<string,string>{
                                                      'terminalID'=>PG_ChasePaymentech.O_TERMINAL_ID,
                                                      'orderID'=> 'R'+string.valueOf(refund.Id).substring(9,15)+'_'+refund.Name,  //refund.ChikPeaO2B__Merchant_Order_Number__c,                
                                                      'txRefNum'=> pay.ChikPeaO2B__Transaction_Id__c,//modified, use trxId for payment that will be refunded
                                                      'comments'=>'ChasePT Web Service Authentication',
                                                      'Amount'=>String.valueof(RefundAmount) //modify - use refund.refund_amount  
                                                    });
                }*/
                //else if()//########## other payment gateways
                if(pgResponse!=null && !pgResponse.isException)
                {
                    if(pgResponse!=null && pgResponse.processStatus.equalsignorecase('SUCCESS') && pgResponse.approvalStatus.equalsignorecase('APPROVED'))
                    {
                        refund.ChikPeaO2B__Account__c=pay.ChikPeaO2B__Account__c;
                        refund.ChikPeaO2B__Invoice_Number__c=pay.ChikPeaO2B__Invoice__c;
                        refund.ChikPeaO2B__Refund_Amount__c=RefundAmount;
                        refund.ChikPeaO2B__Refund_Method__c=selectedoption;
                        refund.ChikPeaO2B__Payment__c=pay.id;
                        refund.ChikPeaO2B__Status__c='Processed';
                        refund.ChikPeaO2B__Transaction_Id__c=pgResponse.transactionId;
                        refund.ChikPeaO2B__Trx_Message__c=pgResponse.responseMessage!=null?pgResponse.responseMessage:pgResponse.responseCode;    
                        refund.ChikPeaO2B__Credit_Card__c=(pay.ChikPeaO2B__Credit_Card__c!=null?pay.ChikPeaO2B__Credit_Card__c:pay.ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c);
                        insert(refund);
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Refund successful.'));
                    }
                    else
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Refund Fail. '+pgResponse.responseMessage));
                    }
                }
                else
                {      
                    system.debug('@@@@'+pgResponse);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Refund Fail. '+pgResponse.responseMessage));
                }
            }
            catch(Exception e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getmessage()));
            }
        }
        else if(selectedoption == 'Paypal') //added by Snehashish Roy on 21 July, 2017
        {
            list<ChikPeaO2B__Gateway__c> gw=new list<ChikPeaO2B__Gateway__c>();
            //gw=[select id from ChikPeaO2B__Gateway__c where Name='PayPal Adaptive Payment' limit 1]; // comment by Jani
            string ref_status=PayPal_AdaptivePayments.partial_refund(pay.id,pay.ChikPeaO2B__Gateway_Reference__c,RefundAmount);
            if(ref_status=='Success')
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Refund successful.'));
            else
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Refund Failed.'));
        }
        pay=[SELECT id,name,ChikPeaO2B__Account__c,ChikPeaO2B__Gateway_Reference__c,ChikPeaO2B__Account__r.name,ChikPeaO2B__Advance_payment__c,ChikPeaO2B__Advance_payment__r.name,ChikPeaO2B__Credit_Note__c,ChikPeaO2B__Credit_Note__r.name,ChikPeaO2B__Payment_Amount__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Date__c,ChikPeaO2B__Has_Processed__c,ChikPeaO2B__Status__c,ChikPeaO2B__Invoice__c,ChikPeaO2B__Invoice__r.name,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Credit_Card__c,ChikPeaO2B__Account__r.ChikPeaO2B__Credit_Card__c,(SELECT id,name,ChikPeaO2B__Refund_Amount__c from Refunds__r where ChikPeaO2B__Has_Processed__c=true) from ChikPeaO2B__Payment__c where id=:controller.getid()];
        TotalRefund=0;
        for(ChikPeaO2B__Refund__c rf : pay.Refunds__r)
        {
            TotalRefund+=(rf.ChikPeaO2B__Refund_Amount__c!=null?rf.ChikPeaO2B__Refund_Amount__c:0);
        }
        refund=new ChikPeaO2B__Refund__c();
        return null;     
    }
}