@isTest
public class TestBatchAchStatus
{

@isTest
    static void testmethod1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Billing_Type__c='Anniversary OneBill',Create_Invoice_on_Process_Order__c=true);
        insert(o2bset);
       
        List<id>accidList=new List<id>();
        Account acc=new Account(name='acc1',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today());
        insert(acc);
        accidList.add(acc.id);

       
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        list<String>itmTypeList=new List<String>();
        //itmTypeList.add('Non-Recurring');
        itmTypeList.add('Recurring and Non-Recurring');

        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        
         List<id>OLIdlist=new List<id>();
         OLIdlist.add(orl1.id);

        //get the invoice id 
         
        boolean isbatch=false;
        id invoice_id=Invoicing.CreateInvoice(accidList,isbatch);
        System.debug('invoice_id------->>>>'+invoice_id);
        //------gateway
        ChikPeaO2B__Gateway__c gwAuthNetReport = new ChikPeaO2B__Gateway__c();
        gwAuthNetReport.Name = 'AuthNet:Reporting';
        gwAuthNetReport.ChikPeaO2B__Gateway_Type__c = 'AuthNet';
        gwAuthNetReport.ChikPeaO2B__Login_Id__c = '5GtpZ88g2';
        gwAuthNetReport.ChikPeaO2B__Password__c = '8dM6bR63KMwxs77P';
        gwAuthNetReport.ChikPeaO2B__Test_Mode__c = true;
        gwAuthNetReport.ChikPeaO2B__Time_out_limit__c = 120;
        gwAuthNetReport.ChikPeaO2B__Primary_Site_Available__c = true;
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
  
        insert gwAuthNetReport;
        //-------------------
        
        //create payment
        ChikPeaO2B__payment__c pay=new ChikPeaO2B__payment__c();
        pay.ChikPeaO2B__Account__c=acc.id;
        pay.ChikPeaO2B__Payment_Amount__c=10.000;
        pay.ChikPeaO2B__Payment_Method__c='ACH';
        //pay.ChikPeaO2B__Payment_Gateway__c='AuthNet:Reporting';
        pay.ChikPeaO2B__Payment_Gateway__c='AuthNet';
        pay.ChikPeaO2B__Invoice__c=invoice_id;
        pay.ChikPeaO2B__Status__c='In Process';
        pay.ChikPeaO2B__Transaction_Id__c='2212958926';
        pay.ChikPeaO2B__Has_Processed__c=true;
        pay.ChikPeaO2B__Trx_Message__c='This transaction has been approved.';
        
        insert(pay); 
        
        Test.startTest();

        /*BatchAchStatus BAS = new BatchAchStatus();
        BAS.email='salesforce@chikpea.com';
        BAS.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet:Reporting\' AND ChikPeaO2B__Payment_Method__c=\'ACH\' AND ChikPeaO2B__Status__c=\'In Process\' AND ChikPeaO2B__Has_Processed__c=true';
        Database.executeBatch(BAS);*/
        BatchAchStatusScheduler BASS = new BatchAchStatusScheduler();
        String sch='0 00 06 * * ?';
        system.schedule('Check Status of ACH',sch, BASS);
        Test.stopTest();

    }
    @isTest
    static void testmethod2()
    {
    
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Billing_Type__c='Anniversary OneBill',Create_Invoice_on_Process_Order__c=true);
        insert(o2bset);
       
        List<id>accidList=new List<id>();
        Account acc=new Account(name='acc1',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today());
        insert(acc);
        accidList.add(acc.id);

       
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        list<String>itmTypeList=new List<String>();
        //itmTypeList.add('Non-Recurring');
        itmTypeList.add('Recurring and Non-Recurring');

        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        
         List<id>OLIdlist=new List<id>();
         OLIdlist.add(orl1.id);

        //get the invoice id 
         
        boolean isbatch=false;
        id invoice_id=Invoicing.CreateInvoice(accidList,isbatch);
        System.debug('invoice_id------->>>>'+invoice_id);
        //------gateway
        ChikPeaO2B__Gateway__c gwAuthNetReport = new ChikPeaO2B__Gateway__c();
        gwAuthNetReport.Name = 'AuthNet:Reporting';
        gwAuthNetReport.ChikPeaO2B__Gateway_Type__c = 'AuthNet';
        gwAuthNetReport.ChikPeaO2B__Login_Id__c = '5GtpZ88g2';
        gwAuthNetReport.ChikPeaO2B__Password__c = '8dM6bR63KMwxs77P';
        gwAuthNetReport.ChikPeaO2B__Test_Mode__c = true;
        gwAuthNetReport.ChikPeaO2B__Time_out_limit__c = 120;
        gwAuthNetReport.ChikPeaO2B__Primary_Site_Available__c = true;
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
  
        insert gwAuthNetReport;
        //-------------------
        
        //create payment
        ChikPeaO2B__payment__c pay=new ChikPeaO2B__payment__c();
        pay.ChikPeaO2B__Account__c=acc.id;
        pay.ChikPeaO2B__Payment_Amount__c=10.000;
        pay.ChikPeaO2B__Payment_Method__c='ACH';
        pay.ChikPeaO2B__Payment_Gateway__c='AuthNet:Reporting';
        pay.ChikPeaO2B__Invoice__c=invoice_id;
        pay.ChikPeaO2B__Status__c='In Process';
        pay.ChikPeaO2B__Transaction_Id__c='2212958926';
        pay.ChikPeaO2B__Has_Processed__c=true;
        pay.ChikPeaO2B__Trx_Message__c='This transaction has been approved.';
        
        insert(pay); 
        
        Test.startTest();

        /*BatchAchStatus BAS = new BatchAchStatus();
        BAS.email='salesforce@chikpea.com';
        BAS.test_status_pending=true;
        BAS.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet:Reporting\' AND ChikPeaO2B__Payment_Method__c=\'ACH\' AND ChikPeaO2B__Status__c=\'In Process\' AND ChikPeaO2B__Has_Processed__c=true';
        Database.executeBatch(BAS);*/
        BatchAchStatusScheduler BASS = new BatchAchStatusScheduler();
        BASS.test_status_pending=true;
        String sch='0 00 06 * * ?';
        system.schedule('Check Status of ACH',sch, BASS);
        Test.stopTest();
    
    }
    @isTest
    static void testmethod3()
    {
    
    
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Billing_Type__c='Anniversary OneBill',Create_Invoice_on_Process_Order__c=true);
        insert(o2bset);
       
        List<id>accidList=new List<id>();
        Account acc=new Account(name='acc1',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today());
        insert(acc);
        accidList.add(acc.id);

       
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        list<String>itmTypeList=new List<String>();
        //itmTypeList.add('Non-Recurring');
        itmTypeList.add('Recurring and Non-Recurring');

        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        
         List<id>OLIdlist=new List<id>();
         OLIdlist.add(orl1.id);

        //get the invoice id 
         
        boolean isbatch=false;
        id invoice_id=Invoicing.CreateInvoice(accidList,isbatch);
        System.debug('invoice_id------->>>>'+invoice_id);
        //------gateway
        ChikPeaO2B__Gateway__c gwAuthNetReport = new ChikPeaO2B__Gateway__c();
        gwAuthNetReport.Name = 'AuthNet:Reporting';
        gwAuthNetReport.ChikPeaO2B__Gateway_Type__c = 'AuthNet';
        gwAuthNetReport.ChikPeaO2B__Login_Id__c = '5GtpZ88g2';
        gwAuthNetReport.ChikPeaO2B__Password__c = '8dM6bR63KMwxs77P';
        gwAuthNetReport.ChikPeaO2B__Test_Mode__c = true;
        gwAuthNetReport.ChikPeaO2B__Time_out_limit__c = 120;
        gwAuthNetReport.ChikPeaO2B__Primary_Site_Available__c = true;
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
  
        insert gwAuthNetReport;
        //-------------------
        
        //create payment
        ChikPeaO2B__payment__c pay=new ChikPeaO2B__payment__c();
        pay.ChikPeaO2B__Account__c=acc.id;
        pay.ChikPeaO2B__Payment_Amount__c=10.000;
        pay.ChikPeaO2B__Payment_Method__c='ACH';
        pay.ChikPeaO2B__Payment_Gateway__c='AuthNet:Reporting';
        pay.ChikPeaO2B__Invoice__c=invoice_id;
        pay.ChikPeaO2B__Status__c='In Process';
        pay.ChikPeaO2B__Transaction_Id__c='2212958926';
        pay.ChikPeaO2B__Has_Processed__c=true;
        pay.ChikPeaO2B__Trx_Message__c='This transaction has been approved.';
        
        insert(pay); 
        
        Test.startTest();

        /* BatchAchStatus BAS = new BatchAchStatus();
        BAS.email='salesforce@chikpea.com';
        BAS.test_status_failed=true;
        BAS.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet:Reporting\' AND ChikPeaO2B__Payment_Method__c=\'ACH\' AND ChikPeaO2B__Status__c=\'In Process\' AND ChikPeaO2B__Has_Processed__c=true';
        Database.executeBatch(BAS);*/
        BatchAchStatusScheduler BASS = new BatchAchStatusScheduler();
        BASS.test_status_failed=true;
        String sch='0 00 06 * * ?';
        system.schedule('Check Status of ACH',sch, BASS);
        
        
        Test.stopTest();
    }
    @isTest
    static void testmethod4()
    {
    
    
    
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Billing_Type__c='Anniversary OneBill',Create_Invoice_on_Process_Order__c=true);
        insert(o2bset);
       
        List<id>accidList=new List<id>();
        Account acc=new Account(name='acc1',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today());
        insert(acc);
        accidList.add(acc.id);

       
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        list<String>itmTypeList=new List<String>();
        //itmTypeList.add('Non-Recurring');
        itmTypeList.add('Recurring and Non-Recurring');

        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        
         List<id>OLIdlist=new List<id>();
         OLIdlist.add(orl1.id);

        //get the invoice id 
         
        boolean isbatch=false;
        id invoice_id=Invoicing.CreateInvoice(accidList,isbatch);
        System.debug('invoice_id------->>>>'+invoice_id);
        //------gateway
        ChikPeaO2B__Gateway__c gwAuthNetReport = new ChikPeaO2B__Gateway__c();
        gwAuthNetReport.Name = 'AuthNet:Reporting';
        gwAuthNetReport.ChikPeaO2B__Gateway_Type__c = 'AuthNet';
        gwAuthNetReport.ChikPeaO2B__Login_Id__c = '5GtpZ88g2';
        gwAuthNetReport.ChikPeaO2B__Password__c = '8dM6bR63KMwxs77P';
        gwAuthNetReport.ChikPeaO2B__Test_Mode__c = true;
        gwAuthNetReport.ChikPeaO2B__Time_out_limit__c = 120;
        gwAuthNetReport.ChikPeaO2B__Primary_Site_Available__c = true;
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Test__c = 'https://api.authorize.net/soap/v1/Service.asmx';
        gwAuthNetReport.ChikPeaO2B__Reporting_Gateway_Url_Prod__c = 'https://apitest.authorize.net/soap/v1/Service.asmx';
  
        insert gwAuthNetReport;
        //-------------------
        
        //create payment
        ChikPeaO2B__payment__c pay=new ChikPeaO2B__payment__c();
        pay.ChikPeaO2B__Account__c=acc.id;
        pay.ChikPeaO2B__Payment_Amount__c=10.000;
        pay.ChikPeaO2B__Payment_Method__c='ACH';
        pay.ChikPeaO2B__Payment_Gateway__c='AuthNet:Reporting';
        pay.ChikPeaO2B__Invoice__c=invoice_id;
        pay.ChikPeaO2B__Status__c='In Process';
        pay.ChikPeaO2B__Transaction_Id__c='2212958926';
        pay.ChikPeaO2B__Has_Processed__c=true;
        pay.ChikPeaO2B__Trx_Message__c='This transaction has been approved.';
        
        insert(pay); 
        
        Test.startTest();

        /*BatchAchStatus BAS = new BatchAchStatus();
        BAS.email='salesforce@chikpea.com';
        BAS.test_status_exception=true;
        BAS.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet:Reporting\' AND ChikPeaO2B__Payment_Method__c=\'ACH\' AND ChikPeaO2B__Status__c=\'In Process\' AND ChikPeaO2B__Has_Processed__c=true';
        Database.executeBatch(BAS);*/
        BatchAchStatusScheduler BASS = new BatchAchStatusScheduler();
        BASS.test_status_exception=true;
        String sch='0 00 06 * * ?';
        system.schedule('Check Status of ACH',sch, BASS);
        Test.stopTest();
    }
}