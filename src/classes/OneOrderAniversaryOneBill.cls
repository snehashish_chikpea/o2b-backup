/*
** Class         :  OneOrderAniversaryOneBill
** Created by    :  Asitm (chikpea Inc.)
** Last Modified :  31 Oct 14 by Spal
** Description   :  Billing fix 
*/

global with sharing class OneOrderAniversaryOneBill{
     Webservice static id aniversaryOneBillIvoicing(list<id> accountIds,list<id> orderlineids,list<string> itmTypeList,list<id>oidList){
        list<Account> AccList=new list<Account>();
        list<Subscription__c> subList= new list<Subscription__c>();
        list<Purchase__c> purList= new list<Purchase__c>();
        list<usage__c> usgList= new list<usage__c>();
        list<Invoice_Line__c> invcLineList= new list<Invoice_Line__c>();
        Account currAcc;
        
        AccList=[SELECT Id, ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Amount_Remaining__c, BillingStreet,
        BillingState,BillingPostalCode, BillingCountry, BillingCity, Bill_Cycle__c,Amount_Due__c,
        Active__c, Activation_Date__c,Payment_Term__c, Auto_Payment__c,(Select Id, ChikPeaO2B__Role__c From Contacts where ChikPeaO2B__Role__c = 'Billing' limit 1)
        FROM
        Account 
        WHERE Id IN :accountIds];/* and Bill_Cycle__c != null*/
        subList=[SELECT Id, Name, ChikPeaO2B__Item__c,ChikPeaO2B__Item__r.ChikPeaO2B__Is_prorate__c, 
        ChikPeaO2B__Recurring_Charge__c,ChikPeaO2B__Next_Bill_Date__c, ChikPeaO2B__Bill_Cycle__c, 
        ChikPeaO2B__Order_Line__c,ChikPeaO2B__Quantity__c,ChikPeaO2B__Billing_Start_Date__c, 
        ChikPeaO2B__Billing_Stop_Date__c,ChikPeaO2B__Description__c,ChikPeaO2B__Billing_Stopped__c, 
        ChikPeaO2B__Discount__c,ChikPeaO2B__Billing_Started__c 
        FROM 
        Subscription__c
        WHERE (Billing_Stop_Date__c = null or Billing_Stopped__c = false) 
        AND (ChikPeaO2B__Order_Line__c in: orderlineids AND  ChikPeaO2B__Order_Line__r.ChikPeaO2B__Item_Type__c IN :itmTypeList)];
        
        purList=[SELECT Id, ChikPeaO2B__Item__c, ChikPeaO2B__Sell_Price__c, ChikPeaO2B__Qty__c,
        ChikPeaO2B__Invoiced__c,ChikPeaO2B__Tax_Rate__c, ChikPeaO2B__Invoice__c, ChikPeaO2B__Order_Line__c,
        ChikPeaO2B__Description__c
        FROM 
        ChikPeaO2B__Purchase__c
        WHERE ChikPeaO2B__Invoiced__c =false
        AND (ChikPeaO2B__Order_Line__c in: orderlineids and  ChikPeaO2B__Order_Line__r.ChikPeaO2B__Item_Type__c IN :itmTypeList)
        ];
        ChikPeaO2B__Order__c order_for_process_order;
        ChikPeaO2B__Quote__c qoute_for_process_order;
        if(oidList!=null&&(!oidList.isEmpty())){
          order_for_process_order=[select Id, name, ChikPeaO2B__Quote__c, ChikPeaO2B__Billing_City__c, ChikPeaO2B__Billing_Country__c,
          ChikPeaO2B__Billing_State_Province__c, ChikPeaO2B__Billing_Street__c, ChikPeaO2B__Billing_Zip_Postal_Code__c,
          ChikPeaO2B__Contact__c, ChikPeaO2B__PO_Number__c, ChikPeaO2B__Shipping_City__c, ChikPeaO2B__Shipping_Country__c,
          ChikPeaO2B__Shipping_Street__c,  ChikPeaO2B__Shipping_Zip_Postal_Code__c
            from ChikPeaO2B__Order__c where Id=:oidList[0]];
            System.debug('#****order_for_process_order='+order_for_process_order); 
          if(order_for_process_order.ChikPeaO2B__Quote__c!=null){
            qoute_for_process_order=[select Id, Name, ChikPeaO2B__Payment_Term__c,ChikPeaO2B__Contact__c from ChikPeaO2B__Quote__c
            where id=:order_for_process_order.ChikPeaO2B__Quote__c];  
          }
        }
        if(!AccList.isEmpty()){
            currAcc=AccList[0];
        }
        else{
            return null;
        } 
         
         
        if((subList.isEmpty() && purList.isEmpty()) || system.today()==currAcc.ChikPeaO2B__Next_Bill_Date__c){//if today is billing date for account then we dont create bill
            return null;
        }
        
        date InvoiceBillDateFrom=system.today();
        date InvoiceBillDateTo=currAcc.ChikPeaO2B__Next_Bill_Date__c;
        Savepoint sp = Database.setSavepoint();//#####save point
        Invoice__c inv = new Invoice__c();
        inv.Account__c = currAcc.Id;
        inv.Bill_Date__c = date.today();
        /*if(currAcc.Contacts != null && currAcc.Contacts.size()>0)
            inv.Billing_Contact__c = currAcc.Contacts[0].Id;*/
            
        inv.ChikPeaO2B__Period_From__c= InvoiceBillDateFrom;
        inv.ChikPeaO2B__Period_To__c= InvoiceBillDateTo;
        inv.ChikPeaO2B__Open_Balance__c= currAcc.ChikPeaO2B__Amount_Remaining__c;
        if(qoute_for_process_order!=null){
            inv.Payment_Term__c=((qoute_for_process_order.ChikPeaO2B__Payment_Term__c!='' && qoute_for_process_order.ChikPeaO2B__Payment_Term__c!=null)?qoute_for_process_order.ChikPeaO2B__Payment_Term__c:currAcc.Payment_Term__c);
            if(qoute_for_process_order.ChikPeaO2B__Contact__c!=null)
                inv.Billing_Contact__c=qoute_for_process_order.ChikPeaO2B__Contact__c;
            else
            {
                if(currAcc.Contacts != null && currAcc.Contacts.size()>0)
                    inv.Billing_Contact__c = currAcc.Contacts[0].Id;
            }
        }else{
            inv.Payment_Term__c = currAcc.Payment_Term__c;
            if(currAcc.Contacts != null && currAcc.Contacts.size()>0)
                    inv.Billing_Contact__c = currAcc.Contacts[0].Id;
        }        
        if(order_for_process_order==null){
          
            inv.Billing_Street__c = currAcc.BillingStreet;
            inv.Billing_City__c = ((currAcc.BillingCity == null || currAcc.BillingCity.length()<30) ? currAcc.BillingCity : currAcc.BillingCity.substring(0,30) );
            inv.ChikPeaO2B__Billing_State_Province__c= currAcc.BillingState;
            inv.ChikPeaO2B__Billing_Zip_Postal_Code__c= currAcc.BillingPostalCode;
            inv.ChikPeaO2B__Billing_Country__c= currAcc.BillingCountry;
          
        }else{
          if(order_for_process_order.ChikPeaO2B__Billing_Street__c!=null && purList.size()==orderlineids.size()){
            inv.Billing_Street__c = order_for_process_order.ChikPeaO2B__Billing_Street__c;
            inv.Billing_City__c = order_for_process_order.ChikPeaO2B__Billing_City__c;
            inv.ChikPeaO2B__Billing_State_Province__c= order_for_process_order.ChikPeaO2B__Billing_State_Province__c;
            inv.ChikPeaO2B__Billing_Zip_Postal_Code__c= order_for_process_order.ChikPeaO2B__Billing_Zip_Postal_Code__c;
            inv.ChikPeaO2B__Billing_Country__c= order_for_process_order.ChikPeaO2B__Billing_Country__c;
          }else{
            inv.Billing_Street__c = currAcc.BillingStreet;
            inv.Billing_City__c = ((currAcc.BillingCity == null || currAcc.BillingCity.length()<30) ? currAcc.BillingCity : currAcc.BillingCity.substring(0,30) );
            inv.ChikPeaO2B__Billing_State_Province__c= currAcc.BillingState;
            inv.ChikPeaO2B__Billing_Zip_Postal_Code__c= currAcc.BillingPostalCode;
            inv.ChikPeaO2B__Billing_Country__c= currAcc.BillingCountry;
          }
        }
        
        inv.ChikPeaO2B__Order__c=oidList[0];
        //=== CURD check [START] ===//
        Map<string,string>InvoiceInsertResultMap=new map<string,string>();
        InvoiceInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Invoice__c',FieldPermissionSet.InvoiceInsert,'insert');
        if(InvoiceInsertResultMap.get('AllowDML')!=null && InvoiceInsertResultMap.get('AllowDML').equalsignorecase('true'))
        {
            insert inv;
        }
        else
        {
            return null;
        }
        //=== CURD check [END] ===//
        if(currAcc.ChikPeaO2B__Bill_Cycle__c!=null){
          for(Subscription__c sub : subList){
              if(currAcc.ChikPeaO2B__Next_Bill_Date__c>date.today() && sub.ChikPeaO2B__Billing_Start_Date__c<currAcc.ChikPeaO2B__Next_Bill_Date__c)//acc nextbill date>=today and start date<acc next bill date, bcoz we will bill only till next bill date
              {
                  map<string,date> onebillcyclefromstartdate= new map<string,date>();
                  onebillcyclefromstartdate.put('Annual',sub.ChikPeaO2B__Billing_Start_Date__c.addyears(1));
                  onebillcyclefromstartdate.put('Quarterly',sub.ChikPeaO2B__Billing_Start_Date__c.addmonths(3));
                  onebillcyclefromstartdate.put('Monthly',sub.ChikPeaO2B__Billing_Start_Date__c.addmonths(1));
                  onebillcyclefromstartdate.put('Weekly',sub.ChikPeaO2B__Billing_Start_Date__c.adddays(7));
                  onebillcyclefromstartdate.put('Daily',sub.ChikPeaO2B__Billing_Start_Date__c.adddays(1));
                  onebillcyclefromstartdate.put('Half Yearly',sub.ChikPeaO2B__Billing_Start_Date__c.addmonths(6));
                  try{
                      onebillcyclefromstartdate.put(currAcc.ChikPeaO2B__Bill_Cycle__c,sub.ChikPeaO2B__Billing_Start_Date__c.adddays(integer.valueof(currAcc.ChikPeaO2B__Bill_Cycle__c)));
                  }catch(Exception e){}
                  Invoice_Line__c invcl = new Invoice_Line__c();
                  invcl.line_type__c = 'Subscription';
                  invcl.Item__c = sub.Item__c;
                  invcl.ChikPeaO2B__Qty__c = sub.ChikPeaO2B__Quantity__c;
                  //invcl.Tax_Rate__c = sub.Tax_Rate__c;
                  invcl.Description__c = sub.Description__c;
                  invcl.ChikPeaO2B__Subscription__c = sub.Id;
                  invcl.Invoice__c=inv.Id;
                  invcl.ChikPeaO2B__Period_From__c=sub.ChikPeaO2B__Billing_Start_Date__c;
  
                  if((currAcc.ChikPeaO2B__Next_Bill_Date__c-1)<=sub.ChikPeaO2B__Billing_Stop_Date__c || sub.ChikPeaO2B__Billing_Stop_Date__c==null)// bill till nextbilldate-1
                  {
                      invcl.ChikPeaO2B__Period_To__c=currAcc.ChikPeaO2B__Next_Bill_Date__c-1;                    
                      if((currAcc.ChikPeaO2B__Next_Bill_Date__c-1)==sub.ChikPeaO2B__Billing_Stop_Date__c)
                          sub.ChikPeaO2B__Billing_Stopped__c=true;
                  }
                  else if((currAcc.ChikPeaO2B__Next_Bill_Date__c-1)>sub.ChikPeaO2B__Billing_Stop_Date__c)// bill till stop date
                  {
                      invcl.ChikPeaO2B__Period_To__c=sub.ChikPeaO2B__Billing_Stop_Date__c;
                      sub.ChikPeaO2B__Billing_Stopped__c=true;
                  }
  
                  invcl.Unit_Rate__c = (sub.ChikPeaO2B__Recurring_Charge__c/(sub.ChikPeaO2B__Billing_Start_Date__c.daysbetween(onebillcyclefromstartdate.get(currAcc.ChikPeaO2B__Bill_Cycle__c)-1)+1))*(invcl.ChikPeaO2B__Period_From__c.daysbetween(invcl.ChikPeaO2B__Period_To__c)+1);
                  invcLineList.add(invcl);
                  
                  sub.ChikPeaO2B__Billing_Started__c = true;
                  sub.ChikPeaO2B__Invoice__c=inv.Id;
              } 
          }
        }
        for(Purchase__c pur :purList){
            Invoice_Line__c invcl = new Invoice_Line__c();
            invcl.line_type__c = 'Purchase';
            invcl.Item__c = pur.Item__c;
            invcl.Unit_Rate__c = pur.Sell_Price__c;
            invcl.Qty__c = pur.Qty__c;
            invcl.Tax_Rate__c = pur.Tax_Rate__c;
            invcl.Description__c = pur.Description__c;
            invcl.Purchase__c = pur.Id;
            invcl.Invoice__c=inv.Id;
            invcLineList.add(invcl);
            
            pur.Invoiced__c = true;
            pur.ChikPeaO2B__Invoice__c=inv.Id;
        }
        
        if(!invcLineList.isEmpty()){
            //=== CURD check [START] ===//
            Map<string,string>InvlInsertResultMap=new map<string,string>();
            InvlInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Invoice_Line__c',FieldPermissionSet.InvoiceLineInsert,'insert');
            if(InvlInsertResultMap.get('AllowDML')!=null && InvlInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                insert invcLineList;
            }
            else
            {        
                Database.rollback(sp);
                return null;
            }
            //=== CURD check [END] ===//
        }
        
        if(!subList.isEmpty()){
            //=== CURD check [START] ===//
            Map<string,string>SubUpdateResultMap=new map<string,string>();
            SubUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Subscription__c',FieldPermissionSet.SubscriptionUpdate,'update');
            if(SubUpdateResultMap.get('AllowDML')!=null && SubUpdateResultMap.get('AllowDML').equalsignorecase('true'))
            {
                update subList;
            }
            else
            {        
                Database.rollback(sp);
                return null;
            }
            //=== CURD check [END] ===//
        }
        
        if(!purList.isEmpty()){
            //=== CURD check [START] ===//
            Map<string,string>PurUpdateResultMap=new map<string,string>();
            PurUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Purchase__c',FieldPermissionSet.PurchaseUpdate,'update');
            if(PurUpdateResultMap.get('AllowDML')!=null && PurUpdateResultMap.get('AllowDML').equalsignorecase('true'))
            {
                update purList;
            }
            else
            {        
                Database.rollback(sp);
                return null;
            }
            //=== CURD check [END] ===//
        }
        
        return inv.id;
     } 
}