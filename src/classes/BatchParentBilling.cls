/*
** Class         :  BatchParentBill
** Created by    :  Asitm9(chikpea Inc.)
** Description   :   
*/

global with sharing class BatchParentBilling implements Database.Batchable<Sobject>{
    global String query;
    global String errmsg;
    global string email;
    
    global database.querylocator start(Database.BatchableContext BC){
        //query='SELECT Id, (SELECT Id FROM ChikPeaO2B__Invoices__r WHERE ChikPeaO2B__Invoice_Status__c=\'Open\' ORDER BY Createddate LIMIT 1)'
        //+' FROM Account WHERE ChikPeaO2B__Parent_Billing__c=True AND ParentId==Null'
        //;
        if(this.query==null){
        	this.query='SELECT Id FROM Account WHERE ParentId=Null';
        }       
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        list<id>accIds= new list<id>();
        for(sObject s : scope){
            Account acc=(Account)s;
            accIds.add(acc.Id);
        }
        //try{
            ParentBillingProcessor.processAccounts(accIds);
        //}catch(Exception e){errmsg=e.getMessage();}
    }
    global void finish(Database.BatchableContext BC){
        if(email==null){
        	email='ad@chikpea.com';
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo(email);
        mail.setSenderDisplayName('ChikPeaO2B Batch Process');
        mail.setSubject('Auto Parent Billing Batch Process Completed');
        if (errmsg == null) errmsg = '';
        mail.setPlainTextBody('Auto Parent Billing Batch Process Completed' + errmsg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}