public class CreateTestData
{
    public static Map<Integer,String> itemtype=new Map<Integer,String>{1 => 'Recurring', 2 => 'One-Off', 3 => 'Post-Paid Usage', 4 => 'Pre-Paid Usage'};
    public enum PricingType{Flat,Tiered,Volumn}
    //#######create Account ########//
    // 1.newname=New Account Name
    // 2.newnumber=New Account Number
    // 3.type=Account Type
    //   Such as:-Prospect,Customer - Direct,Customer - Channel,Channel Partner / Reseller,Installation Partner,Technology Partner,Other
    // 4.industry=Industry Type
    //   Such as:-Agriculture,Apparel,Banking,Biotechnology,Chemicals,Communications,Construction,Consulting,Education,Electronics etc
    // 5.rating=Rating Value
    //   Such as:-Hot,Warm,Cold
    // 6.ownership=OwnerShip Values
    //   Such as:-Public,Private,Subsidiary,Other
    // 7.activedecision=Account is Active or not
    // 8.billcycle=BillCycle Value
    //   Such as:-Daily,Weekly,Monthly,Quarterly,Annual,Half Yearly
    // 9.bill_date=Bill Cycle Date
    // 10.count=Number of Account 
    //####### Create Account #######//
    public static List<Account> createAccount(String newname,String newnumber,String type,String industry,String rating,String ownership,String activedecision,String billcycle,date bill_date,Integer count)
    {
       List<Account> accnt=new list<Account>();
       for(Integer i=1;i<=count;i++)
       {
        Account acc=new Account();
        acc.Name=newname+i;
        acc.AccountNumber=newnumber;
        acc.type=type;
        acc.Industry=industry;
        acc.Rating=rating;
        acc.Ownership=ownership;
        acc.ChikPeaO2B__Active__c=activedecision;
        acc.ChikPeaO2B__Bill_Cycle__c=billcycle;
        acc.ChikPeaO2B__Next_Bill_Date__c=bill_date;
        accnt.add(acc);
       } 
       return accnt;
    }
    //######## Create Price Book ########//
    // 1.newname=Price Book Name
    // 2.count=Number of PriceBook
    //######## Create Price Book#########//
    public static List<ChikPeaO2B__Price_Book__c> createPriceBook(String newname,Integer count)
    {
        List<ChikPeaO2B__Price_Book__c> pricelist=new list<ChikPeaO2B__Price_Book__c>();
        for(Integer i=1;i<=count;i++)
        {
           ChikPeaO2B__Price_Book__c pricebook=new ChikPeaO2B__Price_Book__c();
           pricebook.Name=newname+i;
           pricebook.ChikPeaO2B__Active__c=true;
           pricelist.add(pricebook);
        }
        return pricelist;
    }
    //####### Create Quote ##########//
    // 1.accountid=Account Id
    // 2.pricebook=PriceBook Id
    // 3.status=Quote Status
    //   Such as:-Open,Accepted,Expired,Cancelled
    // 4.submission=Date of Submission
    // 5.expiry=Expiry date of Quote
    // 6.payment_method=Payment Method
    //   Such as:-Credit Card,Check,Cash,Bank Transfer 
    // 7.payment_term=Define Payment Term
    //   Such as:-Net10,Net20,Net30,COD
    // 8.contract_term=Contract Term
    //   Such as:-1,6,12,24,36
    // 9.count=Number of New Quote
    //####### Create Quote #########//
    public static List<ChikPeaO2B__Quote__c> createQuote(id accountid,id pricebook,String status,date submission,date expiry,String payment_method,String payment_term,String contract_term,Integer count)
    {
        List<ChikPeaO2B__Quote__c> qtline=new List<ChikPeaO2B__Quote__c>();
        if(accountid!=null && pricebook!=null)
        {
           for(Integer i=1;i<=count;i++)
           { 
               ChikPeaO2B__Quote__c quote_entry=new ChikPeaO2B__Quote__c();
               quote_entry.ChikPeaO2B__Account__c=accountid;
               quote_entry.ChikPeaO2B__Price_Book__c=pricebook;
               quote_entry.ChikPeaO2B__Status__c=status;
               quote_entry.ChikPeaO2B__Submission__c=submission;
               quote_entry.ChikPeaO2B__Expiration_Date__c=expiry;
               quote_entry.ChikPeaO2B__Payment_Method__c=payment_method;
               quote_entry.ChikPeaO2B__Payment_Term__c=payment_term;
               quote_entry.ChikPeaO2B__Contract_Term_Months__c=contract_term;
             
               qtline.add(quote_entry);
           }  
        }
        else
        {
           return null;
        }
        return qtline;
    
    }
    //#######create Item ########//
    // 1.itemname=Name Of Item
    // 2.itemtype={ 1->Recurring
    //              2->One-Off
    //              3->Post-Paid Usage
    //              4->Pre-Paid Usage }
    // 3.itemcategory=Category of Item
    // 4.shipping=This item is delivered or not
    // 5.active=This item is active or not
    // 6.prorate=prorate is true or not
    // 7.count=Number of Item
    //####### Create Item #######//
    public static List<ChikPeaO2B__Item__c> createItem(String itemname,String itemtype1,String itemcategory,boolean shipping,boolean active,boolean prorate,Integer count,boolean isAgreegate)
    {
      List<ChikPeaO2B__Item__c> itemlist=new List<ChikPeaO2B__Item__c>();
      for(Integer i=1;i<=count;i++)
      {
        ChikPeaO2B__Item__c item=new ChikPeaO2B__Item__c();
        item.Name=itemname+i;
        item.ChikPeaO2B__Item_Type__c=itemtype1;
        item.ChikPeaO2B__Category__c=itemcategory;
        item.ChikPeaO2B__Is_Shipping__c=shipping;
        item.ChikPeaO2B__Active__c=active;
        item.ChikPeaO2B__Is_prorate__c=prorate;
        item.ChikPeaO2B__Is_aggregation__c=isAgreegate;
         
        itemlist.add(item); 
       }
        return itemlist;
     }
     //#######create Quote Line ########//
    // 1.description=which type of item
    // 2.quote=define quote for which quote line is created
    // 3.item=define item for which quote line is created
    // 4.itemtype=define the type of item
    // 5.unit_price=define the price of the item
    // 6.quantity=quantity of item
    // 7.rateplan=define rateplan for which quote line is created
    // 8.count=Number of Quote Line
    //####### Create Quote Line #######//
     public static List<ChikPeaO2B__Quote_Line__c> createQuoteLine(String description,id quote,id item,String itemtype1,Integer unit_price,Integer quantity,id rateplan,Integer count)
     {
     try
     {
         List<ChikPeaO2B__Quote_Line__c> qllist=new List<ChikPeaO2B__Quote_Line__c>();
         if(quote!=null)
         {
            for(Integer i=1;i<=count;i++)
            {
              ChikPeaO2B__Quote_Line__c quoteline=new ChikPeaO2B__Quote_Line__c();
              quoteline.ChikPeaO2B__Description__c=description;
              quoteline.ChikPeaO2B__Quote__c=quote;
              quoteline.ChikPeaO2B__Item__c=item;
              quoteline.ChikPeaO2B__Item_Type__c=itemtype1;
              quoteline.ChikPeaO2B__Unit_Price__c=unit_price;
              quoteline.ChikPeaO2B__Quantity__c=quantity;
              quoteline.ChikPeaO2B__Rate_Plan__c=rateplan;
        
              qllist.add(quoteline);
            }
         }
         else
         {
              return null;
         }
         return qllist;
       }
       catch(Exception e)
       {
            System.debug('Error::::::'+e.getMessage());
       }
       return null;    
     }
     //########## Create Rate Plan ##########//
     // 1.item=Item Id
     // 2.pricebook=PriceBook Id
     // 3.pricing_type=Pricing type
     //   Such as:-Flat,Tiered,Volume
     // 4.bill_cycle=Define Bill Cycle
     //   Such as:-Annual,Daily,Monthly,Quarterly,Weekly,Half Yearly
     // 5.recurring_charge=Recurring Charge for Subscription
     // 6.Non_Recurring_Charge=non Recurring Charge for Puschese
     // 7.usage_rate=Usage Rate
     // 8.uom=Unit of Measurement
     // 9.max_usage=Maximum Usage
     // 10.free_usage=Free Usage for post-paid
     // 11.rating_rule=Rating Rule value
     //    Such as:-MonthlyAggregate,MonthlyAvg,MonthlyLatest,MonthlyMax
     // 12.start_offset=Start Offset after Create Rate Plan
     // 13.start_offset_unit=Select Start Offset Unit
     //    Such as:-Day(s),Week(s),Month(s),Year(s)
     // 14.count=Number of Rate Plan
     // 15.min_usage=Minimum Usage Of Rate Plan
     // 16.min_amount=Minimum amount of Rate Plan
     //########## Create Rate Plan ############//
     public static List<ChikPeaO2B__Rate_Plan__c> createRatePlan(id item,id pricebook,String pricing_type,String bill_cycle,Integer recurring_charge,Integer Non_Recurring_Charge,Integer usage_rate,String uom,Integer max_usage,Integer free_usage,String rating_rule,Integer start_offset,String start_offset_unit,Integer period_offset,String period_offset_unit,Integer count,Integer min_usage,Integer min_amount)
     {
     List<ChikPeaO2B__Rate_Plan__c> rtlist=new List<ChikPeaO2B__Rate_Plan__c>();
     if(item!=null && pricebook!=null)
     {
      for(Integer i=1;i<=count;i++)
      {
       ChikPeaO2B__Rate_Plan__c rateplan=new ChikPeaO2B__Rate_Plan__c();
       rateplan.ChikPeaO2B__Pricing_Type__c=pricing_type;
       rateplan.ChikPeaO2B__Item__c=item;
       rateplan.ChikPeaO2B__Price_Book__c=pricebook;
       if(bill_cycle!=null)
       rateplan.ChikPeaO2B__Bill_Cycle__c=bill_cycle;
       if(recurring_charge!=0)
       rateplan.ChikPeaO2B__Recurring_Charge__c=recurring_charge;
       if(Non_Recurring_charge!=0)
       rateplan.ChikPeaO2B__Non_Recurring_Charge__c=Non_Recurring_Charge;
       if(usage_rate!=0)
       rateplan.ChikPeaO2B__Usage_Rate__c=usage_rate;
       if(uom!=null)
       rateplan.ChikPeaO2B__UOM__c=uom;
       if(max_usage!=0)
       rateplan.ChikPeaO2B__Max_Usage__c=max_usage;
       if(free_usage!=0)
       rateplan.ChikPeaO2B__Free_Usage__c=free_usage;
       if(rating_rule!=null)
       rateplan.ChikPeaO2B__Rating_Rule__c=rating_rule;
       if(start_offset!=0)
       rateplan.ChikPeaO2B__Start_Off_set__c=start_offset;
       if(start_offset_unit!=null)
       rateplan.ChikPeaO2B__Start_Off_Set_Unit__c=start_offset_unit;
       if(period_offset!=0)
       rateplan.ChikPeaO2B__Period_Off_Set__c=period_offset;
       if(period_offset_unit!=null)
       rateplan.ChikPeaO2B__Period_Off_Set_Unit__c=period_offset_unit;
       rateplan.ChikPeaO2B__Min_Usage__c=min_usage;
       rateplan.ChikPeaO2B__Min_Amount__c=min_amount;
       
             
       rtlist.add(rateplan);
       
       }
      }
      else
      {
          return null;
      }
      return rtlist;  
     }
     //######### Tiered Plan Create #########//
     // 1.rateplan=Rateplan Id
     // 2.lower_limit=Lower Limit Of Tierd Plan
     // 3.upper_limit=Upper Limit Of Tiered Plan
     // 4.non_recurring_charge=Non Recurring Charge For One Off Item
     // 5.recurring_charge=Recurring Charge for Recurring Item
     // 6.usage_rate=Usage Rate between Lower limit and Upper Limit
     // 7.item=Item Id
     //######## Tiered Plan Create########//
     public static ChikPeaO2B__Tiered_Pricing__c  createTiered(id rateplan,Integer lower_limit,Integer upper_limit,Integer non_recurring_charge,Integer recurring_charge,Integer usage_rate,id item)
     {
        ChikPeaO2B__Tiered_Pricing__c tierplan=new ChikPeaO2B__Tiered_Pricing__c();
        if(rateplan!=null)
        {
        tierplan.ChikPeaO2B__Rate_Plan__c=rateplan;
        tierplan.ChikPeaO2B__Lower_Limit__c=lower_limit;
        tierplan.ChikPeaO2B__Upper_Limit__c=upper_limit;
        if(recurring_charge!=0)
        tierplan.ChikPeaO2B__Recurring_Charge__c=recurring_charge;
        if(non_recurring_charge!=0)
        tierplan.ChikPeaO2B__Non_Recurring_Charge__c=non_recurring_charge;
        if(usage_rate!=0)
        tierplan.ChikPeaO2B__Usage_Rate__c=usage_rate;
        tierplan.ChikPeaO2B__Item__c=item;
        
        }
        return tierplan;
     }
     //######### Volume Plan Create #########//
     // 1.rateplan=Rateplan Id
     // 2.lower_limit=Lower Limit Of Tierd Plan
     // 3.upper_limit=Upper Limit Of Tiered Plan
     // 4.non_recurring_charge=Non Recurring Charge For One Off Item
     // 5.recurring_charge=Recurring Charge for Recurring Item
     // 6.usage_rate=Usage Rate between Lower limit and Upper Limit
     // 7.item=Item Id
     //######## Volume Plan Create########//
     public static ChikPeaO2B__Volume_Pricing__c createVolume(id rateplan,Integer lower_limit,Integer upper_limit,Integer non_recurring_charge,Integer recurring_charge,Integer usage_rate,id item)
     {
        ChikPeaO2B__Volume_Pricing__c volumnplan=new ChikPeaO2B__Volume_Pricing__c();
        if(rateplan!=null)
        {
        volumnplan.ChikPeaO2B__Rate_Plan__c=rateplan;
        volumnplan.ChikPeaO2B__Lower_Limit__c=lower_limit;
        volumnplan.ChikPeaO2B__Upper_Limit__c=upper_limit;
        if(recurring_charge!=0)
        volumnplan.ChikPeaO2B__Recurring_Charge__c=recurring_charge;
        if(non_recurring_charge!=0)
        volumnplan.ChikPeaO2B__Non_Recurring_Charge__c=non_recurring_charge;
        if(usage_rate!=0)
        volumnplan.ChikPeaO2B__Usage_Rate__c=usage_rate;
        volumnplan.ChikPeaO2B__Item__c=item;
        }
        return volumnplan;            
     }
     //####### Create Order ##########//
    // 1.account=Account Id
    // 2.pricebook=PriceBook Id
    // 3.status=Order Status
    //   Such as:-Open,In Process,Closed,Cancelled
    // 4.invoicefor=Invoice for Recurring or Non Recurring Or Both
    // 5.count=Number of Order
    //####### Create Order #########//
     public static List<ChikPeaO2B__Order__c> createOrder(id account,id pricebook,String status,String invoicefor,String shipping_required,Integer count)
     {
       List<ChikPeaO2B__Order__c> orderlist=new List<ChikPeaO2B__Order__c>();
       if(account!=null && pricebook!=null)
       {
        for(Integer i=1;i<=count;i++)
        {
        ChikPeaO2B__Order__c order_entry=new ChikPeaO2B__Order__c();
        order_entry.ChikPeaO2B__Account__c=account;
        order_entry.ChikPeaO2B__Price_Book__c=pricebook;
        order_entry.ChikPeaO2B__Status__c=status;
        
        order_entry.ChikPeaO2B__Invoice_For__c=invoicefor;
        order_entry.ChikPeaO2B__Shipping_Required__c=shipping_required;
        
        orderlist.add(order_entry);
        }
        return orderlist;
       }
       else
       {
          return null;
       }
     }
     //#######create Order Line ########//
    // 1.order=Define Order for Which Order Line is Created 
    // 2.item=Define Item
    // 3.status=Order Line Status
    //   Such as:-Open,In Process,Closed,Cancelled
    // 4.itemtype1=define the type of item
    // 5.rateplan=RatePlan Id
    // 6.unit_price=Unit Price of Item
    // 7.quantity=Item Quantity
    // 8.subscription_start_date=Subscription Start Date
    // 9.subscription_term_month=Subscription Term Month
    // 10.count=Number of Order Line
    //####### Create Order Line #######//
     public static List<ChikPeaO2B__Order_Line__c> createOrderLine(id order,id item,String status,String itemtype1,id rateplan,Integer unit_price,Integer quantity,date subscription_start_date,String subscription_term_month,Integer count,id agreement)
     {
        List<ChikPeaO2B__Order_Line__c> ordlist=new List<ChikPeaO2B__Order_Line__c>();
        if(order!=null)
        {
        for(Integer i=1;i<=count;i++)
        {
        ChikPeaO2B__Order_Line__c orderline=new ChikPeaO2B__Order_Line__c();  
        orderline.ChikPeaO2B__Order__c=order;
        orderline.ChikPeaO2B__Item__c=item;
        orderline.ChikPeaO2B__Status__c=status;
        orderline.ChikPeaO2B__Item_Type__c=itemtype1;
        orderline.ChikPeaO2B__Rate_Plan__c=rateplan;
        orderline.ChikPeaO2B__Unit_Price__c=unit_price;
        orderline.ChikPeaO2B__Quantity__c=quantity;
        orderline.ChikPeaO2B__Subscription_start_date__c=subscription_start_date;
        orderline.ChikPeaO2B__Subscription_Term_Months__c=subscription_term_month;
        orderline.ChikPeaO2B__Agreement__c=agreement;
        
        ordlist.add(orderline);
        
        }
        }
        else
        {
          return null;
        }
        return ordlist;
     }
     //#######create Purchase ########//
    // 1.acc=Account id 
    // 2.item=Item id
    // 3.orderline=Orderline id
    // 4.rateplan=RatePlan id
    // 5.quantity=Number of Purchase Item
    // 6.sell_price=Define sell price
    //####### Create Purchase #######//
     public static List<ChikPeaO2B__Purchase__c> createPurchase(id acc,id item,List<ChikPeaO2B__Order_Line__c> orderline,id rateplan,Integer quantity,Integer sell_price,boolean invoiced)
     {
        if(acc!=null)
        {
          List<ChikPeaO2B__Purchase__c> purchaselist=new List<ChikPeaO2B__Purchase__c>();
          for(Integer i=0;i<orderline.size();i++)
          { 
           ChikPeaO2B__Purchase__c purchase=new ChikPeaO2B__Purchase__c();
           purchase.ChikPeaO2B__Account__c=acc;
           purchase.ChikPeaO2B__Item__c=item;
           purchase.ChikPeaO2B__Order_Line__c=orderline.get(i).id;
           purchase.ChikPeaO2B__Rate_Plan__c=rateplan;
           purchase.ChikPeaO2B__Qty__c=quantity;
           purchase.ChikPeaO2B__Sell_Price__c=sell_price;
           purchase.ChikPeaO2B__Invoiced__c=invoiced;
           
           purchaselist.add(purchase);
          }
          return  purchaselist;
          
        }
        else
        {
           return null;
        }
     }
    //####### create Subscription ########//
    // 1.acc=Account id 
    // 2.item=Item id
    // 3.rateplan=RatePlan id
    // 4.orderline=Orderline id
    // 5.bill_start_date=Bill Start Date
    // 6.discount=discount rate
    // 7.recurring_charge=Recurring Charge
    // 8.free_usage=Amount of free usage
    // 9.quantity=Number of Subscription Item
    // 10.bill_started=Bill Start checkbox is true or false
    // 11.bill_stop=Bill Stop is true or false
    //####### Create Subscription #######//
    public static List<ChikPeaO2B__Subscription__c> createSubscription(id acc,id item,id rateplan,List<ChikPeaO2B__Order_Line__c> orderline,date bill_start_date,Integer discount,Integer recurring_charge,Integer free_usage,Integer quantity,boolean bill_started,date bill_stop,date next_bill_date)
     {
        if(acc!=null)
        {
              List<ChikPeaO2B__Subscription__c> subscriptionlist=new List<ChikPeaO2B__Subscription__c>();
              for(Integer i=0;i<orderline.size();i++)
              {
              ChikPeaO2B__Subscription__c subscription=new ChikPeaO2B__Subscription__c();
              subscription.ChikPeaO2B__Account__c=acc;
              subscription.ChikPeaO2B__Item__c=item;
              subscription.ChikPeaO2B__Rate_Plan__c=rateplan;
              subscription.ChikPeaO2B__Order_Line__c=orderline.get(i).id;
              subscription.ChikPeaO2B__Billing_Start_Date__c=bill_start_date;
              subscription.ChikPeaO2B__Discount__c=discount;
              subscription.ChikPeaO2B__Recurring_Charge__c=recurring_charge;
              subscription.ChikPeaO2B__Free_usage__c=free_usage;
              subscription.ChikPeaO2B__Quantity__c=quantity;
              subscription.ChikPeaO2B__Billing_Started__c=bill_started;
              subscription.ChikPeaO2B__Billing_Stop_Date__c=bill_stop;
              subscription.ChikPeaO2B__Next_Bill_Date__c=next_bill_date;
                                
              subscriptionlist.add(subscription);
              }
              return subscriptionlist;
         }
         else
         {
              return null;
         }
      }
     //####### create Usage ########//
    // 1.acc=Account id 
    // 2.item=Item id
    // 3.orderline=Orderline id
    // 4.type=Usage type
    // 6.purchase=Purchase id
    // 7.subscription=Subscription id
    // 8.Rateplan=Rateplan id
    // 9.rate=define the rate
    // 10.remaining_usage=Enter Remaining Usage 
    // 11.total_usage=Define the value of total usage
    // 9.free_usage=Amount of free usage
    // 10.quantity=Number of Subscription Item
    // 11.free_usage=Define the amount of free usage
    //####### Create Usage #######//
      public static List<ChikPeaO2B__Usage__c> createUsage(id acc,id item,List<ChikPeaO2B__Order_Line__c> orderline,String type,List<ChikPeaO2B__Purchase__c> purchase,List<ChikPeaO2B__Subscription__c> subscription,id rateplan,Integer rate,Integer remaining_usage,Integer total_usage,Integer free_usage)
      {
         if(acc!=null)
         {
               Integer count;
               if(type.equals('Postpaid'))
               count=(orderline.size()>subscription.size() ? subscription.size() : orderline.size());
               else
               count=(orderline.size()>purchase.size() ? purchase.size() : orderline.size());
               List<ChikPeaO2B__Usage__c> usg=new List<ChikPeaO2B__Usage__c>();
               for(Integer i=0;i<count;i++)
               {
                  for(Integer j=1;j<=orderline.get(i).ChikPeaO2B__Quantity__c;j++)
                  {
                     
                    ChikPeaO2B__Usage__c usage=new ChikPeaO2B__Usage__c();
                    usage.ChikPeaO2B__Account__c=acc;
                    usage.ChikPeaO2B__Item__c=item;
                    if(type.equals('Postpaid'))
                    usage.ChikPeaO2B__Order_Line__c=subscription.get(i).ChikPeaO2B__Order_Line__c;
                    else
                    usage.ChikPeaO2B__Order_Line__c=purchase.get(i).ChikPeaO2B__Order_Line__c;
                    usage.ChikPeaO2B__Type__c=type;
                    if(purchase!=null)
                    usage.ChikPeaO2B__Purchase__c=purchase.get(i).id; 
                    if(subscription!=null)
                    usage.ChikPeaO2B__Subscription__c=subscription.get(i).id;
                    usage.ChikPeaO2B__Rate_Plan__c=rateplan;
                    usage.ChikPeaO2B__Rate__c=rate;
                    usage.ChikPeaO2B__Remaining_Usage__c=remaining_usage;
                    usage.ChikPeaO2B__Total_Usage__c=total_usage;
                    usage.ChikPeaO2B__Free_Usage__c=free_usage;
                    
                    usg.add(usage);
        
                  }
               }
               return usg;
           }
           else
           {
                return null;
           }
       }
       //######## Create Account Based Tiering Pricing ########//
       // 1.account=Account id
       // 2.item=item id
       // 3.lower_limit=lower limit of usage
       // 4.upper_limit=upper limit of usage
       // 5.usage_rate=usage rate
       //######## Create Account Based Tiering Pricing ########// 
       public static ChikPeaO2B__Account_based_tiered_pricing__c createNewAccountBasedTier(id account,id item,Integer lower_limit,Integer upper_limit,Integer usage_rate)
       {
           if(account!=null && item!=null)
           {
               ChikPeaO2B__Account_based_tiered_pricing__c tier=new ChikPeaO2B__Account_based_tiered_pricing__c();
               tier.ChikPeaO2B__Account__c=account;
               tier.ChikPeaO2B__Item__c=item;
               tier.ChikPeaO2B__Lower_Limit__c=lower_limit;
               tier.ChikPeaO2B__Upper_Limit__c=upper_limit;
               tier.ChikPeaO2B__Usage_Rate__c=usage_rate;
               return tier;
           }
           else
              return null;
       }
       //######## Create Account Based Volumn Pricing ########//
       // 1.account=Account id
       // 2.item=item id
       // 3.lower_limit=lower limit of usage
       // 4.upper_limit=upper limit of usage
       // 5.usage_rate=usage rate
       //######## Create Account Based Volumn Pricing ########//
       public static ChikPeaO2B__Account_based_volume_pricing__c createNewAccountBasedVolumn(id account,id item,Integer lower_limit,Integer upper_limit,Integer usage_rate)
       {
       
            if(account!=null && item!=null)
           {
               ChikPeaO2B__Account_based_volume_pricing__c volumn=new ChikPeaO2B__Account_based_volume_pricing__c ();
               volumn.ChikPeaO2B__Account__c=account;
               volumn.ChikPeaO2B__Item__c=item;
               volumn.ChikPeaO2B__Lower_Limit__c=lower_limit;
               volumn.ChikPeaO2B__Upper_Limit__c=upper_limit;
               volumn.ChikPeaO2B__Usage_Rate__c=usage_rate;
               return volumn;
           }
           else
              return null;
       }
       //######## Create Agreement ########//
       // 1.AgreementName=Name of the agreement
       // 2.account=Define account id
       // 3.NBD=Next Bill Date
       // 4.status=status of the argeement
       // 5.termPeriod=Term Period of the agreement
       // 6.start_date=Start Date of the agreement
       //######## Create Agreement ########//
       public static List<ChikPeaO2B__Agreement__c> createAgreement(String AgreementName,id account,date NBD,String status,String termPeriod,date start_date)
       {
           List<ChikPeaO2B__Agreement__c> agreelist=new List<ChikPeaO2B__Agreement__c>();
       
           ChikPeaO2B__Agreement__c agg1=new ChikPeaO2B__Agreement__c();
           agg1.Name=AgreementName;
           agg1.ChikPeaO2B__Account__c=account;
           agg1.ChikPeaO2B__Next_Bill_Date__c=NBD;
           agg1.ChikPeaO2B__Status__c=status;
           agg1.ChikPeaO2B__Term_Period__c=termPeriod;
           agg1.ChikPeaO2B__Start_Date__c=start_date;
           
           agreelist.add(agg1);
           
           return agreelist;
       }
       
       //########### Create Shipment ###########//
       // 1.account=Account id
       // 2.order=Order id 
       // 3.shipment_date=Define Shipment Date
       // 4.shipment_status=Define shipment_status
       // 5.shipment_city=Define Shipment city
       // 6.shipment_country=Define Shipment Country
       // 7.shipment_state=Define Shipment State
       // 8.shipment_street=Define Shipment Street
       // 9.postal_code=Define Shipment Postal code
       //########### Create Shipment ###########//
       
       public static List<ChikPeaO2B__Shipment__c> createShipment(id account,id order,date shipment_date,String shipment_status,String shipment_city,String shipment_country,String shipment_state,String shipment_street,String postal_code,Integer count)
       {
          List<ChikPeaO2B__Shipment__c> shipmentlist=new List<ChikPeaO2B__Shipment__c>();
          
          for(Integer i=1;i<=count;i++)
          {
            ChikPeaO2B__Shipment__c shipment=new ChikPeaO2B__Shipment__c();
            
            shipment.ChikPeaO2B__Account__c=account; 
            shipment.ChikPeaO2B__Order__c=order;
            shipment.ChikPeaO2B__Shipment_date__c=shipment_date;
            shipment.ChikPeaO2B__Shipment_Status__c=shipment_status;
            shipment.ChikPeaO2B__Shipping_City__c=shipment_city;
            shipment.ChikPeaO2B__Shipping_Country__c=shipment_country;
            shipment.ChikPeaO2B__Shipping_State_Province__c=shipment_state;
            shipment.ChikPeaO2B__Shipping_Street__c=shipment_street;
            shipment.ChikPeaO2B__Shipping_Zip_Postal_Code__c=postal_code;
            
            shipmentlist.add(shipment);
          }
          return shipmentlist;
       }
       //########## Create Shipment Line ###########//
       // 1.order_line=Order Line id
       // 2.shipment=Define Shipment id
       // 3.status=Status of the Shipment line
       //########## Create Shipment Line ############//
       public static List<ChikPeaO2B__Shipment_Line__c> createShipmentLine(id order_line,id shipment,String status,Integer count)
       {
          List<ChikPeaO2B__Shipment_Line__c> shipmentlinelist=new List<ChikPeaO2B__Shipment_Line__c>(); 
          
          for(Integer i=1;i<=count;i++)
          {
              ChikPeaO2B__Shipment_Line__c shipmentline=new ChikPeaO2B__Shipment_Line__c();
              
              shipmentline.ChikPeaO2B__Order_Line__c=order_line;
              shipmentline.ChikPeaO2B__Shipment__c=shipment;
              shipmentline.ChikPeaO2B__Status__c=status;
              
              shipmentlinelist.add(shipmentline);
          }
          return shipmentlinelist;
        }
        //###### Create O2B Setting #########//
        // 1.bill_type={ 1->Anniversary Subscription
        //               2->Anniversary OneBill
        //               3->Calendar Month }
        // 2.usage_on_order_line_quantity=Order Line Quantity Usage is true or false
        // 3.acc_based_tiring=Account Based Tiering is true or false
        // 4.invoice_on_process_order=Invoice on process 
        // 5.agreement_override=Agreement Override is true or false
        //####### Create O2B Setting ########//
        public static ChikPeaO2B__O2B_Setting__c createSetting(String bill_type,boolean usage_on_order_line_quantity,boolean acc_based_tiring,boolean invoice_on_process_order,boolean agreement_override)
        {
           ChikPeaO2B__O2B_Setting__c setting=new ChikPeaO2B__O2B_Setting__c();
           setting.ChikPeaO2B__Billing_Type__c=bill_type;
           setting.ChikPeaO2B__Usage_on_orderline_quantity__c=usage_on_order_line_quantity;
           setting.ChikPeaO2B__Account_based_tiering__c=acc_based_tiring;
           setting.ChikPeaO2B__Create_Invoice_on_Process_Order__c=invoice_on_process_order;
           setting.ChikPeaO2B__Agreement_override_Subscription_date__c=agreement_override;
           
           return setting;
        }
        //###### Create Contact #########//
        // 1.account=Account Id
        // 2.lastname=Last Name of a person
        // 3.count=Number of Account which i create
        //####### Create Contact ########//    
        public static List<Contact> createContact(id account,String lastname,String role,Integer count)
        {
            List<Contact> cnt=new List<Contact>();
            for(Integer i=1;i<=count;i++)
            {
               Contact c=new Contact();
               c.AccountId=account;
               c.ChikPeaO2B__Role__c=role;
               c.LastName=lastname+i;
               
               cnt.add(c);
            }
            return cnt;
        }
        public static ChikPeaO2B__Bill_Group__c createBillGroup(id o2bsetting,String billing_type)
        {
            ChikPeaO2B__Bill_Group__c bill=new ChikPeaO2B__Bill_Group__c();
            bill.ChikPeaO2B__O2B_Setting__c=o2bsetting;
            bill.ChikPeaO2B__Billing_Type__c=billing_type;
            return bill;
        }
        public static List<ChikPeaO2B__Subscription__c> createSubscriptionForCalenderMonth(id acc,id item,id rateplan,List<ChikPeaO2B__Order_Line__c> orderline,date bill_start_date,Integer discount,Integer recurring_charge,Integer free_usage,Integer quantity,boolean bill_started,date bill_stop,date next_bill_date,String calender_month)
        {
        if(acc!=null)
        {
              List<ChikPeaO2B__Subscription__c> subscriptionlist=new List<ChikPeaO2B__Subscription__c>();
              for(Integer i=0;i<orderline.size();i++)
              {
              ChikPeaO2B__Subscription__c subscription=new ChikPeaO2B__Subscription__c();
              subscription.ChikPeaO2B__Account__c=acc;
              subscription.ChikPeaO2B__Item__c=item;
              subscription.ChikPeaO2B__Rate_Plan__c=rateplan;
              subscription.ChikPeaO2B__Order_Line__c=orderline.get(i).id;
              subscription.ChikPeaO2B__Billing_Start_Date__c=bill_start_date;
              subscription.ChikPeaO2B__Discount__c=discount;
              subscription.ChikPeaO2B__Recurring_Charge__c=recurring_charge;
              subscription.ChikPeaO2B__Free_usage__c=free_usage;
              subscription.ChikPeaO2B__Quantity__c=quantity;
              subscription.ChikPeaO2B__Billing_Started__c=bill_started;
              subscription.ChikPeaO2B__Billing_Stop_Date__c=bill_stop;
              subscription.ChikPeaO2B__Next_Bill_Date__c=next_bill_date;
              subscription.ChikPeaO2B__Calendar_month__c=calender_month;
              
                                
              subscriptionlist.add(subscription);
              }
              return subscriptionlist;
         }
         else
         {
              return null;
         }
      }   
}