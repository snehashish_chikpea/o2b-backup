global class MonthlyAggregate implements Calculation {
    public decimal calculate(List<Usage_Staging__c>ustg){
        decimal returnval=0;
        for(Usage_Staging__c us : ustg){
            returnval+= us.ChikPeaO2B__Usage_unit__c != null ? us.ChikPeaO2B__Usage_unit__c : 0.00;
        }
        return returnval;
    }
}