/*
** Class            :  InitiateClearBatchLogBatch
** Created by       :  Snehashish Roy (Chikpea Inc.)
** Last Modified    :  23 February 2016 (Created)
** Description      :  Calls the ClearBatchLogBatch class
*/
global with sharing class InitiateClearBatchLogBatch
{ 
   webservice static string run_batch(integer batsize) 
    {
        try
        {
            ClearBatchLogBatch btcdata=new ClearBatchLogBatch();
            ID BatchId = Database.executeBatch(btcdata,batsize);
        }
        catch(exception e)
        {
            return e.getmessage();
        }
        return 'Batch Initiated.';
    }
}