/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-05-05
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
global class TestPG_AuthorizeNetGetTrxDetailsMock implements WebServiceMock {
  
  global void doInvoke(Object stub,
                 Object request,
                 Map<String, Object> response,
                 String endpoint,
                 String soapAction,
                 String requestName,
                 String responseNS,
                 String responseName,
                 String responseType
  ){
        PG_AuthNetSOAPService.MessagesTypeMessage messagesTypeMessage = 
            new PG_AuthNetSOAPService.MessagesTypeMessage();            
        messagesTypeMessage.code = 'I00001';  
        messagesTypeMessage.text = 'Successful.';
        
        PG_AuthNetSOAPService.ArrayOfMessagesTypeMessage arrayOfMessagesTypeMessage =
            new PG_AuthNetSOAPService.ArrayOfMessagesTypeMessage();
        arrayOfMessagesTypeMessage.MessagesTypeMessage = new List<PG_AuthNetSOAPService.MessagesTypeMessage>{
            messagesTypeMessage
        };  
        /*
        PG_AuthNetSOAPService.FDSFilterType fDSFilterType = 
            new PG_AuthNetSOAPService.FDSFilterType();
        fDSFilterType.action = 'action1';
        fDSFilterType.name = 'name1';
        
        PG_AuthNetSOAPService.ArrayOfFDSFilterType arrayOfFDSFilterType = 
            new PG_AuthNetSOAPService.ArrayOfFDSFilterType();
        arrayOfFDSFilterType.FDSFilter = new List<PG_AuthNetSOAPService.FDSFilterType>{
            fDSFilterType
        }; 
        
        PG_AuthNetSOAPService.BatchStatisticType batchStatisticType = 
            new  PG_AuthNetSOAPService.BatchStatisticType();
        batchStatisticType.accountType = 'accountType1';
        batchStatisticType.chargeAmount = 1.0;
        batchStatisticType.chargebackAmount = 2.0;
        batchStatisticType.chargebackAmountSpecified = true;
        batchStatisticType.chargebackCount = 1;
        batchStatisticType.chargebackCountSpecified = true;
        batchStatisticType.chargeChargeBackAmount = 1.0;
        batchStatisticType.chargeChargeBackAmountSpecified = true;
        batchStatisticType.chargeChargeBackCount = 1;
        batchStatisticType.chargeChargeBackCountSpecified = true;
        batchStatisticType.chargeCount = 1;
        batchStatisticType.chargeReturnedItemsAmount = 1.0;
        batchStatisticType.chargeReturnedItemsAmountSpecified = true;
        batchStatisticType.chargeReturnedItemsCount = 1;
        batchStatisticType.chargeReturnedItemsCountSpecified = true;
        batchStatisticType.correctionNoticeCount = 1;
        batchStatisticType.correctionNoticeCountSpecified = true;
        batchStatisticType.currencyCode = 'USD';
        batchStatisticType.declineCount = 1;
        batchStatisticType.errorCount = 2;
        batchStatisticType.refundAmount = 2.0;
        batchStatisticType.refundChargeBackAmount = 1.0;
        batchStatisticType.refundChargeBackAmountSpecified = true;
        batchStatisticType.refundChargeBackCount = 1;
        batchStatisticType.refundChargeBackCountSpecified = true;
        batchStatisticType.refundCount = 1;
        batchStatisticType.refundReturnedItemsAmount = 1.0;
        batchStatisticType.refundReturnedItemsAmountSpecified = true;
        batchStatisticType.refundReturnedItemsCount = 1;
        batchStatisticType.refundReturnedItemsCountSpecified = true;
        batchStatisticType.returnedItemAmount = 1.0;
        batchStatisticType.returnedItemAmountSpecified = true;
        batchStatisticType.returnedItemCount = 1;
        batchStatisticType.returnedItemCountSpecified = true;
        batchStatisticType.voidCount = 1;
            
        PG_AuthNetSOAPService.ArrayOfBatchStatisticType arrayOfBatchStatisticType =
            new PG_AuthNetSOAPService.ArrayOfBatchStatisticType();
        arrayOfBatchStatisticType.statistic = new List<PG_AuthNetSOAPService.BatchStatisticType>{
            batchStatisticType
        };
        */
        PG_AuthNetSOAPService.BatchDetailsType batchDetailsType = 
            new PG_AuthNetSOAPService.BatchDetailsType();
        batchDetailsType.batchId = '3348069';
        //batchDetailsType.paymentMethod = 'paymentMethod1';
        batchDetailsType.settlementState = 'settledSuccessfully';
        batchDetailsType.settlementTimeLocal = System.now();
        batchDetailsType.settlementTimeUTC = System.now();
        //batchDetailsType.statistics = arrayOfBatchStatisticType;
        
        PG_AuthNetSOAPService.OrderExType orderExType = new PG_AuthNetSOAPService.OrderExType();
        //orderExType.description = 'description1'; 
        orderExType.invoiceNumber = 'I-0000000113'; 
        //orderExType.purchaseOrderNumber = 'purchaseOrderNumber1'; 
        /*
        PG_AuthNetSOAPService.ExtendedAmountType extendedAmountType =
            new PG_AuthNetSOAPService.ExtendedAmountType();
        extendedAmountType.amount = 1.0; 
        extendedAmountType.description = 'description1'; 
        extendedAmountType.name = 'name1'; 
        
        PG_AuthNetSOAPService.LineItemType lineItemType = 
            new PG_AuthNetSOAPService.LineItemType();
        lineItemType.description = 'description1';
        lineItemType.itemId = 'itemId1';
        lineItemType.name = 'name1';
        lineItemType.quantity = 2.0;
        lineItemType.taxable = false;
        lineItemType.unitPrice = 3.0;
        */
        PG_AuthNetSOAPService.CreditCardMaskedType creditCardMaskedType =
            new PG_AuthNetSOAPService.CreditCardMaskedType();
        creditCardMaskedType.cardNumber = 'XXXX8764';
        creditCardMaskedType.cardType = 'Visa';
        creditCardMaskedType.expirationDate = 'XXXX';
        
        PG_AuthNetSOAPService.BankAccountMaskedType bankAccountMaskedType =
            new PG_AuthNetSOAPService.BankAccountMaskedType();
        bankAccountMaskedType.accountNumber = 'accountNumber1';
        bankAccountMaskedType.accountType = 'accountType1';
        bankAccountMaskedType.bankName = 'bankName1';
        bankAccountMaskedType.echeckType = 'echeckType1';
        bankAccountMaskedType.nameOnAccount = 'nameOnAccount1';
        bankAccountMaskedType.routingNumber = 'routingNumber1';
        
        PG_AuthNetSOAPService.PaymentMaskedType paymentMaskedType =
            new PG_AuthNetSOAPService.PaymentMaskedType();
        paymentMaskedType.creditCard = creditCardMaskedType;
        paymentMaskedType.bankAccount = bankAccountMaskedType;
        /*
        PG_AuthNetSOAPService.DriversLicenseType driversLicenseType =
            new PG_AuthNetSOAPService.DriversLicenseType();  
        driversLicenseType.dateOfBirth = '1980-05-05'; 
        driversLicenseType.number_x = 'number1';
        */
        PG_AuthNetSOAPService.CustomerDataType customerDataType =
            new PG_AuthNetSOAPService.CustomerDataType();
        //customerDataType.driversLicense = driversLicenseType;
        //customerDataType.email = 'email1';
        customerDataType.id = 'AB Corp';
        //customerDataType.taxId = 'taxId1';
        //customerDataType.type_x = 'type1';
        /*  
        PG_AuthNetSOAPService.CustomerAddressType customerAddressType = 
            new PG_AuthNetSOAPService.CustomerAddressType();
        customerAddressType.faxNumber = 'faxNumber1';
        customerAddressType.phoneNumber = 'phoneNumber1';
        
        PG_AuthNetSOAPService.NameAndAddressType nameAndAddressType =
            new PG_AuthNetSOAPService.NameAndAddressType();
        nameAndAddressType.address = 'address1';
        nameAndAddressType.city = 'city1';
        nameAndAddressType.company = 'company1';
        nameAndAddressType.country = 'country1';
        nameAndAddressType.firstName = 'firstName1';
        nameAndAddressType.lastName = 'lastName1';
        nameAndAddressType.state = 'state1';
        nameAndAddressType.zip = 'zip1';
        
        PG_AuthNetSOAPService.subscriptionPaymentType subscriptionPaymentType =
            new PG_AuthNetSOAPService.subscriptionPaymentType();
        subscriptionPaymentType.id = 1;
        subscriptionPaymentType.payNum = 2;
        */
        PG_AuthNetSOAPService.TransactionDetailsType transactionDetailsType =
            new PG_AuthNetSOAPService.TransactionDetailsType();
        transactionDetailsType.authAmount = 200.00;
        transactionDetailsType.authCode = 'OCZJE0';
        transactionDetailsType.AVSResponse = 'Y';
        transactionDetailsType.batch = batchDetailsType;
        //transactionDetailsType.billTo = customerAddressType;
        //transactionDetailsType.cardCodeResponse = 'cardCodeResponse1';
        //transactionDetailsType.CAVVResponse = 'CAVVResponse1';
        //transactionDetailsType.currencyCode = 'currencyCode1';
        transactionDetailsType.customer = customerDataType;
        //transactionDetailsType.customerIP = 'customerIP1';
        //transactionDetailsType.duty = extendedAmountType;
        //transactionDetailsType.FDSFilterAction = 'FDSFilterAction1';
        //transactionDetailsType.FDSFilters = arrayOfFDSFilterType;
        //transactionDetailsType.lineItems = new List<PG_AuthNetSOAPService.LineItemType>{lineItemType};
        transactionDetailsType.marketType = 'eCommerce';
        transactionDetailsType.order = orderExType;
        transactionDetailsType.payment = paymentMaskedType;
        //transactionDetailsType.prepaidBalanceRemaining = 1.0;
        transactionDetailsType.product = 'Card Not Present';
        transactionDetailsType.recurringBilling = false;
        //transactionDetailsType.refTransId = 'refTransId1';
        //transactionDetailsType.requestedAmount = 1.0;
        //transactionDetailsType.responseCode = 1;
        transactionDetailsType.responseReasonCode = 1;
        transactionDetailsType.responseReasonDescription = 'Approval';
        transactionDetailsType.settleAmount = 200.00;
        //transactionDetailsType.shipping = extendedAmountType;
        //transactionDetailsType.shipTo = nameAndAddressType;
        //transactionDetailsType.splitTenderId = 12345;
        transactionDetailsType.submitTimeLocal = System.now();
        transactionDetailsType.submitTimeUTC = System.now();
        //transactionDetailsType.subscription = subscriptionPaymentType;
        //transactionDetailsType.tax = extendedAmountType;
        transactionDetailsType.taxExempt = false;
        transactionDetailsType.transactionStatus = 'settledSuccessfully';
        transactionDetailsType.transactionType = 'authCaptureTransaction';
        transactionDetailsType.transId = '2211101871';
        
        PG_AuthNetSOAPService.GetTransactionDetailsResponseType getTransactionDetailsResponseType   =
            new PG_AuthNetSOAPService.GetTransactionDetailsResponseType();
        getTransactionDetailsResponseType.resultCode = 'Ok'; 
        getTransactionDetailsResponseType.messages = arrayOfMessagesTypeMessage;
        getTransactionDetailsResponseType.transaction_x = transactionDetailsType;
        
        PG_AuthNetSOAPService.GetTransactionDetailsResponse_element getTransactionDetailsResponse_element =
            new PG_AuthNetSOAPService.GetTransactionDetailsResponse_element();
        getTransactionDetailsResponse_element.GetTransactionDetailsResult = getTransactionDetailsResponseType;
        
        response.put('response_x', getTransactionDetailsResponse_element); 
    }
  
}