/**
----------------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-19
@see 
----------------------------------------------------------------------------------------------------------
*/
global with sharing class PG_PaymentProcessor {

    PG_PaymentGateway paymentGateway;
    
    global PG_PaymentProcessor(PG_PaymentGateway paymentGateway){
        this.paymentGateway = paymentGateway;
    }
    
    global void process(PG.TransactionType transactionType){
        this.paymentGateway.processTransaction(transactionType);        
    }
    
    global PG_Response fetchResponse(){
        return this.paymentGateway.getPGResponse();
    }   

}