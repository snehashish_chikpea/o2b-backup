/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-05
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
//Refund response
global class TestPG_WSOribitalRefundMock implements WebServiceMock {
  
  global void doInvoke(Object stub,
                 Object request,
                 Map<String, Object> response,
                 String endpoint,
                 String soapAction,
                 String requestName,
                 String responseNS,
                 String responseName,
                 String responseType
  ){
       PG_WSOribitalElements.NewOrderResponse_element respElement =  
           new PG_WSOribitalElements.NewOrderResponse_element();
    
      PG_WSOribitalElements.NewOrderResponseElement newOrderRespElement =
        new PG_WSOribitalElements.NewOrderResponseElement();
        
      newOrderRespElement.industryType = 'MO';
      newOrderRespElement.transType = 'FR';
      newOrderRespElement.bin = '000001';
      newOrderRespElement.merchantID = '206734';
      newOrderRespElement.terminalID = '001';
      newOrderRespElement.cardBrand = 'VI';
      newOrderRespElement.orderID = 'ON-0002';
      newOrderRespElement.txRefNum = '526901437CF8E3A386FE319BE80A699E395B542A';
      newOrderRespElement.txRefIdx = '1';
      newOrderRespElement.respDateTime = '20131024071515';
      newOrderRespElement.procStatus = '0';
      newOrderRespElement.approvalStatus = '1';
      newOrderRespElement.respCode = '00';
      newOrderRespElement.avsRespCode = '';
      newOrderRespElement.cvvRespCode = '';
      newOrderRespElement.authorizationCode = '';
      newOrderRespElement.mcRecurringAdvCode = '';
      newOrderRespElement.visaVbVRespCode = '';
      newOrderRespElement.procStatusMessage = '';
      newOrderRespElement.respCodeMessage = '';
      newOrderRespElement.hostRespCode = '';
      newOrderRespElement.hostAVSRespCode = '';
      newOrderRespElement.hostCVVRespCode = '';
      newOrderRespElement.retryTrace = '';
      newOrderRespElement.retryAttempCount = '';
      newOrderRespElement.lastRetryDate = '';
      newOrderRespElement.customerRefNum = '';
      newOrderRespElement.customerName = '';
      newOrderRespElement.profileProcStatus = '';
      newOrderRespElement.profileProcStatusMsg = '';
        
      respElement.return_x = newOrderRespElement;
           
           response.put('response_x', respElement); 
     }
  
}