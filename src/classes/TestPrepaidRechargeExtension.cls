@isTest
public class TestPrepaidRechargeExtension
{
    @isTest
    static void testMethod1()
    {
         Account acc=new Account(name='acc1');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        insert(pre);
        ChikPeaO2B__Rate_Plan__c rp=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        insert(rp);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id);
        insert(ord);
        ChikPeaO2B__Order_Line__c orl=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp.id,ChikPeaO2B__Unit_Price__c=2.5);
        insert(orl);
        ChikPeaO2B__Usage__c usg=new ChikPeaO2B__Usage__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Order_Line__c=orl.id,ChikPeaO2B__Rate__c=1.5,ChikPeaO2B__Rate_Plan__c=rp.id,ChikPeaO2B__Remaining_Usage__c=100,ChikPeaO2B__Total_Usage__c=10,ChikPeaO2B__Type__c='Prepaid');
        insert(usg);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(usg);
        PrepaidRechargeExtension obj=new PrepaidRechargeExtension(stdc);
        PageReference pageRef = Page.PrepaidRecharge;
        Test.setCurrentPage(pageRef);
        obj.getrechargeOptions();
        obj.selectedoption='Recharge by unit';
        obj.currency_unit=20;
        obj.recharge();
        obj.selectedoption='Recharge by currency';
        obj.recharge();
    }
}