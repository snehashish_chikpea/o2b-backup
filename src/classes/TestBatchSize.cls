@isTest
public class TestBatchSize
{
    @isTest
    static void testMethod1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Billing_Type__c='Anniversary OneBill');
        insert(o2bset);
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly',ChikPeaO2B__Active__c='Yes',ChikPeaO2B__Is_Billing__c=true);
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        List<ChikPeaO2B__Subscription__c>SubList=new List<ChikPeaO2B__Subscription__c>();
        ChikPeaO2B__Subscription__c sub9_1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=null,ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9_1);
        ChikPeaO2B__Subscription__c sub9_2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=acc.ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=acc.ChikPeaO2B__Next_Bill_Date__c.adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9_2);
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1);
        ChikPeaO2B__Subscription__c sub1_1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-25),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1_1);
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-5),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub2);
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(15),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub3);
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(45),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub4);
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub5);
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(5),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub6);
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-3),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub7);
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(70),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub8);
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9);
        insert(SubList);
        List<id>AccList=new List<id>();
        AccList.add(acc.id);
        BatchSize.CalculateBatchSize();
    }
}