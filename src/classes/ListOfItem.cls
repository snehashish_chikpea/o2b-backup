/*
** Class         :  ListOfItem
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  12 February 2016
** Modified by   :  Snehashish Roy (Chikpea Inc.)
** Reason        :  New Variable added for deselecting items in Package
** Description   :   
*/


public with sharing class ListOfItem{
    public map<string,List<Item>> itemMap{get;set;}
    
    public ListOfItem()
    {
        itemMap=new map<string,List<Item>>();
    }
    public class Item{
        public Item__c itm{get;set;}
        public string imageid{get;set;}
        public boolean selected{get;set;}
        public boolean ordered{get;set;}
        public Map<id,boolean> selectedRP{get;set;}
        public Map<id,boolean> orderedRP{get;set;}
        public Map<id,boolean> disabledRP{get;set;}
        public integer qty{get;set;}
        public map<id,decimal> SelectedPrice{get;set;}
        public decimal TotalPrice{get;set;}
        public Item(Item__c itm)
        {
            this.itm=itm;
            imageid=(itm.attachments!=null && itm.attachments.size()>0?itm.attachments[0].id:null);
            selected=false;
            ordered=false;
            qty=1;
            TotalPrice=0;
            selectedRP=new Map<id,boolean>();
            orderedRP=new Map<id,boolean>();
            disabledRP=new Map<id,boolean>();
            SelectedPrice=new Map<id,decimal>();
            for(Rate_Plan__c rp:itm.Rate_Plans__r)
            {
                selectedRP.put(rp.id,false);
                orderedRP.put(rp.id,false);
                disabledRP.put(rp.id,false);
                SelectedPrice.put(rp.id,0);
            }
        }
    }
    public map<string,List<Item>> getItems()
    {
        for(Item__c itm:[SELECT id,name,Active__c,Item_Type__c,Category__c,Description__c,Is_Shipping__c,Is_package_Item__c,(SELECT id,name,Non_Recurring_Charge__c,Recurring_Charge__c,Usage_Rate__c,Pricing_Type__c,Bill_Cycle__c  from Rate_Plans__r) from Item__c where Active__c=true order by Item_Type__c,name])
        {
            if(itm.Rate_Plans__r!=null && itm.Rate_Plans__r.size()>0)
            {
                if(!itemMap.containskey(itm.Item_Type__c))
                {
                    List<Item> tempList = new List<Item>();
                    tempList.add(new Item(itm)); 
                    itemMap.put(itm.Item_Type__c,tempList);
                }
                else
                {
                    List<Item> tempList = itemMap.get(itm.Item_Type__c);
                    tempList.add(new Item(itm)); 
                    itemMap.put(itm.Item_Type__c,tempList);    
                }
            }
        }
        Schema.DescribeFieldResult fieldResult = Item__c.Item_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry a : ple )
        { 
            if(!itemMap.containskey(a.getValue()))
            {
                List<Item> tempList = new List<Item>();    
                itemMap.put(a.getValue(),tempList); 
            }
        }
        return itemMap;
    }
    public class o2b2Package
    {
        public Package__c pkg{get;set;}
        public boolean selected{get;set;}
        public boolean ordered{get;set;}
        public integer pkgqty{get;set;}
        public decimal pkgnonrecprice{get;set;}
        public decimal pkgrecprice{get;set;}
        public List<Item__c>ItemList{get;set;}
        public map<string,integer>itemqty{get;set;}
        public map<string,decimal>selectedRPprice{get;set;}
        public map<string,rate_plan__c>rpmap{get;set;}
        public Map<id,boolean> selectedpkgitm{get;set;}//New Variable to deselect Items in Package
        public boolean billonpkg{get;set;}//New Variable used for Customizable Rate Plans
        
        public o2b2Package(Package__c pkg,List<Item__c>ItemList,map<string,rate_plan__c>rpmap,map<string,integer>itemqty,map<string,decimal>selectedRPprice,map<id,boolean>pkgidselmap)
        {
            this.pkg=pkg;
            pkgqty=1;
            pkgnonrecprice=pkg.Bundle_Non_Recurring_Price__c;
            pkgrecprice=pkg.Bundle_Recurring_Price__c;
            this.billonpkg=pkg.ChikPeaO2B__Bill_On_Package__c;
            this.ItemList=ItemList;
            this.itemqty=itemqty;
            this.selectedRPprice=selectedRPprice;
            this.rpmap=rpmap;
            selected=pkgidselmap.get(pkg.id);
            ordered=pkgidselmap.get(pkg.id);
            selectedpkgitm=new map<id,boolean>();
            for(ChikPeaO2B__Item__c itm:ItemList)
            {
                selectedpkgitm.put(itm.id,true);
            }
        }
    }
}