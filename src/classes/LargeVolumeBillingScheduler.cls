/*
** Class         :  LargeVolumeBillingScheduler
** Created by    :  Asit (chikpea Inc.)
** Created date  :  22/08/2013
** Description   : 
*/

global with sharing class LargeVolumeBillingScheduler implements Schedulable{
	global void execute(SchedulableContext sc){
        CronTrigger ct=[SELECT Id,CronJobDetail.Name FROM CronTrigger WHERE Id=:sc.getTriggerId()];
        string grpName=ct.CronJobDetail.Name.remove('Create Large Volume Invoice Batch ');
        grpName=grpName.trim();
        list<ChikPeaO2B__Bill_Group__c> billGrpLst=[SELECT id,ChikPeaO2B__Bill_Schedule_Job_Id__c,
        ChikPeaO2B__Bulk_Data__c,ChikPeaO2B__Maximum_Records__c 
        FROM ChikPeaO2B__Bill_Group__c WHERE Name=:grpName];
        string billGroupId='';
        string query='select id,name,(select id from Subscriptions__r) from Account where ChikPeaO2B__Active__c=\'Yes\' and ChikPeaO2B__Is_Billing__c=true';
        if(!billGrpLst.isEmpty()){
        	billGroupId=billGrpLst[0].Id;
        }
        if(billGroupId!=null
        && billGroupId!=''){
        	query+=' AND ChikPeaO2B__Bill_Group__c=\''+billGroupId +'\'';
        }
        system.debug('----------------->'+query);
        list<Account> AccList=database.query(query);
        
        Integer totalSubSize=0;
    	list<Id> AccIDList= new list<Id>();
    	for(Account acc : AccList){
    		AccIDList.add(acc.Id);
    		totalSubSize=totalSubSize+(acc.Subscriptions__r!=null? acc.Subscriptions__r.size() : 0);
    	}
    	if(totalSubSize<=billGrpLst.get(0).ChikPeaO2B__Maximum_Records__c){
    		CalenderMonthBill.generateCalenderMonthBill(AccIDList, true);
    	}
        
    }

}