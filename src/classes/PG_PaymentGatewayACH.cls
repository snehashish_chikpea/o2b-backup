/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-20
@see 
---------------------------------------------------------------------------------------------------
*/
global interface PG_PaymentGatewayACH {   
 
    void initACHSubmission(PG_BADetails details, 
        PG_Config config, 
        Map<String, String> transactionInfo
    );
     
    void submitTransactionACH(PG.TransactionTypeACH transactionType);
    
    PG_Response getPGResponse();
    
}