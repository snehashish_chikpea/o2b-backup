public with sharing class PG_VantivXmlParser 
{
	
	
	public  Map<String,String> processXML(String xmlString)
    { 
       Map<String,String> vantiv_reponse_map=new Map<String,String>();
        
	     Integer tmp = 0; 
	     String responseText=null;
         try 
         { 
             if(xmlString != null) 
             { 
             	if(xmlString.contains('<HTML>') && xmlString.contains('</HTML>'))
             	{
             	   System.debug('Transaction Failed');
                   String message=xmlString.subString((xmlString.indexOf('<BODY>')+6)).remove('<H3>').remove('</H3>').remove('<BR></BODY></HTML>');
                   //System.debug('res===>'+message);
                  
                   vantiv_reponse_map.put('error_message',message);
                   vantiv_reponse_map.put('process','N');
                   System.debug('vantiv_reponse_map==>'+vantiv_reponse_map);
             	}
             	else
             	{ 
                   XmlStreamReader reader = new XmlStreamReader(xmlString);
                   System.debug('nextTag()---'+reader.nextTag());
                   while(reader.hasNext()) 
                   {
                          tmp=tmp+1;
                          if (reader.getEventType() == XmlTag.START_ELEMENT) 
                          { 
                               responseText = parseXML(reader); 
                               System.debug('getLocalName()-->>'+reader.getLocalName()+'-->>'+responseText);
                               vantiv_reponse_map.put(reader.getLocalName(),responseText);
                          }
                          else
                          {
                               System.debug('Else part of processXML');
                          } 
                          reader.next(); 
                   } 
                   System.debug('Number of times it gone to while loop-->>'+tmp);
                   if(tmp==1)
                   {
                        System.debug('Transaction Failed');
                        String message=xmlString.subString((xmlString.indexOf('message')+9),(xmlString.indexOf('xmlns')-2));
                        vantiv_reponse_map.clear();
                        vantiv_reponse_map.put('error_message',message);
                        vantiv_reponse_map.put('process','N');
                   }
                   else if(tmp>1)
                   {
              	        System.debug('Transaction Processed');
              	        vantiv_reponse_map.put('process','Y');
                   }
                }
             } 
         }
         catch(XmlException xe)
         { 
              System.debug('XMLException ---->>'+xe);
         } 
         Catch(Exception e)
         {
              System.debug('Exception ---->>'+e);
            
         }
         System.debug('vantiv_reponse_map---->>>>'+vantiv_reponse_map);
           return vantiv_reponse_map;
     } 

    public static String parseXML(XmlStreamReader reader)
    { 

         String data = ''; 
         while(reader.hasNext()) 
         { 
               if (reader.getEventType() == XmlTag.END_ELEMENT) 
               { 
                   System.debug('---END_ELEMENT');
        	       break; 
               } 
               else if (reader.getEventType() == XmlTag.CHARACTERS) 
               { 
        	       System.debug('---CHARACTERS');
                   data = reader.getText(); 
               } 
               reader.next(); 
         } 
         return data; 
    }
	

}