/*
** Class         :  InvoicePdfCOnForExternalVF
** Created by    :  Asit (chikpea Inc.)
** Last Modified :  04 march 14
** Description   :  Expose Invoice & invoice line 
*/


global with sharing class InvoicePdfCOnForExternalVF {
	
	//[Class level variable declaration]
	
	public string invId;
	public List<Schema.SObjectField> fldObjMapValuesINV =
	schema.SObjectType.ChikPeaO2B__Invoice__c.fields.getMap().values();
	
	public List<Schema.SObjectField> fldObjMapValuesINVLines =
	schema.SObjectType.ChikPeaO2B__Invoice_Line__c.fields.getMap().values();
	
	// [Variable declaration END]
	global ChikPeaO2B__Invoice__c getinvoice(){
		//invId=Apexpages.currentPage().getParameters().get('id');
		list<ChikPeaO2B__Invoice__c> retInv= new list<ChikPeaO2B__Invoice__c>();
		string relatedList='( SELECT ';
		for(Schema.SObjectField s : fldObjMapValuesINVLines){
			relatedList+= s.getDescribe().getName() + ' , ';
		}
		relatedList = relatedList.subString(0, relatedList.length() - 2);
		relatedList+='  FROM ChikPeaO2B__Invoice_Lines__r ) ';
		
		string mainQuery='  SELECT  ';
		for(Schema.SObjectField s : fldObjMapValuesINV){
			mainQuery+= s.getDescribe().getName() + ' , ';
		}
		string lastPart=' FROM ChikPeaO2B__Invoice__c WHERE Id= \''+ invId + '\' ';
		system.debug('----------->'+mainQuery + relatedList + lastPart);
		try{
			retInv=database.query(mainQuery + relatedList + lastPart);
		}catch(Exception e){system.debug('===>37==>'+e.getMessage());}
		if(!retInv.isEmpty()){
			return retInv[0];
		}
		
		return new ChikPeaO2B__Invoice__c();
	}
	//variables with getter setter
	
	//[get set varialbe  END]
	
	/* *************
		CONTROLLERS
		************
	*/
	//Controller
	global InvoicePdfCOnForExternalVF(){
		invId=Apexpages.currentPage().getParameters().get('id');
	}
	
	//Controller for extensions
	global InvoicePdfCOnForExternalVF(ApexPages.StandardController stdController){
		invId=stdController.getId();
	}
	//[END OF CONTROLLER SECTION]
}