/*
** Class         :  TabPageController
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  28 Apr 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

public with sharing class TabPageController{
    public Map<String,String>KeyprefixMap{get;set;}
    public list<String> reports{get;set;}
    public String url1{get;set;}
    public String url2{get;set;}
    public String url3{get;set;}
    Map<String, Schema.SObjectType> gd=Schema.getGlobalDescribe();
    Schema.DescribeSObjectResult obj;
    
    public TabPageController(){
        reports=new list<String>();
        list<ChikPeaO2B__O2B_Setting__c>o2bset=[select id,name,ChikPeaO2B__Repoert_URL1__c,ChikPeaO2B__Report_URL2__c,ChikPeaO2B__Report_URL3__c from ChikPeaO2B__O2B_Setting__c limit 1];
        if(o2bset.size()>0 && o2bset!=null){ 
         url1='';
         url1=o2bset[0].ChikPeaO2B__Repoert_URL1__c;
         url2='';
         url2=o2bset[0].ChikPeaO2B__Report_URL2__c;
         url3='';
         url3=o2bset[0].ChikPeaO2B__Report_URL3__c ;
      }
        
        
        KeyprefixMap=new Map<string,string>();
        obj=  gd.get('ChikPeaO2B__Price_Book__c').getDescribe();
        KeyprefixMap.put('Price_Book__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Rate_Plan__c').getDescribe();
        KeyprefixMap.put('Rate_Plan__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Package__c').getDescribe();
        KeyprefixMap.put('Package__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__quote__c').getDescribe();
        KeyprefixMap.put('quote__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__order__c').getDescribe();
        KeyprefixMap.put('order__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Subscription__c').getDescribe();
        KeyprefixMap.put('Subscription__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Purchase__c').getDescribe();
        KeyprefixMap.put('Purchase__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__usage__c').getDescribe();
        KeyprefixMap.put('usage__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__invoice__c').getDescribe();
        KeyprefixMap.put('invoice__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Shipment__c').getDescribe();
        KeyprefixMap.put('Shipment__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Credit_Card__c').getDescribe();
        KeyprefixMap.put('Credit_Card__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Bundled_Item__c').getDescribe();
        KeyprefixMap.put('Bundled_Item__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Agreement__c').getDescribe();
        KeyprefixMap.put('Agreement__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Gateway__c').getDescribe();
        KeyprefixMap.put('Gateway__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__O2B_Setting__c').getDescribe();
        //KeyprefixMap.put('O2B_Setting__c','/'+obj.getKeyPrefix());
        string val='';
        if(o2bset!=null && o2bset.size()>0)
            val=o2bset[0].id;
        else
            val=obj.getKeyPrefix();
        KeyprefixMap.put('O2B_Setting__c','/'+val);
        obj =  gd.get('ChikPeaO2B__Gateway_Response__c').getDescribe();
        KeyprefixMap.put('Gateway_Response__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Credit_Card__c').getDescribe();
        KeyprefixMap.put('Credit_Card__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Credit_Note__c').getDescribe();
        KeyprefixMap.put('Credit_Note__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Payment__c').getDescribe();
        KeyprefixMap.put('Payment__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Advance_payment__c').getDescribe();
        KeyprefixMap.put('Advance_payment__c','/'+obj.getKeyPrefix());
        obj =  gd.get('ChikPeaO2B__Refund__c').getDescribe();
        KeyprefixMap.put('Refund__c','/'+obj.getKeyPrefix());
        obj=  gd.get('ChikPeaO2B__Bank_Account__c').getDescribe();
        KeyprefixMap.put('Bank_Account__c','/'+obj.getKeyPrefix());
        obj=  gd.get('ChikPeaO2B__BatchLog__c').getDescribe();
        KeyprefixMap.put('BatchLog__c','/'+obj.getKeyPrefix());
        obj=  gd.get('ChikPeaO2B__Usage_History__c').getDescribe();
        KeyprefixMap.put('Usage_History__c','/'+obj.getKeyPrefix());
        obj=  gd.get('ChikPeaO2B__Dunning_Rule__c').getDescribe();
        KeyprefixMap.put('Dunning_Rule__c','/'+obj.getKeyPrefix());
        obj=  gd.get('ChikPeaO2B__Payment_Staging__c').getDescribe();
        KeyprefixMap.put('Payment_Staging__c','/'+obj.getKeyPrefix());
    }
}