/*
** Class         :  PaymentInsertForBatch
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

public with sharing class PaymentInsertForBatch{
    public static ChikPeaO2B__O2B_Setting__c o2bsetting = [select ChikPeaO2B__Default_Gateway_For_ACH__c,ChikPeaO2B__Default_Gateway_For_ACH__r.Name,ChikPeaO2B__Use_Multi_Gateway__c from ChikPeaO2B__O2B_Setting__c limit 1 ];
    public static void insertpayment(List<id>returnInvIds,boolean linepayment,string gateway)
    {
        List<ChikPeaO2B__Gateway__c>gatewaylist=[SELECT id,name,ChikPeaO2B__Gateway_Type__c from ChikPeaO2B__Gateway__c where name=:gateway limit 1];
        List<ChikPeaO2B__Payment__c> paylist= new List<ChikPeaO2B__Payment__c>();
        if(linepayment)
        {
            Map<Id, ChikPeao2b__Invoice__c> invMap = new Map<Id, ChikPeao2b__Invoice__c>([SELECT Id,  ChikPeao2b__Account__c,ChikPeao2b__Amount_Due__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Credit_Card_Number__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Month__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Year__c,ChikPeaO2B__Due_Date__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Gateway__r.name,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Gateway__c  FROM ChikPeao2b__Invoice__c WHERE ChikPeao2b__Amount_Due__c > 0 
            and ID in :returnInvIds and ChikPeao2b__Account__r.ChikPeao2b__Auto_Payment__c = 'Yes']);
            Map<Id, ChikPeaO2B__Invoice_Line__c> lineMap = new Map<Id, ChikPeaO2B__Invoice_Line__c>([Select Id,ChikPeaO2B__Line_Type__c,ChikPeaO2B__Line_Rate__c,ChikPeaO2B__Subscription__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Credit_Card_Number__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Expiry_Month__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Expiry_Year__c,Chikpeao2b__Invoice__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__r.name,ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__c FROM ChikPeaO2B__Invoice_Line__c WHERE ChikPeaO2B__Invoice__c in :returnInvIds and ChikPeaO2B__Invoice__r.ChikPeao2b__Account__r.ChikPeao2b__Auto_Payment__c = 'Yes']); 
            set<ID> subsIds = new set<ID>();
            for(ChikPeaO2B__Invoice_Line__c invoiceLine : lineMap.values()){
                subsIds.add(invoiceLine.ChikPeaO2B__Subscription__c);    
            }
            Map<Id,ChikPeaO2B__Subscription__c> SubsMap = new Map<id,ChikPeaO2B__Subscription__c>([Select Id, ChikPeaO2B__Auto_Payment_Method__c,chikpeao2b__bank_account__c,chikpeao2b__bank_account__r.ChikPeaO2B__Gateway__c,chikpeao2b__bank_account__r.ChikPeaO2B__Gateway__r.name from ChikPeaO2B__Subscription__c where id in : subsIds]);
            for(ChikPeaO2B__Invoice_Line__c invoiceLine : lineMap.values())
            {
                ChikPeaO2B__Invoice__c inv = invMap.get(invoiceLine.Chikpeao2b__Invoice__c);
                Double lineRate = lineMap.get(invoiceLine.Id).Chikpeao2b__Line_Rate__c;
                if (inv!= null && lineRate > 0)
                {
                    ChikPeaO2B__Payment__c payment=new ChikPeaO2B__Payment__c();
                    payment.ChikPeaO2B__Account__c=inv.ChikPeaO2B__Account__c;
                    payment.ChikPeaO2B__Has_Processed__c=false;
                    payment.ChikPeaO2B__Invoice__c=inv.id;
                    payment.ChikPeaO2B__Payment_Amount__c=lineRate;
                    payment.ChikPeaO2B__payment_method__c = 'Credit Card';
                    payment.ChikPeaO2B__status__c = 'Not Processed';
                    payment.ChikPeaO2B__payment_gateway__c = gateway;
                    payment.ChikPeaO2B__Gateway_Reference__c=((gatewaylist!=null && gatewaylist.size()>0)?gatewaylist[0].id:null);
                    payment.ChikPeaO2B__Invoice_Line__c = invoiceLine.Id; 
                    payment.ChikPeaO2B__Payment_Date__c =date.today(); //inv.ChikPeaO2B__Due_Date__c;
                    if(invoiceLine.ChikPeaO2B__Line_Type__c=='Subscription' && invoiceLine.ChikPeaO2B__Subscription__c!=null)
                    {
                        payment.ChikPeaO2B__payment_method__c = ((SubsMap.get(invoiceLine.ChikPeaO2B__Subscription__c)!=null && SubsMap.get(invoiceLine.ChikPeaO2B__Subscription__c).ChikPeaO2B__Auto_Payment_Method__c != null) ? SubsMap.get(invoiceLine.ChikPeaO2B__Subscription__c).ChikPeaO2B__Auto_Payment_Method__c : 'Credit Card') ; // Added by Dj
                        if(payment.ChikPeaO2B__payment_method__c == 'Credit Card')
                        {
                            if(o2bsetting.ChikPeaO2B__Use_Multi_Gateway__c==true) // Added by Jani
                            {
                                payment.ChikPeaO2B__payment_gateway__c = invoiceLine.ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__r.name;
                                payment.ChikPeaO2B__Gateway_Reference__c=invoiceLine.ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__c; 
                            
                            }
                            payment.ChikPeaO2B__Credit_Card__c=invoiceLine.ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__c;
                            payment.ChikPeaO2B__Credit_Card_Number__c=invoiceLine.ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Credit_Card_Number__c;
                            payment.ChikPeaO2B__Expiry_Month__c=invoiceLine.ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Expiry_Month__c;
                            payment.ChikPeaO2B__Expiry_Year__c=invoiceLine.ChikPeaO2B__Subscription__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Expiry_Year__c;
                        }                            
                        else if(payment.ChikPeaO2B__payment_method__c == 'ACH')
                        {
                            payment.ChikPeaO2B__payment_gateway__c = o2bsetting.ChikPeaO2B__Default_Gateway_For_ACH__r.Name;
                            payment.chikpeao2b__bank_account__c=SubsMap.get(invoiceLine.ChikPeaO2B__Subscription__c).chikpeao2b__bank_account__c;
                            payment.ChikPeaO2B__Gateway_Reference__c=o2bsetting.ChikPeaO2B__Default_Gateway_For_ACH__c;
                            if(o2bsetting.ChikPeaO2B__Use_Multi_Gateway__c==true)
                            {
                                   payment.ChikPeaO2B__payment_gateway__c = SubsMap.get(invoiceLine.ChikPeaO2B__Subscription__c).chikpeao2b__bank_account__r.ChikPeaO2B__Gateway__r.name;
                                   payment.ChikPeaO2B__Gateway_Reference__c=SubsMap.get(invoiceLine.ChikPeaO2B__Subscription__c).chikpeao2b__bank_account__r.ChikPeaO2B__Gateway__c;
                            }
                        }
                        paylist.add(payment);
                    }
                    else if(inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c!=null)
                    {
                        if(o2bsetting.ChikPeaO2B__Use_Multi_Gateway__c==true) // Added by Jani
                        {
                                payment.ChikPeaO2B__payment_gateway__c = inv.ChikPeao2b__Account__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__r.name;
                                payment.ChikPeaO2B__Gateway_Reference__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__c; 
                            
                        }
                        payment.ChikPeaO2B__Credit_Card__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c;
                        payment.ChikPeaO2B__Credit_Card_Number__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Credit_Card_Number__c;
                        payment.ChikPeaO2B__Expiry_Month__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Month__c;
                        payment.ChikPeaO2B__Expiry_Year__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Year__c;
                        paylist.add(payment);
                    }
                }
            }
        }
        else
        {
            Map<Id, ChikPeao2b__Invoice__c> invMap = new Map<Id, ChikPeao2b__Invoice__c>([SELECT Id,  ChikPeao2b__Account__c,ChikPeao2b__Amount_Due__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Credit_Card_Number__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Month__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Year__c,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Gateway__r.name,ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Gateway__c,ChikPeaO2B__Due_Date__c
              , ChikPeao2b__Account__r.ChikPeaO2B__Bank_Account__c,ChikPeao2b__Account__r.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Gateway__c,ChikPeao2b__Account__r.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Gateway__r.name
              FROM ChikPeao2b__Invoice__c 
              WHERE ChikPeao2b__Amount_Due__c > 0 
            and ID in :returnInvIds and ChikPeao2b__Account__r.ChikPeao2b__Auto_Payment__c = 'Yes' ]);//and ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c!=null
            
            
            
            for(Id invid:returnInvIds)
            {
                ChikPeao2b__Invoice__c inv = invMap.get(invid);
                if (inv != null)
                {
                    ChikPeao2b__Payment__c payment=new ChikPeao2b__Payment__c(
                    ChikPeao2b__Account__c=inv.ChikPeao2b__Account__c,
                    ChikPeaO2B__Payment_Date__c=date.today(),//inv.ChikPeaO2B__Due_Date__c,
                    ChikPeao2b__Has_Processed__c=false,
                    ChikPeao2b__Invoice__c=invid,
                    ChikPeao2b__Payment_Amount__c=inv.Amount_Due__c,
                    ChikPeao2b__payment_method__c = 'Credit Card',
                    ChikPeao2b__status__c = 'Not Processed'
                    );
            
                    if(inv.ChikPeao2b__Account__r.ChikPeaO2B__Bank_Account__c!=null)
                    {
                        payment.ChikPeao2b__payment_method__c='ACH';
                        payment.ChikPeaO2B__Bank_Account__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Bank_Account__c;
                        payment.ChikPeaO2B__Payment_Gateway__c=o2bsetting.ChikPeaO2B__Default_Gateway_For_ACH__r.Name;
                        payment.ChikPeaO2B__Gateway_Reference__c=o2bsetting.ChikPeaO2B__Default_Gateway_For_ACH__c;
                        if(o2bsetting.ChikPeaO2B__Use_Multi_Gateway__c==true)
                        {
                            payment.ChikPeaO2B__Payment_Gateway__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Gateway__r.name;
                            payment.ChikPeaO2B__Gateway_Reference__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Gateway__c;
                        }
                    }
                    else if(inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c!=null)
                    {
                        payment.ChikPeao2b__payment_method__c='Credit Card';
                        payment.ChikPeaO2B__Credit_Card__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c;
                        payment.ChikPeao2b__payment_gateway__c = gateway;
                        payment.ChikPeaO2B__Gateway_Reference__c=((gatewaylist!=null && gatewaylist.size()>0)?gatewaylist[0].id:null);
                        payment.ChikPeaO2B__Credit_Card__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__c;
                        payment.ChikPeaO2B__Credit_Card_Number__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Credit_Card_Number__c;
                        payment.ChikPeaO2B__Expiry_Month__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Month__c;
                        payment.ChikPeaO2B__Expiry_Year__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__Credit_Card__r.ChikPeaO2B__Expiry_Year__c;
                        if(o2bsetting.ChikPeaO2B__Use_Multi_Gateway__c==true) // Added by Jani
                        {
                                payment.ChikPeaO2B__payment_gateway__c = inv.ChikPeao2b__Account__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__r.name;
                                payment.ChikPeaO2B__Gateway_Reference__c=inv.ChikPeao2b__Account__r.ChikPeaO2B__credit_card__r.ChikPeaO2B__Gateway__c; 
                            
                        }
                    }
                    else
                    {
                        
                    }
                    
                    paylist.add(payment);
                }
            }
        }
        if(paylist!=null && paylist.size()>0){
            //insert(paylist);
            //=== CURD check [START] ===//
            Map<string,string>PaymentInsertResultMap=new map<string,string>();
            PaymentInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Payment__c',FieldPermissionSet.PaymentInsert,'insert');
            if(PaymentInsertResultMap.get('AllowDML')!=null && PaymentInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                list<Database.SaveResult> insertPay=Database.insert(paylist,false);
                ExceptionLogger.insertLog(insertPay, paylist, 'Dml Exception in Class PaymentInsertForBatch: line no-85','ChikPeaO2B__Invoice__c');
            }
            //=== CURD check [END] ===//
        }
    }
}