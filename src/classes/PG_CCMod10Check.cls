/**
---------------------------------------------------------------------------------------------------
@desc
To be used when we achieve PCI compliance to store Card Number 
@author PN-Chikpea
@version 1.0 2016-03-02
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG_CCMod10Check {
    
    global static Boolean valid(String ccNumber){
        
        if(ccNumber == null || ccNumber == '' || !ccNumber.isNumeric()) return false;
        
        Integer total = 0;
        Integer digit = 0;
        Boolean alter = false;
        
        for(Integer i = ccNumber.length()-1; i>=0 ; i--){
            digit = Integer.valueOf(ccNumber.substring(i, i + 1));
            if(alter){
                digit *= 2;
                if(digit > 9){
                    digit = Math.mod(digit, 10) + 1;
                }
            }
            total += digit;
            alter = !alter;
        }
        
        Integer mod10 = Math.mod(total, 10);    
        return mod10 == 0;
    }

}