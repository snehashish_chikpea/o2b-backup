@isTest

public class TestPG_IPPaymentsService
{
    static testmethod void Test1()
    {
        /*PG_IPPaymentsService.QueryPaymentSchedules_element queryPaymentSchedules_element=new PG_IPPaymentsService.QueryPaymentSchedules_element();
        queryPaymentSchedules_element.scheduleXML='scheduleXML';*/
        
        
        PG_IPPaymentsService.SubmitSingleVoid_element submitSingleVoid_element=new PG_IPPaymentsService.SubmitSingleVoid_element();
        submitSingleVoid_element.trnXML='trnXML';
        
        
       /* PG_IPPaymentsService.QueryTransactionResponse_element queryTransactionResponse_element=new PG_IPPaymentsService.QueryTransactionResponse_element();
        queryTransactionResponse_element.QueryTransactionResult='QueryTransactionResult';
        
        
        PG_IPPaymentsService.SubmitOpenRefundResponse_element submitOpenRefundResponse_element=new PG_IPPaymentsService.SubmitOpenRefundResponse_element();
        submitOpenRefundResponse_element.SubmitOpenRefundResult='SubmitOpenRefundResult';*/
        
        
        PG_IPPaymentsService.SubmitSingleCapture_element submitSingleCapture_element=new PG_IPPaymentsService.SubmitSingleCapture_element();
        submitSingleCapture_element.trnXML='trnXML';
        
        
        /*PG_IPPaymentsService.QueryPaymentSchedulesResponse_element queryPaymentSchedulesResponse_element=new PG_IPPaymentsService.QueryPaymentSchedulesResponse_element();
        queryPaymentSchedulesResponse_element.QueryPaymentSchedulesResult='QueryPaymentSchedulesResult';
        
        
        PG_IPPaymentsService.QueryTransaction_element queryTransaction_element=new PG_IPPaymentsService.QueryTransaction_element();
        queryTransaction_element.queryXML='queryXML';*/
        
        
        PG_IPPaymentsService.SubmitSingleRefund_element submitSingleRefund_element=new PG_IPPaymentsService.SubmitSingleRefund_element();
        submitSingleRefund_element.trnXML='trnXML';
        
        
        /*PG_IPPaymentsService.QuerySurchargeResponse_element querySurchargeResponse_element=new PG_IPPaymentsService.QuerySurchargeResponse_element();
        querySurchargeResponse_element.QuerySurchargeResult='QuerySurchargeResult';
        
        
        PG_IPPaymentsService.IPPAPIAlive_element iPPAPIAlive_element=new PG_IPPaymentsService.IPPAPIAlive_element();
        iPPAPIAlive_element.userName='userName';
        iPPAPIAlive_element.password='password';
        
        
        PG_IPPaymentsService.DeletePaymentSchedule_element deletePaymentSchedule_element=new PG_IPPaymentsService.DeletePaymentSchedule_element();
        deletePaymentSchedule_element.deleteScheduleXML='deleteScheduleXML';
        
        
        PG_IPPaymentsService.SubmitPaymentScheduleResponse_element submitPaymentScheduleResponse_element=new PG_IPPaymentsService.SubmitPaymentScheduleResponse_element();
        submitPaymentScheduleResponse_element.SubmitPaymentScheduleResult='SubmitPaymentScheduleResult';
        
        
        PG_IPPaymentsService.SubmitPaymentSchedule_element submitPaymentSchedule_element=new PG_IPPaymentsService.SubmitPaymentSchedule_element();
        submitPaymentSchedule_element.scheduleXML='scheduleXML';*/
        
        
        PG_IPPaymentsService.SubmitSinglePayment_element submitSinglePayment_element=new PG_IPPaymentsService.SubmitSinglePayment_element();
        submitSinglePayment_element.trnXML='trnXML';
        
        
        PG_IPPaymentsService.SubmitSingleRefundResponse_element submitSingleRefundResponse_element=new PG_IPPaymentsService.SubmitSingleRefundResponse_element();
        submitSingleRefundResponse_element.SubmitSingleRefundResult='SubmitSingleRefundResult';
        
        /*PG_IPPaymentsService.SubmitPaymentSchedule_element submitPaymentSchedule_element=new PG_IPPaymentsService.SubmitPaymentSchedule_element();
        submitPaymentSchedule_element.scheduleXML='scheduleXML';*/
        
        /*PG_IPPaymentsService.SubmitSinglePayment_element submitSinglePayment_element=new PG_IPPaymentsService.SubmitSinglePayment_element();
        submitSinglePayment_element.trnXML='trnXML';*/
        
        /*PG_IPPaymentsService.SubmitSingleRefundResponse_element submitSingleRefundResponse_element=new PG_IPPaymentsService.SubmitSingleRefundResponse_element();
        submitSingleRefundResponse_element.SubmitSingleRefundResult='SubmitSingleRefundResult';*/
        
        PG_IPPaymentsService.SubmitSinglePaymentResponse_element submitSinglePaymentResponse_element=new PG_IPPaymentsService.SubmitSinglePaymentResponse_element();
        submitSinglePaymentResponse_element.SubmitSinglePaymentResult='SubmitSinglePaymentResult';
        
        /*PG_IPPaymentsService.DeletePaymentScheduleResponse_element deletePaymentScheduleResponse_element=new PG_IPPaymentsService.DeletePaymentScheduleResponse_element();
        deletePaymentScheduleResponse_element.DeletePaymentScheduleResult='DeletePaymentScheduleResult';*/
        
        PG_IPPaymentsService.SubmitSingleCaptureResponse_element submitSingleCaptureResponse_element=new PG_IPPaymentsService.SubmitSingleCaptureResponse_element();
        submitSingleCaptureResponse_element.SubmitSingleCaptureResult='SubmitSingleCaptureResult';
        
        PG_IPPaymentsService.SubmitSingleVoidResponse_element submitSingleVoidResponse_element=new PG_IPPaymentsService.SubmitSingleVoidResponse_element();
        submitSingleVoidResponse_element.SubmitSingleVoidResult='SubmitSingleVoidResult';
        
        /*PG_IPPaymentsService.IPPAPIAliveResponse_element iPPAPIAliveResponse_element=new PG_IPPaymentsService.IPPAPIAliveResponse_element();
        iPPAPIAliveResponse_element.IPPAPIAliveResult=0;
        
        PG_IPPaymentsService.SubmitOpenRefund_element submitOpenRefund_element=new PG_IPPaymentsService.SubmitOpenRefund_element();
        submitOpenRefund_element.trnXML='trnXML';
        
        PG_IPPaymentsService.QuerySurcharge_element querySurcharge_element=new PG_IPPaymentsService.QuerySurcharge_element();
        querySurcharge_element.querySurchargeXML='querySurchargeXML';*/
        
        /*PG_IPPaymentsService.dtsSoap dtsSoap1=new PG_IPPaymentsService.dtsSoap();
        String parameterXml='parameterXml';
        /*dtsSoap1.QueryPaymentSchedules(parameterXml);
        dtsSoap1.SubmitOpenRefund(parameterXml);
        dtsSoap1.DeletePaymentSchedule(parameterXml);
        dtsSoap1.QuerySurcharge(parameterXml);
        dtsSoap1.SubmitPaymentSchedule(parameterXml);
        dtsSoap1.QueryTransaction(parameterXml);
        dtsSoap1.IPPAPIAlive(parameterXml,parameterXml);
        dtsSoap1.SubmitSingleRefund(parameterXml);
        dtsSoap1.SubmitSingleCapture(parameterXml);
        dtsSoap1.SubmitSinglePayment(parameterXml);
        dtsSoap1.SubmitSingleVoid(parameterXml);*/
        
    }
}