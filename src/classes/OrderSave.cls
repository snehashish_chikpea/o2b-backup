/*
** Class         :  OrderSave
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by	 :  Asitm (chikpea Inc.)
** Reason		 :  Secuirty Issue fixed.(sharing)
** Description   :   
*/


////////////////////////////////////////////////////////////////////////////////////////
//  Request example:                                                                  //
//  <?xml version='1.0' encoding='us-ascii'?> -->[1st line will be exactly same]      //  
//  <OrderDetails>                                                                    // 
//       <Data type="Status__c">Open</Data>                                           // 
//       .                                                                            //                 
//       .                                                                            //
//       <Data type="Rate_Plan">                                                      //
//       RatePlanID:Quantity:UnitPrice,RatePlanID:Quantity:UnitPrice                  //
//                                       |             |      |                       //
//                                       |-> Required<-|      |->[Optional]           //
//                                                                                    //
//       </Data>                                                                      //
//       <Data type="Account__c">AccountID </Data>                                    //
//  </OrderDetails>                                                                   //  
////////////////////////////////////////////////////////////////////////////////////////

global with sharing class OrderSave{
    Webservice static String OrderInsert(String request){ 
        Map<String, String> inputDetails;
        string returnval;
        Map<string,string>TypeMap=new map<string,string>();
        Map<String, Schema.SObjectType> gd=Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> M=gd.get('ChikPeaO2B__order__c').getDescribe().fields.getMap();
        for(Schema.SObjectField sf:M.values())
        {
            TypeMap.put(sf.getDescribe().getName(),string.valueof(sf.getDescribe().getType()));
        }

        if(request != null && request.trim() != '')
        {
            try{
                request=request.removeStartIgnoreCase('<?xml version=\'1.0\' encoding=\'us-ascii\'?>');
                inputDetails = XMLHandler.parseDetails( request );
                System.debug('------reqIPDet>' + inputDetails);
                List<order_line__c>OrlList=new List<order_line__c>();
                Schema.SObjectType targetType = Schema.getGlobalDescribe().get('ChikPeaO2B__order__c');
                order__c order = (order__c)targetType.newSObject();
                List<string>rpidsAndQtyAndUnitp=new List<string>();
                //order__c order=new order__c();
                for(string key : inputDetails.keyset()){
                    if(key.equalsignorecase('ChikPeaO2B__Rate_Plan__c') && inputDetails.get(key)!=null && inputDetails.get(key)!='')
                    {
                        rpidsAndQtyAndUnitp=inputDetails.get(key).removeEnd(',').split(',');
                    }
                    else{
                        if(TypeMap.get(key).equalsignorecase('CURRENCY')||TypeMap.get(key).equalsignorecase('PERCENT')||TypeMap.get(key).equalsignorecase('DOUBLE')){
                            if(inputDetails.get(key)!=null && inputDetails.get(key)!='' && (!inputDetails.get(key).equalsignorecase('null')))
                            {
                                system.debug('!!!!'+key+'!!!'+inputDetails.get(key));
                                order.put(key,decimal.valueof(inputDetails.get(key)));
                            }
                            
                        }
                        else if(TypeMap.get(key).equalsignorecase('BOOLEAN'))
                            order.put(key,boolean.valueof(inputDetails.get(key)));
                        else if(TypeMap.get(key).equalsignorecase('DATETIME'))
                        {
                            if(inputDetails.get(key)!=null && inputDetails.get(key)!='' && (!inputDetails.get(key).equalsignorecase('null')))
                            {
                                system.debug('*****'+inputDetails.get(key));
                                //Datetime dt = DateTime.parse('10/14/2011 11:46 AM');//2013-10-24 18:12:00
                                String[] str = inputDetails.get(key).split(' ');
                                String[] ds = str[0].split('-');//2013-10-24
                                string[] ts = str[1].split(':');//18:12:00
                                Datetime dt = datetime.newInstance(integer.valueof(ds[0]), integer.valueof(ds[1]), integer.valueof(ds[2]), integer.valueof(ts[0]), integer.valueof(ts[1]), integer.valueof(ts[2]));
                                order.put(key,dt);
                            }
                        }
                        else if(TypeMap.get(key).equalsignorecase('DATE'))
                        {
                            if(inputDetails.get(key)!=null && inputDetails.get(key)!='' && (!inputDetails.get(key).equalsignorecase('null')))
                            {
                                System.debug('$$$$$==>'+inputDetails.get(key));
                                //Date d = Date.parse(inputDetails.get(key));//2013-10-24 00:00:00
                                String[] str = inputDetails.get(key).split(' ');
                                String[] ds = str[0].split('-');//2013-10-24
                                Date d = date.newinstance(integer.valueof(ds[0]), integer.valueof(ds[1]), integer.valueof(ds[2]));
                                order.put(key,d);
                            }
                        }
                        else{
                            system.debug('^^^^'+key+inputDetails.get(key));
                            if(inputDetails.get(key)!=null && inputDetails.get(key)!='' && (!inputDetails.get(key).equalsignorecase('null')))
                            {
                                system.debug('@@@@'+key+'@@@@'+inputDetails.get(key)+'@@@@'+inputDetails.get(key).equalsignorecase('null'));
                                order.put(key,inputDetails.get(key));        
                            }   
                        }
                    }
                }
                //########## Create Order Line[START] ############//
                
                if(rpidsAndQtyAndUnitp!=null && rpidsAndQtyAndUnitp.size()>0){
                    boolean haserror=false;
                    Map<String,integer>rpidqtyMap=new Map<String,integer>();
                    Map<String,decimal>rpidunitpMap=new Map<String,decimal>();
                    for(string s : rpidsAndQtyAndUnitp)
                    {
                        string[] str=s.split(':');
                        if(str!=null && str.size()>2 && (str[0]!=null && str[0]!='') && (str[1]!=null && str[1]!='') && (str[2]!=null && str[2]!=''))
                        {
                            if(!rpidqtyMap.containskey(str[0]))
                            {
                                try{
                                    rpidqtyMap.put(str[0],integer.valueof(str[1])); 
                                    rpidunitpMap.put(str[0],decimal.valueof(str[2]));   
                                }
                                catch(Exception e){
                                    haserror=true;
                                    returnval='invalid quantity or unit price';
                                }
                            }
                        }
                        else if(str!=null && str.size()==2 && (str[0]!=null && str[0]!='') && (str[1]!=null && str[1]!=''))
                        {
                                try{
                                    rpidqtyMap.put(str[0],integer.valueof(str[1]));      
                                    rpidunitpMap.put(str[0],OrderFormExtension.calculateunitprice(str[0],integer.valueof(str[1])));   
                                }
                                catch(Exception e){
                                    haserror=true;
                                    returnval='invalid quantity';
                                }
                        }
                    }
                    if(!haserror){
                        Map<id,Rate_Plan__c>ItemIdRatePlanMap= new Map<id,Rate_plan__c>();
                        if(rpidqtyMap.keyset()!=null && rpidqtyMap.keyset().size()>0)
                        {
                            for(Rate_plan__c rp : [SELECT id,name,Bill_Cycle__c,Free_Usage__c,Item__c,item__r.Item_Type__c,Max_Usage__c,Non_Recurring_Charge__c,Period_Off_Set__c,
                            Period_Off_Set_Unit__c,Price_Book__c,Pricing_Type__c,Recurring_Charge__c,Start_Off_set__c,Start_Off_Set_Unit__c,UOM__c,Usage_Rate__c from Rate_Plan__c where id in : rpidqtyMap.keyset()])
                            {
                                if(!ItemIdRatePlanMap.containskey(rp.item__c))
                                {
                                    ItemIdRatePlanMap.put(rp.item__c,rp);    
                                }    
                            }
                            
                            for(rate_plan__c rp1:ItemIdRatePlanMap.values()){
                                Order_Line__c orl=new Order_Line__c();
                                orl.Item__c=rp1.item__c;
                                orl.Item_Type__c=rp1.item__r.Item_Type__c;
                                orl.Quantity__c=rpidqtyMap.get(rp1.id);
                                orl.Rate_Plan__c=rp1.id;
                                orl.Status__c='Open';
                                orl.Unit_Price__c=rpidunitpMap.get(rp1.id);
                                OrlList.add(orl);
                            }
                        }   
                    } 
                }
                //########## Create Order Line[END] ##############//
                try{
                    system.debug('Orderrrrr'+order);                    
                    insert(order);
                    returnval=order.id;
                    if(OrlList!=null && OrlList.size()>0)
                    {
                        for(order_line__c orl : OrlList)
                            orl.order__c = order.id;
                        insert(OrlList);
                    }    
                }
                catch(exception e){
                    returnval='Error:'+e.getmessage();
                }
            }
            catch(Exception ex)
            {
                returnval='Error:XML parsing Error';
            }
        }
        else
        {
            returnval='Error:insufficient information';
        }    
        return returnval;
    }
}