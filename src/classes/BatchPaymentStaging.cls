global with sharing class BatchPaymentStaging implements Database.Batchable<Sobject>,Database.Stateful{
    global String query;
    global String msg;
    global string email;
    global List<ChikPeaO2B__O2B_Setting__c> o2bset;
    global BatchPaymentStaging()
    {
        msg='';
        o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, ChikPeaO2B__Batch_Size__c,
            ChikPeaO2B__IsStaging_InvoiceRef__c,
                ChikPeaO2B__Line_Payment__c from ChikPeaO2B__O2B_Setting__c limit 1];
        this.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?
                    o2bset[0].ChikPeaO2B__Admin_Email__c : 'o2b@chikpea.com');
        if(o2bset[0].ChikPeaO2B__IsStaging_InvoiceRef__c){
            this.query='select id, name, ChikPeaO2B__Account_Reference__c,ChikPeaO2B__Split_to_Lines__c, ChikPeaO2B__Amount__c, ChikPeaO2B__Payment_Date__c,'
            +'ChikPeaO2B__Used_Amount__c, ChikPeaO2B__Invoice_Ref__c, ChikPeaO2B__Payment_Method__c, ChikPeaO2B__Check_Ref__c,ChikPeaO2B__Batch_Ref__c '
            +'from ChikPeaO2B__Payment_Staging__c where ChikPeaO2B__Invoice_Ref__c!=null and '+
            'ChikPeaO2B__Status__c!=\'Uploaded\'';
        }else{
            System.debug('#****Account Ref ');
            this.query='select id,name,ChikPeaO2B__Amount_Due_Remaining__c,ChikPeaO2B__Account__c,'+
            'ChikPeaO2B__Account__r.AccountNumber,ChikPeaO2B__Due_Date__c from ChikPeaO2B__Invoice__c'+
            ' where ChikPeaO2B__Account__r.ChikPeaO2B__Active__c=\'Yes\' and '+
            'ChikPeaO2B__Account__r.ChikPeaO2B__Is_Billing__c=true and '+
            'ChikPeaO2B__Account__r.ChikPeaO2B__Auto_Payment__c!=\'Yes\' and '+
            ' ChikPeaO2B__Account__r.AccountNumber!=null and(ChikPeaO2B__Invoice_Status__c=\'Open\' '+
            'or ChikPeaO2B__Invoice_Status__c=\'Paid Partial\')';
        }
    }
    global database.querylocator start(Database.BatchableContext BC)
    {
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        //Payment Stagings have invoice reference
        if(o2bset[0].ChikPeaO2B__IsStaging_InvoiceRef__c){          
            System.debug('#BatchPaymentStaging Invoice Reference--->Start');
            List<ChikPeaO2B__Payment_Staging__c>stg_list=new List<ChikPeaO2B__Payment_Staging__c>();
            for(Sobject obj : scope)
            {
                ChikPeaO2B__Payment_Staging__c stg=(ChikPeaO2B__Payment_Staging__c)obj;
                stg_list.add(stg);
            }
            try{
                //PaymentStagingToInvoicePayment method does not care about o2bset[0].ChikPeaO2B__Line_Payment__c
                //overloaded method
                PaymentStaging.PaymentStagingToInvoicePayment(stg_list,o2bset[0].ChikPeaO2B__Line_Payment__c);
                msg+='Success';
            }
            catch(Exception e)
            {
                msg+=e.getmessage();
            }
        }else{
            //Payment Stagings does not have invoice reference
            System.debug('#BatchPaymentStaging No Invoice Reference--->Start');
            System.debug('#BatchPaymentStaging--->scope.size='+scope.size());
            List<ChikPeaO2B__Invoice__c>InvoiceList=new List<ChikPeaO2B__Invoice__c>();
            for(Sobject obj : scope)
            {
                ChikPeaO2B__Invoice__c inv=(ChikPeaO2B__Invoice__c)obj;
                InvoiceList.add(inv);
            }
            try{
                PaymentStaging.PaymentStagingToInvoicePayment(InvoiceList,o2bset[0].ChikPeaO2B__Line_Payment__c);
                msg+='Success';
            }
            catch(Exception e)
            {
                msg+=e.getmessage();
            }
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo(email);
        mail.setSenderDisplayName('Payment Staging Process');
        mail.setSubject('Payment Staging on '+date.today().format()+' Completed');
        if (msg == null) msg = '';
        mail.setPlainTextBody('Payment Staging on '+date.today().format()+' Completed ' + msg);
    }
}