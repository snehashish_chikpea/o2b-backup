/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-05
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
global class TestPG_WSOribitalReversalMock implements WebServiceMock {
  
  global void doInvoke(Object stub,
                 Object request,
                 Map<String, Object> response,
                 String endpoint,
                 String soapAction,
                 String requestName,
                 String responseNS,
                 String responseName,
                 String responseType
  ){
       PG_WSOribitalElements.ReversalResponse_element respElement =  
           new PG_WSOribitalElements.ReversalResponse_element();
    
      PG_WSOribitalElements.ReversalResponseElement reversalRespElement =
        new PG_WSOribitalElements.ReversalResponseElement();
        
      reversalRespElement.version = '';
      reversalRespElement.outstandingAmt = '0';
      reversalRespElement.bin = '000001';
      reversalRespElement.merchantID = '206734';
      reversalRespElement.terminalID = '001';
      reversalRespElement.orderID = 'R-1000';
      reversalRespElement.txRefNum = '5269085432D4AA80D3F3C840918AC843BCC4540F';
      reversalRespElement.txRefIdx = '1';
      reversalRespElement.respDateTime = '20131024075002';
      reversalRespElement.procStatus = '0';
      reversalRespElement.procStatusMessage = '';
      reversalRespElement.retryTrace = '';
      reversalRespElement.retryAttempCount = '';
      reversalRespElement.lastRetryDate = '';
      reversalRespElement.approvalStatus = '';
      reversalRespElement.respCode = '';
      reversalRespElement.hostRespCode = '';
        
      respElement.return_x = reversalRespElement;
           
           response.put('response_x', respElement); 
     }
  
}