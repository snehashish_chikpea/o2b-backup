@isTest
private class Testusagestaging{
    static testMethod void usgcalc(){
        Account acc = new Account();
        acc.Name = 'Test_account';
        Insert acc;
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        ChikPeaO2B__Item__c recV2=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        insert (recV2);
        
        
        ChikPeaO2B__Rate_Plan__c rp6=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Rating_Rule__c = 'MonthlyAggregate' ,ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV2.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp6);
        
        ChikPeaO2B__Rate_Plan__c rp7=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Rating_Rule__c = 'O2BAPI',ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV2.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp7);
        ChikPeaO2B__Usage__c usg1 = new ChikPeaO2B__Usage__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Item__c=recV2.id,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Total_Usage__c=0.00);
        insert usg1;
        
        ChikPeaO2B__Usage__c usg = new ChikPeaO2B__Usage__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Item__c=recV2.id,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Total_Usage__c=0.00);
        insert usg;
        list<ChikPeaO2B__Usage_Staging__c> uslist = new list<ChikPeaO2B__Usage_Staging__c>();
        ChikPeaO2B__Usage_Staging__c us1 = new ChikPeaO2B__Usage_Staging__c(ChikPeaO2B__Usage__c=usg.id , ChikPeaO2B__Usage_unit__c = 150.00);
        uslist.add(us1);
        ChikPeaO2B__Usage_Staging__c us2 = new ChikPeaO2B__Usage_Staging__c(ChikPeaO2B__Usage__c=usg.id, ChikPeaO2B__Usage_unit__c = 50.00);
        uslist.add(us2);
        ChikPeaO2B__Usage_Staging__c us3 = new ChikPeaO2B__Usage_Staging__c(ChikPeaO2B__Usage__c=usg.id, ChikPeaO2B__Usage_unit__c = 250.00);
        uslist.add(us3);
        ChikPeaO2B__Usage_Staging__c us4 = new ChikPeaO2B__Usage_Staging__c(ChikPeaO2B__Usage__c=usg1.id, ChikPeaO2B__Usage_unit__c = 350.00);
        uslist.add(us4);
        insert uslist;
        List<ChikPeaO2B__Usage__c> uid = [SELECT id,name,ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Rating_Rule__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Total_Usage__c,(select id,name,ChikPeaO2B__Usage__c,ChikPeaO2B__Usage_unit__c from ChikPeaO2B__Usage_Stagings__r) from ChikPeaO2B__Usage__c where id =: usg.id or id =: usg1.id];
        
        O2BAPI.UsageCalculation(uid);
        
        
    }
}