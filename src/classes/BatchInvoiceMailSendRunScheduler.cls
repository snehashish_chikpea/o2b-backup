/*
** Class         :  BatchInvoiceMailSend
** Created by    :  Asitm (chikpea Inc.)
** Last Modified :  28 march 14
** Modified by	 :  
** Reason		 :  
** Description   :   
*/

global with sharing class BatchInvoiceMailSendRunScheduler {
	Webservice static String executeschedule(){
		String error;
        String sch;
        BatchInvoiceMailSendScheduler bi= new BatchInvoiceMailSendScheduler();
        
        DateTime cur_time=system.now().addMinutes(2);
        Integer min=cur_time.minute();
        string mm=string.valueOf(min);
        string hh=string.valueOf(cur_time.hour());
        sch='0 '+mm+' '+hh+' * * ?';
        try{
            system.schedule('Auto Invoice E-mail Send Process',sch, bi);
            error='success';
        }catch(Exception E){error=E.getMessage();}
        return error;
	}
}