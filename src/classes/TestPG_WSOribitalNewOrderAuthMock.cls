/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-05
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
global class TestPG_WSOribitalNewOrderAuthMock implements WebServiceMock {
  
  global void doInvoke(Object stub,
                 Object request,
                 Map<String, Object> response,
                 String endpoint,
                 String soapAction,
                 String requestName,
                 String responseNS,
                 String responseName,
                 String responseType
  ){
       PG_WSOribitalElements.NewOrderResponse_element respElement =  
           new PG_WSOribitalElements.NewOrderResponse_element();
    
      PG_WSOribitalElements.NewOrderResponseElement newOrderRespElement =
        new PG_WSOribitalElements.NewOrderResponseElement();
        
      newOrderRespElement.industryType = 'MO';
      newOrderRespElement.transType = 'A';
      newOrderRespElement.bin = '000001';
      newOrderRespElement.merchantID = '206734';
      newOrderRespElement.terminalID = '001';
      newOrderRespElement.cardBrand = 'VI';
      newOrderRespElement.orderID = 'ON-0001';
      newOrderRespElement.txRefNum = '528DE1512115A4339AF63D1F2D75AE51912154B1';
      newOrderRespElement.txRefIdx = '0';
      newOrderRespElement.respDateTime = '20131121053249';
      newOrderRespElement.procStatus = '0';
      newOrderRespElement.approvalStatus = '1';
      newOrderRespElement.respCode = '00';
      newOrderRespElement.avsRespCode = 'B';
      newOrderRespElement.cvvRespCode = 'U';
      newOrderRespElement.authorizationCode = 'tst94E';
      newOrderRespElement.mcRecurringAdvCode = '';
      newOrderRespElement.visaVbVRespCode = '';
      newOrderRespElement.procStatusMessage = 'Approved';
      newOrderRespElement.respCodeMessage = '';
      newOrderRespElement.hostRespCode = '100';
      newOrderRespElement.hostAVSRespCode = 'I3';
      newOrderRespElement.hostCVVRespCode = 'U';
      newOrderRespElement.retryTrace = '';
      newOrderRespElement.retryAttempCount = '';
      newOrderRespElement.lastRetryDate = '';
      newOrderRespElement.customerRefNum = '';
      newOrderRespElement.customerName = '';
      newOrderRespElement.profileProcStatus = '';
      newOrderRespElement.profileProcStatusMsg = '';
        
      respElement.return_x = newOrderRespElement;
           
           response.put('response_x', respElement); 
     }
  
}