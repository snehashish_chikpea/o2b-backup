/*
** Class         :  QuotePdfConForExternalVF
** Created by    :  Asit (chikpea Inc.)
** Last Modified :  04 march 14
** Description   :  Expose Quote & Quote line 
*/

global with sharing class QuotePdfConForExternalVF {
	//[Class level variable declaration]
	
	public string quoteId;
	public List<Schema.SObjectField> fldObjMapValuesQ =
	schema.SObjectType.ChikPeaO2B__Quote__c.fields.getMap().values();
	
	public List<Schema.SObjectField> fldObjMapValuesQL =
	schema.SObjectType.ChikPeaO2B__Quote_Line__c.fields.getMap().values();
	
	// [Variable declaration END]
	global ChikPeaO2B__Quote__c getquote(){
		//invId=Apexpages.currentPage().getParameters().get('id');
		list<ChikPeaO2B__Quote__c> retQuo= new list<ChikPeaO2B__Quote__c>();
		string relatedList='( SELECT ';
		for(Schema.SObjectField s : fldObjMapValuesQL){
			relatedList+= s.getDescribe().getName() + ' , ';
		}
		relatedList = relatedList.subString(0, relatedList.length() - 2);
		relatedList+='  FROM ChikPeaO2B__Quote_Lines__r ) ';
		
		string mainQuery='  SELECT  ';
		for(Schema.SObjectField s : fldObjMapValuesQ){
			mainQuery+= s.getDescribe().getName() + ' , ';
		}
		string lastPart=' FROM ChikPeaO2B__Quote__c WHERE Id= \''+ quoteId + '\' ';
		system.debug('----------->'+mainQuery + relatedList + lastPart);
		try{
			retQuo=database.query(mainQuery + relatedList + lastPart);
		}catch(Exception e){system.debug('===>37==>'+e.getMessage());}
		if(!retQuo.isEmpty()){
			return retQuo[0];
		}
		
		return new ChikPeaO2B__Quote__c();
	}
	//variables with getter setter
	
	//[get set varialbe  END]
	
	/* *************
		CONTROLLERS
		************
	*/
	//Controller
	global QuotePdfConForExternalVF(){
		quoteId=Apexpages.currentPage().getParameters().get('id');
	}
	
	//Controller for extensions
	global QuotePdfConForExternalVF(ApexPages.StandardController stdController){
		quoteId=stdController.getId();
	}
	//[END OF CONTROLLER SECTION]
}