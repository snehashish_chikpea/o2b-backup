@isTest
public with sharing class TestBatchParentBill {
    @isTest
    static void BatchParentBillRunScheduler(){
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Schedule_Time__c='03:30',
        ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,
        ChikPeaO2B__Is_Parent_Billing_Enabled__c=true,ChikPeaO2B__Delete_blank_invoice_after_parent_billin__c=true);
        insert(o2bset);
        BatchParentBillRunScheduler.executeschedule();
    }
    @isTest
    static void testMethod2()
    {
        
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Schedule_Time__c='03:30',
        ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,
        ChikPeaO2B__Is_Parent_Billing_Enabled__c=true,ChikPeaO2B__Delete_blank_invoice_after_parent_billin__c=true
        ,Create_Invoice_on_Process_Order__c=true,ChikPeaO2B__Billing_Type__c='Anniversary Subscription');
        insert(o2bset);
        
        List<Account> ac_list=new List<Account>();
        Account acc00=new Account(name='acc00',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),ChikPeaO2B__Parent_Billing__c=true);
        ac_list.add(acc00);
        
        Account a=new Account(name='acc',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),ChikPeaO2B__Parent_Billing__c=true);
        ac_list.add(a);
        Account acc=new Account(name='acc1',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),ChikPeaO2B__Bill_To_Parent__c=true);
        ac_list.add(acc);
        Account acc1=new Account(name='acc2',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),ChikPeaO2B__Bill_To_Parent__c=true);
        ac_list.add(acc1);   
        
        Account ac_l2=new Account(name='ac_l2',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),
        	ChikPeaO2B__Parent_Billing__c=true, ChikPeaO2B__Bill_To_Parent__c=false);
        ac_list.add(ac_l2);   
        Account ac_l3=new Account(name='ac_l3',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),
        	ChikPeaO2B__Parent_Billing__c=false, ChikPeaO2B__Bill_To_Parent__c=true);	
        ac_list.add(ac_l3);  
        
        Account ac_l21=new Account(name='ac_l21',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),
        	ChikPeaO2B__Parent_Billing__c=true, ChikPeaO2B__Bill_To_Parent__c=false);
        ac_list.add(ac_l21);	
        Account ac_l31=new Account(name='ac_l31',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today(),
        	ChikPeaO2B__Parent_Billing__c=false, ChikPeaO2B__Bill_To_Parent__c=true);	
        ac_list.add(ac_l31);
        insert ac_list;
        
        //creating hierarchy
        acc.ParentId=a.Id;
        acc1.ParentId=acc.Id;
        ac_l2.ParentId=a.Id;
        ac_l3.ParentId=ac_l2.Id;
        ac_l21.ParentId=a.Id;
        ac_l31.ParentId=ac_l2.Id;
        //save hierarchy data
        update ac_list;
        
        
        //insert billing contact
        Contact con=new Contact(AccountId=acc00.Id, FirstName='Contract', LastName='Name');
        insert con;
        
        ChikPeaO2B__Invoice__c inv0=new ChikPeaO2B__Invoice__c();
        inv0.ChikPeaO2B__Account__c=ac_l21.Id;        
        insert inv0;
        
        ChikPeaO2B__Invoice__c inv1=new ChikPeaO2B__Invoice__c();
        inv1.ChikPeaO2B__Account__c=acc00.Id;        
        insert inv1;
        
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        
        ChikPeaO2B__Order__c ord_ac_l3=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=ac_l3.id,
        	ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord_ac_l3);
        
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        
        ChikPeaO2B__Order_Line__c ol_ac_l3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',
        	ChikPeaO2B__Order__c=ord_ac_l3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(ol_ac_l3);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        oidList.add(ol_ac_l3.id);
        //ProcessOrder.OrderProcessor(oidList);
        //o2bset.ChikPeaO2B__Billing_Type__c='';
        //update o2bset;
        ProcessOrder.OrderProcessor(oidList);
        //Test.startTest();
        ChikPeaO2B__Order__c ord1=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc1.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord1);
        List<ChikPeaO2B__Order_Line__c>OLlist1=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl11=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist1.add(orl11);
        insert(OLlist1);
        ProcessOrder.OrderProcessor(new list<id>{ord1.id});
        //test.stopTest();
        ParentBillingProcessor.processAccounts(new list<id>{a.Id});
        BatchParentBillRunScheduler.executeschedule();
        
    }
    
    @isTest
    static void testmethod3()
    {
    	//ChikPeaO2B__Billing_Type__c='Anniversary Subscription',
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,
        ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Is_Parent_Billing_Enabled__c=true,
        ChikPeaO2B__Delete_blank_invoice_after_parent_billin__c=true);
        insert(o2bset);
        Account acc=new Account(name='acc1',ChikPeaO2B__Active__c='Yes');
        insert(acc);
        BatchCreateBillScheduler schd=new BatchCreateBillScheduler();
        BatchParentBilling BCB = new BatchParentBilling();
        BCB.email='o2b@chikpea.com';
        BCB.query='select id,name from Account where ChikPeaO2B__Active__c=\'Yes\'';
        Database.executeBatch(bcb,50);
    }
    
    @isTest
    //No O2B Settings
    static void testmethod4()
    {
        Account acc=new Account(name='acc1',ChikPeaO2B__Active__c='Yes');
        insert(acc);
		List<Id> ac_list=new List<Id>();
		ac_list.add(acc.id);
		ParentBillingProcessor.processAccounts(ac_list);
    }
}