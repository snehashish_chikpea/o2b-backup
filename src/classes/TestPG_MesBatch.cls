@isTest
private class TestPG_MesBatch {
    
    @isTest 
    static void test_mes_batch() {
        
        Test.startTest();

        Account acc =  new Account();
        acc.Name = 'Test Account';
        acc.SLA__c = 'Bronze';
        acc.SLASerialNumber__c = '1234';
        acc.SLAExpirationDate__c = Date.today();
        insert acc;

        Gateway__c gwMeSBatch = new Gateway__c();
        gwMeSBatch.Name = 'MeS:Batch';
        gwMeSBatch.Gateway_Type__c = 'MeS';
        gwMeSBatch.Address_Verification__c = true;
        gwMeSBatch.Attach_Note__c = true;
        gwMeSBatch.Batch_Size__c = 10;
        gwMeSBatch.Capture_Card_Details__c = true;
        gwMeSBatch.Card_Code_Verification__c = true;
        gwMeSBatch.Currency__c = 'USD';
        gwMeSBatch.Gateway_Url_Prod__c = 'https://www.merchante-solutions.com';
        gwMeSBatch.Gateway_Url_Prod_Fallback__c = '';
        gwMeSBatch.Gateway_Url_Test__c = 'https://www.merchante-solutions.com';
        gwMeSBatch.Gateway_Url_Test_Fallback__c = '';
        gwMeSBatch.Login_Id__c = 'MESBATCHLOGINID';
        gwMeSBatch.Merchant_Reference__c = '';
        gwMeSBatch.Terminal_ID__c = '';
        //gwMeSBatch.O2B_Setting__c = '';
        gwMeSBatch.Partial_Authorization__c = false;
        gwMeSBatch.Password__c = 'MESBATCHPASSWORD';
        gwMeSBatch.Primary_Site_Available__c = false;
        gwMeSBatch.Retry_Count__c = 14;
        gwMeSBatch.Retry_Interval__c = 2;
        gwMeSBatch.Test_Mode__c = true;
        gwMeSBatch.Time_out_limit__c = 120;
        insert gwMeSBatch;

        Credit_Card__c cc = new Credit_Card__c();
        cc.CC_City__c = 'Bedford';
        cc.CC_Country__c = 'US';
        cc.CC_State__c = 'NH';
        cc.CC_Street1__c = 'Apt 2';
        cc.CC_Street2__c = '123 Northeastern Blvd';
        cc.CC_First_Name__c = 'Visa';
        cc.CC_Last_Name__c = 'MeS';
        cc.CC_Postal_Code__c = '55555';
        cc.Credit_Card_Number__c = '3010';//4012301230123010
        cc.Credit_Card_Type__c = 'Visa';
        cc.Expiry_Month__c = 5;
        cc.Expiry_Year__c = 2020;
        cc.Account__c = acc.Id;
        insert cc;

        Invoice__c inv = new Invoice__c();
        inv.Account__c = acc.Id;
        insert inv;

        Payment__c payment = new Payment__c();
        payment.Payment_Method__c = 'Credit Card'; 
        payment.Payment_Gateway__c = 'MeS:Batch';
        payment.Payment_Amount__c = 1.01;
        payment.Status__c = 'Not Processed';
        payment.Credit_Card__c = cc.Id;
        payment.Invoice__c = inv.Id;
        payment.Account__c = acc.Id;
        payment.Payment_Date__c = System.today();
        insert payment;
        
        List<Payment__c> payments = [SELECT p.Credit_Card__r.Expiry_Month__c, p.Credit_Card__r.Expiry_Year__c,
            p.Credit_Card__r.CC_Postal_Code__c, p.Credit_Card__r.CC_Street2__c,
            p.Credit_Card__r.Credit_Card_Number__c,
            p.Payment_Amount__c, p.Invoice__r.Name, p.Id, p.Merchant_Order_Number__c, p.Status__c,
            p.Payment_Date__c, p.Payment_Method__c, p.Invoice__r.Invoice_Status__c
            FROM Payment__c p
            Where p.Id = :payment.Id            
        ];

        //List<Payment__c> payments = new List<Payment__c>();
        //payments.add(payment);
        
        Test.stopTest();
        
        String paymentGateway = 'MeS:Batch';
        String uploadPath = '/srv/api/bpUpload';
        String downloadPath = '/srv/api/bpDownload';
        String paymentId = payment.Id;

        //Successfull Submission
        PG_MeSBatch mesBat = new PG_MeSBatch();
        PG_MeSBatch.TEST_UPLOAD_RES = 'rspCode=000&rspMessage=OK&reqFileId=1533';
        mesBat.init(payments, paymentGateway);
        mesBat.getConfig();
        String reqFileId = mesBat.uploadFile(uploadPath);
        System.assertEquals('1533', reqFileId);

        //Sccessfully processed
        String resFileId = '1534';
        PG_MeSBatch mesBat1 = new PG_MeSBatch();
        PG_MeSBatch.TEST_DOWNLOAD_RES = 'transaction_id=7b616e0c6de036318747463969841e03&error_code=000&auth_response_text=Exact Match&avs_result=Y&cvv2_result=M&auth_code=T95361&eresp_trx_track_id='+paymentId+'\n';
        mesBat1.init(payments, paymentGateway);
        mesBat1.downloadFile(downloadPath, resFileId);
        //System.assertEquals('Processed', payment.Status__c);
        
        //Failed Submission - System error
        PG_MeSBatch mesBat2 = new PG_MeSBatch();
        PG_MeSBatch.TEST_UPLOAD_RES = 'rspCode=001&rspMessage=OK';
        mesBat2.init(payments, paymentGateway);
        String reqFileId2 = mesBat2.uploadFile(uploadPath);
        
        //Failed Submission - empty response from server
        PG_MeSBatch mesBat3 = new PG_MeSBatch();
        PG_MeSBatch.TEST_UPLOAD_RES = '';
        mesBat3.init(payments, paymentGateway);
        String reqFileId3 = mesBat3.uploadFile(uploadPath);

    }
    
    @isTest
    static void test_mes_batch_upload_schedular(){
        
        Test.startTest();
        
        Account acc =  new Account();
        acc.Name = 'Test Account';
        acc.SLA__c = 'Bronze';
        acc.SLASerialNumber__c = '1234';
        acc.SLAExpirationDate__c = Date.today();
        insert acc;
        
        Gateway__c gwMeSBatch = new Gateway__c();
        gwMeSBatch.Name = 'MeS:Batch';
        gwMeSBatch.Gateway_Type__c = 'MeS';
        gwMeSBatch.Address_Verification__c = true;
        gwMeSBatch.Attach_Note__c = true;
        gwMeSBatch.ChikPeaO2B__Batch_Download_Interval__c = 1;
        gwMeSBatch.ChikPeaO2B__Batch_Upload_Interval__c = 1;
        gwMeSBatch.ChikPeaO2B__Batch_Res_File_Id__c = '';
        gwMeSBatch.Batch_Size__c = 1;   //made it in order tocreate less payment records
        gwMeSBatch.Capture_Card_Details__c = true;
        gwMeSBatch.Card_Code_Verification__c = true;
        gwMeSBatch.Currency__c = 'USD';
        gwMeSBatch.Gateway_Url_Prod__c = 'https://www.merchante-solutions.com';
        gwMeSBatch.Gateway_Url_Prod_Fallback__c = '';
        gwMeSBatch.Gateway_Url_Test__c = 'https://www.merchante-solutions.com';
        gwMeSBatch.Gateway_Url_Test_Fallback__c = '';
        gwMeSBatch.Login_Id__c = 'MESBATCHLOGINID';
        gwMeSBatch.Merchant_Reference__c = '';
        gwMeSBatch.Terminal_ID__c = '';
        //gwMeSBatch.O2B_Setting__c = '';
        gwMeSBatch.Partial_Authorization__c = false;
        gwMeSBatch.Password__c = 'MESBATCHPASSWORD';
        gwMeSBatch.Primary_Site_Available__c = false;
        gwMeSBatch.Retry_Count__c = 14;
        gwMeSBatch.Retry_Interval__c = 2;
        gwMeSBatch.Test_Mode__c = true;
        gwMeSBatch.Time_out_limit__c = 120;
        insert gwMeSBatch;
        
        Credit_Card__c cc = new Credit_Card__c();
        cc.CC_City__c = 'Bedford';
        cc.CC_Country__c = 'US';
        cc.CC_State__c = 'NH';
        cc.CC_Street1__c = 'Apt 2';
        cc.CC_Street2__c = '123 Northeastern Blvd';
        cc.CC_First_Name__c = 'Visa';
        cc.CC_Last_Name__c = 'MeS';
        cc.CC_Postal_Code__c = '55555';
        cc.Credit_Card_Number__c = '3010'; //4012301230123010
        cc.Credit_Card_Type__c = 'Visa';
        cc.Expiry_Month__c = 5;
        cc.Expiry_Year__c = 2020;
        cc.Account__c = acc.Id;
        insert cc;

        Invoice__c inv = new Invoice__c();
        inv.Account__c = acc.Id;
        //inv.Invoice_Status__c = 'Open';
        insert inv;

        Payment__c payment = new Payment__c();
        payment.Payment_Method__c = 'Credit Card'; 
        payment.Payment_Gateway__c = 'MeS:Batch';
        payment.Payment_Amount__c = 1.01;
        payment.Status__c = 'Not Processed';
        payment.Credit_Card__c = cc.Id;
        payment.Invoice__c = inv.Id;
        payment.Account__c = acc.Id;
        payment.Payment_Date__c = System.today();
        insert payment;
        
        List<Payment__c> payments = [SELECT p.Credit_Card__r.Expiry_Month__c, p.Credit_Card__r.Expiry_Year__c,
            p.Credit_Card__r.CC_Postal_Code__c, p.Credit_Card__r.CC_Street2__c,
            p.Credit_Card__r.Credit_Card_Number__c, 
            p.Payment_Amount__c, p.Invoice__r.Name, p.Id, p.Merchant_Order_Number__c, p.Status__c,
            p.Payment_Date__c, p.Payment_Method__c, p.Invoice__r.Invoice_Status__c,
            p.Payment_Gateway__c, p.Has_Processed__c
            FROM Payment__c p
            Where p.Id = :payment.Id            
        ];
        /*
        System.debug('==='+payments);
        
        Datetime later = Datetime.now();
        later = later.addMinutes(1);
        String cronExpr = later.second() + ' ' + later.minute() + ' ' + later.hour() + ' * * ?';
        PG_MeSBatchUploaderSchedulable uploader = new PG_MeSBatchUploaderSchedulable();
        String cronTriggerID = System.schedule(PG_MeSBatchUploaderSchedulable.JOB_NAME, cronExpr, uploader);
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
            FROM CronTrigger 
            WHERE id = :cronTriggerID
        ];
        
        System.assertEquals(cronExpr, ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        */
        PG_MeSBatchUploaderSchedulable.scheduleThisAfter(0, 1, 0);
        Test.stopTest();
        
        
    }
    
    @isTest
    static void test_mes_batch_download_schedular(){
        
        Test.startTest();
        
        Account acc =  new Account();
        acc.Name = 'Test Account';
        acc.SLA__c = 'Bronze';
        acc.SLASerialNumber__c = '1234';
        acc.SLAExpirationDate__c = Date.today();
        insert acc;
        
        Gateway__c gwMeSBatch = new Gateway__c();
        gwMeSBatch.Name = 'MeS:Batch';
        gwMeSBatch.Gateway_Type__c = 'MeS';
        gwMeSBatch.Address_Verification__c = true;
        gwMeSBatch.Attach_Note__c = true;
        gwMeSBatch.ChikPeaO2B__Batch_Download_Interval__c = 1;
        gwMeSBatch.ChikPeaO2B__Batch_Upload_Interval__c = 1;
        gwMeSBatch.ChikPeaO2B__Batch_Res_File_Id__c = '';
        gwMeSBatch.Batch_Size__c = 1;   //made it in order tocreate less payment records
        gwMeSBatch.Capture_Card_Details__c = true;
        gwMeSBatch.Card_Code_Verification__c = true;
        gwMeSBatch.Currency__c = 'USD';
        gwMeSBatch.Gateway_Url_Prod__c = 'https://www.merchante-solutions.com';
        gwMeSBatch.Gateway_Url_Prod_Fallback__c = '';
        gwMeSBatch.Gateway_Url_Test__c = 'https://www.merchante-solutions.com';
        gwMeSBatch.Gateway_Url_Test_Fallback__c = '';
        gwMeSBatch.Login_Id__c = 'MESBATCHLOGINID';
        gwMeSBatch.Merchant_Reference__c = '';
        gwMeSBatch.Terminal_ID__c = '';
        //gwMeSBatch.O2B_Setting__c = '';
        gwMeSBatch.Partial_Authorization__c = false;
        gwMeSBatch.Password__c = 'MESBATCHPASSWORD';
        gwMeSBatch.Primary_Site_Available__c = false;
        gwMeSBatch.Retry_Count__c = 14;
        gwMeSBatch.Retry_Interval__c = 2;
        gwMeSBatch.Test_Mode__c = true;
        gwMeSBatch.Time_out_limit__c = 120;
        insert gwMeSBatch;
        
        Credit_Card__c cc = new Credit_Card__c();
        cc.CC_City__c = 'Bedford';
        cc.CC_Country__c = 'US';
        cc.CC_State__c = 'NH';
        cc.CC_Street1__c = 'Apt 2';
        cc.CC_Street2__c = '123 Northeastern Blvd';
        cc.CC_First_Name__c = 'Visa';
        cc.CC_Last_Name__c = 'MeS';
        cc.CC_Postal_Code__c = '55555';
        cc.Credit_Card_Number__c = '3010'; // 4012301230123010
        cc.Credit_Card_Type__c = 'Visa';
        cc.Expiry_Month__c = 5;
        cc.Expiry_Year__c = 2020;
        cc.Account__c = acc.Id;
        insert cc;

        Invoice__c inv = new Invoice__c();
        inv.Account__c = acc.Id;
        //inv.Invoice_Status__c = 'Open';
        insert inv;

        Payment__c payment = new Payment__c();
        payment.Payment_Method__c = 'Credit Card'; 
        payment.Payment_Gateway__c = 'MeS:Batch';
        payment.Payment_Amount__c = 1.01;
        payment.Status__c = 'Not Processed';
        payment.Credit_Card__c = cc.Id;
        payment.Invoice__c = inv.Id;
        payment.Account__c = acc.Id;
        payment.Payment_Date__c = System.today();
        insert payment;
        
        List<Payment__c> payments = [SELECT p.Credit_Card__r.Expiry_Month__c, p.Credit_Card__r.Expiry_Year__c,
            p.Credit_Card__r.CC_Postal_Code__c, p.Credit_Card__r.CC_Street2__c, 
            p.Credit_Card__r.Credit_Card_Number__c, 
            p.Payment_Amount__c, p.Invoice__r.Name, p.Id, p.Merchant_Order_Number__c, p.Status__c,
            p.Payment_Date__c, p.Payment_Method__c, p.Invoice__r.Invoice_Status__c,
            p.Payment_Gateway__c, p.Has_Processed__c
            FROM Payment__c p
            Where p.Id = :payment.Id            
        ];
        
        PG_MeSBatchDownloaderSchedulable.scheduleThisAfter(0, 1, 0);
        
        Test.stopTest();
    }
    
}