/*
** Class         :  itemImageUpload
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by	 :  Asitm (chikpea Inc.)
** Reason		 :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

public with sharing class itemImageUpload{
    public Blob contentFile{get;set;}
    public string nameFile{get;set;}
    public integer sizeFile{get;set;}
    public id itemid{get;set;}
    public boolean done{get;set;}
    public String contentType{get;set;}
    public itemImageUpload(){
        itemid=ApexPages.currentPage().getparameters().get('itmid');
        done=false;
    }
    public pagereference ImageUpload(){
        
        String NameOfTheFile=nameFile;
        if(NameOfTheFile!=null &&                         //#######Image file validation[23 different type]
           (NameOfTheFile.endswithignorecase('.ANI')||    //Windows animated cursor file format
            NameOfTheFile.endswithignorecase('.BMP')||    //Bitmap graphic files for Windows and OS/2
            NameOfTheFile.endswithignorecase('.CAL')||    //Windows calendar files
            NameOfTheFile.endswithignorecase('.FAX')||    //GFI fax image file
            NameOfTheFile.endswithignorecase('.GIF')||    //CompuServe Graphics Interchange File
            NameOfTheFile.endswithignorecase('.IMG')||    //Graphical Environment Manager (GEM) Image file
            NameOfTheFile.endswithignorecase('.JBG')||    //(JBIG) Joint Bi-level Image experts Group file
            NameOfTheFile.endswithignorecase('.JPE')||    //JPEG/JIFF Image file
            NameOfTheFile.endswithignorecase('.JPEG')||   //JPEG bitmap graphic file
            NameOfTheFile.endswithignorecase('.JPG')||    //JPEG/JIFF Image file
            NameOfTheFile.endswithignorecase('.MAC')||    //MacPaint image file
            NameOfTheFile.endswithignorecase('.PBM')||    //Portable BitMap image file
            NameOfTheFile.endswithignorecase('.PCD')||    //Kodak Photo-CD image file
            NameOfTheFile.endswithignorecase('.PCX')||    //RLE compressed ZSoft PC paintbrush bitmap file
            NameOfTheFile.endswithignorecase('.PCT')||    //MacIntosh PICT image file
            NameOfTheFile.endswithignorecase('.PGM')||    //Portable Greymap file
            NameOfTheFile.endswithignorecase('.PNG')||    //Portable Network Graphics bitmap graphic file
            NameOfTheFile.endswithignorecase('.PPM')||    //Portable Pixmap file
            NameOfTheFile.endswithignorecase('.PSD')||    //Adobe Photoshop bitmap file
            NameOfTheFile.endswithignorecase('.RAS')||    //Sun Raster image file
            NameOfTheFile.endswithignorecase('.TGA')||    //Targa bitmap
            NameOfTheFile.endswithignorecase('.TIFF')||   //Tag Image File Format bitmap file 
            NameOfTheFile.endswithignorecase('.WMF')      //Windows Meta File  
            )
          )
         {
             if(sizeFile<=(50*1024)){
                 try{
                     Attachment att=new Attachment();
                     att.Body=contentFile;//Blob.valueOf(nameFile);
                     att.Name=NameOfTheFile;
                     att.parentId=itemid;
                     att.contentType=contentType;
                     insert att;
                     done=true;
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.CONFIRM,'Image file successfully attached');
                     ApexPages.addMessage(errormsg);       
                 }
                 catch(Exception e)
                 {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,e.getmessage());
                     ApexPages.addMessage(errormsg);
                 }
             }
             else{
                 ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'file size is too big('+sizeFile/1024+'KB) ,maximum size can be 50KB');
                 ApexPages.addMessage(errormsg);
             }
             
         }
         else
         {
             ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Not a valid image file');
             ApexPages.addMessage(errormsg);
         }
        return null;
    }
}