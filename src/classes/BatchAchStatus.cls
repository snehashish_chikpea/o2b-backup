/*
** Class         :  BatchAchStatus
** Created by    :  Satya Chikpea
** Last Modified :  14 may 14
** Description   :  It is responsible for checking the status of Authorize.net.
*/

global with sharing class BatchAchStatus implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful
{
  global String query;
  global String email;
  global String errmsg;
  global boolean test_status_pending=false;
  global boolean test_status_failed=false;
  global boolean test_status_exception=false;
  global BatchAchStatus()
  {
    errmsg='';
  }

  global database.querylocator start(Database.BatchableContext BC)
  {
    //query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet\' AND ChikPeaO2B__Payment_Method__c=\'ACH\' AND ChikPeaO2B__Status__c=\'In Process\' AND ChikPeaO2B__Has_Processed__c=true';
    system.debug('----------------->query'+query);
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext BC, List<sObject> scope)
  {
   List<ChikPeaO2B__Payment__c> payment_list=new List<ChikPeaO2B__Payment__c>();
   Map<String, String> payment_map = new Map<String, String>();
   Map<String, String> resMap=new Map<String, String>();
   List<ChikPeaO2B__payment__c> pay_update_list=new List<ChikPeaO2B__payment__c>();
   List<Exception> exception_list=new List<Exception>();
   ChikPeaO2B__payment__c payment=null;
   for(sObject s:scope)
   {
     try
     {
      payment=(ChikPeaO2B__payment__c)s;
      payment_map.put('transId',payment.ChikPeaO2B__Transaction_Id__c); 
      payment_map.put('paymentGatewayName',payment.ChikPeaO2B__Payment_Gateway__c);
      payment_map.put('status',payment.ChikPeaO2B__Status__c );
      payment_map.put('paymentType',payment.ChikPeaO2B__Payment_Method__c);
     }
     catch(Exception e)
     {
      System.debug('Exception while puting record in payment_map:'+e);
      try{
        exception_list.add((Exception)e);
      }catch(Exception ex){
        
      }
     }
     System.debug('payment_map:'+payment_map);
     if(payment_map!=null && payment_map.isEmpty()!=true)
     {
       try
       {   
         System.debug('before calling---getTrx');
         resMap=PG_PaymentProcessorInvoker.getTrxDetail(payment_map);
         System.debug('resMap--------->>>>'+resMap);
       }
       catch(Exception e)
       {
         System.debug('Exception while sending status request:'+e);
         exception_list.add((Exception)e);
       }
      }
      if(resMap!=null && resMap.isEmpty()!=true)
      {
       try 
       {
          String settlementStatus=resMap.get('settlementStatus');
          System.debug('settlementStatus--------->>'+settlementStatus);
          System.debug('Payment Id------'+payment.Id);
          if(Test.isRunningTest())
          {
            if(test_status_pending)
            {
             settlementStatus='pending';
            }
            if(test_status_failed)
            {
             settlementStatus='failed';
            }
            if(test_status_exception)
            {
             settlementStatus='exception';
            }
          }
          if(settlementStatus.equals('settled'))
          {
            payment.ChikPeaO2B__Status__c='Processed';
          }
          else if(settlementStatus.equals('pending'))
          {
            payment.ChikPeaO2B__Status__c='In Process';
          }
          else if(settlementStatus.equals('failed'))
          {
            payment.ChikPeaO2B__Status__c='Failed';
          }
          else if(settlementStatus.equals('na'))
          {
            payment.ChikPeaO2B__Status__c='Submitted';
          }
          else
          {
            throw new BatchException('Status is not found.');
          }
          
          if(resMap.get('batchId')!=null && resMap.containsKey('batchId'))
          {
            System.debug('batchId*****'+resMap.get('batchId'));
            payment.ChikPeaO2B__Batch_ID__c=resMap.get('batchId');
          }
          
          pay_update_list.add(payment);
        }
        catch(Exception ex)
        {
          System.debug('Exception while matching the settlementstatus:'+ex);
          try{
            exception_list.add((Exception)ex);
          }catch(Exception e){
            
          }
        }
      }
   }
   if(pay_update_list!=null && pay_update_list.size()>0)
   {
     try
     {
       update(pay_update_list); 
       System.debug('Payment updated------->');
     }
     catch(Exception e)
     {
       System.debug('Exception while updating'+e);
       try{
        exception_list.add((Exception)e);
      }catch(Exception ex){
        
      }
     }
   }
   if(exception_list!=null && exception_list.size()>0)
   {
     ExceptionLogger.insertLog(exception_list, 'BatchAchStatus');
   }
  }
  global void finish(Database.BatchableContext BC)
  {
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   System.debug('email:'+ email);
   mail.setToAddresses(new String[] {email});
   mail.setSenderDisplayName('ACH Status Batch Process');
   mail.setSubject('ACH Status Batch Process Completed');
   mail.setPlainTextBody('ACH Status Batch Process body');
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
  }
}