global with sharing class BatchCreateBillRunSchedulerForBillGroup {
    
    global static string billGroupId='';
    Webservice static String executeschedule(string grpId)
    {
        //=== CURD check [START] ===//
        Map<string,string>BillGroupUpdateResultMap=new map<string,string>();
        BillGroupUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Bill_Group__c',FieldPermissionSet.BillGroupUpdate,'update');
        if(BillGroupUpdateResultMap.get('AllowDML')!=null && BillGroupUpdateResultMap.get('AllowDML').equalsignorecase('false'))
        {
            return null;//### Bill group update permission check
        }
        //=== CURD check [END] ===//
        String error;
        String sch;
        billGroupId= grpId;
        system.debug('------------------->billGroupId-->'+billGroupId);   
        BatchCreateBillScheduler BCBS=new BatchCreateBillScheduler();
        //list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Billing_Day__c,ChikPeaO2B__Schedule_Time__c from ChikPeaO2B__O2B_Setting__c limit 1];
        
        list<ChikPeaO2B__Bill_Group__c>  billGrpList=[SELECT id FROM ChikPeaO2B__Bill_Group__c WHERE ChikPeaO2B__isExecuting__c=true];
        /*
        if(!billGrpList.isEmpty()){
            error='Please try after some time,One Scheduler is inprocess. ';
            return error;
        }
        */
        
        billGrpList=[SELECT id,Name,ChikPeaO2B__Schedule_Time__c,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Billing_Day__c
        FROM ChikPeaO2B__Bill_Group__c WHERE Id=:grpId];
        if(billGrpList!=null && billGrpList.size()>0)
        {
            billGrpList[0].ChikPeaO2B__isExecuting__c=true;
            update billGrpList;
            if(billGrpList[0].ChikPeaO2B__Schedule_Time__c!=null && billGrpList[0].ChikPeaO2B__Schedule_Time__c!='')
            {
                List<String> ScheduleTime=billGrpList[0].ChikPeaO2B__Schedule_Time__c.split(':');
                if(billGrpList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
                    sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' '+(billGrpList[0].ChikPeaO2B__Billing_Day__c!=null?billGrpList[0].ChikPeaO2B__Billing_Day__c:'1')+' * ?';
                else
                    sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' * * ?';
            }    
            else
            {
                if(billGrpList[0].ChikPeaO2B__Schedule_Time__c=='Calendar Month')
                    sch='0 00 06 '+(billGrpList[0].ChikPeaO2B__Billing_Day__c!=null?billGrpList[0].ChikPeaO2B__Billing_Day__c:'1')+' * ?';
                else
                    sch='0 00 06 * * ?';
            }
            try{
                //sch='0 69 06 * * ?';
                string grpName=billGrpList[0].Name;
                system.schedule('Create Invoice Batch '+grpName,sch, BCBS);
                error='success';
            }
            catch(Exception e)
            {
                error=e.getMessage();
                //throw new BatchException('Some error occured in BatchCreateBillRunScheduler Class.',e);
            }
        }
        return error;  
    }
}