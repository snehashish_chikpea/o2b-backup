/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-24
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG {
    
    /**
    AUTH_CAPTURE
    AUTH_ONLY/VERIFY
    PRIOR_AUTH_CAPTURE
    FORCE_CAPTURE
    CREDIT/REFUND/RETURN
    CAPTURE_ONLY/MARK_FOR_CAPTURE
    VOID/REVERSAL
    INQUIRY
    */
    global enum TransactionType {
        VERIFY,//zero amnt AUTH_ONLY
        AUTHORIZATION_CAPTURE,
        AUTHORIZATION,//AUTH_ONLY
        PRIOR_AUTHORIZATION_CAPTURE,//CAPTURE_ONLY/MARK_FOR_CAPTURE/DELAYED_CAPTURE
        FORCE_CAPTURE,//CAPTURE_WITHOUT_AUTHORIZATION
        REFUND,//CREDIT/RETURN
        REVERSEAL,//VOID
        AUTHORIZATION_REVERSEAL,//VOID_AND_UNHOLD
        //Reporting
        INQUIRY
    }
    
    /**
    DEBIT/SALE
    CREDIT
    INQUIRY
    CANCEL/VOID
    PRENOTE
    */
    global enum TransactionTypeACH {
        DEBIT,
        CREDIT,
        INQUIRY,
        CANCEL,
        PRENOTE
    }
    /*
    global enum EcheckType {
        ARC,
        //Accounts Receivable Conversion, single-entry debit against customer?s checking account
        BOC,
        //Back Office Conversion, one-time charge transaction against a customer?s checking account
        CCD,
        //Cash Concentration or Disbursement, charge or refund against a business checking account.
        PPD,
        //Prearranged Payment and Deposit Entry, charge or refund against a customer?s checking or savings account. 
        TEL,
        //Telephone-Initiated Entry, one-time charge against a customer?s checking or savings account.
        WEB
        //Internet-Initiated Entry,One-time/recurring charge against a customer?s checking or savings account.
    }
    */
    global enum Status {
        SUCCESS,
        FAILURE,
        APPROVED,
        DECLINED,
        ERROR,
        HELD
    }
    
    //According to the code type list in Gateway response object
    global static final String GATEWAY_RESPONSE_CODE_TYPE_RESPONSE = 'Response';
    global static final String GATEWAY_RESPONSE_CODE_TYPE_AVS_RESPONSE = 'AVSResponse';
    global static final String GATEWAY_RESPONSE_CODE_TYPE_CCV_RESPONSE = 'CCVResponse';
    global static final String GATEWAY_RESPONSE_CODE_TYPE_PROCESS_STATUS = 'ProcessStatus';
    
    //According to the payment gateway list in Payment object
    global static final String GATEWAY_AUTHORIZE_DOT_NET = 'AuthNet';
    global static final String GATEWAY_PAY_PAL = 'PayPal';
    global static final String GATEWAY_PAY_FLOW_PRO = 'PayFlow';
    global static final String GATEWAY_CHASE_ORBITAL = 'ChasePT';
    global static final String GATEWAY_MERCHANT_E_SOLUTIONS = 'MeS';
    global static final String GATEWAY_CHASE_SPECTRUM = 'ChaseSpectrum';//not added yet
    global static final String GATEWAY_VANTIV = 'Vantiv';
    global static final String GATEWAY_IP = 'IP';    //Modified by Snehashish Roy on 15/06/2015
    global static final String GATEWAY_BLUESNAP = 'BlueSnap'; //Added by Snehashish Roy on 29/09/2016
    global static final String GATEWAY_CARDCONNECT = 'CardConnect'; //Added by Snehashish Roy on 21/11/2016
    
    
    //According to Credit Card Type List in Credit Card Object
    global static final String CARD_TYPE_MASTER_CARD = 'Master';
    global static final String CARD_TYPE_VISA = 'Visa';
    global static final String CARD_TYPE_DISCOVER = 'Discover';
    global static final String CARD_TYPE_AMERICAN_EXPRESS = 'American Express';
    
    global static final String HTTP_METHOD_POST = 'POST';
    global static final String HTTP_METHOD_GET = 'GET';
    global static final String HTTP_METHOD_HEAD = 'HEAD';
    global static final String HTTP_METHOD_PUT = 'PUT';
    global static final String HTTP_METHOD_DELETE = 'DELETE';
    
    global static final String PAYMENT_TYPE_CC = 'CC';
    global static final String PAYMENT_TYPE_ACH = 'ACH';

    global static Map<String, String> paymentGatewayTypes;
    //global static Map<String, String> reponseHandlerTypes;
    //global static Map<String, String> recommendedActions;
    
    static{
        paymentGatewayTypes = new Map<String, String>{
            GATEWAY_AUTHORIZE_DOT_NET => 'PG_AuthorizeNet', 
            GATEWAY_PAY_FLOW_PRO => 'PG_PayFlowPro',
            GATEWAY_CHASE_ORBITAL => 'PG_ChasePaymentech',
            GATEWAY_MERCHANT_E_SOLUTIONS => 'PG_MeS',
            GATEWAY_VANTIV => 'PG_Vantiv',
            GATEWAY_IP => 'PG_IPPayments',    //Modified by Snehashish Roy on 15/06/2015
            GATEWAY_BLUESNAP => 'PG_BlueSnap', // Added by Snehashish Roy on 29/09/2016
            GATEWAY_CARDCONNECT => 'PG_CardConnect' // Added by Snehashish Roy on 21/11/2016
        };
        /*
        reponseHandlerTypes = new Map<String, String>{
            GATEWAY_AUTHORIZE_DOT_NET => 'PG_AuthorizeNetHandler',
            GATEWAY_PAY_PAL => 'PG_PayPalHandler',
            GATEWAY_PAY_FLOW_PRO => 'PG_PayFlowProHandler',
            GATEWAY_CHASE_ORBITAL => 'PG_ChasePaymentechHandler'
        };
        //As in the Action list in Gateway response object
        
        recommendedActions = new Map<String, String>{
            'Call' => 'Call your Chase Paymentech Customer Service representative for assistance.',
            'Cust' => 'Try to resolve with customer or obtain alternate payment method.',
            'Fix' => 'There is an invalid value being sent. Fix and resend.',
            'None' => 'No action required.',
            'Resend' => 'Send this transaction back at any time.',
            'Voice' => 'Perform a voice authorization per instructions provided by Chase Paymentech.',
            'Wait' => 'Wait 2?3 days before resending or try to resolve with the customer.'
        };
        */
    }
    
    global static String getPaymentGatewayTypeName(String paymentGatewayType){
        /*
        if(paymentGatewayName.contains(GATEWAY_AUTHORIZE_DOT_NET))
            return paymentGatewayTypes.get(GATEWAY_AUTHORIZE_DOT_NET);
        if(paymentGatewayName.contains(GATEWAY_PAY_PAL))
            return paymentGatewayTypes.get(GATEWAY_PAY_PAL);
        if(paymentGatewayName.contains(GATEWAY_PAY_FLOW_PRO))
            return paymentGatewayTypes.get(GATEWAY_PAY_FLOW_PRO);
        if(paymentGatewayName.contains(GATEWAY_CHASE_ORBITAL))
            return paymentGatewayTypes.get(GATEWAY_CHASE_ORBITAL);
        if(paymentGatewayName.contains(GATEWAY_MERCHANT_E_SOLUTIONS))
            return paymentGatewayTypes.get(GATEWAY_MERCHANT_E_SOLUTIONS);
        */
        return paymentGatewayTypes.get(paymentGatewayType);
    }
    
    global static String mapAsString(Map<String, String> aMap){
        String str = '';
        for(String key : aMap.keySet()){
            str += key + '=' + aMap.get(key) + ', ';
        }
        return str.substring(0, str.length()-2);
    }
    
    /*
        
    leaveUnmask 
    0 : complete masking (no masking if pick = true)
    n: begining of the String
    -n : end of the String 
    n < l; l=lenght of String
    
    uses:
    PG.maskWord('News Letter', '', -6, true);//return last 6 char
    PG.maskWord('News Letter', '', 4, true);//return first 4 char
    PG.maskWord('News Letter', '', 0, true);//return all char
    
    PG.maskWord('News Letter', '', 0, false);//return all char masked X
    PG.maskWord('News Letter', '*', 0, false);//return all char masked *
    PG.maskWord('News Letter', '', -6, false);//return all masked except last 6 char
    PG.maskWord('News Letter', '', 4, false);//return all masked except first 4 char
    
    */
    global static String maskWord(String word, String maskChar, Integer leaveUnmask, Boolean pick ){
        
        if(word == null || word == '') return '';
                
        Integer startInd = 0, len = word.length(), endInd = len;
        String maskedWord = '';     
        if(pick){
            if(leaveUnmask == 0 || leaveUnmask > len || Math.abs(leaveUnmask) > len) 
                maskedWord = word;
            else if(leaveUnmask > 0){
                //endInd = leaveUnmask;
                maskedWord = word.substring(startInd, leaveUnmask);
            }else{// < 0
                //startInd = endInd + leaveUnmask;
                maskedWord = word.substring(endInd + leaveUnmask, endInd);
            }
        }else{
            if(maskChar == null || maskChar == '') maskChar = 'X';
            String mSection = '', oSection = '';
            Integer mSectionLen = 0;
            if(leaveUnmask == 0 || leaveUnmask > len || Math.abs(leaveUnmask) > len){
                mSectionLen = len;
            }else if(leaveUnmask > 0){
                mSectionLen = len - leaveUnmask;
                oSection = word.substring(startInd, leaveUnmask);
            }else{// < 0
                mSectionLen = len + leaveUnmask;
                oSection = word.substring(endInd + leaveUnmask, endInd);
            }
            for(Integer i=0; i<mSectionLen; i++)
                    mSection += maskChar;
            
            if(leaveUnmask == 0){
                maskedWord = mSection;
            }else if(leaveUnmask > 0){
                maskedWord = oSection + mSection;
            }else{
                maskedWord = mSection + oSection;
            }
        }
        
        return maskedWord;      
    }
    /*
    public static String getResponseHandlerName(String paymentGatewayName){
        return reponseHandlerTypes.get(paymentGatewayName);
    }
    */
    
    /**
    public static final String AUTHORIZE_DOT_NET_URL_TEST = 'https://test.authorize.net/gateway/transact.dll';
    public static final String AUTHORIZE_DOT_NET_URL_PROD = 'https://secure.authorize.net/gateway/transact.dll';
    
    public static final String PAY_PAL_URL_TEST = 'https://api-3t.sandbox.paypal.com/nvp';
    public static final String PAY_PAL_URL_PROD = 'https://api-3t.paypal.com/nvp';
    
    public static final String PAY_FLOW_PRO_URL_TEST = 'https://pilot-payflowpro.paypal.com';
    public static final String PAY_FLOW_PRO_URL_PROD = 'https://payflowpro.paypal.com';
    
    public static final String CHASE_PAYMENTECH_URL_TEST = '';
    public static final String CHASE_PAYMENTECH_URL_PROD = '';
    
    public static final String MERCHANT_E_SOLUTIONS_URL_TEST = 'https://cert.merchante-solutions.com/mes-api/tridentApi?';
    public static final String MERCHANT_E_SOLUTIONS_URL_PROD = 'https://cert.merchante-solutions.com/mes-api/tridentApi?';
    */
}