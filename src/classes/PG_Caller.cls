/**
----------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 2.0 2015-12-17 2013-09-23 
@see 
----------------------------------------------------------------------------------------------------
*/
global with sharing class PG_Caller {

    global Httprequest req { get; set; }
    global Httpresponse res { get; set; }
    global Integer resStatusCode { get; set; }
    global String resStatusMessage {get;set;} //Added by Snehashish Roy on 3 March, 2017
    global String reqDebug { get; set; } // partial log, in order to log securely
    
    global PG_Caller(   
        Map<String, String> httpHeader, 
        String httpMethod, 
        String endPoint, 
        String body, 
        Integer timeoutms){
        
        req = new Httprequest();
        req.setMethod(httpMethod);
        req.setEndpoint(endPoint);
        if(body != '')
            req.setBody(body);
        req.setTimeout(timeoutms);
        
        if(httpHeader != null && httpHeader.size() > 0){
            for(String key : httpHeader.keySet()){
                req.setHeader(key, httpHeader.get(key));
            }
        }
        
    }
    
    /*
    ------------------------------------------------------------------------------------------------
    @desc
    
    @param void - []
    @return String resBody []
    @throws PG_Exception []
    ------------------------------------------------------------------------------------------------
    */
    global String call(){//@throws PG_Exception
        System.debug('---httpRequestBody='+req.getBody());
        System.debug('---httpRequestBody='+reqDebug);
        String resBody = '';
        resStatusCode = -1;
        resStatusMessage='';
        try{
            Http http = new Http();
            if(!Test.isRunningTest()){
                res = http.send(req);
                resStatusCode = res.getStatusCode();
                resStatusMessage=res.getStatus();
            }else{
                res = new Httpresponse();
                resStatusCode = 200;
            }
        }catch(CalloutException ex){
            throw new PG_Exception('HttpCallOut Exception::'+ex.getMessage(), ex);
        }catch(SecurityException ex){
            throw new PG_Exception('HttpCallOut Exception::'+ex.getMessage(), ex);
        }catch(Exception ex){
            throw new PG_Exception('HttpCallOut Exception::'+ex.getMessage(), ex);
        }
        if(res != null && (resStatusCode == 200 || res.getBody()!=null) ){
            resBody = res.getBody();
        }else{
            throw new PG_Exception('HttpCallOut Exception::error response. Status code ='
                +resStatusCode+((resStatusMessage!=null && resStatusMessage!='')?('; Status Message= '+resStatusMessage):''));
        }
        System.debug('---httpResponseBody='+resBody);
        return resBody;
    }
    
    global static String generateQueryString(Boolean lastPart, Map<String, String> params){
        String queryString = '';
        for(String key : params.keySet()){
            queryString += key + '=' + params.get(key) + '&';
        }       
        return lastPart?queryString.removeEnd('&'):queryString;
    }
    
    global static String generateQueryStringComplete(Map<String, String> params){
        return generateQueryString(true, params);
    }

}