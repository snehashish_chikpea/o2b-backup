@isTest
public class TestAgreementOneBill
{
    @isTest
    static void testMethod1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false);
        insert(o2bset);
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly');
        insert(acc);
        contact con = new contact(accountid=acc.id,ChikPeaO2B__Role__c = 'Billing',LastName='NN');
        insert con;
        ChikPeaO2B__Agreement__c agree = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='Monthly',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree;
        ChikPeaO2B__Agreement__c agree1 = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='Annual',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree1;
        ChikPeaO2B__Agreement__c agree2 = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='Weekly',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree2;
        ChikPeaO2B__Agreement__c agree3 = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='Half Yearly',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree3;
        ChikPeaO2B__Agreement__c agree4 = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='Quarterly',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree4;
        
        ChikPeaO2B__Agreement__c agree5 = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='Daily',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree5;
        
        ChikPeaO2B__Agreement__c agree6 = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='12',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree6;
        
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Is_prorate__c=true);    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Is_prorate__c=true,ChikPeaO2B__Is_aggregation__c=true);    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true,ChikPeaO2B__Is_prorate__c=true);    
        itemlist.add(pre);
        
        insert(itemlist);
        ChikPeaO2B__Rate_Plan__c rp21=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Min_Amount__c=0,ChikPeaO2B__Min_usage__c=0,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=1,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp21);
        ChikPeaO2B__Rate_Plan__c rp23=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Min_Amount__c=10,ChikPeaO2B__Min_usage__c=0,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=1,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp23);
        ChikPeaO2B__Rate_Plan__c rp22=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Min_Amount__c=10,ChikPeaO2B__Min_usage__c=0,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=1,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp22);
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp5=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Min_Amount__c=10,ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add (rp5);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        List<ChikPeaO2B__Subscription__c>SubList=new List<ChikPeaO2B__Subscription__c>();
        ChikPeaO2B__Subscription__c sub9_1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=null,ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9_1);
        ChikPeaO2B__Subscription__c sub9_2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree1.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=acc.ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=acc.ChikPeaO2B__Next_Bill_Date__c.adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9_2);
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree2.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1);
        ChikPeaO2B__Subscription__c sub1_1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree3.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-25),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1_1);
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree4.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-5),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub2);
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree5.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(15),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub3);
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(45),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub4);
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree6.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub5);
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(5),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub6);
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-3),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub7);
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(70),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub8);
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9);
        ChikPeaO2B__Subscription__c sub10=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-15),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-10),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub10);
        ChikPeaO2B__Subscription__c sub11=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-15),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-10),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub11);
        ChikPeaO2B__Subscription__c sub12=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-15),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-10),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub12);
        ChikPeaO2B__Subscription__c sub13=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-65),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-10),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub13);
        ChikPeaO2B__Subscription__c sub14=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(10),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(120),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c= rp21.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub14);
        ChikPeaO2B__Subscription__c sub15=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(10),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(120),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c= rp22.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub15);
        ChikPeaO2B__Subscription__c sub16=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today(),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(120),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c= rp23.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub16);
        ChikPeaO2B__Subscription__c sub17=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today(),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(120),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c= rp23.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub17);
        ChikPeaO2B__Subscription__c sub18=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(40),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(120),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c= rp23.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub18);
        ChikPeaO2B__Subscription__c sub26=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(40),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(120),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c= rp23.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub26);
        ChikPeaO2B__Subscription__c sub27=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(40),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(120),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c= rp23.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Aggregate_usage__c=100);
        SubList.add(sub27);
        insert(SubList);
        
        list<ChikPeaO2B__usage__c> ulist = new list<ChikPeaO2B__usage__c>();
        ChikPeaO2B__usage__c usg = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=10,ChikPeaO2B__Rate__c=2,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub11.id);        
        ulist.add(usg);
        ChikPeaO2B__usage__c usg1 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=2,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub12.id);        
        ulist.add(usg1);
        ChikPeaO2B__usage__c usg2 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=2,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp21.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub14.id);        
        ulist.add(usg2);
        ChikPeaO2B__usage__c usg3 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=2,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp22.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub15.id);        
        ulist.add(usg3);
        ChikPeaO2B__usage__c usg4 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=2,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp22.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub17.id);        
        ulist.add(usg4);
        ChikPeaO2B__usage__c usg5 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=0,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp23.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub16.id);        
        ulist.add(usg5);
        ChikPeaO2B__usage__c usg7 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=0,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp23.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub18.id);        
        ulist.add(usg7);
        ChikPeaO2B__usage__c usg8 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=2,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp23.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub26.id);        
        ulist.add(usg8);
        ChikPeaO2B__usage__c usg9 = new ChikPeaO2B__usage__c(ChikPeaO2B__Total_usage__c=110,ChikPeaO2B__Rate__c=0,ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Rate_Plan__c=rp23.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Type__c='Postpaid',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Subscription__c=sub27.id);        
        ulist.add(usg9);
        insert ulist;
        
        List<id>agreeList=new List<id>();
        //for(ChikPeaO2B__Agreement__c ag : [select id from ChikPeaO2B__Agreement__c where ChikPeaO2B__Account__c =: acc.id]){
          agreeList.add(acc.id);      
        //}
        /*agreeList.add(agree.id);
        agreeList.add(agree1.id);
        agreeList.add(agree2.id);
        agreeList.add(agree3.id);
        agreeList.add(agree4.id);
        agreeList.add(agree5.id);
        agreeList.add(agree6.id);*/
        AgreementOneBill.generateAgreementOneBill(agreeList,false);
        
    }
    
    @isTest
    static void testMethod2()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false);
        insert(o2bset);
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        ChikPeaO2B__Agreement__c agree = new ChikPeaO2B__Agreement__c(name='ag1',ChikPeaO2B__Status__c='new',ChikPeaO2B__Start_Date__c=date.today().addmonths(-1),ChikPeaO2B__Account__c=acc.id,Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=date.today());
        insert agree;
        
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Is_aggregation__c=true,ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Is_aggregation__c=true,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5,ChikPeaO2B__Usage_Rate__c=0.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        List<usage__c>usglist=[select id,ChikPeaO2B__Total_Usage__c,subscription__c from usage__c where ChikPeaO2B__Type__c='Postpaid'];
        for(usage__c usg : usglist)
        {
            usg.ChikPeaO2B__Total_Usage__c=101;
        }
        Update(usglist);
        list<subscription__c>sublist1=[select id,ChikPeaO2B__Aggregate_Usage__c from ChikPeaO2B__Subscription__c];
        for(subscription__c sub : sublist1)
        {
            sub.ChikPeaO2B__Aggregate_Usage__c=202;
        }
        update(sublist1);
        List<ChikPeaO2B__Subscription__c>SubList=new List<ChikPeaO2B__Subscription__c>();
        ChikPeaO2B__Subscription__c sub9_1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=null,ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9_1);
        ChikPeaO2B__Subscription__c sub9_2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=acc.ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=acc.ChikPeaO2B__Next_Bill_Date__c.adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9_2);
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1);
        ChikPeaO2B__Subscription__c sub1_1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-25),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1_1);
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-5),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub2);
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(15),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub3);
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(45),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub4);
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub5);
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(5),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub6);
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-3),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub7);
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(70),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub8);
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Agreement__c = agree.id,ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9);
        insert(SubList);
        List<id>agreeList=new List<id>();
        agreeList.add(agree.id);
        AgreementOneBill.generateAgreementOneBill(agreeList,false);
        
    }
    
}