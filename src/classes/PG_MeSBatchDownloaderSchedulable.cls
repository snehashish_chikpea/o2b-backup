/**
----------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-12-11
@see 
----------------------------------------------------------------------------------------------------
*/
global with sharing class PG_MeSBatchDownloaderSchedulable implements Schedulable {

    public static final String JOB_NAME = 'MeS Batch File Downloader';
    public static final String PAYMENT_GATEWAY = 'MeS:Batch';
    public static final String DOWNLOAD_PATH = '/srv/api/bpDownload';

    global void execute(SchedulableContext SC) {
        String cronTriggerId = SC.getTriggerId();
        download(cronTriggerId);
    }

    @future(callout=true)
    static void download(String cronTriggerId){
        //TODO: catch and log exceptions in log object
        List<Payment__c> payments = null;
        //TODO: retrive response file Id
        PG_MeSBatch mesBat = new PG_MeSBatch();
        mesBat.init(payments, PAYMENT_GATEWAY);
        String resFileId = mesBat.getConfig().batchResFieldId;
        Integer batchUploadInterval = mesBat.getConfig().batchUploadInterval;
        mesBat.downloadFile(DOWNLOAD_PATH, resFileId);

        //Abort scheduled download job and schedule upload job immidiately
        System.abortJob(cronTriggerId);
        PG_MeSBatchUploaderSchedulable.scheduleThisAfter(0, batchUploadInterval, 0);//after 5 min
    }

    /*
        PG_MeSBatchDownloaderSchedulable.scheduleThisAfter(0, 1, 0);
    */
    //global static void scheduleThisAfter(Integer seconds, Integer minutes, Integer hours, String resFileId){
    global static void scheduleThisAfter(Integer seconds, Integer minutes, Integer hours){
        
        Datetime later = Datetime.now();
        if(hours > 0) later = later.addHours(hours);
        if(minutes > 0) later = later.addMinutes(minutes);
        if(seconds > 0) later = later.addSeconds(seconds);
        String CRON_EXPR = later.second() + ' ' + later.minute() + ' ' + later.hour() + ' * * ?';
        System.debug('---CRON_EXPR='+CRON_EXPR);
        PG_MeSBatchDownloaderSchedulable downloader = new PG_MeSBatchDownloaderSchedulable();
        //downloader.resFileId = resFileId;
        String cronTriggerID = System.schedule(PG_MeSBatchDownloaderSchedulable.JOB_NAME, CRON_EXPR, downloader);
        System.debug('---cron trigger Id = '+cronTriggerID);

        //Save the cron Trigger Id in Gateway Object Download Job Id
        /*List<ChikPeaO2B__Gateway__c> gateways = [Select c.Name, c.Id, 
            c.ChikPeaO2B__Download_Job_Id__c
            From ChikPeaO2B__Gateway__c c 
            Where c.Name = :PAYMENT_GATEWAY Limit 1];
        if(gateways != null && !gateways.isEmpty()){
            ChikPeaO2B__Gateway__c gateway = gateways[0];
            gateway.ChikPeaO2B__Upload_Job_Id__c = cronTriggerID;
            update gateway;
        }*/
    }
}