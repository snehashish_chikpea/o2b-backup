@isTest
global class TestPG_IPPaymentsServiceMock_Void implements WebServiceMock {
  
  global void doInvoke(Object stub,
                 Object request,
                 Map<String, Object> response,
                 String endpoint,
                 String soapAction,
                 String requestName,
                 String responseNS,
                 String responseName,
                 String responseType
  ){
        PG_IPPaymentsService.SubmitSingleVoid_element submitSingleVoid_element=new PG_IPPaymentsService.SubmitSingleVoid_element();
        submitSingleVoid_element.trnXML='trnXML';
        
        PG_IPPaymentsService.SubmitSingleCapture_element submitSingleCapture_element=new PG_IPPaymentsService.SubmitSingleCapture_element();
        submitSingleCapture_element.trnXML='trnXML';
       
        PG_IPPaymentsService.SubmitSingleRefund_element submitSingleRefund_element=new PG_IPPaymentsService.SubmitSingleRefund_element();
        submitSingleRefund_element.trnXML='trnXML';
        
        PG_IPPaymentsService.SubmitSinglePayment_element submitSinglePayment_element=new PG_IPPaymentsService.SubmitSinglePayment_element();
        submitSinglePayment_element.trnXML='trnXML';
        
        
        PG_IPPaymentsService.SubmitSingleRefundResponse_element submitSingleRefundResponse_element=new PG_IPPaymentsService.SubmitSingleRefundResponse_element();
        submitSingleRefundResponse_element.SubmitSingleRefundResult='SubmitSingleRefundResult';
        
        PG_IPPaymentsService.SubmitSinglePaymentResponse_element submitSinglePaymentResponse_element=new PG_IPPaymentsService.SubmitSinglePaymentResponse_element();
        submitSinglePaymentResponse_element.SubmitSinglePaymentResult='SubmitSinglePaymentResult';
        
        PG_IPPaymentsService.SubmitSingleCaptureResponse_element submitSingleCaptureResponse_element=new PG_IPPaymentsService.SubmitSingleCaptureResponse_element();
        submitSingleCaptureResponse_element.SubmitSingleCaptureResult='SubmitSingleCaptureResult';
        
        PG_IPPaymentsService.SubmitSingleVoidResponse_element submitSingleVoidResponse_element=new PG_IPPaymentsService.SubmitSingleVoidResponse_element();
        submitSingleVoidResponse_element.SubmitSingleVoidResult='SubmitSingleVoidResult';
        
        /*response.put('response_x', submitSingleRefundResponse_element); 
        response.put('response_x', submitSinglePaymentResponse_element);
        response.put('response_x', submitSingleCaptureResponse_element);*/
        response.put('response_x', submitSingleVoidResponse_element);
  }
}