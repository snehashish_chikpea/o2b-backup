/*
** Class         :  AnniversarySubscriptionBilling
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  10 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
** Last Modified :  19 May, 2017 by Snehashish Roy
** Description   :  Reverse Billing Date Fix.
*/

global with sharing class AnniversarySubscriptionBilling{
    Webservice static List<Id> generateSubscriptionBill(List<Id> accountIds,boolean isbatch){
        List<Invoice__c> invList = new List<Invoice__c>();
        Map<Id,List<Purchase__c>> purListMap = new Map<Id,List<Purchase__c>>();
        Map<Id,List<Subscription__c>> subListMap = new Map<Id,List<Subscription__c>>();
        Map<Id,List<Invoice_Line__c>> invLineMap = new Map<Id,List<Invoice_Line__c>>();
        List<ChikPeaO2B__Subscription__c> subAllList = new List<ChikPeaO2B__Subscription__c>();
        List<Subscription__c> stopSubList = new List<Subscription__c>();
        List<Id> returnInvIds = new List<Id>();
        set<id>SubIds=new set<id>();
        Map<id,List<Invoice_Line_History__c>>InvLineHistory=new Map<id,List<Invoice_Line_History__c>>();
        Map<id,List<Invoice_Line_History__c>>InvLineHistoryUpdate=new Map<id,List<Invoice_Line_History__c>>();
        
        ChikPeaO2B__O2B_Setting__c[] o2bSet = [Select Id, ChikPeaO2B__Default_Gateway__c, ChikPeaO2B__Default_Gateway__r.Name, ChikPeaO2B__Line_Payment__c from ChikPeaO2B__O2B_Setting__c limit 1];
        List<Account> accList = [Select ChikPeaO2B__Next_Bill_Date__c, Id, Name, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity, ChikPeaO2B__Bill_Cycle__c, ChikPeaO2B__Amount_Remaining__c, ChikPeaO2B__Amount_Due__c, Active__c, ChikPeaO2B__Activation_Date__c,
         ChikPeaO2B__Payment_Term__c, ChikPeaO2B__Auto_Payment__c, (Select Id, ChikPeaO2B__Role__c From Contacts where ChikPeaO2B__Role__c = 'Billing' limit 1), 
         (Select Id, ChikPeaO2B__Item__c, ChikPeaO2B__Sell_Price__c, ChikPeaO2B__Qty__c, ChikPeaO2B__Invoiced__c, ChikPeaO2B__Tax_Rate__c, ChikPeaO2B__Invoice__c, ChikPeaO2B__Order_Line__c,ChikPeaO2B__Description__c From ChikPeaO2B__Purchases__r where ChikPeaO2B__Invoiced__c = false),
         (Select Id, Name, ChikPeaO2B__Item__c,ChikPeaO2B__Item__r.ChikPeaO2B__Is_prorate__c, ChikPeaO2B__Recurring_Charge__c, ChikPeaO2B__Billing_Started__c, 
            ChikPeaO2B__Next_Bill_Date__c, ChikPeaO2B__Bill_Cycle__c, ChikPeaO2B__Order_Line__c, ChikPeaO2B__Quantity__c, ChikPeaO2B__Billing_Start_Date__c, 
            ChikPeaO2B__Billing_Stop_Date__c,ChikPeaO2B__Description__c, 
            ChikPeaO2B__Billing_Stopped__c, ChikPeaO2B__Discount__c, ChikPeaO2B__Invoice__c,ChikPeaO2B__Billing_Stop_Reason__c From ChikPeaO2B__Subscriptions__r where 
            (ChikPeaO2B__Billing_Stop_Date__c= null or ChikPeaO2B__Billing_Stopped__c= false) and 
            (ChikPeaO2B__Next_Bill_Date__c= :Date.today() 
            //or ChikPeaO2B__Billing_Stop_Date__c= :Date.today() Commented by Snehashish Roy on 19 May, 2017 as BillingStop is done in each section
            )
         and ChikPeaO2B__Bill_Cycle__c!= null and ChikPeaO2B__Billing_Start_Date__c<= :Date.today()) From Account where Id IN :accountIds];
    
        for(Account a:accList)
        {
            for(Subscription__c sub : a.Subscriptions__r)
                SubIds.add(sub.id);
        }
        
        for(Subscription__c sub : [SELECT id,(SELECT id,name,ChikPeaO2B__Item__c,ChikPeaO2B__Line_Type__c,ChikPeaO2B__Period_From__c,ChikPeaO2B__Period_To__c,ChikPeaO2B__Qty__c,ChikPeaO2B__Unit_Rate__c from Invoice_Line_History__r where ChikPeaO2B__Invoice_Required__c='Yes')from Subscription__c where id in : SubIds])
        {
            InvLineHistory.put(sub.id,sub.Invoice_Line_History__r);
        }
        for(Account a:accList){

            List<Subscription__c> subList = a.Subscriptions__r;
            List<Purchase__c> purList = a.Purchases__r;
            
            if ( ( (subList != null) && (subList.size() > 0) ) || (purList != null && purList.size() > 0) )
            {   
                
                Invoice__c inv = new Invoice__c();
                inv.Account__c = a.Id;
                inv.Bill_Date__c = date.today();
                inv.ChikPeaO2B__Accounting_Period__c=(date.newinstance(date.today().year(), date.today().month(),1).addmonths(1))-1;//last day of current month, Added By Spal 21-DEC-2014
                if(a.Contacts != null && a.Contacts.size()>0)
                    inv.Billing_Contact__c = a.Contacts[0].Id;
                inv.ChikPeaO2B__Open_Balance__c= a.ChikPeaO2B__Amount_Remaining__c;
                inv.Payment_Term__c = a.Payment_Term__c;
                inv.Billing_Street__c = a.BillingStreet;
                inv.Billing_City__c = ((a.BillingCity == null || a.BillingCity.length()<30) ? a.BillingCity : a.BillingCity.substring(0,30) );
                inv.ChikPeaO2B__Billing_State_Province__c= a.BillingState;
                inv.ChikPeaO2B__Billing_Zip_Postal_Code__c= a.BillingPostalCode;
                inv.ChikPeaO2B__Billing_Country__c= a.BillingCountry;
                
                invList.add(inv);
            }
            
            List<Invoice_Line__c> invcLineList = new List<Invoice_Line__c>();
            List<Purchase__c> purUpdateList = new List<Purchase__c>();
            List<Subscription__c> subUpdateList = new List<Subscription__c>();
            
            if (purList != null && purList.size() > 0)
            {
                for(Purchase__c pur:purList){
                    Invoice_Line__c invcl = new Invoice_Line__c();
                    invcl.ChikPeaO2B__Line_Type__c= 'Purchase';
                    invcl.ChikPeaO2B__Item__c= pur.Item__c;
                    invcl.Unit_Rate__c = pur.Sell_Price__c;
                    invcl.Qty__c = pur.Qty__c;
                    invcl.Tax_Rate__c = pur.Tax_Rate__c;
                    invcl.Description__c = pur.Description__c;
                    invcl.ChikPeaO2B__Purchase__c= pur.Id;
                    invcLineList.add(invcl);
                    
                    pur.Invoiced__c = true;
                    pur.Refunded__c=false; // edited by Debajyoti M. to ensure successfull billing of a refunded purchase. 
                    purUpdateList.add(pur);
                }
                purListMap.put(a.Id,purUpdateList);
            }
            System.debug('PURCHASES>>>' + invcLineList);

            if (subList != null && subList.size() > 0)
            {
                for(Subscription__c sub:subList){
                    System.debug('sub stop reason--->'+sub.ChikPeaO2B__Billing_Stop_Reason__c);
                    List<ChikPeaO2B__Invoice_Line_History__c>InvlHisUpdateList=new List<ChikPeaO2B__Invoice_Line_History__c>();
                    
                    if(sub.ChikPeaO2B__Billing_Stop_Date__c==sub.ChikPeaO2B__Next_Bill_Date__c-1 && sub.ChikPeaO2B__Billing_Started__c) //Added by Snehashish Roy on 19 May, 2017 (Subscription is stopped if stop date is 1 day before next bill date)
                    {
                        sub.ChikPeaO2B__Billing_Stopped__c=true;
                        if (sub.ChikPeaO2B__Billing_Stop_Reason__c== null){
                            System.debug('Inside Stop REason Condition---->'+sub.ChikPeaO2B__Billing_Stop_Reason__c);
                            sub.ChikPeaO2B__Billing_Stop_Reason__c= 'Expired';
                        }
                    }
                    else
                    {
                        Invoice_Line__c invcl = new Invoice_Line__c();
                        invcl.line_type__c = 'Subscription';
                        invcl.Item__c = sub.Item__c;
                        
                        integer daysGapNextAndStrt=sub.ChikPeaO2B__Next_Bill_Date__c.daysBetween(sub.ChikPeaO2B__Billing_Start_Date__c);
                        system.debug('==1'+daysGapNextAndStrt);
                        date onebillcyclegapdate;
                        integer daysgaponebillcycle;
                        date billfrom;
                        date BillToOneBillCycle;
                        integer BillingDays;
                        if(daysGapNextAndStrt<0)//start date < next bill date
                        {
                            if(sub.ChikPeaO2B__Bill_Cycle__c=='Annual')
                                onebillcyclegapdate=sub.ChikPeaO2B__Next_Bill_Date__c.addyears(-1);
                            else if(sub.ChikPeaO2B__Bill_Cycle__c=='Monthly')
                                onebillcyclegapdate=sub.ChikPeaO2B__Next_Bill_Date__c.addmonths(-1);
                            else if(sub.ChikPeaO2B__Bill_Cycle__c=='Daily')
                                onebillcyclegapdate=sub.ChikPeaO2B__Next_Bill_Date__c.adddays(-1);
                            else if(sub.ChikPeaO2B__Bill_Cycle__c=='Quarterly') 
                                onebillcyclegapdate=sub.ChikPeaO2B__Next_Bill_Date__c.addmonths(-3);
                            else if(sub.ChikPeaO2B__Bill_Cycle__c=='Weekly')
                                onebillcyclegapdate=sub.ChikPeaO2B__Next_Bill_Date__c.adddays(-7);
                            else if(sub.ChikPeaO2B__Bill_Cycle__c=='Half Yearly')
                                onebillcyclegapdate=sub.ChikPeaO2B__Next_Bill_Date__c.addmonths(-6);
                            else
                                onebillcyclegapdate=sub.ChikPeaO2B__Next_Bill_Date__c.adddays(-integer.valueof(sub.ChikPeaO2B__Bill_Cycle__c));
                                
                            daysgaponebillcycle=sub.ChikPeaO2B__Next_Bill_Date__c.daysBetween(onebillcyclegapdate);
                            system.debug('==2'+daysgaponebillcycle);
                            daysGapNextAndStrt*=-1;
                            daysgaponebillcycle*=-1;
                            if(daysGapNextAndStrt<daysgaponebillcycle && !sub.ChikPeaO2B__Billing_Started__c)//start date less than one bill cycle behind
                            {
                                billfrom=sub.ChikPeaO2B__Billing_Start_Date__c;   
                            }
                            else// start date more than one bill cycle behind
                            {
                                billfrom=sub.ChikPeaO2B__Next_Bill_Date__c;
                            }
                            system.debug('==3'+billfrom);
                        }        
                        else if(daysGapNextAndStrt==0)// start date == next bill date
                        {
                            billfrom=sub.ChikPeaO2B__Next_Bill_Date__c;//bill period from = subscription's next bill date
                        }
                        //BillToOneBillCycle set to one bill cycle after billfrom date
                        if(sub.ChikPeaO2B__Bill_Cycle__c=='Annual')
                            BillToOneBillCycle=billfrom.addyears(1)-1;
                        else if(sub.ChikPeaO2B__Bill_Cycle__c=='Monthly')
                            BillToOneBillCycle=billfrom.addmonths(1)-1;
                        else if(sub.ChikPeaO2B__Bill_Cycle__c=='Daily')
                            BillToOneBillCycle= billfrom.adddays(1)-1;
                        else if(sub.ChikPeaO2B__Bill_Cycle__c=='Quarterly')
                            BillToOneBillCycle=billfrom.addmonths(3)-1;
                        else if(sub.ChikPeaO2B__Bill_Cycle__c=='Weekly')
                            BillToOneBillCycle=billfrom.adddays(7)-1;
                        else if(sub.ChikPeaO2B__Bill_Cycle__c=='Half Yearly')
                            BillToOneBillCycle=billfrom.addmonths(6)-1;               
                        else
                            BillToOneBillCycle=billfrom.adddays(integer.valueof(sub.ChikPeaO2B__Bill_Cycle__c))-1;
                        
                        BillingDays=billfrom.daysBetween(BillToOneBillCycle);
                        system.debug('==4'+BillingDays);
                        integer DaysGapStopdateAndTodate;
                        if(sub.ChikPeaO2B__Billing_Stop_Date__c==null)
                        {
                            invcl.ChikPeaO2B__Unit_Rate__c=sub.ChikPeaO2B__Recurring_Charge__c;
                            invcl.ChikPeaO2B__Period_From__c=billfrom;
                            invcl.ChikPeaO2B__Period_To__c=BillToOneBillCycle;
                            
                        }
                        else//billing stop date exist
                        {
                            DaysGapStopdateAndTodate=BillToOneBillCycle.daysBetween(sub.ChikPeaO2B__Billing_Stop_Date__c);
                            system.debug('DaysGapStopdateAndTodate------>'+DaysGapStopdateAndTodate);
                            System.debug('BillToOneBillCycle----->'+BillToOneBillCycle);
                            System.debug('BillingDays----->'+BillingDays);
                            if(DaysGapStopdateAndTodate>0)//stop date > BillToOneBillCycle
                            {
                                invcl.ChikPeaO2B__Unit_Rate__c=sub.ChikPeaO2B__Recurring_Charge__c;
                                invcl.ChikPeaO2B__Period_From__c=billfrom;
                                invcl.ChikPeaO2B__Period_To__c=BillToOneBillCycle;    
                            }
                            else//stop date < BillToOneBillCycle//PRO RATE
                            {
                                
                                if(BillingDays>(DaysGapStopdateAndTodate*-1))//stop date>from date and stopdate<onebillcycletodate
                                {
                                    if(sub.ChikPeaO2B__Item__r.ChikPeaO2B__Is_prorate__c)//if PRO RATE true
                                    {
                                        invcl.ChikPeaO2B__Unit_Rate__c=(sub.ChikPeaO2B__Recurring_Charge__c/(BillingDays+1))*((BillingDays+1)+DaysGapStopdateAndTodate);//DaysGapStopdateAndTodate will be -ve    
                                        system.debug('reccharge-->'+sub.ChikPeaO2B__Recurring_Charge__c);
                                        system.debug('billingdays-->'+BillingDays+'DaysGapStopdateAndTodate-->'+DaysGapStopdateAndTodate);
                                    }
                                    else
                                        invcl.ChikPeaO2B__Unit_Rate__c=sub.ChikPeaO2B__Recurring_Charge__c;
                                    invcl.ChikPeaO2B__Period_From__c=billfrom;
                                    invcl.ChikPeaO2B__Period_To__c=sub.ChikPeaO2B__Billing_Stop_Date__c;
                                    
                                    //stop
                                    sub.ChikPeaO2B__Billing_Stopped__c=true; //Added by Snehashish Roy on 19 May, 2017
                                    if (sub.ChikPeaO2B__Billing_Stop_Reason__c== null){
                                        System.debug('Inside Stop REason Condition---->'+sub.ChikPeaO2B__Billing_Stop_Reason__c);
                                        sub.ChikPeaO2B__Billing_Stop_Reason__c= 'Expired';
                                    }
                                }
                                else// for -ve invoice , stop date< from date , stop date in past
                                {
                                    if(sub.ChikPeaO2B__Billing_Started__c)
                                    {
                                        if(sub.ChikPeaO2B__Item__r.ChikPeaO2B__Is_prorate__c)//if PRO RATE true
                                        {
                                            //if(sub.ChikPeaO2B__Billing_Stop_Date__c.daysbetween(sub.ChikPeaO2B__Next_Bill_Date__c)>1) // Condition added by Snehashish Roy on 17 Feb 2017, for reverse billing last day check
                                            //{
                                                invcl.ChikPeaO2B__Unit_Rate__c=-(sub.ChikPeaO2B__Recurring_Charge__c/(BillingDays+1))*((sub.ChikPeaO2B__Billing_Stop_Date__c.adddays(1)).daysbetween(billfrom));//((BillingDays+1)+DaysGapStopdateAndTodate);//DaysGapStopdateAndTodate will be -ve    
                                                System.debug('daysBetween---->'+sub.ChikPeaO2B__Billing_Stop_Date__c.daysbetween(sub.ChikPeaO2B__Next_Bill_Date__c));
                                            //}
                                        }
                                        else
                                            invcl.ChikPeaO2B__Unit_Rate__c=0;//its a invalid condition,if prorate false then its set to 0(Zero)
                                        system.debug('BillingDays==>'+BillingDays+'DaysGapStopdateAndTodate==>'+DaysGapStopdateAndTodate);
                                        if(sub.ChikPeaO2B__Billing_Stop_Date__c==billfrom)// Added by Snehashish Roy on 19 May,2017 (If Next Bill Date = Stop Date, Bill is created for 1 Day, otherwise it results in periodfrom=today+1, periodto=today-1)
                                        {
                                            invcl.ChikPeaO2B__Period_From__c=billfrom;
                                            invcl.ChikPeaO2B__Period_To__c=sub.ChikPeaO2B__Billing_Stop_Date__c;
                                            if(!sub.ChikPeaO2B__Item__r.ChikPeaO2B__Is_prorate__c)//if Not PRO RATE true
                                                invcl.ChikPeaO2B__Unit_Rate__c=sub.ChikPeaO2B__Recurring_Charge__c;//Added by Snehashish Roy on 25 May, 2017
                                        }
                                        else //Added by Snehashish Roy on 19 May, 2017, to perform existing function of reverse billing other than aforementioned condition
                                        {
                                            invcl.ChikPeaO2B__Period_From__c=sub.ChikPeaO2B__Billing_Stop_Date__c.adddays(1);
                                            invcl.ChikPeaO2B__Period_To__c=billfrom-1;
                                        }
                                        //stop
                                        sub.ChikPeaO2B__Billing_Stopped__c=true; //Added by Snehashish Roy on 19 May, 2017
                                        if (sub.ChikPeaO2B__Billing_Stop_Reason__c == null){
                                            System.debug('Inside Stop REason Condition---->'+sub.ChikPeaO2B__Billing_Stop_Reason__c);
                                            sub.ChikPeaO2B__Billing_Stop_Reason__c= 'Expired';
                                        }
                                    }
                                    else //start date=stop date and billing started=false
                                    {
                                        if(sub.ChikPeaO2B__Item__r.ChikPeaO2B__Is_prorate__c)//if PRO RATE true
                                            invcl.ChikPeaO2B__Unit_Rate__c=(sub.ChikPeaO2B__Recurring_Charge__c/(BillingDays+1))*(sub.ChikPeaO2B__Billing_Start_Date__c.daysBetween(sub.ChikPeaO2B__Billing_Stop_Date__c)+1);//DaysGapStopdateAndTodate will be -ve    
                                        else
                                            invcl.ChikPeaO2B__Unit_Rate__c=sub.ChikPeaO2B__Recurring_Charge__c;//Changed from 0 to ChikPeaO2B__Recurring_Charge__c by Snehashish Roy on 25 May, 2017
                                        invcl.ChikPeaO2B__Period_From__c=sub.ChikPeaO2B__Billing_Start_Date__c;
                                        invcl.ChikPeaO2B__Period_To__c=sub.ChikPeaO2B__Billing_Stop_Date__c;  
                                        
                                        //stop
                                        sub.ChikPeaO2B__Billing_Stopped__c=true; //Added by Snehashish Roy on 19 May, 2017 (Invoice Line is billed till Stop Date)
                                        if (sub.ChikPeaO2B__Billing_Stop_Reason__c== null){
                                            System.debug('Inside Stop REason Condition---->'+sub.ChikPeaO2B__Billing_Stop_Reason__c);
                                            sub.ChikPeaO2B__Billing_Stop_Reason__c= 'Expired';
                                        }
                                    }
                                }
                            } 
                        }
                        invcl.Qty__c = sub.ChikPeaO2B__Quantity__c;
                        invcl.Description__c = sub.Description__c;
                        invcl.Subscription__c = sub.Id;
                        invcLineList.add(invcl);        
                        
                        if ( (sub.Billing_Stop_Date__c == null) || (sub.Billing_Stop_date__c > sub.Next_Bill_date__c) )
                            sub.Next_Bill_Date__c = BillToOneBillCycle+1;
                        
                    }
                    sub.Bill_Date__c=date.today();
                    sub.ChikPeaO2B__Billing_Started__c=true;
                    subUpdateList.add(sub);
                   //========== Invoice Line from Invoice Line History[START] ==============//
                   if(InvLineHistory.get(sub.id)!=null && InvLineHistory.get(sub.id).size()>0)
                   {
                       for(Invoice_Line_History__c invlh: InvLineHistory.get(sub.id))
                       {
                           Invoice_Line__c invcl2 = new Invoice_Line__c();
                           invcl2.line_type__c = 'Subscription';
                           invcl2.Item__c = invlh.ChikPeaO2B__Item__c;
                           invcl2.ChikPeaO2B__Unit_Rate__c = invlh.ChikPeaO2B__Unit_Rate__c;
                           invcl2.ChikPeaO2B__Period_From__c = invlh.ChikPeaO2B__Period_From__c;
                           invcl2.ChikPeaO2B__Period_To__c = invlh.ChikPeaO2B__Period_To__c;
                           invcl2.Qty__c = invlh.ChikPeaO2B__Qty__c;
                           invcl2.Description__c = sub.Description__c;
                           invcl2.Subscription__c = sub.id;
                           invcLineList.add(invcl2);
                           invlh.ChikPeaO2B__Invoice_Required__c='No';
                           InvlHisUpdateList.add(invlh);     
                       }
                       InvLineHistoryUpdate.put(sub.id,InvlHisUpdateList);
                   }
                   //========== Invoice Line from Invoice Line History[START] ==============//
                }
                subListMap.put(a.Id, subUpdateList);
            }
            invLineMap.put(a.Id,invcLineList);
        }// Account for loop end
        Savepoint sp = Database.setSavepoint();//#####Save Point
        if(invList.size()>0){
            //insert invList;
            //=== CURD check [START] ===//
            Map<string,string>InvoiceInsertResultMap=new map<string,string>();
            InvoiceInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Invoice__c',FieldPermissionSet.InvoiceInsert,'insert');
            if(InvoiceInsertResultMap.get('AllowDML')!=null && InvoiceInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                list<Database.SaveResult> instInv=Database.insert(invList,false);
                ExceptionLogger.insertLog(instInv, invList, 'Dml Exception in Class AnniversarySubscriptionBilling for list invList','ChikPeaO2B__Account__c');
            }
            else
            {        
                Database.rollback(sp);
                return null;
            }
            //=== CURD check [END] ===//
            List<Invoice_Line__c> invLineList = new List<Invoice_Line__c>();
            List<Purchase__c> purList = new List<Purchase__c>();
            list<Invoice_Line_History__c>AllInvlhUpdate=new list<Invoice_Line_History__c>();
            for(Invoice__c inv:invList){
                List<Invoice_Line__c> ilList = invLineMap.get(inv.Account__c);
                if(ilList != null){
                    for(Invoice_Line__c il:ilList){
                        il.Invoice__c = inv.Id;
                        invLineList.add(il);
                    }
                }

                List<Purchase__c> purUpdateList = purListMap.get(inv.Account__c);
                if(purUpdateList != null){
                    for(Purchase__c pur:purUpdateList){
                        pur.Invoice__c = inv.Id;
                        purList.add(pur);
                    }
                }

                List<Subscription__c> subUpdateList = subListMap.get(inv.Account__c);             
                if(subUpdateList != null){
                    for(Subscription__c sub:subUpdateList){
                        sub.Invoice__c = inv.Id;
                        subAllList.add(sub);
                        /*commented by Spal:08/03/2015[START]
                        if(InvLineHistoryUpdate.get(sub.id)!=null && InvLineHistoryUpdate.get(sub.id).size()>0)
                        {
                            for(Invoice_Line_History__c invlh : InvLineHistoryUpdate.get(sub.id))
                                AllInvlhUpdate.add(invlh);
                        }commented by Spal:08/03/2015[END]*/
                    }
                }
                
                returnInvIds.add(inv.Id);
            }
            //Added by Spal:08/03/2015[START]
          if(invList!=null && invList.size()>0 && InvLineHistoryUpdate!=null && InvLineHistoryUpdate.size()>0)
          {
              for(id subid : InvLineHistoryUpdate.keyset())
              {
                  if(InvLineHistoryUpdate.get(subid)!=null && InvLineHistoryUpdate.get(subid).size()>0)
                  {
                      for(Invoice_Line_History__c invlh : InvLineHistoryUpdate.get(subid))
                                AllInvlhUpdate.add(invlh);
                  }
              }
          }
          //Added by Spal:08/03/2015[END]
            if(invLineList.size()>0){
                //insert invLineList;
                //=== CURD check [START] ===//
                Map<string,string>InvlInsertResultMap=new map<string,string>();
                InvlInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Invoice_Line__c',FieldPermissionSet.InvoiceLineInsert,'insert');
                if(InvlInsertResultMap.get('AllowDML')!=null && InvlInsertResultMap.get('AllowDML').equalsignorecase('true'))
                {
                    list<Database.SaveResult> instInvLn=Database.insert(invLineList,false);
                    ExceptionLogger.insertLog(instInvLn, invLineList, 'Dml Exception in Class AnniversarySubscriptionBilling for list invLineList','ChikPeaO2B__Invoice__c');
                }
                else
                {        
                    Database.rollback(sp);
                    return null;
                }
                //=== CURD check [END] ===//
            }

            if(purList.size()>0){
                //update purList;
                //=== CURD check [START] ===//
                Map<string,string>PurUpdateResultMap=new map<string,string>();
                PurUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Purchase__c',FieldPermissionSet.PurchaseUpdate,'update');
                if(PurUpdateResultMap.get('AllowDML')!=null && PurUpdateResultMap.get('AllowDML').equalsignorecase('true'))
                {
                    list<Database.SaveResult> updatePur=Database.update(purList,false);
                    ExceptionLogger.insertLog(updatePur, purList, 'Dml Exception in Class AnniversarySubscriptionBilling for list purList','ChikPeaO2B__Account__c');
                }
                else
                {        
                    Database.rollback(sp);
                    return null;
                }
                //=== CURD check [END] ===//
            }
            
            if(subAllList.size()>0){
               //update subAllList;
               //=== CURD check [START] ===//
                Map<string,string>SubAllUpdateResultMap=new map<string,string>();
                SubAllUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Subscription__c',FieldPermissionSet.SubscriptionUpdate,'update');
                if(SubAllUpdateResultMap.get('AllowDML')!=null && SubAllUpdateResultMap.get('AllowDML').equalsignorecase('true'))
                {
                   list<Database.SaveResult> updateSub=Database.update(subAllList,false);
                   ExceptionLogger.insertLog(updateSub, subAllList, 'Dml Exception in Class AnniversarySubscriptionBilling for list subAllList','ChikPeaO2B__Account__c');
                }
                else
                {        
                    Database.rollback(sp);
                    return null;
                }
                //=== CURD check [END] ===//
                
            }
            
            if(AllInvlhUpdate!=null && AllInvlhUpdate.size()>0)
            {
                //update(AllInvlhUpdate);
                //=== CURD check [START] ===//
                Map<string,string>InvlHistoryUpdateResultMap=new map<string,string>();
                InvlHistoryUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Invoice_Line_History__c',FieldPermissionSet.InvoiceLineHistoryUpdate,'update');
                if(InvlHistoryUpdateResultMap.get('AllowDML')!=null && InvlHistoryUpdateResultMap.get('AllowDML').equalsignorecase('true'))
                {
                    list<Database.SaveResult> updateInvH=Database.update(AllInvlhUpdate,false);
                    ExceptionLogger.insertLog(updateInvH, AllInvlhUpdate, 'Dml Exception in Class AnniversarySubscriptionBilling for list AllInvlhUpdate','ChikPeaO2B__Subscription__c');
                }
                else
                {        
                    Database.rollback(sp);
                    return null;
                }
                //=== CURD check [END] ===//
            }
            
            if(isbatch)
            {
                list<ChikPeaO2B__O2B_Setting__c> obStt=[SELECT Id,ChikPeaO2B__Line_Payment__c 
                FROM ChikPeaO2B__O2B_Setting__c WHERE ChikPeaO2B__Bill_Schedule_Job_Id__c=:BatchCreateBill.apexJobId];
                
                list<ChikPeaO2B__Bill_Group__c> billGrp=[SELECT Id,ChikPeaO2B__Line_Payment__c 
                FROM ChikPeaO2B__Bill_Group__c WHERE ChikPeaO2B__Bill_Schedule_Job_Id__c=:BatchCreateBill.apexJobId];
                boolean linePayment;
                if(!obStt.isEmpty()){
                    linePayment=obStt[0].ChikPeaO2B__Line_Payment__c;
                }
                if(!billGrp.isEmpty()){
                    linePayment=billGrp[0].ChikPeaO2B__Line_Payment__c;
                }
                
                PaymentInsertForBatch.insertpayment(returnInvIds,linePayment,o2bSet[0].ChikPeaO2B__Default_Gateway__r.name);
                //PaymentInsertForBatch.insertpayment(returnInvIds,o2bSet[0].ChikPeaO2B__Line_Payment__c,o2bSet[0].ChikPeaO2B__Default_Gateway__r.name);
                /**/                   
            }
        }
        
        
        //Stop the stop date subscriptions
        //Commented by Snehashish Roy on 19 May, 2017 as BillingStopped is done in each section
        /*for(Subscription__c stopSub : [Select Id from Subscription__c where ChikPeaO2B__Billing_Stopped__c = false and ChikPeaO2B__Billing_Stop_Date__c<= :Date.Today()
                                        and account__c in :accountIds])
        {
            stopSub.Billing_Stopped__c = true;
            if (stopSub.ChikPeaO2B__Billing_Stop_Reason__c== null)
                  stopSub.ChikPeaO2B__Billing_Stop_Reason__c= 'Expired';
            stopSubList.add(stopSub);
        }*/
        if (stopSubList.size() > 0){
             //update stopSubList;
             //=== CURD check [START] ===//
            Map<string,string>SubStpUpdateResultMap=new map<string,string>();
            SubStpUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Subscription__c',FieldPermissionSet.SubscriptionUpdate,'update');
            if(SubStpUpdateResultMap.get('AllowDML')!=null && SubStpUpdateResultMap.get('AllowDML').equalsignorecase('true'))
            {
                list<Database.SaveResult> updateSub1=Database.update(stopSubList,false);
                ExceptionLogger.insertLog(updateSub1, stopSubList, 'Dml Exception in Class AnniversarySubscriptionBilling for list subAllList','ChikPeaO2B__Account__c');
            }
            else
            {        
                Database.rollback(sp);
                return null;
            }
            //=== CURD check [END] ===//
        }
        
        if (returnInvIds.size() > 0)
            return returnInvIds;
        else
            return null;
    }
}