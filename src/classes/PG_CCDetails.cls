/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 3.0 2013-09-19 2015-07-29 2015-11-05
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG_CCDetails {
    
    global String ccCity { get; set; }
    global String ccCountry { get; set; }
    global String ccFirstName { get; set; }
    global String ccLastName { get; set; }
    global String ccPostalCode { get; set; }
    global String ccState { get; set; }
    global String ccStreet1 { get; set; }
    global String ccStreet2 { get; set; }
    global String creditCardNumber { get; set; }
    global String creditCardType { get; set; }
    global String expiryMonth { get; set; }
    global String expiryYear { get; set; }
    global String customerProfileId { get; set; }
    global String customerPaymentProfileId { get; set; }
    
    global PG_CCDetails(){
        
    }
    
    global PG_CCDetails(Id objId){//@throws PG_Exception
        Schema.SObjectType token = objId.getSObjectType();
        //System.debug('---token='+token);
        Schema.DescribeSObjectResult dr = token.getDescribe();
        //System.debug('---dr='+dr);
        
        String queryString = 'Select '+
            's.Name, s.Id, s.Credit_Card__r.Id, s.Credit_Card__r.Expiry_Year__c, s.Credit_Card__r.Expiry_Month__c, '+
            's.Credit_Card__r.Credit_Card_Type__c, s.Credit_Card__r.Credit_Card_Number__c, '+ 
            's.Credit_Card__r.CC_Street1__c, s.Credit_Card__r.CC_Street2__c, s.Credit_Card__r.CC_State__c, '+ 
            's.Credit_Card__r.CC_Postal_Code__c, s.Credit_Card__r.CC_Last_Name__c, s.Credit_Card__r.CC_First_Name__c, '+ 
            's.Credit_Card__r.CC_Country__c, s.Credit_Card__r.CC_City__c, '+
            's.Credit_Card__r.Customer_Profile_ID__c, s.Credit_Card__r.Customer_Payment_Profile_Id__c '+
            'From '+ dr.getName() + ' s '+
            //'where s.Id = \'' + objId + '\'';                         
            'where s.Id = :objId';                          
        System.debug('---queryString='+queryString);
            
        sObject[] objList = Database.query(queryString);        
        if(objList != null && !objList.isEmpty()){
            Credit_Card__c cc = (Credit_Card__c)objList[0].getSObject('Credit_Card__r');
            //System.debug('---Credit_Card_Type__c='+cc.Credit_Card_Type__c);
            ccCity = cc.CC_City__c;     
            ccCountry = cc.CC_Country__c;       
            ccFirstName = cc.CC_First_Name__c;      
            ccLastName = cc.CC_Last_Name__c;        
            ccPostalCode = cc.CC_Postal_Code__c;     
            ccState = cc.CC_State__c;       
            ccStreet1 = cc.CC_Street1__c;       
            ccStreet2 = cc.CC_Street2__c;       
            creditCardNumber = cc.Credit_Card_Number__c;        
            creditCardType = cc.Credit_Card_Type__c;
            Decimal expMon = cc.Expiry_Month__c;
            expiryMonth = expMon < 10?'0':'';   
            expiryMonth += String.valueOf(cc.Expiry_Month__c);      
            expiryYear = String.valueOf(cc.Expiry_Year__c);     
            customerProfileId = cc.Customer_Profile_ID__c;      
            customerPaymentProfileId = cc.Customer_Payment_Profile_Id__c;       
        }else{
            throw new PG_Exception('Gateway Exception::Not found '+token+'(Id='+objId+')');
        }       
    }
    
    global String debug(){
    	String debugString = 'PG_CCDetails:[ccCity='+ccCity+', ccCountry='+ccCountry+', ccFirstName='+ccFirstName+', ccLastName='+ccLastName+', ccPostalCode='+ccPostalCode+', ccState='+ccState+', ccStreet1='+ccStreet1+', ccStreet2='+ccStreet2+', creditCardNumber='+PG.maskWord(creditCardNumber, '', -4, true)+', creditCardType='+creditCardType+', customerPaymentProfileId='+customerPaymentProfileId+', customerProfileId='+customerProfileId+', expiryMonth='+expiryMonth+', expiryYear='+expiryYear+']';
    	return debugString;    	
    }
    /*
    public static void tryIt(){
        PG_CCDetails details = new PG_CCDetails('a0Fi0000008BukQ');
        System.debug('---details='+details);
    }
    */
}