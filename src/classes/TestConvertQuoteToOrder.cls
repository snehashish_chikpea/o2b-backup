@isTest
public class TestConvertQuoteToOrder
{
    @isTest
    static void testMethod1()
    {
        Account acc=new Account(name='acc1');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        insert(oneoff);
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp1);
        ChikPeaO2B__quote__c qt=new ChikPeaO2B__quote__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Status__c='Open');
        insert(qt);
        ChikPeaO2B__quote_Line__c qtl=new ChikPeaO2B__quote_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__quote__c=qt.id,ChikPeaO2B__Quantity__c=10,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=1);
        insert(qtl);
        list<id>qidList=new List<id>();
        qidList.add(qt.id);
        Test.startTest();
        
        ConvertQuoteToOrder.quoteToOrder(qidList);
        Test.stopTest();
    }
}