/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-02
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
private class TestPG_CCMod10Check {

    static testMethod void testCCNumberValid() {
        
        Test.startTest();
        //String ccNumber = '4012888888881881'; 
        //valid Visa - 4112344112344113, Disc - 6559906559906557, MC - 5112345112345114 
        String ccNumber = '6559906559906557';       
        System.assertEquals(true, PG_CCMod10Check.valid(ccNumber));
        Test.stopTest();
        
    }
    
    static testMethod void testCCNumberInvalid() {
        
        Test.startTest();
        String ccNumber = '4012888888881885';       
        System.assertEquals(false, PG_CCMod10Check.valid(ccNumber));        
        Test.stopTest();
        
    }
    
    static testMethod void testCCNumberNull() {
        
        Test.startTest();
        String ccNumber;        
        System.assertEquals(false, PG_CCMod10Check.valid(ccNumber));        
        Test.stopTest();
        
    }
    
}