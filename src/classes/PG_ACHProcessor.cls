/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-21
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG_ACHProcessor {
    
    PG_PaymentGatewayACH paymentGatewayACH;
    
    global PG_ACHProcessor(PG_PaymentGatewayACH paymentGatewayACH){
        this.paymentGatewayACH = paymentGatewayACH;
    }
    
    global void processACH(PG.TransactionTypeACH transactionTypeACH){
        this.paymentGatewayACH.submitTransactionACH(transactionTypeACH);        
    }
    
    global PG_Response fetchResponseACH(){
        return this.paymentGatewayACH.getPGResponse();
    }

}