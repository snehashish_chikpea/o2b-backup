//    #1.Advance Payment    #2.Account based tiered pricing    #3.Account based volume pricing    #4.Agreement
//    #5.Bank Account    #6.BatchLog    #7.Bill Group    #8.Bundled Item    #9.Credit Card    #10.Refund
//    #11.Volume Pricing    #12.Usage History    #13.Usage    #14.Tiered Pricing    #15.Subscription    #16.Credit Note
//    #17.Shipment Line    #18.Shipment    #19.Rate Plan    #20.Quote Line    #21.Purchase    #22.Price Book
//    #23.Payment Staging    #24.Payment    #25.Gateway Transaction    #26.Gateway Response    #27.Gateway    #28.Dunning Rule
//    #29.Invoice    #30.Invoice Line    #31.Quote    #32.Package    #33.Order Line    #34.Order    #35.O2B Setting    #36.Item    #37.Invoice Line History
global with sharing class FieldPermissionSet{
    
    //######### #1.Advance Payment [START] ############//
    public static set<string>AdvancepaymentInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Advanced_Payment__c',
    'ChikPeaO2B__Card_Failure__c','ChikPeaO2B__Check_Date__c','ChikPeaO2B__Check_Number__c','ChikPeaO2B__Comment__c',
    'ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Expiry_Month__c','ChikPeaO2B__Expiry_Year__c','ChikPeaO2B__Failure_Exception__c',
    'ChikPeaO2B__Payment_Date__c','ChikPeaO2B__Payment_Method__c','ChikPeaO2B__Payment_Gateway__c','ChikPeaO2B__Status__c',
    'ChikPeaO2B__Transaction_Id__c','ChikPeaO2B__Trx_Message__c','ChikPeaO2B__Used_Amount__c'};
    
    public static set<string>AdvancepaymentUpdate=new set<string>{'ChikPeaO2B__Advanced_Payment__c',
    'ChikPeaO2B__Card_Failure__c','ChikPeaO2B__Check_Date__c','ChikPeaO2B__Check_Number__c','ChikPeaO2B__Comment__c',
    'ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Expiry_Month__c','ChikPeaO2B__Expiry_Year__c','ChikPeaO2B__Failure_Exception__c',
    'ChikPeaO2B__Payment_Date__c','ChikPeaO2B__Payment_Method__c','ChikPeaO2B__Payment_Gateway__c','ChikPeaO2B__Status__c',
    'ChikPeaO2B__Transaction_Id__c','ChikPeaO2B__Trx_Message__c','ChikPeaO2B__Used_Amount__c'};
    
    public static set<string>AdvancepaymentDelete=new set<string>();
    //######### Advance Payment [END] ############//
    
    //######### #2.Account based tiered pricing [START] ############//
    public static set<string>AccountbasedtieredpricingInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Item__c',
    'ChikPeaO2B__Lower_Limit__c','ChikPeaO2B__Upper_Limit__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>AccountbasedtieredpricingUpdate=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Lower_Limit__c',
    'ChikPeaO2B__Upper_Limit__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>AccountbasedtieredpricingDelete=new set<string>();
    //######### Account based tiered pricing [END] ############//
    
    //######### #3.Account based volume pricing [START] ############//
    public static set<string>AccountbasedvolumepricingInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Item__c',
    'ChikPeaO2B__Lower_Limit__c','ChikPeaO2B__Upper_Limit__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>AccountbasedvolumepricingUpdate=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Lower_Limit__c',
    'ChikPeaO2B__Upper_Limit__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>AccountbasedvolumepricingDelete=new set<string>();
    //######### Account based volume pricing [END] ############//
    
    //######### #4.Agreement [START] ############//
    public static set<string>AgreementInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__End_Date__c','ChikPeaO2B__Start_Date__c'};
    
    public static set<string>AgreementUpdate=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__End_Date__c','ChikPeaO2B__Start_Date__c'};
    
    public static set<string>AgreementDelete=new set<string>();
    //######### Agreement [END] ############//
    
    //######## #5.Bank Account [START] ######//
    public static set<string>BankAccountInsert=new set<string>{'ChikPeaO2B__ABA_Number__c','ChikPeaO2B__Account_Name__c','ChikPeaO2B__Account_Number__c','ChikPeaO2B__Account_Type__c'};
    
    public static set<string>BankAccountUpdate=new set<string>{'ChikPeaO2B__ABA_Number__c','ChikPeaO2B__Account_Name__c','ChikPeaO2B__Account_Number__c','ChikPeaO2B__Account_Type__c'};
    
    public static set<string>BankAccountDelete=new set<string>();
    //######## Bank Account[END] #####//
    
    //######## #6.BatchLog[START] #####//
    public static set<string>BatchLogInsert=new set<string>{'ChikPeaO2B__Error_Log__c'};
    
    public static set<string>BatchLogUpdate=new set<string>{'ChikPeaO2B__Error_Log__c'};
    
    public static set<string>BatchLogDelete=new set<string>();
    //######## BatchLog[END] #####//
    
    //######## #7.Bill Group[START] ######//
    public static set<string>BillGroupInsert=new set<string>{'ChikPeaO2B__Batch_Size__c','ChikPeaO2B__Billing_Day__c','ChikPeaO2B__Billing_Type__c','ChikPeaO2B__Bill_Schedule_Job_Id__c',
    'ChikPeaO2B__Bulk_Data__c','ChikPeaO2B__isExecuting__c','ChikPeaO2B__Line_Payment__c','ChikPeaO2B__Maximum_Records__c','ChikPeaO2B__O2B_Setting__c','ChikPeaO2B__Schedule_Time__c'};
    
    public static set<string>BillGroupUpdate=new set<string>{'ChikPeaO2B__Batch_Size__c','ChikPeaO2B__Billing_Day__c','ChikPeaO2B__Billing_Type__c','ChikPeaO2B__Bill_Schedule_Job_Id__c',
    'ChikPeaO2B__Bulk_Data__c','ChikPeaO2B__isExecuting__c','ChikPeaO2B__Line_Payment__c','ChikPeaO2B__Maximum_Records__c','ChikPeaO2B__Schedule_Time__c'};
    
    public static set<string>BillGroupDelete=new set<string>();
    //######## Bill Group[END] #######//
    
    //######## #8.Bundled Item[START] #######//
    public static set<string>BundledItemInsert=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Package__c','ChikPeaO2B__Quantity__c',
    'ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>BundledItemUpdate=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Quantity__c',
    'ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>BundledItemDelete=new set<string>();
    //######## Bundled Item[END] #######//
    
    //####### #9.Credit Card[START] ######//
    public static set<string>CreditCardInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__CC_Street1__c','ChikPeaO2B__CC_City__c','ChikPeaO2B__CC_Country__c','ChikPeaO2B__Credit_Card_Type__c','ChikPeaO2B__Expiry_Month__c',
    'ChikPeaO2B__CC_First_Name__c','ChikPeaO2B__CC_First_Name__c','ChikPeaO2B__CC_Postal_Code__c','ChikPeaO2B__CC_State__c','ChikPeaO2B__CC_Street2__c','ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Expiry_Year__c'};
    
    public static set<string>CreditCardUpdate=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__CC_Street1__c','ChikPeaO2B__CC_City__c','ChikPeaO2B__CC_Country__c','ChikPeaO2B__Credit_Card_Type__c','ChikPeaO2B__Expiry_Month__c',
    'ChikPeaO2B__CC_First_Name__c','ChikPeaO2B__CC_First_Name__c','ChikPeaO2B__CC_Postal_Code__c','ChikPeaO2B__CC_State__c','ChikPeaO2B__CC_Street2__c','ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Expiry_Year__c'};
    
    public static set<string>CreditCardDelete=new set<string>();
    //####### Credit Card[END]######//
    
    //####### #10.Refund[START] ######//
    public static set<string>RefundInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Advance_payment__c','ChikPeaO2B__Check_Date__c','ChikPeaO2B__Check_Number__c','ChikPeaO2B__Credit_Card__c','ChikPeaO2B__Credit_Note__c','ChikPeaO2B__Invoice_Number__c','ChikPeaO2B__Payment__c','ChikPeaO2B__Refund_Amount__c','ChikPeaO2B__Refund_Method__c','ChikPeaO2B__Refund_Reason__c','ChikPeaO2B__Status__c','ChikPeaO2B__Transaction_Id__c','ChikPeaO2B__Trx_Message__c'};
    
    public static set<string>RefundUpdate=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Advance_payment__c','ChikPeaO2B__Check_Date__c','ChikPeaO2B__Check_Number__c','ChikPeaO2B__Credit_Card__c','ChikPeaO2B__Credit_Note__c','ChikPeaO2B__Invoice_Number__c','ChikPeaO2B__Payment__c','ChikPeaO2B__Refund_Amount__c','ChikPeaO2B__Refund_Method__c','ChikPeaO2B__Refund_Reason__c','ChikPeaO2B__Status__c','ChikPeaO2B__Transaction_Id__c','ChikPeaO2B__Trx_Message__c'};
    
    public static set<string>RefundDelete=new set<string>();
    //####### Refund[END]######//
    
    //######## #11. Volume Pricing [START] ############//
    public static set<string>VolumePricingInsert=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Lower_Limit__c',
    'ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Upper_Limit__c',
    'ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>VolumePricingUpdate=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Lower_Limit__c',
    'ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Upper_Limit__c',
    'ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>VolumePricingDelete=new set<string>();
    //######### Volume Pricing [END] ############//
       
    //######## #12. Usage History [START] ############//
    public static set<string>UsageHistoryInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Bill_Date__c',
    'ChikPeaO2B__Free_Usage__c','ChikPeaO2B__Invoice_Cancelled__c','ChikPeaO2B__Rate__c','ChikPeaO2B__Remaining_Usage__c',
    'ChikPeaO2B__Subscription__c', 'ChikPeaO2B__Total_Usage__c', 'ChikPeaO2B__Usage__c'};
    
    public static set<string>UsageHistoryUpdate=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Bill_Date__c',
    'ChikPeaO2B__Free_Usage__c','ChikPeaO2B__Invoice_Cancelled__c','ChikPeaO2B__Rate__c','ChikPeaO2B__Remaining_Usage__c',
    'ChikPeaO2B__Subscription__c', 'ChikPeaO2B__Total_Usage__c', 'ChikPeaO2B__Usage__c'};
    
    public static set<string>UsageHistoryDelete=new set<string>();
    //######### Usage History [END] ############//
    
    //######## #13. Usage [START] ############//
    public static set<string>UsageInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Cancelled__c',
    'ChikPeaO2B__Free_Usage__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Item__c','ChikPeaO2B__Order_Line__c',
    'ChikPeaO2B__Purchase__c', 'ChikPeaO2B__Rate__c', 'ChikPeaO2B__Rate__c'};
    
    public static set<string>UsageUpdate=new set<string>{'ChikPeaO2B__Cancelled__c',
    'ChikPeaO2B__Free_Usage__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Item__c','ChikPeaO2B__Order_Line__c',
    'ChikPeaO2B__Purchase__c', 'ChikPeaO2B__Rate__c', 'ChikPeaO2B__Rate__c'};
    
    public static set<string>UsageDelete=new set<string>();
    //######### Usage [END] ############//
    
    //######## #14. Tiered Pricing [START] ############//
    public static set<string>TieredPricingInsert=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Lower_Limit__c',
    'ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Upper_Limit__c',
    'CChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>TieredPricingUpdate=new set<string>{'ChikPeaO2B__Item__c','ChikPeaO2B__Lower_Limit__c',
    'ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Upper_Limit__c',
    'CChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>TieredPricingDelete=new set<string>();
    //######### Tiered Pricing [END] ############//
    
    //######## #15. Subscription [START] ############//
    public static set<string>SubscriptionInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Aggregate_Usage__c',
    'ChikPeaO2B__Bill_Date__c','ChikPeaO2B__Billing_Start_Date__c','ChikPeaO2B__Billing_Started__c','ChikPeaO2B__Billing_Stop_Date__c',
    'ChikPeaO2B__Billing_Stopped__c', 'ChikPeaO2B__Billing_Stop_Reason__c', 'ChikPeaO2B__Calendar_month__c', 'ChikPeaO2B__Credit_Card__c', 'ChikPeaO2B__Discount__c', 'ChikPeaO2B__Discount_Period_Days__c', 'ChikPeaO2B__Free_usage__c', 'ChikPeaO2B__Invoice__c', 'ChikPeaO2B__Item__c', 'ChikPeaO2B__Next_Bill_Date__c', 'ChikPeaO2B__Order_Line__c', 'ChikPeaO2B__Payment_Status__c', 'ChikPeaO2B__Quantity__c', 'ChikPeaO2B__Rate_Plan__c', 'ChikPeaO2B__Recurring_Charge__c'};
    
    public static set<string>SubscriptionUpdate=new set<string>{'ChikPeaO2B__Aggregate_Usage__c',
    'ChikPeaO2B__Bill_Date__c','ChikPeaO2B__Billing_Start_Date__c','ChikPeaO2B__Billing_Started__c','ChikPeaO2B__Billing_Stop_Date__c',
    'ChikPeaO2B__Billing_Stopped__c', 'ChikPeaO2B__Billing_Stop_Reason__c', 'ChikPeaO2B__Calendar_month__c', 'ChikPeaO2B__Credit_Card__c', 'ChikPeaO2B__Discount__c', 'ChikPeaO2B__Discount_Period_Days__c', 'ChikPeaO2B__Free_usage__c', 'ChikPeaO2B__Invoice__c', 'ChikPeaO2B__Item__c', 'ChikPeaO2B__Next_Bill_Date__c', 'ChikPeaO2B__Order_Line__c', 'ChikPeaO2B__Payment_Status__c', 'ChikPeaO2B__Quantity__c', 'ChikPeaO2B__Rate_Plan__c', 'ChikPeaO2B__Recurring_Charge__c'};
    
    public static set<string>SubscriptionDelete=new set<string>();
    //######### Subscription [END] ############//   
    
    //##### #16. Credit Note [START] ############//
    public static set<string>CreditNoteInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Amount__c',
    'ChikPeaO2B__Carry_Forward_Credit_Note__c','ChikPeaO2B__Credit_Date__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Ref_Id__c',
    'ChikPeaO2B__Ref_Name__c', 'ChikPeaO2B__Type__c', 'ChikPeaO2B__Used_Amount__c', 'ChikPeaO2B__Used_Amount__c', 'ChikPeaO2B__Validity__c'};
    
    public static set<string>CreditNoteUpdate=new set<string>{'ChikPeaO2B__Amount__c',
    'ChikPeaO2B__Carry_Forward_Credit_Note__c','ChikPeaO2B__Credit_Date__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Ref_Id__c',
    'ChikPeaO2B__Ref_Name__c', 'ChikPeaO2B__Type__c', 'ChikPeaO2B__Used_Amount__c', 'ChikPeaO2B__Used_Amount__c', 'ChikPeaO2B__Validity__c'};
    
    public static set<string>CreditNoteDelete=new set<string>();
    //######### Credit Note [END] ############//

    //##### #17. Shipment Line [START] ############//
    public static set<string>ShipmentLineInsert=new set<string>{'ChikPeaO2B__Order_Line__c','ChikPeaO2B__Shipment__c',
    'ChikPeaO2B__Status__c'};
    
    public static set<string>ShipmentLineUpdate=new set<string>{'ChikPeaO2B__Order_Line__c', 'ChikPeaO2B__Status__c'};
    
    public static set<string>ShipmentLineDelete=new set<string>();
    //######### Shipment Line [END] ############//
    
    //##### #18. Shipment [START] ############//
    public static set<string>ShipmentInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Carrier__c','ChikPeaO2B__Contact__c','ChikPeaO2B__Order__c','ChikPeaO2B__Shipment_Status__c','ChikPeaO2B__Shipment_date__c','ChikPeaO2B__Shipping_City__c','ChikPeaO2B__Shipping_Country__c','ChikPeaO2B__Shipping_State_Province__c','ChikPeaO2B__Shipping_Street__c','ChikPeaO2B__Shipping_Zip_Postal_Code__c','ChikPeaO2B__Tracking_Number__c'};
    
    public static set<string>ShipmentUpdate=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Carrier__c','ChikPeaO2B__Contact__c','ChikPeaO2B__Shipment_Status__c','ChikPeaO2B__Shipment_date__c','ChikPeaO2B__Shipping_City__c','ChikPeaO2B__Shipping_Country__c','ChikPeaO2B__Shipping_State_Province__c','ChikPeaO2B__Shipping_Street__c','ChikPeaO2B__Shipping_Zip_Postal_Code__c','ChikPeaO2B__Tracking_Number__c'};
    
    public static set<string>ShipmentDelete=new set<string>();
    //######### Shipment [END] ############//
    
    //##### #19. Rate Plan [START] ############//
    public static set<string>RatePlanInsert=new set<string>{'ChikPeaO2B__Bill_Cycle__c','ChikPeaO2B__Free_Usage__c','ChikPeaO2B__Item__c','ChikPeaO2B__Max_Usage__c','ChikPeaO2B__Min_Usage__c','ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Period_Off_Set_Unit__c','ChikPeaO2B__Period_Off_Set__c','ChikPeaO2B__Price_Book__c','ChikPeaO2B__Pricing_Type__c','ChikPeaO2B__Rating_Rule__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Start_Off_Set_Unit__c','ChikPeaO2B__Start_Off_set__c','ChikPeaO2B__Tax_Ref__c','ChikPeaO2B__UOM__c','ChikPeaO2B__Usage_Rate_Code__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>RatePlanUpdate=new set<string>{'ChikPeaO2B__Bill_Cycle__c','ChikPeaO2B__Free_Usage__c','ChikPeaO2B__Max_Usage__c','ChikPeaO2B__Min_Usage__c','ChikPeaO2B__Non_Recurring_Charge__c','ChikPeaO2B__Period_Off_Set_Unit__c','ChikPeaO2B__Period_Off_Set__c','ChikPeaO2B__Price_Book__c','ChikPeaO2B__Pricing_Type__c','ChikPeaO2B__Rating_Rule__c','ChikPeaO2B__Recurring_Charge__c','ChikPeaO2B__Start_Off_Set_Unit__c','ChikPeaO2B__Start_Off_set__c','ChikPeaO2B__Tax_Ref__c','ChikPeaO2B__UOM__c','ChikPeaO2B__Usage_Rate_Code__c','ChikPeaO2B__Usage_Rate__c'};
    
    public static set<string>RatePlanDelete=new set<string>();
    //######### Rate Plan [END] ############//
    
    //##### #20. Quote Line [START] ############//
    public static set<string>QuoteLineInsert=new set<string>{'ChikPeaO2B__Description__c','ChikPeaO2B__Item_Type__c','ChikPeaO2B__Item__c','ChikPeaO2B__Package__c','ChikPeaO2B__Quantity__c','ChikPeaO2B__Quote__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Subscription_Start_Date__c','ChikPeaO2B__Subscription_Term_Months__c','ChikPeaO2B__Tax_Rate__c','ChikPeaO2B__Unit_Price__c'};
    
    public static set<string>QuoteLineUpdate=new set<string>{'ChikPeaO2B__Description__c','ChikPeaO2B__Item_Type__c','ChikPeaO2B__Item__c','ChikPeaO2B__Package__c','ChikPeaO2B__Quantity__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Subscription_Start_Date__c','ChikPeaO2B__Subscription_Term_Months__c','ChikPeaO2B__Tax_Rate__c','ChikPeaO2B__Unit_Price__c'};
    
    public static set<string>QuoteLineDelete=new set<string>();
    //######### Quote Line [END] ############//

    //##### #21. Purchase [START] ############//
    public static set<string>PurchaseInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Aggregate_Usage__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Invoiced__c','ChikPeaO2B__Item__c','ChikPeaO2B__Max_Usage__c','ChikPeaO2B__Order_Line__c','ChikPeaO2B__Qty__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Refunded__c','ChikPeaO2B__Sell_Price__c','ChikPeaO2B__Tax_Rate__c'};
    
    public static set<string>PurchaseUpdate=new set<string>{'ChikPeaO2B__Aggregate_Usage__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Invoiced__c','ChikPeaO2B__Item__c','ChikPeaO2B__Max_Usage__c','ChikPeaO2B__Order_Line__c','ChikPeaO2B__Qty__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Refunded__c','ChikPeaO2B__Sell_Price__c','ChikPeaO2B__Tax_Rate__c'};
    
    public static set<string>PurchaseDelete=new set<string>();
    //######### Purchase [END] ############//   
    
    //##### #22. Price Book [START] ############//
    public static set<string>PriceBookInsert=new set<string>{'ChikPeaO2B__Active__c','ChikPeaO2B__Description__c'};
    
    public static set<string>PriceBookUpdate=new set<string>{'ChikPeaO2B__Active__c','ChikPeaO2B__Description__c'};
    
    public static set<string>PriceBookDelete=new set<string>();
    //######### Price Book [END] ############// 
    
    //##### #23. Payment Staging [START] ############//
    public static set<string>PaymentStagingInsert=new set<string>{'ChikPeaO2B__Account_Reference__c','ChikPeaO2B__Amount__c','ChikPeaO2B__Batch_Ref__c','ChikPeaO2B__Comment__c','ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Currency_Code__c','ChikPeaO2B__O2B_Ref__c','ChikPeaO2B__Response_Code__c','ChikPeaO2B__Response_Date__c','ChikPeaO2B__Status__c','ChikPeaO2B__Trx_Action_Code__c','ChikPeaO2B__Trx_Method__c','ChikPeaO2B__Trx_Ref_Number__c','ChikPeaO2B__Used_Amount__c'};
    
    public static set<string>PaymentStagingUpdate=new set<string>{'ChikPeaO2B__Account_Reference__c','ChikPeaO2B__Amount__c','ChikPeaO2B__Batch_Ref__c','ChikPeaO2B__Comment__c','ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Currency_Code__c','ChikPeaO2B__O2B_Ref__c','ChikPeaO2B__Response_Code__c','ChikPeaO2B__Response_Date__c','ChikPeaO2B__Status__c','ChikPeaO2B__Trx_Action_Code__c','ChikPeaO2B__Trx_Method__c','ChikPeaO2B__Trx_Ref_Number__c','ChikPeaO2B__Used_Amount__c'};
    
    public static set<string>PaymentStagingDelete=new set<string>();
    //######### Payment Staging [END] ############//    
    
    //##### #24. Payment  [START] ############//
    public static set<string>PaymentInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Adjustments__c','ChikPeaO2B__Advance_payment__c','ChikPeaO2B__Bank_Account__c','ChikPeaO2B__Batch_ID__c','ChikPeaO2B__Card_Failure__c','ChikPeaO2B__Chargeback_Date__c','ChikPeaO2B__Check_Date__c','ChikPeaO2B__Check_Number__c','ChikPeaO2B__Comment__c','ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Credit_Card__c','ChikPeaO2B__Credit_Note__c','ChikPeaO2B__Expiry_Month__c','ChikPeaO2B__Expiry_Year__c','ChikPeaO2B__Failure_Exception__c','ChikPeaO2B__Hard_Declined__c','ChikPeaO2B__Has_Processed__c','ChikPeaO2B__Invoice_Line__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Last_Payment_Process_Date__c','ChikPeaO2B__Merchant_Order_Id__c','ChikPeaO2B__Payment_Amount__c','ChikPeaO2B__Payment_Date__c','ChikPeaO2B__Payment_Gateway__c','ChikPeaO2B__Payment_Method__c','ChikPeaO2B__Payment_Staging__c','ChikPeaO2B__Soft_Declined__c','ChikPeaO2B__Status__c','ChikPeaO2B__Transaction_Id__c','ChikPeaO2B__Trx_Message__c'};
    
    public static set<string>PaymentUpdate=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Adjustments__c','ChikPeaO2B__Advance_payment__c','ChikPeaO2B__Bank_Account__c','ChikPeaO2B__Batch_ID__c','ChikPeaO2B__Card_Failure__c','ChikPeaO2B__Chargeback_Date__c','ChikPeaO2B__Check_Date__c','ChikPeaO2B__Check_Number__c','ChikPeaO2B__Comment__c','ChikPeaO2B__Credit_Card_Number__c','ChikPeaO2B__Credit_Card__c','ChikPeaO2B__Credit_Note__c','ChikPeaO2B__Expiry_Month__c','ChikPeaO2B__Expiry_Year__c','ChikPeaO2B__Failure_Exception__c','ChikPeaO2B__Hard_Declined__c','ChikPeaO2B__Has_Processed__c','ChikPeaO2B__Invoice_Line__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Last_Payment_Process_Date__c','ChikPeaO2B__Merchant_Order_Id__c','ChikPeaO2B__Payment_Amount__c','ChikPeaO2B__Payment_Date__c','ChikPeaO2B__Payment_Gateway__c','ChikPeaO2B__Payment_Method__c','ChikPeaO2B__Payment_Staging__c','ChikPeaO2B__Soft_Declined__c','ChikPeaO2B__Status__c','ChikPeaO2B__Transaction_Id__c','ChikPeaO2B__Trx_Message__c'};
    
    public static set<string>PaymentDelete=new set<string>();
    //######### Payment [END] ############//    
    
    //##### #25.Gateway Transaction  [START] ############//
    public static set<string>GatewayTransactionInsert=new set<string>{'ChikPeaO2B__Approval_Status__c','ChikPeaO2B__Exception_Details__c','ChikPeaO2B__Gateway_Name__c','ChikPeaO2B__Input_Text__c','ChikPeaO2B__Is_Exception__c','ChikPeaO2B__Payment__c','ChikPeaO2B__Process_Status__c','ChikPeaO2B__Refund__c','ChikPeaO2B__Request_Text__c','ChikPeaO2B__Response_Code__c','ChikPeaO2B__Response_Message__c','ChikPeaO2B__Response_Text__c','ChikPeaO2B__Transaction_Time__c','ChikPeaO2B__Transaction_Type__c'};
    
    public static set<string>GatewayTransactionUpdate=new set<string>{'ChikPeaO2B__Approval_Status__c','ChikPeaO2B__Exception_Details__c','ChikPeaO2B__Gateway_Name__c','ChikPeaO2B__Input_Text__c','ChikPeaO2B__Is_Exception__c','ChikPeaO2B__Payment__c','ChikPeaO2B__Process_Status__c','ChikPeaO2B__Refund__c','ChikPeaO2B__Request_Text__c','ChikPeaO2B__Response_Code__c','ChikPeaO2B__Response_Message__c','ChikPeaO2B__Response_Text__c','ChikPeaO2B__Transaction_Time__c','ChikPeaO2B__Transaction_Type__c'};
    
    public static set<string>GatewayTransactionDelete=new set<string>();
    //######### Gateway Transaction [END] ############//    

    //##### #26. Gateway Response  [START] ############//
    public static set<string>GatewayResponseInsert=new set<string>{'ChikPeaO2B__Acceptable__c','ChikPeaO2B__Action__c','ChikPeaO2B__Code_Type__c','ChikPeaO2B__Code__c','ChikPeaO2B__Definition__c','ChikPeaO2B__Payment_Gateway__c','ChikPeaO2B__Screen_Message__c','ChikPeaO2B__Status__c'};
    
    public static set<string>GatewayResponseUpdate=new set<string>{'ChikPeaO2B__Acceptable__c','ChikPeaO2B__Action__c','ChikPeaO2B__Code_Type__c','ChikPeaO2B__Code__c','ChikPeaO2B__Definition__c','ChikPeaO2B__Payment_Gateway__c','ChikPeaO2B__Screen_Message__c','ChikPeaO2B__Status__c'};
    
    public static set<string>GatewayResponseDelete=new set<string>();
    //######### Gateway Response [END] ############//   
    
    //##### #27. Gateway  [START] ############//
    public static set<string>GatewayInsert=new set<string>{'ChikPeaO2B__Address_Verification_ZIP_Only__c','ChikPeaO2B__Address_Verification__c','ChikPeaO2B__Attach_Note__c','ChikPeaO2B__Capture_Card_Details__c','ChikPeaO2B__Card_Code_Verification__c','ChikPeaO2B__Currency__c','ChikPeaO2B__Gateway_Type__c','ChikPeaO2B__Gateway_Url_Prod_Fallback__c','ChikPeaO2B__Gateway_Url_Prod__c','ChikPeaO2B__Gateway_Url_Test_Fallback__c','ChikPeaO2B__Gateway_Url_Test__c','ChikPeaO2B__Login_Id__c','ChikPeaO2B__Merchant_Reference__c','ChikPeaO2B__O2B_Setting__c','ChikPeaO2B__Partial_Authorization__c','ChikPeaO2B__Password__c','ChikPeaO2B__Primary_Site_Available__c','ChikPeaO2B__Reporting_Gateway_Url_Prod__c','ChikPeaO2B__Reporting_Gateway_Url_Test__c','ChikPeaO2B__Retry_Count__c','ChikPeaO2B__Retry_Interval__c','ChikPeaO2B__Terminal_ID__c','ChikPeaO2B__Test_Mode__c','ChikPeaO2B__Time_out_limit__c','Name'};
    
    public static set<string>GatewayUpdate=new set<string>{'ChikPeaO2B__Address_Verification_ZIP_Only__c','ChikPeaO2B__Address_Verification__c','ChikPeaO2B__Attach_Note__c','ChikPeaO2B__Capture_Card_Details__c','ChikPeaO2B__Card_Code_Verification__c','ChikPeaO2B__Currency__c','ChikPeaO2B__Gateway_Type__c','ChikPeaO2B__Gateway_Url_Prod_Fallback__c','ChikPeaO2B__Gateway_Url_Prod__c','ChikPeaO2B__Gateway_Url_Test_Fallback__c','ChikPeaO2B__Gateway_Url_Test__c','ChikPeaO2B__Login_Id__c','ChikPeaO2B__Merchant_Reference__c','ChikPeaO2B__O2B_Setting__c','ChikPeaO2B__Partial_Authorization__c','ChikPeaO2B__Password__c','ChikPeaO2B__Primary_Site_Available__c','ChikPeaO2B__Reporting_Gateway_Url_Prod__c','ChikPeaO2B__Reporting_Gateway_Url_Test__c','ChikPeaO2B__Retry_Count__c','ChikPeaO2B__Retry_Interval__c','ChikPeaO2B__Terminal_ID__c','ChikPeaO2B__Test_Mode__c','ChikPeaO2B__Time_out_limit__c','Name'};
    
    public static set<string>GatewayDelete=new set<string>();
    //######### Gateway [END] ############//    
    
    //##### #28. Dunning Rule  [START] ############//
    public static set<string>DunningRuleInsert=new set<string>{'ChikPeaO2B__Account_Due__c','ChikPeaO2B__Invoice_Due__c','ChikPeaO2B__O2B_Setting__c','ChikPeaO2B__Past_Days__c','Name'};
    
    public static set<string>DunningRuleUpdate=new set<string>{'ChikPeaO2B__Account_Due__c','ChikPeaO2B__Invoice_Due__c','ChikPeaO2B__O2B_Setting__c','ChikPeaO2B__Past_Days__c','Name'};
    
    public static set<string>DunningRuleDelete=new set<string>();
    //######### Dunning Rule [END] ############//
    
    //##### #29.Invoice  [START] ############//
    public static set<string>InvoiceInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Bill_Date__c','ChikPeaO2B__Billing_City__c','ChikPeaO2B__Billing_Contact__c','ChikPeaO2B__Billing_Country__c','ChikPeaO2B__Billing_State_Province__c',
    'ChikPeaO2B__Billing_Street__c','ChikPeaO2B__Billing_Zip_Postal_Code__c','ChikPeaO2B__Cancel_Delete_Allowed__c','ChikPeaO2B__Cancelled__c','ChikPeaO2B__Collection_Email_Date__c','ChikPeaO2B__Currency_Code__c','ChikPeaO2B__Currency_Rate__c',
    'ChikPeaO2B__Disputed__c','ChikPeaO2B__Email_Sent__c','ChikPeaO2B__Open_Balance__c','ChikPeaO2B__Order__c','ChikPeaO2B__Other_Charges__c','ChikPeaO2B__Payment_Term__c','ChikPeaO2B__Period_From__c','ChikPeaO2B__Period_To__c','ChikPeaO2B__Purchase_Discount__c',
    'ChikPeaO2B__Reminder__c','ChikPeaO2B__Tax_Charge__c'};
    
    public static set<string>InvoiceUpdate=new set<string>{'ChikPeaO2B__Bill_Date__c','ChikPeaO2B__Billing_City__c','ChikPeaO2B__Billing_Contact__c','ChikPeaO2B__Billing_Country__c','ChikPeaO2B__Billing_State_Province__c','ChikPeaO2B__Billing_Street__c',
    'ChikPeaO2B__Billing_Zip_Postal_Code__c','ChikPeaO2B__Cancel_Delete_Allowed__c','ChikPeaO2B__Cancelled__c','ChikPeaO2B__Collection_Email_Date__c','ChikPeaO2B__Currency_Code__c','ChikPeaO2B__Currency_Rate__c','ChikPeaO2B__Disputed__c',
    'ChikPeaO2B__Email_Sent__c','ChikPeaO2B__Open_Balance__c','ChikPeaO2B__Order__c','ChikPeaO2B__Other_Charges__c','ChikPeaO2B__Payment_Term__c','ChikPeaO2B__Period_From__c',
    'ChikPeaO2B__Period_To__c','ChikPeaO2B__Purchase_Discount__c','ChikPeaO2B__Reminder__c','ChikPeaO2B__Tax_Charge__c'};
    
    public static set<string>InvoiceDelete=new set<string>();
    //######### Invoice [END] ############//
    
    //##### #30.Invoice Line  [START] ############//
    public static set<string>InvoiceLineInsert=new set<string>{'ChikPeaO2B__Description__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Item__c','ChikPeaO2B__Line_Type__c','ChikPeaO2B__Period_From__c','ChikPeaO2B__Period_To__c','ChikPeaO2B__Purchase__c','ChikPeaO2B__Qty__c','ChikPeaO2B__Subscription__c','ChikPeaO2B__Tax_Rate__c','ChikPeaO2B__Unit_Rate__c','ChikPeaO2B__Usage__c','ChikPeaO2B__isCredit_Note_Created__c'};
    
    public static set<string>InvoiceLineUpdate=new set<string>{'ChikPeaO2B__Description__c','ChikPeaO2B__Invoice__c','ChikPeaO2B__Item__c','ChikPeaO2B__Line_Type__c','ChikPeaO2B__Period_From__c','ChikPeaO2B__Period_To__c','ChikPeaO2B__Purchase__c','ChikPeaO2B__Qty__c','ChikPeaO2B__Subscription__c','ChikPeaO2B__Tax_Rate__c','ChikPeaO2B__Unit_Rate__c','ChikPeaO2B__Usage__c','ChikPeaO2B__isCredit_Note_Created__c'};
    
    public static set<string>InvoiceLineDelete=new set<string>();
    //######### Invoice Line [END] ############//
    
    //##### #31. Quote  [START] ############//
    public static set<string>QuoteInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Comments__c','ChikPeaO2B__Contact__c','ChikPeaO2B__Contract_Term_Months__c','ChikPeaO2B__Expiration_Date__c','ChikPeaO2B__PO_Number__c','ChikPeaO2B__Payment_Method__c','ChikPeaO2B__Payment_Term__c','ChikPeaO2B__Price_Book__c','ChikPeaO2B__Purchase_Discount__c','ChikPeaO2B__Status__c','ChikPeaO2B__Submission__c','ChikPeaO2B__Tax_Rate__c'};
    
    public static set<string>QuoteUpdate=new set<string>{'ChikPeaO2B__Comments__c','ChikPeaO2B__Contact__c','ChikPeaO2B__Contract_Term_Months__c','ChikPeaO2B__Expiration_Date__c','ChikPeaO2B__PO_Number__c','ChikPeaO2B__Payment_Method__c','ChikPeaO2B__Payment_Term__c','ChikPeaO2B__Price_Book__c','ChikPeaO2B__Purchase_Discount__c','ChikPeaO2B__Status__c','ChikPeaO2B__Submission__c','ChikPeaO2B__Tax_Rate__c'};
    
    public static set<string>QuoteDelete=new set<string>();
    //######### Quote [END] ############//
    
    //##### #32.Package  [START] ############//
    public static set<string>PackageInsert=new set<string>{'ChikPeaO2B__Bill_Cycle__c','ChikPeaO2B__Bill_On_Package__c','ChikPeaO2B__Description__c','ChikPeaO2B__Package_Discount__c','ChikPeaO2B__Package_Item__c','ChikPeaO2B__Package_Type__c','ChikPeaO2B__Price_Book__c','Name'};
    
    public static set<string>PackageUpdate=new set<string>{'ChikPeaO2B__Bill_Cycle__c','ChikPeaO2B__Bill_On_Package__c','ChikPeaO2B__Description__c','ChikPeaO2B__Package_Discount__c','ChikPeaO2B__Package_Item__c','ChikPeaO2B__Package_Type__c','ChikPeaO2B__Price_Book__c','Name'};
    
    public static set<string>PackageDelete=new set<string>();
    //######### Package [END] ############//
    
    //##### #33.Order Line  [START] ############//
    public static set<string>OrderLineInsert=new set<string>{'ChikPeaO2B__Agreement__c','ChikPeaO2B__Description__c','ChikPeaO2B__Item_Type__c','ChikPeaO2B__Item__c','ChikPeaO2B__Order__c','ChikPeaO2B__Package__c','ChikPeaO2B__Quantity__c','ChikPeaO2B__Quote_Line__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Status__c','ChikPeaO2B__Subscription_Term_Months__c','ChikPeaO2B__Subscription_start_date__c','ChikPeaO2B__Unit_Price__c'};
    
    public static set<string>OrderLineUpdate=new set<string>{'ChikPeaO2B__Agreement__c','ChikPeaO2B__Description__c','ChikPeaO2B__Item_Type__c','ChikPeaO2B__Item__c','ChikPeaO2B__Package__c','ChikPeaO2B__Quantity__c','ChikPeaO2B__Quote_Line__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Status__c','ChikPeaO2B__Subscription_Term_Months__c','ChikPeaO2B__Subscription_start_date__c','ChikPeaO2B__Unit_Price__c'};
    
    public static set<string>OrderLineDelete=new set<string>();
    //######### Order Line [END] ############//
    
    //##### #34.Order  [START] ############//
    public static set<string>OrderInsert=new set<string>{'ChikPeaO2B__Account__c','ChikPeaO2B__Billing_City__c','ChikPeaO2B__Billing_Country__c','ChikPeaO2B__Billing_State_Province__c','ChikPeaO2B__Billing_Street__c','ChikPeaO2B__Billing_Zip_Postal_Code__c','ChikPeaO2B__Contact__c','ChikPeaO2B__Invoice_For__c','ChikPeaO2B__Item__c','ChikPeaO2B__PO_Number__c','ChikPeaO2B__Price_Book__c','ChikPeaO2B__Quantity__c','ChikPeaO2B__Quote__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Shipping_City__c','ChikPeaO2B__Shipping_Country__c','ChikPeaO2B__Shipping_Required__c','ChikPeaO2B__Shipping_State_Province__c','ChikPeaO2B__Shipping_Street__c','ChikPeaO2B__Shipping_Zip_Postal_Code__c','ChikPeaO2B__Status__c','ChikPeaO2B__Tax_Rate__c','ChikPeaO2B__Unit_Price__c'};
    
    public static set<string>OrderUpdate=new set<string>{'ChikPeaO2B__Billing_City__c','ChikPeaO2B__Billing_Country__c','ChikPeaO2B__Billing_State_Province__c','ChikPeaO2B__Billing_Street__c','ChikPeaO2B__Billing_Zip_Postal_Code__c','ChikPeaO2B__Contact__c','ChikPeaO2B__Invoice_For__c','ChikPeaO2B__Item__c','ChikPeaO2B__PO_Number__c','ChikPeaO2B__Price_Book__c','ChikPeaO2B__Quantity__c','ChikPeaO2B__Quote__c','ChikPeaO2B__Rate_Plan__c','ChikPeaO2B__Shipping_City__c','ChikPeaO2B__Shipping_Country__c','ChikPeaO2B__Shipping_Required__c','ChikPeaO2B__Shipping_State_Province__c','ChikPeaO2B__Shipping_Street__c','ChikPeaO2B__Shipping_Zip_Postal_Code__c','ChikPeaO2B__Status__c','ChikPeaO2B__Tax_Rate__c','ChikPeaO2B__Unit_Price__c'};
    
    public static set<string>OrderDelete=new set<string>();
    //######### Order [END] ############//
    
    //##### #35.O2B Setting  [START] ############//
    public static set<string>O2BSettingInsert=new set<string>{'ChikPeaO2B__Account_based_tiering__c','ChikPeaO2B__Admin_Email__c','ChikPeaO2B__Batch_Size__c','ChikPeaO2B__Bill_Schedule_Job_Id__c','ChikPeaO2B__Billing_Day__c','ChikPeaO2B__Billing_Type__c','ChikPeaO2B__Card_Process_Frequency__c','ChikPeaO2B__Card_Process_Interval_days__c','ChikPeaO2B__Create_Invoice_on_Process_Order__c','ChikPeaO2B__Credit_Note_Default_Validity__c','ChikPeaO2B__Default_Gateway__c','ChikPeaO2B__Enable_Gateway_Approved_Trx_Log__c','ChikPeaO2B__Enable_Gateway_Trx_Log__c','ChikPeaO2B__Gateway_Plugin__c','ChikPeaO2B__Is_Parent_Billing_Enabled__c','ChikPeaO2B__Line_Payment__c','ChikPeaO2B__Paypal_URL__c','ChikPeaO2B__Refund_Any_Amount__c','ChikPeaO2B__Repoert_URL1__c','ChikPeaO2B__Report_URL2__c','ChikPeaO2B__Report_URL3__c','ChikPeaO2B__Save_Card_Number__c','ChikPeaO2B__Save_Credit_Card__c','ChikPeaO2B__Save_Failed_Payment__c','ChikPeaO2B__Schedule_Time_For_Credit_Card_Batch__c','ChikPeaO2B__Schedule_Time__c','ChikPeaO2B__Scheduled_Time_for_Collection_Batch__c','ChikPeaO2B__Show_Billing_Address_On_Order_Screen__c','ChikPeaO2B__Show_Item_Image__c','ChikPeaO2B__Show_Shipping_Addresss_On_Order_Screen__c','ChikPeaO2B__Single_Item_Order__c','ChikPeaO2B__Subscription_Payment_Status__c','ChikPeaO2B__Usage_on_orderline_quantity__c'};
    
    public static set<string>O2BSettingUpdate=new set<string>{'ChikPeaO2B__Account_based_tiering__c','ChikPeaO2B__Admin_Email__c','ChikPeaO2B__Batch_Size__c','ChikPeaO2B__Bill_Schedule_Job_Id__c','ChikPeaO2B__Billing_Day__c','ChikPeaO2B__Billing_Type__c','ChikPeaO2B__Card_Process_Frequency__c','ChikPeaO2B__Card_Process_Interval_days__c','ChikPeaO2B__Create_Invoice_on_Process_Order__c','ChikPeaO2B__Credit_Note_Default_Validity__c','ChikPeaO2B__Default_Gateway__c','ChikPeaO2B__Enable_Gateway_Approved_Trx_Log__c','ChikPeaO2B__Enable_Gateway_Trx_Log__c','ChikPeaO2B__Gateway_Plugin__c','ChikPeaO2B__Is_Parent_Billing_Enabled__c','ChikPeaO2B__Line_Payment__c','ChikPeaO2B__Paypal_URL__c','ChikPeaO2B__Refund_Any_Amount__c','ChikPeaO2B__Repoert_URL1__c','ChikPeaO2B__Report_URL2__c','ChikPeaO2B__Report_URL3__c','ChikPeaO2B__Save_Card_Number__c','ChikPeaO2B__Save_Credit_Card__c','ChikPeaO2B__Save_Failed_Payment__c','ChikPeaO2B__Schedule_Time_For_Credit_Card_Batch__c','ChikPeaO2B__Schedule_Time__c','ChikPeaO2B__Scheduled_Time_for_Collection_Batch__c','ChikPeaO2B__Show_Billing_Address_On_Order_Screen__c','ChikPeaO2B__Show_Item_Image__c','ChikPeaO2B__Show_Shipping_Addresss_On_Order_Screen__c','ChikPeaO2B__Single_Item_Order__c','ChikPeaO2B__Subscription_Payment_Status__c','ChikPeaO2B__Usage_on_orderline_quantity__c'};
    
    public static set<string>O2BSettingDelete=new set<string>();
    //######### O2B Setting [END] ############//
    
    //##### #36.Item  [START] ############//
    public static set<string>ItemInsert=new set<string>{'ChikPeaO2B__Active__c','ChikPeaO2B__Category__c','ChikPeaO2B__Description__c','ChikPeaO2B__Is_Shipping__c','ChikPeaO2B__Is_aggregation__c','ChikPeaO2B__Is_package_Item__c','ChikPeaO2B__Is_prorate__c','ChikPeaO2B__Item_Type__c','Name'};
    
    public static set<string>ItemUpdate=new set<string>{'ChikPeaO2B__Active__c','ChikPeaO2B__Category__c','ChikPeaO2B__Description__c','ChikPeaO2B__Is_Shipping__c','ChikPeaO2B__Is_aggregation__c','ChikPeaO2B__Is_package_Item__c','ChikPeaO2B__Is_prorate__c','ChikPeaO2B__Item_Type__c','Name'};
    
    public static set<string>ItemDelete=new set<string>();
    //######### Item [END] ############//   
    
    //##### #37.Invoice Line History  [START] ############//
    public static set<string>InvoiceLineHistoryInsert=new set<string>{'ChikPeaO2B__Invoice_Line__c','ChikPeaO2B__Invoice_Required__c','ChikPeaO2B__Item__c','ChikPeaO2B__Line_Type__c','ChikPeaO2B__Period_From__c','ChikPeaO2B__Period_To__c','ChikPeaO2B__Qty__c','ChikPeaO2B__Subscription__c','ChikPeaO2B__Unit_Rate__c'};
    
    public static set<string>InvoiceLineHistoryUpdate=new set<string>{'CChikPeaO2B__Invoice_Line__c','ChikPeaO2B__Invoice_Required__c','ChikPeaO2B__Item__c','ChikPeaO2B__Line_Type__c','ChikPeaO2B__Period_From__c','ChikPeaO2B__Period_To__c','ChikPeaO2B__Qty__c','ChikPeaO2B__Unit_Rate__c'};
    
    public static set<string>InvoiceLineHistoryDelete=new set<string>();
    //######### Invoice Line History [END] ############//    
}