/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-02-27
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
private class TestPG_Exceptions {

    static testMethod void testPgException() {
        
        Test.startTest();
        
        String errCode = 'SF001';
        String errType = 'GatewayException';
        String errMessage = 'An error occured';
        
        PG_Exception px = new PG_Exception();
        px.setErrorCode(errCode);
        px.setErrorType(errType);
        px.setErrorMesssage(errMessage);
        
        System.assertEquals(errCode, px.getErrorCode());
        System.assertEquals(errType, px.getErrorType());
        System.assertEquals(errMessage, px.getErrorMesssage());
        
        Test.stopTest();
    }
}