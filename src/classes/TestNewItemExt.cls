@isTest
public class TestNewItemExt
{
    @isTest
    static void testMethod1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false);
        insert(o2bset);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        Item__c itm=new Item__c();
        
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(itm);
        NewItemExt newitmext = new NewItemExt(stdc);
        PageReference pageRef = Page.NewItem;
        Test.setCurrentPage(pageRef);
        itm.name='oneoff';
        itm.Item_Type__c='One-Off';
        itm.Category__c='software';
        itm.Active__c=true;
        newitmext.SaveNewItem();
        insert(itm);
        newitmext.edititm();
        itm.category__c='Hardware';
        newitmext.UpdateItem();
        newitmext.cancel();
    }
    @isTest
    static void testMethod2()
    {
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        insert(oneoff);
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp1);
        Apexpages.Standardcontroller stdc1 = new Apexpages.Standardcontroller(oneoff);
        NewItemExt newitmext1 = new NewItemExt(stdc1);
        PageReference pageRef1 = Page.NewItem;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('id',oneoff.id);
        NewItemExt newitmext2 = new NewItemExt(stdc1);
        newitmext1.itm=oneoff;
        newitmext1.itm.category__c='Hardware';
        newitmext1.UpdateItem();
        newitmext1.cancel();
        newitmext1.getRatePlans();
        newitmext1.getRatePlans();
        newitmext1.EditRateID=rp1.id;
        newitmext1.updateRate();
        newitmext1.getRatePlans();
        newitmext1.EditRateID=rp1.id;
        newitmext1.cancelrateupdate();
        newitmext1.addrp();
        newitmext1.CancelAddRP();
        newitmext1.newRatePlan.Pricing_Type__c='Flat';
        newitmext1.newRatePlan.Price_book__c=pb.id;
        newitmext1.insertRP();
        newitmext1.pricingType();
        newitmext1.DelRateID=rp1.id;
        newitmext1.getRatePlans();
        newitmext1.deleteRP();
    }
    @isTest
    static void testMethod3()
    {
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        insert(rec);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp3);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(rec);
        NewItemExt newitmext = new NewItemExt(stdc);
        PageReference pageRef = Page.NewItem;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',rec.id);
        newitmext.itm=rec;
        newitmext.getRatePlans();
        newitmext.getRatePlans();
        newitmext.ADDTP();
        newitmext.newtp=new Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        newitmext.AddNewTP();
        newitmext.insertTP();
        Tiered_Pricing__c tp1=new Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        insert(tp1);
        newitmext.getTieredPs();
        newitmext.EditTieredID=tp1.id;
        newitmext.updateTP();
        newitmext.cancelTieredPriceupdate();
        newitmext.cancelTP();
        newitmext.DelTieredID=tp1.id;
        newitmext.deleteTP();
    }
    @isTest
    static void testMethod4()
    {
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        insert(post);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp3);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(post);
        NewItemExt newitmext = new NewItemExt(stdc);
        PageReference pageRef = Page.NewItem;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',post.id);
        newitmext.itm=post;
        newitmext.getRatePlans();
        newitmext.getRatePlans();
        newitmext.ADDVolumePricing();
        newitmext.newvp=new volume_Pricing__c(ChikPeaO2B__Item__c=post.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        //newitmext.AddNewVP();
        newitmext.insertVolumePricing();
        Volume_Pricing__c vp1=new Volume_Pricing__c(ChikPeaO2B__Item__c=post.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        insert(vp1);
        newitmext.getVolumePricings();
        newitmext.EditVolumeID=vp1.id;
        newitmext.updateVolumePrice();
        newitmext.cancelVolumePriceupdate();
        newitmext.cancelVolumePriceInsert();
        newitmext.DelVolumeID=vp1.id;
        newitmext.deleteVolumePrice();
        newitmext.changepricingtype();
        newitmext.Finish();
    }
    @isTest
    static void testMethod5()
    {
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        insert(post);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp3);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(post);
        NewItemExt newitmext = new NewItemExt(stdc);
        PageReference pageRef = Page.NewItem;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',post.id);
        newitmext.itm=post;
        newitmext.getRatePlans();
        newitmext.getRatePlans();
        newitmext.ADDTieredFlatPricing();
        newitmext.newtf=new ChikPeaO2B__Tiered_Flat_Pricing__c(ChikPeaO2B__Item__c=post.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        //newitmext.AddNewVP();
        newitmext.insertTieredFlatPricing();
        ChikPeaO2B__Tiered_Flat_Pricing__c tf1=new ChikPeaO2B__Tiered_Flat_Pricing__c(ChikPeaO2B__Item__c=post.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        insert(tf1);
        newitmext.getTieredFlats();
        newitmext.EditTieredFlatID=tf1.id;
        newitmext.updateTieredFlatPrice();
        newitmext.cancelTieredFlatPriceupdate();
        newitmext.cancelTieredFlatInsert();
        newitmext.DelTieredFlatID=tf1.id;
        newitmext.deleteTieredFlatPrice();
        newitmext.changepricingtype();
        newitmext.Finish();
    }
    @isTest
    static void testMethod6()
    {
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        insert(post);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp3);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(post);
        NewItemExt newitmext = new NewItemExt(stdc);
        PageReference pageRef = Page.NewItem;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',post.id);
        newitmext.itm=post;
        newitmext.getRatePlans();
        newitmext.getRatePlans();
        newitmext.ADDVolumeTieredPricing();
        newitmext.newvt=new ChikPeaO2B__Volume_Tiered_pricing__c(ChikPeaO2B__Item__c=post.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        newitmext.insertVolumeTieredPricing();
        ChikPeaO2B__Volume_Tiered_pricing__c vt1=new ChikPeaO2B__Volume_Tiered_pricing__c(ChikPeaO2B__Item__c=post.id,Lower_Limit__c=1,Upper_Limit__c=100,Recurring_Charge__c=33,ChikPeaO2B__Rate_Plan__c=rp3.id);
        insert(vt1);
        newitmext.getVolumeTiereds();
        newitmext.EditVolumeTieredID=vt1.id;
        newitmext.updateVolumeTieredPrice();
        newitmext.cancelVolumeTieredPriceupdate();
        newitmext.cancelVolumeTieredInsert();
        newitmext.DelVolumeTieredID=vt1.id;
        newitmext.deleteVolumeTieredPrice();
        newitmext.changepricingtype();
        newitmext.Finish();
    }
}