/*
** Class         :  InvoiceCancelOrDelete
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  8 april 14
** Modified by   :  S Pal (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

global with sharing class InvoiceCancelOrDelete{
    Webservice static String SubscriptionValidationForCancelDelete(Id Invid)
    {
        string returnString='';
        ChikPeaO2B__Invoice__c Inv=[SELECT id,name,ChikPeaO2B__Cancel_Delete_Allowed__c,ChikPeaO2B__Cancelled__c,(SELECT id,name,ChikPeaO2B__Line_Type__c,ChikPeaO2B__Subscription__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__Invoice__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__Billing_Stopped__c,ChikPeaO2B__Usage__r.ChikPeaO2B__Invoice__c,ChikPeaO2B__Line_Tax__c from ChikPeaO2B__Invoice_Lines__r where ChikPeaO2B__Line_Type__c='Subscription' or ChikPeaO2B__Line_Type__c='PostPaid Usage' or ChikPeaO2B__Line_Type__c='PrePaid Usage') from ChikPeaO2B__Invoice__c where id =:Invid];
        if(Inv.ChikPeaO2B__Invoice_Lines__r!=null && inv.ChikPeaO2B__Invoice_Lines__r.size()>0)
        {
            for(ChikPeaO2B__Invoice_Line__c invl : inv.ChikPeaO2B__Invoice_Lines__r)
            {
                if(invl.ChikPeaO2B__Line_Type__c=='Subscription')
                {
                    if(invl.ChikPeaO2B__Subscription__r.ChikPeaO2B__Billing_Stopped__c)
                    { 
                        if(invl.ChikPeaO2B__Subscription__r.ChikPeaO2B__Invoice__c==Invid)
                            returnString='Success';
                        else
                        {
                            returnString=invl.name+' can not be Cancelled or Deleted';
                            break;
                        }
                                
                    }
                    else
                        returnString='Success';
                }
                else if(invl.ChikPeaO2B__Line_Type__c=='PostPaid Usage' || invl.ChikPeaO2B__Line_Type__c=='PrePaid Usage')
                {
                    if(invl.ChikPeaO2B__Usage__r.ChikPeaO2B__Invoice__c==Invid)
                    {
                        returnString='Success';   
                    }
                    else
                    {
                        returnString=invl.name+' can not be Cancelled or Deleted';
                        break;
                    }
                }
            }
        }
        else
            returnString='Success';
        return returnString;
    }
    Webservice static void cancelordel(id Invid)
    {
        Map<id,set<id>>SubidMap=new Map<id,set<id>>();
        Map<id,ChikPeaO2B__Invoice_Line__c>InvlMap=new Map<id,ChikPeaO2B__Invoice_Line__c>();
        set<id>Purid =new set<id>();
        List<ChikPeaO2B__Purchase__c>PurUpdateList=new List<ChikPeaO2B__Purchase__c >();
        List<ChikPeaO2B__Invoice_Line_History__c>InvlHistory=new List<ChikPeaO2B__Invoice_Line_History__c>();
        Set<ChikPeaO2B__Usage_History__c> usgHistorySet=new Set<ChikPeaO2B__Usage_History__c>();
        Map<id,id>UsageMap=new Map<id,id>();
        List<usage__c>UsageUpdateList=new List<usage__c>();
        
        ChikPeaO2B__Invoice__c Inv=[SELECT id,name,ChikPeaO2B__Cancel_Delete_Allowed__c,
        	ChikPeaO2B__Cancelled__c,(SELECT id,name,ChikPeaO2B__Line_Type__c,ChikPeaO2B__Subscription__c,
        		ChikPeaO2B__Purchase__c,ChikPeaO2B__Usage__c,ChikPeaO2B__Item__c,
        			ChikPeaO2B__Period_From__c,ChikPeaO2B__Period_To__c,ChikPeaO2B__Qty__c,
        				ChikPeaO2B__Unit_Rate__c,ChikPeaO2B__Line_Tax__c,
        				ChikPeaO2B__Usage_History__c,ChikPeaO2B__Usage_History__r.ChikPeaO2B__Invoice_Cancelled__c
        				 from ChikPeaO2B__Invoice_Lines__r) 
        				from ChikPeaO2B__Invoice__c where id =:Invid];
        if(Inv.ChikPeaO2B__Invoice_Lines__r!=null && inv.ChikPeaO2B__Invoice_Lines__r.size()>0)
        {
            for(ChikPeaO2B__Invoice_Line__c invl : inv.ChikPeaO2B__Invoice_Lines__r)
            {
                if(invl.ChikPeaO2B__Line_Type__c=='Purchase')
                {
                    Purid.add(invl.ChikPeaO2B__Purchase__c);        
                }
                else if(invl.ChikPeaO2B__Line_Type__c=='Subscription' && invl.ChikPeaO2B__Subscription__c!=null)
                {
                    if(!SubidMap.containskey(invl.ChikPeaO2B__Subscription__c))
                    {
                        set<id>temp=new set<id>();
                        temp.add(invl.id);
                        SubidMap.put(invl.ChikPeaO2B__Subscription__c,temp);    
                    }
                    else
                    {
                        set<id>temp=SubidMap.get(invl.ChikPeaO2B__Subscription__c);
                        temp.add(invl.id);
                        SubidMap.put(invl.ChikPeaO2B__Subscription__c,temp);    
                    }
                }
                else if((invl.ChikPeaO2B__Line_Type__c=='PostPaid Usage' || invl.ChikPeaO2B__Line_Type__c=='PrePaid Usage') && invl.ChikPeaO2B__Usage__c!=null)
                {
                    UsageMap.put(invl.ChikPeaO2B__Usage__c,invl.id);
                }
                InvlMap.put(invl.id,invl);
                //Canceling Usage History
	            	//to avoid extra query creating a new Usage History with the Id of the History to be canceled
	            	if(invl.ChikPeaO2B__Usage_History__c!=null){
	            		ChikPeaO2B__Usage_History__c usage_his=new ChikPeaO2B__Usage_History__c(Id=invl.ChikPeaO2B__Usage_History__c);
	            		usage_his.ChikPeaO2B__Invoice_Cancelled__c=true;
	            		usgHistorySet.add(usage_his);
	            	}
	            // 
            }
        }
        if(Purid!=null && Purid.size()>0)
        {
            for(ChikPeaO2B__Purchase__c pur : [SELECT id,name,ChikPeaO2B__Refunded__c,ChikPeaO2B__Invoiced__c,ChikPeaO2B__Invoice__c from ChikPeaO2B__Purchase__c where id in : Purid])
            {
                pur.ChikPeaO2B__Refunded__c=true;
                pur.ChikPeaO2B__Invoiced__c=false;
                pur.ChikPeaO2B__Invoice__c =null;
                PurUpdateList.add(pur);
            }
        }
        if(SubidMap.keyset()!=null && SubidMap.keyset().size()>0)
        {
            for(id subid : SubidMap.keyset())
            {
                if(SubidMap.get(subid)!=null)
                {
                    for(id invlid:SubidMap.get(subid))
                    {
                        if(InvlMap.get(invlid)!=null)
                        {
                            ChikPeaO2B__Invoice_Line_History__c invlh=new ChikPeaO2B__Invoice_Line_History__c();
                            invlh.ChikPeaO2B__Invoice_Line__c=invlid;
                            invlh.ChikPeaO2B__Item__c=InvlMap.get(invlid).ChikPeaO2B__Item__c;
                            invlh.ChikPeaO2B__Line_Type__c=InvlMap.get(invlid).ChikPeaO2B__Line_Type__c;
                            invlh.ChikPeaO2B__Period_From__c=InvlMap.get(invlid).ChikPeaO2B__Period_From__c;
                            invlh.ChikPeaO2B__Period_To__c=InvlMap.get(invlid).ChikPeaO2B__Period_To__c;
                            invlh.ChikPeaO2B__Qty__c=InvlMap.get(invlid).ChikPeaO2B__Qty__c;
                            invlh.ChikPeaO2B__Invoice_Required__c='Yes';
                            invlh.ChikPeaO2B__Subscription__c=subid;
                            invlh.ChikPeaO2B__Unit_Rate__c=InvlMap.get(invlid).ChikPeaO2B__Unit_Rate__c;
                            invlh.ChikPeaO2B__Line_Tax__c=InvlMap.get(invlid).ChikPeaO2B__Line_Tax__c;
                            InvlHistory.add(invlh);
                        }
                    }
                }
            }
        }
        if(UsageMap!=null && UsageMap.keyset().size()>0)
        {
            for(ChikPeaO2B__Usage__c usg : [SELECT id,name,ChikPeaO2B__Total_Usage__c,ChikPeaO2B__Total_Usage_Back_Up__c, 
            (select Id, ChikPeaO2B__Total_Usage__c, ChikPeaO2B__Remaining_Usage__c  from Usage_Histories__r order by createdDate desc limit 1)
            from ChikPeaO2B__Usage__c where id in : UsageMap.keyset()])
            {
                if(UsageMap.get(usg.id)!=null && InvlMap.get(UsageMap.get(usg.id))!=null)
                {
                    system.debug('===>'+usg.ChikPeaO2B__Total_Usage__c+'=='+UsageMap.get(usg.id)+'=='+InvlMap.get(UsageMap.get(usg.id)));
                    usg.ChikPeaO2B__Total_Usage_Back_Up__c = usg.ChikPeaO2B__Total_Usage__c;
                    
                    //by mehebub on 18-May-2015
                    //usg.ChikPeaO2B__Total_Usage__c = InvlMap.get(UsageMap.get(usg.id)).ChikPeaO2B__Qty__c;                    
                    if(usg.Usage_Histories__r.isEmpty()){
                    	usg.ChikPeaO2B__Total_Usage__c = InvlMap.get(UsageMap.get(usg.id)).ChikPeaO2B__Qty__c;
                    }else{
                    	if(usg.Usage_Histories__r[0].ChikPeaO2B__Remaining_Usage__c==null){
                    		usg.Usage_Histories__r[0].ChikPeaO2B__Remaining_Usage__c=0;
                    	}
                    	if(usg.Usage_Histories__r[0].ChikPeaO2B__Total_Usage__c==0){
                    		usg.Usage_Histories__r[0].ChikPeaO2B__Total_Usage__c=0;
                    	}
                    	usg.ChikPeaO2B__Remaining_Usage__c=usg.Usage_Histories__r[0].ChikPeaO2B__Remaining_Usage__c
                    		+usg.Usage_Histories__r[0].ChikPeaO2B__Total_Usage__c;
                    	usg.ChikPeaO2B__Total_Usage__c=usg.Usage_Histories__r[0].ChikPeaO2B__Total_Usage__c;
                    }
                    //
                    
                    usg.ChikPeaO2B__Cancelled__c=true;
                    UsageUpdateList.add(usg);
                }
            }
        }
        
        Savepoint sp = Database.setSavepoint();
        if(PurUpdateList!=null && PurUpdateList.size()>0)
        {
            //=== CURD check [START] ===//
            Map<string,string>PurUpdateResultMap=new map<string,string>();
            PurUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Purchase__c',FieldPermissionSet.PurchaseUpdate,'update');
            if(PurUpdateResultMap.get('AllowDML')!=null && PurUpdateResultMap.get('AllowDML').equalsignorecase('true'))
            {
                update(PurUpdateList);
            }
            else
            {        
                Database.rollback(sp);
                return;
            }
            //=== CURD check [END] ===//
        }
        if(InvlHistory!=null && InvlHistory.size()>0)
        {
            //=== CURD check [START] ===//
            Map<string,string>InvlHisInsertResultMap=new map<string,string>();
            InvlHisInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Invoice_Line_History__c',FieldPermissionSet.InvoiceLineHistoryInsert,'insert');
            if(InvlHisInsertResultMap.get('AllowDML')!=null && InvlHisInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                insert(InvlHistory);
            }
            else
            {        
                Database.rollback(sp);
                return;
            }
            //=== CURD check [END] ===//
        }

        if(UsageUpdateList!=null && UsageUpdateList.size()>0)
        {
            //=== CURD check [START] ===//
            Map<string,string>UsgUpdateResultMap=new map<string,string>();
            UsgUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Usage__c',FieldPermissionSet.UsageUpdate,'update');
            if(UsgUpdateResultMap.get('AllowDML')!=null && UsgUpdateResultMap.get('AllowDML').equalsignorecase('true'))
            {
                update(UsageUpdateList);
            }
            else
            {        
                Database.rollback(sp);
                return;
            }
            //=== CURD check [END] ===// 
        }
        inv.ChikPeaO2B__Cancelled__c=true;
        //=== CURD check [START] ===//
        Map<string,string>InvoUpdateResultMap=new map<string,string>();
        InvoUpdateResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__Invoice__c',FieldPermissionSet.InvoiceUpdate,'update');
        if(InvoUpdateResultMap.get('AllowDML')!=null && InvoUpdateResultMap.get('AllowDML').equalsignorecase('true'))
        {
            update(inv); 
            //Canceling Usage History
            	List<ChikPeaO2B__Usage_History__c> usgHistorylist=new List<ChikPeaO2B__Usage_History__c>(usgHistorySet);
            	update usgHistorylist;
            // 
        }    
        else
        {        
            Database.rollback(sp);
            return;
        }
        //=== CURD check [END] ===//     
    }
}