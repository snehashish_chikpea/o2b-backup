@isTest
public class TestListOfItem
{
    @isTest
    static void testMethod1()
    {
        Account acc=new Account(name='acc1');
        insert(acc);
    
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        List<ChikPeaO2B__Item__c>ItemList=new List<ChikPeaO2B__Item__c>();
        Map<string,rate_plan__c>rpmap=new Map<string,rate_plan__c>();
        Map<string,integer>itmqty=new Map<string,integer>();
        Map<string,decimal>selectedRPprice=new Map<string,decimal>();
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        insert(oneoff);        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp1);
        ItemList.add(oneoff);

        ChikPeaO2B__Package__c pkg = new ChikPeaO2B__Package__c(name='pkg1',ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Package_Type__c='Post-Paid Usage',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Bill_On_Package__c=false);
        insert(pkg);
        ChikPeaO2B__Bundled_Item__c bi=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Usage_Rate__c=1.2);
        insert(bi);
        rpmap.put(string.valueof(pkg.id)+string.valueof(oneoff.id),rp1);        
        itmqty.put(string.valueof(pkg.id)+string.valueof(oneoff.id),3);
        selectedRPprice.put(string.valueof(pkg.id)+string.valueof(oneoff.id),6.5);
        Test.startTest();
        ListOfItem loi=new ListOfItem();
        loi.getItems();
        map<id,boolean> pkgselmap=new map<id,boolean>();
        pkgselmap.put(pkg.id,true);
        ListOfItem.o2b2Package custompkg=new ListOfItem.o2b2Package(pkg,ItemList,rpmap,itmqty,selectedRPprice,pkgselmap);
        Test.stopTest();
    }
}