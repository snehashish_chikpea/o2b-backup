@isTest
public class TestOrderFormExtension
{
    @isTest
    static void testMethod1()
    {
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        List<ChikPeaO2B__Tiered_Pricing__c>tieredlist=new List<ChikPeaO2B__Tiered_Pricing__c>();
        List<ChikPeaO2B__Volume_Pricing__c>volumelist=new list<ChikPeaO2B__Volume_Pricing__c>();
        
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,Show_Billing_Address_On_Order_Screen__c=true,Show_Shipping_Addresss_On_Order_Screen__c=true);
        insert(o2bset);
        Account acc=new Account(name='acc1');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c rec1=new ChikPeaO2B__Item__c(name='rec1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec1);
        ChikPeaO2B__Item__c recV=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(recV);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        itemlist.add(pre);
        ChikPeaO2B__Item__c oneoffT=new ChikPeaO2B__Item__c(name='oneoffT',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoffT);
        ChikPeaO2B__Item__c oneoffV=new ChikPeaO2B__Item__c(name='oneoffV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoffV);
        ChikPeaO2B__Item__c oneofford=new ChikPeaO2B__Item__c(name='oneofford',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneofford);        
        ChikPeaO2B__Item__c record=new ChikPeaO2B__Item__c(name='record',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(record);
        ChikPeaO2B__Item__c postord=new ChikPeaO2B__Item__c(name='postord',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(postord);
        ChikPeaO2B__Item__c preord=new ChikPeaO2B__Item__c(name='preord',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        itemlist.add(preord);
        ChikPeaO2B__Item__c itm1=new ChikPeaO2B__Item__c(name='Item1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(itm1);
        ChikPeaO2B__Item__c itmx=new ChikPeaO2B__Item__c(name='Itemx',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(itmx);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp4);
        ChikPeaO2B__Rate_Plan__c rp5=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp5);
        ChikPeaO2B__Rate_Plan__c rp6=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='GB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp6);
        ChikPeaO2B__Rate_Plan__c rp7=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Non_Recurring_Charge__c=35,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='KB',ChikPeaO2B__Usage_Rate__c=1.5);
        RPlist.add(rp7);
        ChikPeaO2B__Rate_Plan__c rp8=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp8);
        ChikPeaO2B__Rate_Plan__c rp9=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneofford.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp9);
        ChikPeaO2B__Rate_Plan__c rp10=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=record.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp10);
        ChikPeaO2B__Rate_Plan__c rp11=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=postord.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp11);
        ChikPeaO2B__Rate_Plan__c rp12=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=preord.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp12);
        ChikPeaO2B__Rate_Plan__c rpp=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rpp);
        ChikPeaO2B__Rate_Plan__c rppx=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itmx.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rppx);
        insert(RPlist);
             
        ChikPeaO2B__Tiered_Pricing__c tr1=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr1);
        ChikPeaO2B__Tiered_Pricing__c tr2=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr2);
        ChikPeaO2B__Tiered_Pricing__c tr3=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr3);
        ChikPeaO2B__Tiered_Pricing__c tr4=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=200,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr4);
        ChikPeaO2B__Tiered_Pricing__c tr5=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=201,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=11,ChikPeaO2B__Usage_Rate__c=4);
        tieredlist.add(tr5);
        ChikPeaO2B__Tiered_Pricing__c tr6=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Non_Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr6);
        ChikPeaO2B__Tiered_Pricing__c tr7=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Non_Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr7);
        insert(tieredlist);
        
        ChikPeaO2B__Volume_Pricing__c vp1=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp1);
        ChikPeaO2B__Volume_Pricing__c vp2=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Non_Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp2);
        ChikPeaO2B__Volume_Pricing__c vp3=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp3);
        ChikPeaO2B__Volume_Pricing__c vp4=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Non_Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp4);
        ChikPeaO2B__Volume_Pricing__c vp5=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp8.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp5);
        ChikPeaO2B__Volume_Pricing__c vp6=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp8.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp6);
        insert(volumelist);
        
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c();
        
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(ord);
        OrderFormExtension ordext = new OrderFormExtension(stdc);
        PageReference pageRef = Page.Order_Form;
        Test.setCurrentPage(pageRef);
        ordext.getGeneralSectionFields();
        ordext.getBillingSectionFields();
        ordext.getShippingSectionFields();
        ordext.getSingleItemOrderSectionFields();
        ordext.copybillinginfo();
        ordext.copyshipinfo();
        ordext.copybillinfo=true;
        ordext.copyshipinfo=true;
        ordext.copybillinginfo();
        ordext.copyshipinfo();
        ordext.order.Rate_Plan__c=rp1.id;
        ordext.order.quantity__c=2;
        ordext.copydefaultprice();
        ordext.order.Rate_Plan__c=rp2.id;
        ordext.order.quantity__c=2;
        ordext.copydefaultprice();
        ordext.order.Rate_Plan__c=rp3.id;
        ordext.order.quantity__c=2;
        ordext.copydefaultprice();
        ordext.order.Rate_Plan__c=rp4.id;
        ordext.order.quantity__c=2;
        ordext.copydefaultprice();
        ordext.order.Rate_Plan__c=rp5.id;
        ordext.order.quantity__c=2;
        ordext.copydefaultprice();
        ordext.order.Rate_Plan__c=rp6.id;
        ordext.order.quantity__c=2;
        ordext.copydefaultprice();
        ordext.order.Rate_Plan__c=rp7.id;
        ordext.order.quantity__c=2;
        ordext.copydefaultprice();
        ordext.order.Rate_Plan__c=rp8.id;
        ordext.order.quantity__c=2;
        ordext.order.ChikPeaO2B__Account__c=acc.id;
        ordext.order.ChikPeaO2B__Price_Book__c=pb.id;
        ordext.order.ChikPeaO2B__Item__c=recV.id;
        ordext.order.ChikPeaO2B__Status__c='Open';
        ordext.order.ChikPeaO2B__Unit_Price__c=3;
        ordext.copydefaultprice();
        ordext.SaveXmlOrder();
        ordext.Cancel();
        
        string request='';
        request+='<?xml version=\'1.0\' encoding=\'us-ascii\'?>'+
                '<OrderDetails>'+
                '<Data type="ChikPeaO2B__Contact__c">null</Data>'+
                '<Data type="ChikPeaO2B__Tax_Rate__c">null</Data>'+
                '<Data type="ChikPeaO2B__Shipping_State_Province__c">null</Data>'+
                '<Data type="ChikPeaO2B__Status__c">Open</Data>'+
                '<Data type="ChikPeaO2B__Billing_Country__c">null</Data>'+
                '<Data type="ChikPeaO2B__Billing_State_Province__c">null</Data>'+
                '<Data type="ChikPeaO2B__Account__c">'+acc.id+'</Data>'+
                '<Data type="ChikPeaO2B__Billing_Zip_Postal_Code__c">null</Data>'+
                '<Data type="ChikPeaO2B__Tax_Rate__c">2</Data>'+
                '<Data type="ChikPeaO2B__Price_Book__c">'+pb.id+'</Data>'+
                '<Data type="ChikPeaO2B__Rate_Plan__c">'+rp8.id+':2'+'</Data>'+
                '<Data type="ChikPeaO2B__Shipping_Street__c">null</Data>'+
                '<Data type="ChikPeaO2B__PO_Number__c">null</Data>'+
                '<Data type="ChikPeaO2B__Billing_Street__c">null</Data>'+
                '<Data type="ChikPeaO2B__Shipping_Required__c">null</Data>'+
                '<Data type="ChikPeaO2B__Shipping_City__c">null</Data>'+
                '<Data type="ChikPeaO2B__Shipping_Zip_Postal_Code__c">null</Data>'+
                '<Data type="ChikPeaO2B__Billing_City__c">null</Data>'+
                '<Data type="ChikPeaO2B__Shipping_Country__c">null</Data>'+
                '</OrderDetails>';
         OrderSave.OrderInsert(request);    
    }

}