/*
** Class         :  Invoicing
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  11 march 14
** Description   :   
**Version        : 1.0(written by S Pal)
**Version        : 1.01(editted by Asitm9) Secuirty Issue fixed.(sharing)
**Version        : 1.02(editted by Asitm9) Exception Handling 
**Version        : 1.03(editted by Debajyoti M) agreement one bill added
*/

global with sharing class Invoicing{
    Webservice static id CreateInvoice(list<id> accidList,boolean isbatch){
        list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Schedule_Time__c from ChikPeaO2B__O2B_Setting__c limit 1];
        List<id>ReturnInvoiceIds=new List<id>();
        if(O2BsetList!=null && O2BsetList.size()>0)
        {
            if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Anniversary Subscription')
            {
                try{
                ReturnInvoiceIds = AnniversarySubscriptionBilling.generateSubscriptionBill(accidList,isbatch);
                system.debug('###'+ReturnInvoiceIds );
                }catch(Exception e){throw new BatchException('Some error occured in AnniversarySubscriptionBilling Class.',e);}
            }
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Anniversary OneBill')
            {
                try{
                ReturnInvoiceIds = AnniversaryOneBill.generateAnniversaryOneBill(accidList,isbatch);
                }catch(Exception e){throw new BatchException('Some error occured in AnniversaryOneBill Class.',e);}
            }
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Agreement OneBill')
            {
                try{
                    ReturnInvoiceIds = AgreementOneBill.generateAgreementOneBill(accidList,isbatch);
                }catch(Exception e){throw new BatchException('Some error occured in AgreementOneBill Class.',e);}
            }
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
            {
                try{
                ReturnInvoiceIds = CalenderMonthBill.generateCalenderMonthBill(accidList,isbatch);
                }catch(Exception e){throw new BatchException('Some error occured in CalenderMonthBill Class.',e);}
                
            }
        }
        if(ReturnInvoiceIds!=null && ReturnInvoiceIds.size()>0)
            return ReturnInvoiceIds[0];//bcoz,As Create bill button hit then only one invoice will be created
        else
            return null;
    }
    public static id generateInvoice(list<id> accidList,boolean isbatch,string billType){
        //list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Schedule_Time__c from ChikPeaO2B__O2B_Setting__c limit 1];
        list<ChikPeaO2B__O2B_Setting__c>O2BsetList=new list<ChikPeaO2B__O2B_Setting__c>
        {new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Billing_Type__c=billType)};
        //O2BsetList[0].ChikPeaO2B__Billing_Type__c=billType;
        List<id>ReturnInvoiceIds=new List<id>();
        if(O2BsetList!=null && O2BsetList.size()>0)
        {
            if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Anniversary Subscription')
            {
                try{
                ReturnInvoiceIds = AnniversarySubscriptionBilling.generateSubscriptionBill(accidList,isbatch);
                system.debug('###'+ReturnInvoiceIds );
                }catch(Exception e){throw new BatchException('Some error occured in AnniversarySubscriptionBilling Class.',e);}
            }
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Anniversary OneBill')
            {
                try{
                ReturnInvoiceIds = AnniversaryOneBill.generateAnniversaryOneBill(accidList,isbatch);
                }catch(Exception e){throw new BatchException('Some error occured in AnniversaryOneBill Class.',e);}
            }
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Agreement OneBill')
            {
                try{
                    ReturnInvoiceIds = AgreementOneBill.generateAgreementOneBill(accidList,isbatch);
                }catch(Exception e){throw new BatchException('Some error occured in AgreementOneBill Class.',e);}
            }
            
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
            {
                try{
                ReturnInvoiceIds = CalenderMonthBill.generateCalenderMonthBill(accidList,isbatch);
                }catch(Exception e){throw new BatchException('Some error occured in CalenderMonthBill Class.',e);}
                
            }            
        }
        if(ReturnInvoiceIds!=null && ReturnInvoiceIds.size()>0)
            return ReturnInvoiceIds[0];//bcoz,As Create bill button hit then only one invoice will be created
        else
            return null;
    }
}