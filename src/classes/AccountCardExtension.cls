/*
** Class         :  AccountCardExtension
** Created by    :  Snehashish Roy (chikpea Inc.)
** Description   :  Controller for AccountCreditCard Page
*/
public with sharing class AccountCardExtension
{
    public ApexPages.StandardController controller;
    public Account Payaccount{get;set;}
    public List<CreditCard>CCList{get;set;}
    public ChikPeaO2B__Payment__c Payment_gateway_dialog{get;set;}
    public ChikPeaO2B__Credit_Card__c NewCC{get;set;}
    public set<id> CCListids;
    public boolean showEmail{get;set;}
    public boolean hasgwerror{get;set;}
    public boolean cc_token_enabled{get;set;}
    public String cc_token_email{get;set;}
    public boolean cc_token_succ{get;set;}
    public string cc_secode{get;set;}
    public string cc_number{get;set;}
    public string selectedmonth{get;set;}
    public string selectedyr{get;set;}
    public boolean hasccnumbererror{get;set;}
    public boolean hascvverror{get;set;}
    
    private List<ChikPeaO2B__O2B_Setting__c>O2Bset=new List<ChikPeaO2B__O2B_Setting__c>();
    private map<id,ChikPeaO2B__Credit_Card__c>CCmap=new map<id,ChikPeaO2B__Credit_Card__c>();
    private String gateid;
    public AccountCardExtension(ApexPages.StandardController Controller)
    {
        this.controller = Controller;
        O2Bset=[SELECT id,name,ChikPeaO2B__Save_Failed_Payment__c,ChikPeaO2B__Save_Credit_Card__c,ChikPeaO2B__Supported_Gateways__c from ChikPeaO2B__O2B_Setting__c limit 1];
        Payment_gateway_dialog=new ChikPeaO2B__Payment__c(Payment_Method__c='Credit Card');
        Payaccount=new Account();
        Payaccount=[SELECT id,name,ChikPeaO2B__Credit_Card__c,Customer_Profile_Id__c from Account where id=:controller.getid()];
        NewCC=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=Payaccount.id);
        CCListids=new set<id>();
        showEmail=false;
        hasgwerror=false;
        hasccnumbererror=false;
        hascvverror=false;
        gateid='';
    }
    
    public class CreditCard{
        public ChikPeaO2B__Credit_Card__c cc{get;set;}
        public Boolean selected{get;set;}
        public Boolean Disable{get;set;}
        public String comment{get;set;}
        public String status{get;set;}
        public CreditCard(ChikPeaO2B__Credit_Card__c cc,Account Payaccount){
            this.cc = cc;
            selected = false;
            Disable=false;
            if(cc.id==Payaccount.ChikPeaO2B__Credit_Card__c)
                comment='Linked with Account: '+Payaccount.name;
            else
            {
                comment='Linked with Subscription: ';
                boolean b=false;
            }
            if(cc.ChikPeaO2B__Customer_Profile_ID__c == null || cc.ChikPeaO2B__Customer_Payment_Profile_Id__c == null){
                Date lastdateofcc=(date.newInstance(integer.valueof(cc.ChikPeaO2B__Expiry_Year__c),integer.valueof(cc.ChikPeaO2B__Expiry_Month__c),1)).addmonths(1)-1;
                if(lastdateofcc>=date.today())
                    status='Active';
                else
                    status='Expired';  
            } else {// Tokenization is used
                status='Active';
            }
        }
    }
    
    public List<CreditCard> getCreditCards()
    {
        if(CCList==null)
        {
            CCList=new List<CreditCard>();
            for(ChikPeaO2B__Credit_Card__c cc:[SELECT id,name,ChikPeaO2B__CC_Street1__c,ChikPeaO2B__CC_City__c,ChikPeaO2B__CC_Country__c,ChikPeaO2B__CC_First_Name__c,ChikPeaO2B__CC_Last_Name__c,ChikPeaO2B__CC_Postal_Code__c,ChikPeaO2B__CC_State__c,ChikPeaO2B__CC_Street2__c,ChikPeaO2B__Credit_Card_Number__c,ChikPeaO2B__Credit_Card_Type__c,ChikPeaO2B__Expiry_Month__c,ChikPeaO2B__Expiry_Year__c, ChikPeaO2B__Customer_Profile_ID__c, ChikPeaO2B__Customer_Payment_Profile_Id__c from ChikPeaO2B__Credit_Card__c where ChikPeaO2B__Account__c =: Payaccount.id order by createddate desc])
            {
                //System.debug('---Credit Card='+cc);
                CreditCard ccc=new CreditCard(cc,Payaccount);
                CCList.add(ccc);
                CCmap.put(cc.id,cc);
                CCListids.add(cc.id);
            }
        }
        else
        {
            //CCList=new List<CreditCard>();
            //CCmap=new map<id,ChikPeaO2B__Credit_Card__c>();
            integer flag=0;
            for(ChikPeaO2B__Credit_Card__c cc:[SELECT id,name,ChikPeaO2B__CC_Street1__c,ChikPeaO2B__CC_City__c,ChikPeaO2B__CC_Country__c,ChikPeaO2B__CC_First_Name__c,ChikPeaO2B__CC_Last_Name__c,ChikPeaO2B__CC_Postal_Code__c,ChikPeaO2B__CC_State__c,ChikPeaO2B__CC_Street2__c,ChikPeaO2B__Credit_Card_Number__c,ChikPeaO2B__Credit_Card_Type__c,ChikPeaO2B__Expiry_Month__c,ChikPeaO2B__Expiry_Year__c, ChikPeaO2B__Customer_Profile_ID__c, ChikPeaO2B__Customer_Payment_Profile_Id__c from ChikPeaO2B__Credit_Card__c where ChikPeaO2B__Account__c =: Payaccount.id order by createddate desc])
            {
                if(!CCListids.contains(cc.id))
                {
                    CreditCard ccc=new CreditCard(cc,Payaccount);
                    CCList.add(ccc);
                    CCmap.put(cc.id,cc); 
                    CCListids.add(cc.id);
                    flag++;
                }
                /*CreditCard ccc=new CreditCard(cc,Payaccount);
                CCList.add(ccc);
                CCmap.put(cc.id,cc);*/
            }
            if(flag>0)
            {
                CreditCard cctemp=CCList[CCList.size()-1];
                for(integer i=CCList.size()-2;i>=0;i--)
                {
                    CCList[i+1]=CCList[i];
                }
                CCList[0]=cctemp;
            }
            boolean oneccselected=false;
            for(CreditCard ccc:CCList)    
            {
                if(ccc.selected)
                {
                    oneccselected=true;
                    //selectedcc=ccc.cc.id;
                    break;
                }
            }
            if(oneccselected)
            {
                for(CreditCard ccc:CCList)
                {
                    if(!ccc.selected)
                        ccc.Disable=true;    
                }
            }
            else
            {
                //selectedcc=null;
                for(CreditCard ccc:CCList)
                {
                    ccc.Disable=false;
                }    
            }
        }        
        return CCList;
    }
    public void gatewaySelected(){
        System.debug('#****payment.Payment_Gateway__c='+
        Payment_gateway_dialog.Payment_Gateway__c);
        cc_token_enabled=false;
        List<ChikPeaO2B__Gateway__c> gtw_list=[select Id, Name, ChikPeaO2B__Tokenization__c,ChikPeaO2B__Gateway_Type__c from ChikPeaO2B__Gateway__c
          where Name=:Payment_gateway_dialog.Payment_Gateway__c limit 1];
            if(gtw_list.size()>0)
            {
                if((O2Bset[0].ChikPeaO2B__Supported_Gateways__c!=null) && O2Bset[0].ChikPeaO2B__Supported_Gateways__c.contains(gtw_list[0].ChikPeaO2B__Gateway_Type__c))
                {
                    if(gtw_list.size()>0&&gtw_list[0].ChikPeaO2B__Tokenization__c){
                        //checking if tokenization enabled
                        System.debug('#****tokenization enabled...');
                        cc_token_enabled=true;
                        gateid=gtw_list[0].id;
                    }else{
                        cc_token_enabled=false;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Tokenization is disabled for this Gateway.'));
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'We do not support this Gateway for Credit Card Transactions'));
                }
            }
            hasgwerror=ApexPages.hasMessages(ApexPages.Severity.ERROR);
        //tokenization end  
    }
    public void useNewCcard()
    {
        //System.debug('---Inside useNewCcard---');
        //System.debug('---Token Inside useNewCcard---'+cc_token_enabled);
        NewCC=new ChikPeaO2B__Credit_Card__c();
        NewCC.ChikPeaO2B__Account__c=Payaccount.id;
        cc_secode='';
        cc_number='';
        cc_token_email='';
        selectedmonth='01';
        selectedyr=string.valueof(date.today().year());
        showEmail=(Payaccount.Customer_Profile_Id__c!=null && Payaccount.Customer_Profile_Id__c!=''?false:true);
        
        for(CreditCard ccc:CCList)    
        {
            ccc.selected=false;
            ccc.disable=false;
        }    
        //DisableDoPaymentBtn();
        
        //System.debug('---Payment_gateway_dialog---'+Payment_gateway_dialog.Payment_Gateway__c);
        /*if((Payment_gateway_dialog.Payment_Gateway__c==null ||Payment_gateway_dialog.Payment_Gateway__c=='') && (payment.Payment_Gateway__c!=null && payment.Payment_Gateway__c!=''))
            Payment_gateway_dialog.Payment_Gateway__c=payment.Payment_Gateway__c*/
        
        //tokenization for AuthNet
        //System.debug('#****useNewCcard Payment_gateway_dialog.Payment_Method__c='+
            //Payment_gateway_dialog.Payment_Gateway__c);
        List<ChikPeaO2B__Gateway__c> gtw_list=[select Id, Name,ChikPeaO2B__Gateway_Type__c, ChikPeaO2B__Tokenization__c from ChikPeaO2B__Gateway__c
            where Name=:Payment_gateway_dialog.Payment_Gateway__c limit 1];
            if(gtw_list.size()>0)
            {
                if(gtw_list[0].ChikPeaO2B__Gateway_Type__c!=null && gtw_list[0].ChikPeaO2B__Gateway_Type__c!='' && o2bset.size()>0)
                {
                    if((o2bset[0].ChikPeaO2B__Supported_Gateways__c==null) || !o2bset[0].ChikPeaO2B__Supported_Gateways__c.contains(gtw_list[0].ChikPeaO2B__Gateway_Type__c))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'We do not support this Gateway for Credit Card Transactions'));
                        Payment_gateway_dialog=new ChikPeaO2B__Payment__c(Payment_Method__c='Credit Card');
                        cc_token_enabled=false;
                        return;
                    }
                    else if(gtw_list[0].ChikPeaO2B__Tokenization__c){
                        //checking if tokenization enabled
                        //System.debug('#****tokenization enabled...');
                        cc_token_enabled=true;
                        gateid=gtw_list[0].id;
                    }
                    else{
                        cc_token_enabled=false;
                        Payment_gateway_dialog=new ChikPeaO2B__Payment__c(Payment_Method__c='Credit Card');
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Tokenization is disabled for this Gateway.'));
                        return;
                    }
                }
            }
        //tokenization end
    }
    public void validateCcard()
    {
        if(cc_number!='')
        {
            try{
                Double validint=Double.valueOf(cc_number);
            }
            catch(Exception e)
            {
                hasccnumbererror=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Credit Card Number is not valid.'));
                return;
            }
            boolean result=PG_CCMod10Check.valid(cc_number);
            if(!result)
            {
                hasccnumbererror=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Credit Card Number is not valid.'));
                return;
            }
            else
                hasccnumbererror=false;
        }
    }
    
    public void validatecvv()
    {
        if(cc_secode!='')
        {
            if(cc_secode.length()<3 || cc_secode.length()>4)
            {
                hascvverror=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'CVV is not valid.'));
                return;
            }
            try{
                Integer validint=Integer.valueOf(cc_secode);
            }
            catch(Exception e)
            {
                hascvverror=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'CVV is not valid.'));
                return;
            }
            hascvverror=false;
        }
    }
    
    public void showccErrormsg()
    {
        if(hasccnumbererror)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Credit Card Number is not valid.'));
        else if(hascvverror)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'CVV is not valid.'));
    }
    
    public void emptyccard()
    {
        if(cc_number=='')
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Credit Card Number is missing.'));
            return;
        }
        if(NewCC.CC_First_Name__c=='' || NewCC.CC_Last_Name__c=='' || NewCC.CC_First_Name__c==null || NewCC.CC_Last_Name__c==null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Name is incomplete.'));
            return;
        }
        if(cc_token_email=='' && showEmail==true)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Email is Missing.'));
            return;
        }
    }
    
    public List<SelectOption> getMonthOptions()
    {
        List<SelectOption>MonthOptions=new List<SelectOption>();
        for(integer i=1;i<=12;i++)
        {
            string month=string.valueof(i);
            month=(month.length()==1?'0'+month:month);
            MonthOptions.add(new SelectOption(month,month));
        }
        return MonthOptions;
    }
    
    public List<SelectOption> getYrOptions()
    {
        List<SelectOption>YrOptions=new List<SelectOption>();
        for(integer i=Integer.valueof(Date.today().year());i<=(Integer.valueof(Date.today().year())+30);i++)
            YrOptions.add(new SelectOption(string.valueof(i),string.valueof(i)));
        return YrOptions;
    }
    
    public void saveCcardToken(){
        try{
            if(cc_number!='')
            {
                try{
                    Double validint=Double.valueOf(cc_number);
                }
                catch(Exception e)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Credit Card Number is not valid.'));
                    return;
                }
                boolean result=PG_CCMod10Check.valid(cc_number);
                if(!result)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Credit Card Number is not valid.'));
                    return;
                }
            }
            
            if(cc_secode!='')
            {
                if(cc_secode.length()<3 || cc_secode.length()>4)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'CVV is not valid.'));
                    return;
                }
                try{
                    Integer validint=Integer.valueOf(cc_secode);
                }
                catch(Exception e)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'CVV is not valid.'));
                    return;
                }
            }
            NewCC.ChikPeaO2B__Expiry_Month__c=Integer.valueof(selectedmonth);
            NewCC.ChikPeaO2B__Expiry_Year__c=Integer.valueof(selectedyr);
            if(NewCC.ChikPeaO2B__Expiry_Month__c<date.today().month() && NewCC.ChikPeaO2B__Expiry_Year__c==date.today().year())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Card could not be saved. Invalid expiry date.'));
                return;
            }
            if(Payaccount.Customer_Profile_Id__c==null ){  
                Map<String, String> reqMap = new Map<String, String>{
                  'PaymentGatewayName' => Payment_gateway_dialog.Payment_Gateway__c,
                  'validationMode' => ChikPeaO2B.PG_AuthNetCIMAPI.VALIDATION_MODE_NONE, // when payment profile is not present
                  'email' => cc_token_email
                };
                   
                List<Map<String, String>> paymentProfileList = new List<Map<String, String>>();
                map<string,string> paymentProfileMap=new map<string,string>();
                paymentProfileMap.put('PaymentGatewayName',Payment_gateway_dialog.Payment_Gateway__c);
                paymentProfileMap.put('validationMode',PG_AuthNetCIMAPI.VALIDATION_MODE_NONE);
                paymentProfileMap.put('firstName',NewCC.CC_First_Name__c);
                paymentProfileMap.put('lastName',NewCC.CC_Last_Name__c);
                paymentProfileMap.put('cardNumber',cc_number);
                paymentProfileMap.put('bankAccount','false');
                paymentProfileMap.put('expirationDate',selectedyr+'-'+selectedmonth);
                paymentProfileMap.put('cardCode',cc_secode);
                paymentProfileList.add(paymentProfileMap);
            
                List<Map<String, String>> shippingAddresses = new List<Map<String, String>>();     
                String customerProfileId;
             
                
                if(Test.isRunningTest()){
                  customerProfileId='123456';
                  NewCC.Customer_Payment_Profile_Id__c='654321';
                }
                else{
                  ChikPeaO2B.PG_AuthNetCIMAPI.CreateCustomerProfileResult createCustomerProfileResult 
                      = ChikPeaO2B.PG_AuthNetCIMAPI.createCustomerProfile(reqMap, 
                    paymentProfileList, 
                    shippingAddresses
                    );
                  customerProfileId=String.valueOf(createCustomerProfileResult.customerProfileId);
                  if(createCustomerProfileResult.customerPaymentProfileIdList.size()>0)
                    NewCC.Customer_Payment_Profile_Id__c=String.valueof(createCustomerProfileResult.customerPaymentProfileIdList[0]);
                }
                Payaccount.Customer_Profile_Id__c=customerProfileId;
                NewCC.Customer_Profile_ID__c=customerProfileId;
                NewCC.ChikPeaO2B__Credit_Card_Number__c=(cc_number!=''?cc_number.substring(cc_number.length()-4,cc_number.length()):'');
                System.debug('---newcc.creditcard--'+NewCC.ChikPeaO2B__Credit_Card_Number__c);
                //System.debug('---newcc.Customer_Payment_Profile_Id__c--'+NewCC.Customer_Payment_Profile_Id__c);
                //update Payaccount;
                //System.debug('---res='+createCustomerProfileResult );
                //Long a = createCustomerProfileResult.customerProfileId;
                //System.debug('---customerprofileid='+createCustomerProfileResult.customerProfileId);
                //System.debug('---------'+userinfo.getName()+userinfo.getProfileId());
            }
            else
            {
                System.debug('Payment Profile');
                NewCC.Customer_Profile_ID__c=Payaccount.Customer_Profile_Id__c;
                //save card
                Map<String, String> reqMap = new Map<String, String>{
                    'PaymentGatewayName' => Payment_gateway_dialog.Payment_Gateway__c,
                    'validationMode' => PG_AuthNetCIMAPI.VALIDATION_MODE_NONE, // when payment profile is present
                    'firstName' => NewCC.CC_First_Name__c,
                    'lastName' => NewCC.CC_Last_Name__c,
                    'customerProfileId' => Payaccount.Customer_Profile_Id__c,
                    'cardNumber' => cc_number,
                    'bankAccount' => 'false',
                    'expirationDate' => selectedyr +'-'+selectedmonth,
                    'cardCode' => cc_secode
                };
            
                if(Test.isRunningTest()){
                  NewCC.Customer_Payment_Profile_Id__c='654321';
                }
                else
                {
                    Long res = PG_AuthNetCIMAPI.createCustomerPaymentProfile(reqMap);
                    //System.debug('Pass2');
                    NewCC.Customer_Payment_Profile_Id__c=String.valueof(res);
                }
                NewCC.ChikPeaO2B__Credit_Card_Number__c=(cc_number!=''?cc_number.substring(cc_number.length()-4,cc_number.length()):'');
                System.debug('---newccreditcard-1-'+NewCC.ChikPeaO2B__Credit_Card_Number__c);
                //NewCC=token_NewCC;
                //DisableDoPaymentBtn();
                //System.debug('---reqMap='+res);
            }
            if(NewCC.Customer_Profile_Id__c!='' && NewCC.Customer_Payment_Profile_Id__c!='' && NewCC.ChikPeaO2B__Credit_Card_Number__c!='' && NewCC.ChikPeaO2B__Credit_Card_Number__c!=null)
            {
                if(gateid!='' && gateid!=null)
                NewCC.ChikPeaO2B__Gateway__c=gateid;
                insert NewCC;
                Payaccount.ChikPeaO2B__Credit_Card__c=NewCC.id;
                update Payaccount;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,
                'Card Saved'));
                Payment_gateway_dialog=new ChikPeaO2B__Payment__c(Payment_Method__c='Credit Card');
                cc_token_enabled=false;
            }
        }
        catch(ChikPeaO2B.PG_Exception e){
            cc_token_succ=false;
            System.debug('---e.getErrorCode='+e.getErrorCode());
            System.debug('---e.getErrorCode='+e.getMessage());
            Payment_gateway_dialog=new ChikPeaO2B__Payment__c(Payment_Method__c='Credit Card');
            cc_token_enabled=false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                'Unable to Save Card.\n'+e.getErrorMesssage()));
            //e.getErrorCode
        }
    }
    
    public pagereference exitpage()
    {
        pagereference pgr=new  pagereference('/'+controller.getid());
        return pgr;
    }
}