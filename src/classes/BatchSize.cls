/*
** Class         :  BatchSize
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by	 :  Asitm (chikpea Inc.)
** Reason		 :  Secuirty Issue fixed.(sharing)
** Description   :   
*/


global with sharing class BatchSize{
    Webservice static string CalculateBatchSize()
    {
        final decimal EXCESS_VS_AVERAGE=1.5;
        integer TOTAL_DML_ROWS=0;
        integer totalInvoiceinsert=0;
        integer totalSubscriptionupdate=0;
        integer totalPurchaseupdate=0;
        integer totalInvoiceLineinsert=0;
        integer totalUsageHistoryinsert=0;
        integer totalUsageUpdate=0;
        integer totalInvoiceLineHistoryupdate=0;
        integer numberofautopayment=0;
        integer totalPaymentinsert=0;
        boolean valid=true;
        string returnmsg='';
        integer batchsize;
        
        List<ChikPeaO2B__O2B_Setting__c> o2bset=[Select id,name,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Line_Payment__c from ChikPeaO2B__O2B_Setting__c limit 1];
        List<Account>accountList=[select id,name from Account where ChikPeaO2B__Active__c='Yes' and ChikPeaO2B__Is_Billing__c=true];
        List<ID>accountIds=new List<ID>();
        Map<id,integer>Acc_DML_ROW_Map=new Map<id,integer>();
        Map<id,string>AccNameMap=new Map<id,string>();
        for(Account acc : accountList)
        {
            accountIds.add(acc.id);
            AccNameMap.put(acc.id,acc.name);
            Acc_DML_ROW_Map.put(acc.id,0);//initialize the map with Zero
        }
        if(o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Billing_Type__c=='Anniversary OneBill')
        {
             List<usage__c>usgAllList1=new List<usage__c>();
             List<Account> accList = [Select Id,Name, ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Amount_Remaining__c, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity, Bill_Cycle__c, Amount_Due__c, Active__c, Activation_Date__c,
             Payment_Term__c, Auto_Payment__c, (Select Id, Role__c From Contacts where Role__c = 'Billing' limit 1),
             (Select Id, ChikPeaO2B__Item__c, ChikPeaO2B__Sell_Price__c, ChikPeaO2B__Qty__c, ChikPeaO2B__Invoiced__c, ChikPeaO2B__Tax_Rate__c, ChikPeaO2B__Invoice__c, ChikPeaO2B__Order_Line__c,ChikPeaO2B__Description__c From ChikPeaO2B__Purchases__r where ChikPeaO2B__Invoiced__c = false),
             (Select Id, Name, ChikPeaO2B__Item__c,ChikPeaO2B__Item__r.ChikPeaO2B__Is_prorate__c, ChikPeaO2B__Recurring_Charge__c, ChikPeaO2B__Next_Bill_Date__c, ChikPeaO2B__Bill_Cycle__c, ChikPeaO2B__Order_Line__c, ChikPeaO2B__Quantity__c, ChikPeaO2B__Billing_Start_Date__c, ChikPeaO2B__Billing_Stop_Date__c,ChikPeaO2B__Description__c, 
             ChikPeaO2B__Billing_Stopped__c, ChikPeaO2B__Discount__c, ChikPeaO2B__Billing_Started__c From Subscriptions__r where Billing_Stop_Date__c = null or Billing_Stopped__c = false),
             (select id,name,ChikPeaO2B__Account__c,ChikPeaO2B__Billing_Stopped__c,ChikPeaO2B__Free_Usage__c,ChikPeaO2B__Invoiced__c,ChikPeaO2B__Item__c,ChikPeaO2B__Order_Line__c,ChikPeaO2B__Purchase__c,ChikPeaO2B__Rate__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Remaining_Usage__c,ChikPeaO2B__Subscription__c,ChikPeaO2B__Total_Usage__c,
             ChikPeaO2B__UOM__c,ChikPeaO2B__Usage_Value__c,ChikPeaO2B__Item__r.ChikPeaO2B__Is_aggregation__c,ChikPeaO2B__Subscription__r.ChikPeaO2B__Aggregate_Usage__c,ChikPeaO2B__Type__c,ChikPeaO2B__Item__r.ChikPeaO2B__Description__c from Usages__r where ChikPeaO2B__Billing_Stopped__c=false and ChikPeaO2B__Invoiced__c=false and (ChikPeaO2B__Total_Usage__c>0 or ChikPeaO2B__Type__c= 'PrePaid'))
             From Account where Id IN :accountIds and Bill_Cycle__c != null and ChikPeaO2B__Next_Bill_Date__c=TODAY];
                    
             set<id>SubIds=new set<id>();
             for(Account a:accList)
             {
                 if(a.Auto_Payment__c=='Yes')
                     numberofautopayment++;
                for(Subscription__c sub : a.Subscriptions__r)
                    SubIds.add(sub.id);
             }
            
            for(Subscription__c sub : [SELECT id,Account__c,(SELECT id,name,ChikPeaO2B__Item__c,ChikPeaO2B__Line_Type__c,ChikPeaO2B__Period_From__c,ChikPeaO2B__Period_To__c,ChikPeaO2B__Qty__c,ChikPeaO2B__Unit_Rate__c from Invoice_Line_History__r where ChikPeaO2B__Invoice_Required__c='Yes')from Subscription__c where id in : SubIds])
            {
                totalInvoiceLineHistoryupdate+=sub.Invoice_Line_History__r.size();
                Acc_DML_ROW_Map.put(sub.Account__c,Acc_DML_ROW_Map.get(sub.account__c)+sub.Invoice_Line_History__r.size());
            }
            
            for(Account acc : accList)
            {
                date InvoiceBillDateFrom=acc.ChikPeaO2B__Next_Bill_Date__c;//is equal to today
                date InvoiceBillDateTo;
                if(acc.Bill_Cycle__c=='Annual')
                    InvoiceBillDateTo=InvoiceBillDateFrom.addyears(1)-1;
                else if(acc.Bill_Cycle__c=='Quarterly')    
                    InvoiceBillDateTo=InvoiceBillDateFrom.addmonths(3)-1;
                else if(acc.Bill_Cycle__c=='Monthly')
                    InvoiceBillDateTo=InvoiceBillDateFrom.addmonths(1)-1;
                else if(acc.Bill_Cycle__c=='Weekly')
                    InvoiceBillDateTo=InvoiceBillDateFrom.adddays(7)-1;
                else if(acc.Bill_Cycle__c=='Daily')
                    InvoiceBillDateTo=InvoiceBillDateFrom.adddays(1)-1;
                else if(acc.bill_cycle__c=='Half Yearly')
                    InvoiceBillDateTo=InvoiceBillDateFrom.addmonths(6)-1;
                else
                    InvoiceBillDateTo=InvoiceBillDateFrom.adddays(integer.valueof(acc.bill_cycle__c))-1;
                if(acc.Subscriptions__r.size()>6000)
                {
                    returnmsg+='A single account can not contain more than 6000 subscription.\n'+acc.name+' contain '+acc.Subscriptions__r.size()+' subscription.';    
                    valid=false;
                    break;
                }
                else
                {
                totalInvoiceinsert++;//Number of account=Number of invoice
                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);//1 invoice for 1 account
                totalSubscriptionupdate+=acc.Subscriptions__r.size();
                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+acc.Subscriptions__r.size());
                totalPurchaseupdate+=acc.ChikPeaO2B__Purchases__r.size();
                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+acc.ChikPeaO2B__Purchases__r.size());
                //============== Subscription[START] ===================//

                for(ChikPeaO2B__Subscription__c sub : acc.Subscriptions__r)
                {
                date OneBillCycleBehind;
                Invoice_Line__c invcl = new Invoice_Line__c();
                if(acc.ChikPeaO2B__Bill_Cycle__c=='Annual')
                    OneBillCycleBehind=acc.ChikPeaO2B__Next_Bill_Date__c.addyears(-1);
                else if(acc.ChikPeaO2B__Bill_Cycle__c=='Quarterly')
                    OneBillCycleBehind=acc.ChikPeaO2B__Next_Bill_Date__c.addmonths(-3);
                else if(acc.ChikPeaO2B__Bill_Cycle__c=='Monthly')
                    OneBillCycleBehind=acc.ChikPeaO2B__Next_Bill_Date__c.addmonths(-1);
                else if(acc.ChikPeaO2B__Bill_Cycle__c=='Weekly')    
                    OneBillCycleBehind=acc.ChikPeaO2B__Next_Bill_Date__c.adddays(-7);
                else if(acc.ChikPeaO2B__Bill_Cycle__c=='Daily')    
                    OneBillCycleBehind=acc.ChikPeaO2B__Next_Bill_Date__c.adddays(-1);
                else if(acc.ChikPeaO2B__Bill_Cycle__c=='Half Yearly')    
                    OneBillCycleBehind=acc.ChikPeaO2B__Next_Bill_Date__c.addmonths(-6);
                else    
                    OneBillCycleBehind=acc.ChikPeaO2B__Next_Bill_Date__c.adddays(-integer.valueof(acc.ChikPeaO2B__Bill_Cycle__c));
                    
                date OneBillCycleForward=InvoiceBillDateTo;

                //Subscription start date < Account next bill date
                if(sub.ChikPeaO2B__Billing_Start_Date__c<InvoiceBillDateFrom)
                {
                    //subscription start date inside one bill cycle behind
                    
                    if(OneBillCycleBehind <= sub.ChikPeaO2B__Billing_Start_Date__c)
                    {
                        date billfrom=sub.ChikPeaO2B__Billing_Start_Date__c;
                        date billto;
                        if(sub.ChikPeaO2B__Billing_Stop_Date__c==null)//no stop date
                        {
                            if(!sub.ChikPeaO2B__Billing_Started__c)//Billing Started false
                            {
                                totalInvoiceLineinsert++;
                                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                            }
                            totalInvoiceLineinsert++;
                            Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                        }
                        else// stop date exist
                        {   //subscription stop date< account next bill date 
                            
                            if(sub.ChikPeaO2B__Billing_Stop_Date__c<acc.ChikPeaO2B__Next_Bill_Date__c)
                            {
                                if(!sub.ChikPeaO2B__Billing_Started__c)//Billing Started false
                                {
                                    totalInvoiceLineinsert++;
                                    Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                                }
                            }
                            //stop date>=nextbill date, and stop date inside or equal one bill cycle forward
                            
                            else if(sub.ChikPeaO2B__Billing_Stop_Date__c>=acc.ChikPeaO2B__Next_Bill_Date__c && sub.ChikPeaO2B__Billing_Stop_Date__c<=OneBillCycleForward)    
                            {
                                if(!sub.ChikPeaO2B__Billing_Started__c)//Billing Started false
                                {
                                    billto=acc.ChikPeaO2B__Next_Bill_Date__c-1 ;
                                    totalInvoiceLineinsert++;
                                    Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                                }
                                totalInvoiceLineinsert++;
                                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                            }
                            //stop date>next bill cycle, and stop date after one bill cycle forward
                            
                            else if(sub.ChikPeaO2B__Billing_Stop_Date__c>OneBillCycleForward)
                            {
                                if(!sub.ChikPeaO2B__Billing_Started__c)//Billing Started false
                                {
                                    billto=acc.ChikPeaO2B__Next_Bill_Date__c-1;
                                    totalInvoiceLineinsert++;
                                    Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                                }
                                totalInvoiceLineinsert++;
                                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                            }
                        }    
                    }
                    else// subscription start date more than account one bill cycle behind
                    {
                        date billfrom=acc.ChikPeaO2B__Next_Bill_Date__c;
                        date billto;
                        if(sub.ChikPeaO2B__Billing_Stop_Date__c==null)//no stop date
                        {
                            billto=OneBillCycleForward;
                            totalInvoiceLineinsert++;
                            Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                        }
                        else// stop date exist
                        {   //subscription stop date <account next bill date 
                            if(sub.ChikPeaO2B__Billing_Stop_Date__c<acc.ChikPeaO2B__Next_Bill_Date__c)// subscription stop date< account next bill date,-ve invoice line
                            {
                                totalInvoiceLineinsert++;
                                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                            }
                            //subscription stop date>=next bill date and stop date inside or equal one bill cycle forward
                            
                            else if(sub.ChikPeaO2B__Billing_Stop_Date__c>=acc.ChikPeaO2B__Next_Bill_Date__c && sub.ChikPeaO2B__Billing_Stop_Date__c<=OneBillCycleForward)
                            {
                                totalInvoiceLineinsert++;
                                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                            }
                            //subscription stop date > account next bill date, and outside one bill cycle forward
                            
                            else if(sub.ChikPeaO2B__Billing_Stop_Date__c>OneBillCycleForward)
                            {
                                totalInvoiceLineinsert++;
                                Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                            }
                        }   
                    }
                    
                }
                else//subscription start date>= account next bill date
                {
                    //subscription start date > account next bill date
                    if(sub.ChikPeaO2B__Billing_Start_Date__c>acc.ChikPeaO2B__Next_Bill_Date__c && sub.ChikPeaO2B__Billing_Start_Date__c<=OneBillCycleForward)
                    {
                        //stop date inside or equal one bill cycle forward
                        if(sub.ChikPeaO2B__Billing_Stop_Date__c<=OneBillCycleForward)
                        {
                            totalInvoiceLineinsert++;
                            Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                        }
                        //stop date outside one bill cycle forward or stop date = null
                        
                        else if(sub.ChikPeaO2B__Billing_Stop_Date__c>OneBillCycleForward || sub.ChikPeaO2B__Billing_Stop_Date__c==null)
                        {
                            totalInvoiceLineinsert++;
                            Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                        }
                        
                    }
                    //Billing start date=account next bill date
                    else if(sub.ChikPeaO2B__Billing_Start_Date__c==acc.ChikPeaO2B__Next_Bill_Date__c)
                    {
                        //stop date inside or equal one bill cycle forward
                        if(sub.ChikPeaO2B__Billing_Stop_Date__c<=OneBillCycleForward)
                        {
                            totalInvoiceLineinsert++;
                            Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                        }
                        //stop date outside one bill cycle forward or stop date = null
                        
                        else if(sub.ChikPeaO2B__Billing_Stop_Date__c>OneBillCycleForward || sub.ChikPeaO2B__Billing_Stop_Date__c==null)
                        {
                            totalInvoiceLineinsert++;
                            Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                        }
                    }
                }              
            }
            //============== Subscription [END] ====================//
            //================= Usage[START] ==================//
                for(usage__c usg : acc.Usages__r)
                {
                    if(usg.Total_Usage__c>0)
                    {
                        totalUsageHistoryinsert++;   
                        Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                    }    
                    if(usg.ChikPeaO2B__Type__c=='Postpaid')
                    {
                        totalUsageUpdate++;
                        Acc_DML_ROW_Map.put(acc.id,Acc_DML_ROW_Map.get(acc.id)+1);
                        usgAllList1.add(usg);
                    }
                    else if(usg.ChikPeaO2B__Type__c=='Prepaid')
                    {
                        usgAllList1.add(usg);    
                    }
                }
              }//subscription 6000 more else end    
            }//Account for loop end
            if(valid)
            {
                for(usage__c usg : usgAllList1)
                {
                    if(usg.ChikPeaO2B__Type__c=='Postpaid')
                    {
                        totalInvoiceLineinsert++;
                        Acc_DML_ROW_Map.put(usg.account__c,Acc_DML_ROW_Map.get(usg.account__c)+1);
                        totalUsageUpdate++;
                    }
                    else if(usg.ChikPeaO2B__Type__c=='Prepaid')
                    {
                        if (usg.Total_usage__c > 0)
                        {
                            totalInvoiceLineinsert++;
                            Acc_DML_ROW_Map.put(usg.account__c,Acc_DML_ROW_Map.get(usg.account__c)+1);    
                            if (usg.Remaining_usage__c < 0)
                            {
                                totalInvoiceLineinsert++;
                                Acc_DML_ROW_Map.put(usg.account__c,Acc_DML_ROW_Map.get(usg.account__c)+1);    
                            }
                            totalUsageUpdate++;
                            Acc_DML_ROW_Map.put(usg.account__c,Acc_DML_ROW_Map.get(usg.account__c)+1);
                        }
                    }
                }
                //================= Usage[END] ==================//
                totalInvoiceLineinsert+=(totalPurchaseupdate+totalInvoiceLineHistoryupdate);//invoice line for purchase=number of purchase
                                                                                //invoice line for Subscription is already calculated above
                                                                                //invoice line for Invoice line history = totalInvoiceLineHistoryupdate
                                                                                //invoice line for usage is already calculated above
            
                if(numberofautopayment>0 && totalInvoiceinsert!=0)
                    totalPaymentinsert=o2bset[0].ChikPeaO2B__Line_Payment__c?((totalInvoiceLineinsert/totalInvoiceinsert)*numberofautopayment):((totalInvoiceinsert/totalInvoiceinsert)*numberofautopayment);//Average payment per account * number of auto payment account 
                
                TOTAL_DML_ROWS+=totalInvoiceinsert+totalSubscriptionupdate+totalPurchaseupdate+totalInvoiceLineinsert+totalUsageHistoryinsert+totalUsageUpdate+totalInvoiceLineHistoryupdate+totalPaymentinsert;
                integer Avg_TOTAL_DML_ROWS_Per_Acc=totalInvoiceinsert!=0?TOTAL_DML_ROWS/totalInvoiceinsert:0;
                batchsize=Avg_TOTAL_DML_ROWS_Per_Acc!=0?9000/Avg_TOTAL_DML_ROWS_Per_Acc:0;
                if(Avg_TOTAL_DML_ROWS_Per_Acc!=0)
                {
                    boolean b=false;
                    for(id accid : Acc_DML_ROW_Map.keyset())
                    {
                        if(Acc_DML_ROW_Map.get(accid)>9000 && Acc_DML_ROW_Map.get(accid)>Avg_TOTAL_DML_ROWS_Per_Acc*EXCESS_VS_AVERAGE)
                        {
                            b=true;
                            returnmsg+=AccNameMap.get(accid)+'('+Acc_DML_ROW_Map.get(accid)+')/'+accid+',';
                        }
                    }
                    if(b)
                    {
                        returnmsg=returnmsg.removeend(',')+' account(s) have more than average data, may cause invoice batch to fail.\n'+' if account contain less that 6000 data ignore the above message\n';
                    }
                    else
                    {
                        returnmsg='';
                    }
                }
                if(batchsize==0)
                {
                    returnmsg+='No data to process.';
                }
                else if(batchsize<1 && batchsize!=0)
                {
                    returnmsg+='Too many data, can not be processed at once.';
                }
                else
                {
                    returnmsg+='batch size should be '+(batchsize>200?'200':string.valueof(batchsize))+' or less.';
                }
            }//Valid if END                                                                           
        }//one bill if END
        return returnmsg;
    } 
}