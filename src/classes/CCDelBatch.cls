global with sharing class CCDelBatch implements Database.Batchable<Sobject>,Database.Stateful{
    global String query;
    public Decimal mnths;
    Date conditiondate;
    public set<id> ccids;
    public List<ChikPeaO2B__Credit_Card__c> cclist;
    public List<ChikPeaO2B__Credit_Card__c> delcclist;
    global CCDelBatch(Decimal mnths){
        this.mnths=mnths;
        mnths=(mnths!=null?(mnths*(-1)):-13);
        conditiondate=date.today().addmonths(Integer.valueof(mnths));
        query = 'select id from ChikPeaO2B__Credit_Card__c';
        if(!Test.isRunningTest())
            query=query+' where createddate<:conditiondate';
        system.debug('SROY Query=>'+query);
    }
    global database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Sobject> scope){
        ccids=new set<id>();
        cclist=new List<ChikPeaO2B__Credit_Card__c>();
        delcclist=new List<ChikPeaO2B__Credit_Card__c>();
        for(sObject s:scope)
        {
            ChikPeaO2B__Credit_Card__c cc = (ChikPeaO2B__Credit_Card__c) s;
            if(cc.id != null){
                ccids.add(cc.id);
            }            
        }
        
        if(ccids.size()>0)
        {
            cclist=[select id,(select id from ChikPeaO2B__Payments__r where createddate>:date.today().addmonths(Integer.valueof(mnths))),(select id from 
                ChikPeaO2B__Subscriptions__r where ChikPeaO2B__Billing_Stopped__c=false) from ChikPeaO2B__Credit_Card__c where id in :ccids];
            if(cclist.size()>0)
            {
                for(ChikPeaO2B__Credit_Card__c cc:cclist)
                {
                    if(cc.ChikPeaO2B__Payments__r.size()==0)
                    {
                        if(cc.ChikPeaO2B__Subscriptions__r.size()==0)
                            delcclist.add(cc);
                    }
                }
                try
                {
                    if(delcclist.size()>0)
                        delete delcclist;
                }
                catch(Exception e)
                {
                    ChikPeaO2B__BatchLog__c bl=new ChikPeaO2B__BatchLog__c();
                    bl.ChikPeaO2B__Error_Log__c=string.valueof(e.getMessage());
                    insert bl;
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC){
       
    }
}