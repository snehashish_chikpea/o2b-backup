global with sharing class BatchAchStatusScheduler implements Schedulable
{
	global boolean test_status_pending=false;
    global boolean test_status_failed=false;
    global boolean test_status_exception=false;
	global void execute(SchedulableContext sc)
	{
		BatchAchStatus bas= new BatchAchStatus();
		List<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c from ChikPeaO2B__O2B_Setting__c limit 1];
		bas.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
		if(Test.isRunningTest())
		{
		  bas.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet:Reporting\' AND ChikPeaO2B__Payment_Method__c=\'ACH\' AND ChikPeaO2B__Status__c=\'In Process\' AND ChikPeaO2B__Has_Processed__c=true';
		  if(test_status_pending)
          {
             bas.test_status_pending=true;
          }
          if(test_status_failed)
          {
             bas.test_status_failed=true;
          }
          if(test_status_exception)
          {
             bas.test_status_exception=true;
          }
		}
		else
		{
	      bas.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet\' AND ChikPeaO2B__Payment_Method__c=\'ACH\' AND ChikPeaO2B__Status__c=\'In Process\' AND ChikPeaO2B__Has_Processed__c=true';
		}
		ID batchprocessid = Database.executeBatch(bas,1);
	}
}