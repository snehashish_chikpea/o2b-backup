/*
** Class         :  BatchParentBillRunScheduler
** Created by    :  Asitm9(chikpea Inc.)
** Description   :   
*/

global with sharing class BatchParentBillRunScheduler {
    
    Webservice static String executeschedule(){
        String error;
        String sch;        
        BatchParentBillScheduler BCBS=new BatchParentBillScheduler();
        list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Is_Parent_Billing_Enabled__c,
        ChikPeaO2B__Schedule_Time__c,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Billing_Day__c
        from ChikPeaO2B__O2B_Setting__c limit 1];
        if(O2BsetList==null || O2BsetList.isEmpty()){return 'error';}
        if(O2BsetList[0].ChikPeaO2B__Is_Parent_Billing_Enabled__c){
            O2BsetList[0].ChikPeaO2B__Schedule_Time__c=(O2BsetList[0].ChikPeaO2B__Schedule_Time__c==null
            || O2BsetList[0].ChikPeaO2B__Schedule_Time__c=='')?'6:00':O2BsetList[0].ChikPeaO2B__Schedule_Time__c;
            O2BsetList[0].ChikPeaO2B__Billing_Day__c=
            O2BsetList[0].ChikPeaO2B__Billing_Day__c!=null?O2BsetList[0].ChikPeaO2B__Billing_Day__c:'01';
            list<String> ScheduleTime=O2BsetList[0].ChikPeaO2B__Schedule_Time__c.split(':');
            string mm=string.valueOf(ScheduleTime[1]);
            Integer h=Integer.valueOf(ScheduleTime[0])+3>=24?
            Integer.valueOf(ScheduleTime[0])+3-24 :Integer.valueOf(ScheduleTime[0])+3;
            //string hh=string.valueOf(Integer.valueOf(ScheduleTime[0])+3);
            string hh=string.valueOf(Integer.valueOf(h));
            //hh=hh.length()==2?hh:'0'+hh;
            string dd=string.valueOf(O2BsetList[0].ChikPeaO2B__Billing_Day__c);
            sch=O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month'
            ?'0 '+mm+' '+hh+' '+dd+' * ?'
            :'0 '+mm+' '+hh+' * * ?';
            system.debug('------------->'+sch);
            //string sch='0 0 8 13 2 ?';
            //sch='0 '+min+' '+hh+' '+dd+' '+mm+' '+'?';
            try{
                system.schedule('Parent Billing Batch',sch, BCBS);
                error='success';
            }catch(Exception E){error=E.getMessage();}
        }
        return error;  
    }
}