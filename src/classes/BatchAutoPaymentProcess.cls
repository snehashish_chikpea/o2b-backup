/*
** Class         :  BatchAutoPaymentProcess
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  10 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

global with sharing class BatchAutoPaymentProcess implements Database.Batchable<Sobject>, Database.AllowsCallouts,Database.Stateful{

    global String query;
    global String errmsg;
    global string email;
    global decimal intervaldays;
    global Boolean enableGWTrxLog;
    global Boolean enableGWApprovedTrxLog;
    global Map<id,decimal>InvoiceLinePaidAmnt;//Added by Spal 21-DEC-2014
    global BatchAutoPaymentProcess()
    {
        errmsg='';
        InvoiceLinePaidAmnt=new Map<id,decimal>();//Added by Spal 21-DEC-2014
    }
    global database.querylocator start(Database.BatchableContext BC)
    {
        system.debug('----------------->query'+query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        set<id>CCid=new set<id>();
        for(sObject s:scope)
        {
            ChikPeaO2B__payment__c pay=(ChikPeaO2B__payment__c)s;
            CCid.add(pay.ChikPeaO2B__Credit_Card__c);
        }
        Map<id,ChikPeaO2B__Credit_Card__c>CCMap=new Map<id,ChikPeaO2B__Credit_Card__c>();
        for(ChikPeaO2B__Credit_Card__c cc : [SELECT id,ChikPeaO2B__Account__c,ChikPeaO2B__CC_Street1__c,ChikPeaO2B__CC_City__c,ChikPeaO2B__CC_Country__c,ChikPeaO2B__CC_First_Name__c,ChikPeaO2B__CC_Last_Name__c,ChikPeaO2B__CC_Postal_Code__c,ChikPeaO2B__CC_State__c,ChikPeaO2B__CC_Street2__c,ChikPeaO2B__Credit_Card_Number__c,ChikPeaO2B__Credit_Card_Type__c,ChikPeaO2B__Expiry_Month__c,ChikPeaO2B__Expiry_Year__c, ChikPeaO2B__Customer_Profile_ID__c, ChikPeaO2B__Customer_Payment_Profile_Id__c from ChikPeaO2B__Credit_Card__c where id in : CCid])
        {
            CCMap.put(cc.id,cc);
        }
        List<ChikPeaO2B__payment__c> payupdatelist=new List<ChikPeaO2B__payment__c>();
        try{
        for(sObject s:scope)
        {
             ChikPeaO2B__payment__c pay=(ChikPeaO2B__payment__c)s;
             date NxtCardProcessDate;
             if((pay.ChikPeaO2B__Card_Failure__c==null || pay.ChikPeaO2B__Card_Failure__c==0 || pay.ChikPeaO2B__Last_Payment_Process_Date__c==null))
                 NxtCardProcessDate=date.today();
             else
                 NxtCardProcessDate=pay.ChikPeaO2B__Last_Payment_Process_Date__c.adddays(integer.valueof(intervaldays));
             System.debug('---NxtCardProcessDate='+NxtCardProcessDate);
             //### valid for process ###//
             if(NxtCardProcessDate==date.today() || pay.ChikPeaO2B__Payment_Method__c.containsIgnoreCase('ACH'))
             {
                PG_Response pgResponse = null; 
                map<string,string> ccDetailsMap= new map<string,string>();            
                try{
                        System.debug('---pay='+pay);
                      if(pay.ChikPeaO2B__Payment_Method__c.containsIgnoreCase('Credit Card')){
                        ChikPeaO2B__Credit_Card__c cc2Bused=CCmap.get(pay.ChikPeaO2B__Credit_Card__c);
                        if(cc2Bused!=null){
                            ccDetailsMap.put('PaymentGatewayName',pay.Payment_Gateway__c);
                            ccDetailsMap.put('ccCity',cc2Bused.ChikPeaO2B__CC_City__c);
                            ccDetailsMap.put('ccCountry',cc2Bused.ChikPeaO2B__CC_Country__c);
                            ccDetailsMap.put('ccFirstName',cc2Bused.ChikPeaO2B__CC_First_Name__c);
                            ccDetailsMap.put('ccLastName',cc2Bused.ChikPeaO2B__CC_Last_Name__c);
                            ccDetailsMap.put('ccPostalCode',cc2Bused.ChikPeaO2B__CC_Postal_Code__c);
                            ccDetailsMap.put('ccState',cc2Bused.ChikPeaO2B__CC_State__c);
                            ccDetailsMap.put('ccStreet1',cc2Bused.ChikPeaO2B__CC_Street1__c);
                            ccDetailsMap.put('ccStreet2',cc2Bused.ChikPeaO2B__CC_Street2__c);
                            ccDetailsMap.put('ccNumber',cc2Bused.ChikPeaO2B__Credit_Card_Number__c);
                            ccDetailsMap.put('ccType',cc2Bused.ChikPeaO2B__Credit_Card_Type__c);
                            ccDetailsMap.put('ccExpMonth',String.valueof(cc2Bused.ChikPeaO2B__Expiry_Month__c));
                            ccDetailsMap.put('ccExpYear',String.valueof(cc2Bused.ChikPeaO2B__Expiry_Year__c));
                            ccDetailsMap.put('customerProfileId',String.valueof(cc2Bused.ChikPeaO2B__Customer_Profile_ID__c));
                            ccDetailsMap.put('customerPaymentProfileId',String.valueof(cc2Bused.ChikPeaO2B__Customer_Payment_Profile_Id__c));
                            ccDetailsMap.put('TransactionType',PG.TransactionType.AUTHORIZATION_CAPTURE.name());
                            ccDetailsMap.put('Amount',String.valueof(pay.ChikPeaO2B__Payment_Amount__c));
                            ccDetailsMap.put('InvoiceRef',(pay.ChikPeaO2B__Invoice_Line__c!=null?pay.ChikPeaO2B__Invoice_Line__r.name:pay.ChikPeaO2B__Invoice__r.name));
                            ccDetailsMap.put('CustomerRef',pay.ChikPeaO2B__Account__r.name);
                            ccDetailsMap.put('OrderID',pay.ChikPeaO2B__Merchant_Order_Number__c);
                            ccDetailsMap.put('Comment','WSA');
                            pgResponse=PG_PaymentProcessorInvoker.process(ccDetailsMap);
                            //TODO: use list insert to reduce no of dml/batch run, not necessary at present 
                            if(enableGWTrxLog 
                                && (enableGWApprovedTrxLog || pgResponse.approvalStatus != 'APPROVED')){
                                Id sfLogId = PG_PaymentProcessorInvoker.logCC(ccDetailsMap, 0, pay.Id, pgResponse, false, '');
                            }
                        }
                        
                      }else if(pay.ChikPeaO2B__Payment_Method__c.containsIgnoreCase('ACH')){
                        map<string,string> achDetails= new map<string,string>();
                        achDetails.put('PaymentGatewayName',pay.Payment_Gateway__c);
                        achDetails.put('AccountId',(pay.ChikPeaO2B__Invoice_Line__r.ChikPeaO2B__Subscription__c!=null?pay.ChikPeaO2B__Invoice_Line__r.ChikPeaO2B__Subscription__c:pay.ChikPeaO2B__Account__c));
                        achDetails.put('TransactionTypeACH','DEBIT');
                        achDetails.put('InvoiceRef',pay.ChikPeaO2B__Invoice__r.name);
                        achDetails.put('CustomerRef',pay.ChikPeaO2B__Account__r.name);
                        achDetails.put('Amount',String.valueof(pay.ChikPeaO2B__Payment_Amount__c));
                        achDetails.put('EcheckType','CCD');
                        achDetails.put('COMMENT1','Comment payflow');
                        pgResponse=PG_PaymentProcessorInvoker.processACH(achDetails);
                      } 
                      //Modify payment inspecting responce received 
                      if(pgResponse!=null 
                        //&& pgResponse.processStatus.equalsignorecase('SUCCESS') 
                        && pgResponse.approvalStatus.equalsignorecase('APPROVED')
                      ){
                          pay.ChikPeao2b__Has_Processed__c = true;
                          pay.ChikPeao2b__Status__c = 'Processed';
                          if(pay.ChikPeaO2B__Payment_Method__c.containsIgnoreCase('ACH')){
                            pay.ChikPeao2b__Status__c = 'In Process';
                          }
                          pay.ChikPeao2b__Trx_Message__c = pgResponse.responseMessage!=null?pgResponse.responseMessage:pgResponse.responseCode;
                          pay.ChikPeao2b__Transaction_Id__c = pgResponse.transactionId;
                          pay.ChikPeaO2B__Last_Payment_Process_Date__c=date.today();
                          payupdatelist.add(pay);
                          //Added by Spal 21-DEC-2014[START]
                          if(pay.ChikPeaO2B__Invoice_Line__c!=null){
                              if(!InvoiceLinePaidAmnt.containskey(pay.ChikPeaO2B__Invoice_Line__c))
                                  InvoiceLinePaidAmnt.put(pay.ChikPeaO2B__Invoice_Line__c,pay.ChikPeaO2B__Payment_Amount__c);
                              else
                                  InvoiceLinePaidAmnt.put(pay.ChikPeaO2B__Invoice_Line__c,InvoiceLinePaidAmnt.get(pay.ChikPeaO2B__Invoice_Line__c)+pay.ChikPeaO2B__Payment_Amount__c);        
                          }
                          //Added by Spal 21-DEC-2014[END]
                      }else{
                          pay.ChikPeao2b__Has_Processed__c =false;
                          pay.ChikPeao2b__Status__c = 'Failed';
                          pay.ChikPeaO2B__Last_Payment_Process_Date__c=date.today();
                          pay.ChikPeaO2B__Failure_Exception__c=pgResponse.responseMessage;
                          if(pay.ChikPeaO2B__Payment_Method__c.containsIgnoreCase('Credit Card'))
                              pay.ChikPeaO2B__Card_Failure__c=pay.ChikPeaO2B__Card_Failure__c!=null?(pay.ChikPeaO2B__Card_Failure__c+1):1;
                          payupdatelist.add(pay);
                      }                 
                }catch(PG_Exception pex){                   
                    Exception cause = pex.getCause();
                    Exception ex = null;
                    if(cause != null) 
                        ex = cause;
                    else 
                        ex = pex;
                    pay.ChikPeao2b__Has_Processed__c =false;
                    pay.ChikPeao2b__Status__c = 'Failed';
                    pay.ChikPeaO2B__Last_Payment_Process_Date__c=date.today();
                    if(pex.getErrorType()=='SOAPFault' ){
                        //Necessary only for Chase Orbital
                        pay.ChikPeaO2B__Failure_Exception__c=pex.getErrorMesssage();
                    }else{
                        pay.ChikPeaO2B__Failure_Exception__c=ex.getMessage();
                    }
                    if(pay.ChikPeaO2B__Payment_Method__c.containsIgnoreCase('Credit Card'))
                      pay.ChikPeaO2B__Card_Failure__c=pay.ChikPeaO2B__Card_Failure__c!=null?(pay.ChikPeaO2B__Card_Failure__c+1):1;
                    payupdatelist.add(pay);
                    //TODO: use list insert to reduce no of dml/batch run, not necessary at present
                    Id sfLogId = enableGWTrxLog?PG_PaymentProcessorInvoker.logCC(ccDetailsMap, 0, pay.Id, pgResponse, true, String.valueOf(ex)+'\n'+ex.getStackTraceString()):'BatchAutoPaymentProcess';
                    ExceptionLogger.insertLog(ex, sfLogId);
                }catch(Exception ex){
                    pay.ChikPeao2b__Has_Processed__c =false;
                    pay.ChikPeao2b__Status__c = 'Failed';
                    pay.ChikPeaO2B__Last_Payment_Process_Date__c=date.today();
                    pay.ChikPeaO2B__Failure_Exception__c=ex.getMessage();
                    if(pay.ChikPeaO2B__Payment_Method__c.containsIgnoreCase('Credit Card'))
                      pay.ChikPeaO2B__Card_Failure__c=pay.ChikPeaO2B__Card_Failure__c!=null?(pay.ChikPeaO2B__Card_Failure__c+1):1;
                    payupdatelist.add(pay);
                    //TODO: use list insert to reduce no of dml/batch run, not necessary at present
                    Id sfLogId = enableGWTrxLog?PG_PaymentProcessorInvoker.logCC(ccDetailsMap, 0, pay.Id, pgResponse, true, String.valueOf(ex)+'\n'+ex.getStackTraceString()):'BatchAutoPaymentProcess';
                    ExceptionLogger.insertLog(ex, sfLogId);                 
                }   
             
             }  
        }
        System.debug('---payupdatelist='+payupdatelist);
        if(payupdatelist!=null && payupdatelist.size()>0)
        {
            update(payupdatelist);
            CCGatewayConnector.SubscriptionUpdate(payupdatelist);
        }
        }
        catch(Exception e){errmsg = e.getMessage();ExceptionLogger.insertLog(e, 'BatchAutoPaymentProcess');}
    }
    global void finish(Database.BatchableContext BC)
    {
        //Added by Spal 21-DEC-2014[START]
        List<ChikPeaO2B__Invoice_Line__c>updateInvlList=new List<ChikPeaO2B__Invoice_Line__c>();
        for(ChikPeaO2B__Invoice_Line__c invl :[select id,name,ChikPeaO2B__Line_Rate__c,ChikPeaO2B__Line_Tax__c,ChikPeaO2B__IsLine_Paid__c from ChikPeaO2B__Invoice_Line__c where id in : InvoiceLinePaidAmnt.keyset()])
        {
            if((invl.ChikPeaO2B__Line_Rate__c+invl.ChikPeaO2B__Line_Tax__c)<=InvoiceLinePaidAmnt.get(invl.id))
            {
                invl.ChikPeaO2B__IsLine_Paid__c=true;
                updateInvlList.add(invl);    
            }
        }
        if(updateInvlList!=null && updateInvlList.size()>0)
            update(updateInvlList);
        //Added by Spal 21-DEC-2014[END]    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo(email);
        mail.setSenderDisplayName('ChikPeaO2B Batch Process');
        mail.setSubject('Auto Payment Batch Process Completed');
        if (errmsg == null) errmsg = '';
        mail.setPlainTextBody('Auto payment Batch Process has completed ' + errmsg);
        try{
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception e){}
    }
}