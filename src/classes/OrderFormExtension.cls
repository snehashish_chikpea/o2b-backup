/*
** Class         :  OrderFormExtension
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

global with sharing class OrderFormExtension{
     public ApexPages.StandardController controller;
     public order__c order{get;set;}
     public List<O2B_Setting__c>o2bsetList{get;set;}
     public boolean copybillinfo{get;set;}
     public boolean copyshipinfo{get;set;}
     public boolean checkboxdisabled{get;set;}
     private string cancelUrl;
     public OrderFormExtension(ApexPages.StandardController Controller){
        this.controller=controller;
        order = new order__c();
        boolean fromAcc=false;
        id lkid;
        for(string key : ApexPages.currentPage().getParameters().keyset())
        {
            if(key.endsWith('_lkid'))
            {
                fromAcc=true;
                lkid=ApexPages.currentPage().getParameters().get(key);
                order.ChikPeaO2B__Account__c=lkid;
                break;
            }
        }
        copybillinfo=false;
        copyshipinfo=false;
        if(fromAcc)
        {
            cancelUrl='/'+lkid;
        }
        else
        {
            Map<String, Schema.SObjectType> gd=Schema.getGlobalDescribe();
            //Map<String, Schema.SObjectField> M=gd.get('ChikPeaO2B__order__c').getDescribe().fields.getMap();
            Schema.DescribeSObjectResult r =  gd.get('ChikPeaO2B__order__c').getDescribe();
            cancelUrl='/'+r.getKeyPrefix();
        }
        /*String soql='select ';
        for(Schema.SObjectField sf:M.values())
        {
            if(sf.getDescribe().isCreateable())
                soql+=string.valueof(sf)+',';
        }
        soql=soql.removeEndIgnoreCase(',')+' from order__c where id=:controller.getid()';
        order = database.query(soql); */
        copyornot();
        o2bsetList=[SELECT id,name,Single_Item_Order__c,Show_Billing_Address_On_Order_Screen__c,Show_Shipping_Addresss_On_Order_Screen__c from O2B_Setting__c limit 1];
     }
     
     public List<Schema.FieldSetMember> getGeneralSectionFields() {
        return SObjectType.order__c.FieldSets.General_Section.getFields();
     }
     public List<Schema.FieldSetMember> getBillingSectionFields() {
        return SObjectType.order__c.FieldSets.Billing_Address.getFields();
     }
     public List<Schema.FieldSetMember> getShippingSectionFields() {
        return SObjectType.order__c.FieldSets.Shipping_Address.getFields();
     }
     public List<Schema.FieldSetMember> getSingleItemOrderSectionFields() {
        return SObjectType.order__c.FieldSets.Single_Item_Order.getFields();
     }
     public void copyornot()
     {
         if(order.Account__c!=null){
             checkboxdisabled=false;
             system.debug('$$$$$$1111');    
         }    
         else{
             checkboxdisabled=true;    
             system.debug('$$$$$$2222');
         }
     }
     public void copybillinginfo()
     {
         List<account> acc=[SELECT id,name,BillingCity,BillingCountry,BillingState,BillingStreet,BillingPostalCode,ShippingCity,ShippingCountry,ShippingState,ShippingStreet,ShippingPostalCode from account where id=:order.account__c];    
         if(acc!=null && acc.size()>0 && copybillinfo){
             order.Billing_City__c=acc[0].BillingCity;
             order.Billing_Country__c=acc[0].BillingCountry;
             order.Billing_State_Province__c=acc[0].BillingState;
             order.Billing_Street__c=acc[0].BillingStreet;
             order.Billing_Zip_Postal_Code__c=acc[0].BillingPostalCode;
         }
         else
         {
             order.Billing_City__c=null;
             order.Billing_Country__c=null;
             order.Billing_State_Province__c=null;
             order.Billing_Street__c=null;
             order.Billing_Zip_Postal_Code__c=null;
         }
     }
     public void copyshipinfo()
     {
         List<account> acc=[SELECT id,name,BillingCity,BillingCountry,BillingState,BillingStreet,BillingPostalCode,ShippingCity,ShippingCountry,ShippingState,ShippingStreet,ShippingPostalCode from account where id=:order.account__c];        
         if(acc!=null && acc.size()>0 && copyshipinfo){
             order.Shipping_City__c=acc[0].ShippingCity;
             order.Shipping_Country__c=acc[0].ShippingCountry;
             order.Shipping_State_Province__c=acc[0].ShippingState;
             order.Shipping_Street__c=acc[0].ShippingStreet;
             order.Shipping_Zip_Postal_Code__c=acc[0].ShippingPostalCode;
         }
         else
         {
             order.Shipping_City__c=null;
             order.Shipping_Country__c=null;
             order.Shipping_State_Province__c=null;
             order.Shipping_Street__c=null;
             order.Shipping_Zip_Postal_Code__c=null;
         }
     }
     public void copydefaultprice()
     {
         order.unit_price__c=calculateunitprice(order.Rate_Plan__c,integer.valueof(order.quantity__c));
     }
     /*
     public Pagereference Save(){
         if(order!=null){
             try{
                 insert(order);
                 pagereference pageref=new Pagereference('/'+order.id);
                 pageref.setRedirect(true);
                 return pageref;
             }
             catch(Exception e)
             {
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getmessage()));
                 return null;
             }
         }
         else
             return null;
     }*/
     public Pagereference Cancel(){
         pagereference pageref=New Pagereference(cancelUrl);
         pageref.setRedirect(True);
         return pageref;    
     }
     public pagereference SaveXmlOrder(){
         string request;
         Map<String,String>InputValueMap=new Map<string,string>();
         for(Schema.FieldSetMember gf : this.getGeneralSectionFields()){
             InputValueMap.put(string.valueof(gf.fieldPath),string.valueof(order.get(string.valueof(gf.fieldPath))));
         }
         for(Schema.FieldSetMember gf : this.getBillingSectionFields()){
             InputValueMap.put(string.valueof(gf.fieldPath),string.valueof(order.get(string.valueof(gf.fieldPath))));
         }
         for(Schema.FieldSetMember gf : this.getShippingSectionFields()){
             InputValueMap.put(string.valueof(gf.fieldPath),string.valueof(order.get(string.valueof(gf.fieldPath))));
         }
         string rpid;string qty;string unitp;
         for(Schema.FieldSetMember gf : this.getSingleItemOrderSectionFields()){
             if(string.valueof(gf.fieldPath).equalsIgnorecase('ChikPeaO2B__Rate_Plan__c'))
                 rpid= string.valueof(order.get(string.valueof(gf.fieldPath)));
             if(string.valueof(gf.fieldPath).equalsIgnorecase('ChikPeaO2B__Quantity__c'))       
                 qty= string.valueof(order.get(string.valueof(gf.fieldPath)));
             if(string.valueof(gf.fieldPath).equalsIgnorecase('ChikPeaO2B__Unit_Price__c'))       
                 unitp= string.valueof(order.get(string.valueof(gf.fieldPath)));    
         }
         if(rpid!=null && rpid!='' && qty!=null && qty!='' && unitp!=null && unitp!='')
             InputValueMap.put('ChikPeaO2B__Rate_Plan__c',rpid+':'+qty+':'+unitp);
         request='<?xml version=\'1.0\' encoding=\'us-ascii\'?>'+'<OrderDetails>';
         for(string key: InputValueMap.keyset()){
             request+='<Data type="'+key+'">'+InputValueMap.get(key)+'</Data>';   
         }
         request+='</OrderDetails>';
         system.debug('Requesttt'+request);
         string result=OrderSave.OrderInsert(request);
         system.debug('Resultttt'+result);
         if(result!=null){
             if(!result.startsWithIgnoreCase('Error'))
             {
                 pagereference pageref=new Pagereference('/'+result);
                 pageref.setRedirect(true);
                 return pageref;    
             }
             else
             {
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,result));
                 return null;
             }
         }
         else
             return null;
     }
     
     webservice static decimal calculateunitprice(id rpid, integer qty)
     {   
     	//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'#****rpid='+rpid+' \nqty='+qty)); 
     	system.debug('~~~'+rpid+'~~'+qty);
         Rate_Plan__c rp=[SELECT id,name,Bill_Cycle__c,Free_Usage__c,Item__c,item__r.Item_Type__c,Max_Usage__c,Non_Recurring_Charge__c,Period_Off_Set__c,
                        Period_Off_Set_Unit__c,Price_Book__c,Pricing_Type__c,Recurring_Charge__c,Start_Off_set__c,Start_Off_Set_Unit__c,UOM__c,Usage_Rate__c from Rate_Plan__c where id=: rpid];
         
         decimal SelectedPrice=0;
         decimal TotalPrice=0;
         if(rp.item__r.Item_Type__c=='One-Off')
         {
            if(rp.Pricing_Type__c=='Flat'){
                
                SelectedPrice = (rp.Non_Recurring_Charge__c!=null?rp.Non_Recurring_Charge__c:0).setscale(3); 
            }
            else if(rp.Pricing_Type__c=='Tiered')
            {    
                integer trc=1;
                List<Tiered_Pricing__c>TieredIterator=[SELECT id,name,Lower_Limit__c,Upper_Limit__c,Non_Recurring_Charge__c,Recurring_Charge__c,Usage_Rate__c from Tiered_Pricing__c where Rate_Plan__c=:rp.id order by Lower_Limit__c asc];
                if(TieredIterator!=null && TieredIterator.size()>0)
                {
                    decimal remitmqty=qty;
                    TotalPrice=0;

                    for(Tiered_Pricing__c tr:TieredIterator)
                    {          
                        if(remitmqty<=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1))
                        {
                            TotalPrice+=(tr.Non_Recurring_Charge__c!=null?tr.Non_Recurring_Charge__c:0)*remitmqty;
                            remitmqty=0;
                            break;
                        }
                        else if(remitmqty>=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1) && trc<TieredIterator.size())
                        {
                            TotalPrice+=(tr.Non_Recurring_Charge__c!=null?tr.Non_Recurring_Charge__c:0)*((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                            remitmqty-=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                        }
                        else if(trc==TieredIterator.size())//remitmqty>=tr.Upper_Limit__c && 
                        {
                            TotalPrice+=(tr.Non_Recurring_Charge__c!=null?tr.Non_Recurring_Charge__c:0)*remitmqty;
                            remitmqty=0;
                        }
                                                                    
                        trc++;
                    }
                    SelectedPrice = (TotalPrice/qty).setscale(3);
                   
                }
                else
                {
                    TotalPrice=0;
                    SelectedPrice = TotalPrice/qty;
                }
            }
            else if(rp.Pricing_Type__c=='Volume')
            {    
                integer vpc=1;
                List<Volume_Pricing__c>VolumeIterator=[SELECT id,name,Lower_Limit__c,Upper_Limit__c,Non_Recurring_Charge__c,Recurring_Charge__c,Usage_Rate__c from Volume_Pricing__c where Rate_Plan__c=:rp.id order by Lower_Limit__c asc];

                if(VolumeIterator!=null && VolumeIterator.size()>0)
                {
                    for(Volume_Pricing__c vp:VolumeIterator)
                    {
                        if((qty<=vp.Lower_Limit__c && vpc==1) || (qty>=vp.Lower_Limit__c && qty<=vp.Upper_Limit__c)||(qty>=vp.Upper_Limit__c && vpc==VolumeIterator.size()))
                        {
                            TotalPrice=(vp.Non_Recurring_Charge__c!=null?vp.Non_Recurring_Charge__c:0);
                            SelectedPrice = (TotalPrice/qty).setscale(3);
                            break;
                        }
                        vpc++;
                    }
                }
                else
                {
                TotalPrice=0;
                SelectedPrice=0;
                }
            }   
         }
         else if(rp.item__r.Item_Type__c=='Pre-Paid Usage')
         {
             SelectedPrice = (rp.Non_Recurring_Charge__c!=null?rp.Non_Recurring_Charge__c:0).setscale(3);
         }
         else if(rp.item__r.Item_Type__c=='Recurring')
         {
             if(rp.Pricing_Type__c=='Flat'){
             	
                
                SelectedPrice = (rp.Recurring_Charge__c!=null?rp.Recurring_Charge__c:0).setscale(3);
            }
            else if(rp.Pricing_Type__c=='Tiered')
            {    
                integer trc=1;
                List<Tiered_Pricing__c>TieredIterator=[SELECT id,name,Lower_Limit__c,Upper_Limit__c,Non_Recurring_Charge__c,Recurring_Charge__c,Usage_Rate__c from Tiered_Pricing__c where Rate_Plan__c=:rp.id order by Lower_Limit__c asc];
                if(TieredIterator!=null && TieredIterator.size()>0)
                {
                    decimal remitmqty=qty;
                    TotalPrice=0;

                    for(Tiered_Pricing__c tr:TieredIterator)
                    {          
                        if(remitmqty<=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1))
                        {
                            TotalPrice+=(tr.Recurring_Charge__c!=null?tr.Recurring_Charge__c:0)*remitmqty;
                            remitmqty=0;
                            break;
                        }
                        else if(remitmqty>=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1) && trc<TieredIterator.size())
                        {
                            TotalPrice+=(tr.Recurring_Charge__c!=null?tr.Recurring_Charge__c:0)*((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                            remitmqty-=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                        }
                        else if(trc==TieredIterator.size())//remitmqty>=tr.Upper_Limit__c && 
                        {
                            TotalPrice+=(tr.Recurring_Charge__c!=null?tr.Recurring_Charge__c:0)*remitmqty;
                            remitmqty=0;
                        }
                                                                    
                        trc++;
                    }
                    SelectedPrice = (TotalPrice/qty).setscale(3);
                   
                }
                else
                {
                    TotalPrice=0;
                    SelectedPrice = TotalPrice/qty;
                }
            }
            else if(rp.Pricing_Type__c=='Volume')
            {    
                integer vpc=1;
                List<Volume_Pricing__c>VolumeIterator=[SELECT id,name,Lower_Limit__c,Upper_Limit__c,Non_Recurring_Charge__c,Recurring_Charge__c,Usage_Rate__c from Volume_Pricing__c where Rate_Plan__c=:rp.id order by Lower_Limit__c asc];

                if(VolumeIterator!=null && VolumeIterator.size()>0)
                {
                    for(Volume_Pricing__c vp:VolumeIterator)
                    {
                        if((qty<=vp.Lower_Limit__c && vpc==1) || (qty>=vp.Lower_Limit__c && qty<=vp.Upper_Limit__c)||(qty>=vp.Upper_Limit__c && vpc==VolumeIterator.size()))
                        {
                            TotalPrice=(vp.Recurring_Charge__c!=null?vp.Recurring_Charge__c:0);
                            SelectedPrice = (TotalPrice/qty).setscale(3);
                            break;
                        }
                        vpc++;
                    }
                    system.debug('@@##$$'+SelectedPrice+'@@'+TotalPrice);
                }
                else
                {
                TotalPrice=0;
                SelectedPrice=0;
                }
            }    
         }
         else if(rp.item__r.Item_Type__c=='Post-Paid Usage')
         {
             SelectedPrice = (rp.Recurring_Charge__c!=null?rp.Recurring_Charge__c:0).setscale(3);    
         }
         
         system.debug('222@@##$$'+SelectedPrice);
         //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'#****SelectedPrice='+SelectedPrice));
         return SelectedPrice;
     }
}