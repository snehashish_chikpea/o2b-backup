/*
** Class         :  SubscriptionHistory
** Test Class	 :  TestSubscriptionHistory
** Created by    :  Mehebub Hossain(chikpea Inc.)
** Created On	 :  20-03-2015
** Description   :  Subscrion change history 
*/
public with sharing class SubscriptionHistory {
	private ApexPages.StandardController controller;
	public List<ChikPeaO2B__Subscription__c> sub_history{get;set;}
	public List<SubWrap> sub_history_wrap{get;set;}
	public SubscriptionHistory(ApexPages.StandardController controller){
		this.controller=controller;
		//Initilize Susbscription history
		sub_history=new List<ChikPeaO2B__Subscription__c>();
		sub_history_wrap=new List<SubWrap>();
		pullHistory();
	}
	/* 
		Look for previous versions of Subscription up 5 level
	    and create history of of Suscription
	*/ 
	public void pullHistory(){
		//Querying previous version Subscriptions Ids
		ChikPeaO2B__Subscription__c history_id=[select id, Name,
		ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c,
		ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c,
		ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c 
		from ChikPeaO2B__Subscription__c where Id=:controller.getId()];
		//Collecting previous version Suscription
		List<Id> history_id_list=new List<Id>();
		//adding current subscripion
		history_id_list.add(controller.getId());
		
		//Checking previous version availability 
		if(history_id.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c!=null){
			//previous version Subscrioption Level2
			history_id_list.add(history_id.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c);
			if(history_id.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c!=null){
				//previous version Subscrioption Level3
				history_id_list.add(history_id.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c);
				Id level4=history_id.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c;
				if(level4!=null){
					//previous version Subscrioption Level4
					history_id_list.add(level4);
					//Querying previous version Subscriptions Ids with Leavel > 4
					ChikPeaO2B__Subscription__c history_id4=[select id, Name,
						ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c,
						ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c,
						ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c 
						from ChikPeaO2B__Subscription__c where Id=:level4];
						if(history_id4.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c!=null){
							//previous version Subscrioption Level5
							history_id_list.add(history_id4.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c);
							if(history_id4.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c!=null){
							//previous version Subscrioption Level6
							history_id_list.add(history_id4.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c);
							Id level7=history_id4.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c;
								if(level7!=null){
									history_id_list.add(level7);
									ChikPeaO2B__Subscription__c history_id7=[select id, Name,
										ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c,
										ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c,
										ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c 
										from ChikPeaO2B__Subscription__c where Id=:level7];
									if(history_id7.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c!=null){
										//previous version Subscrioption Level8
										history_id_list.add(history_id7.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c);
										if(history_id7.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c!=null){
											//previous version Subscrioption Level9
											history_id_list.add(history_id7.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c);
											Id level10=history_id7.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__r.ChikPeaO2B__Order_Line__r.ChikPeaO2B__Old_Subscription__c;
											if(level10!=null){
												//previous version Subscrioption Level10
												history_id_list.add(level10);
											}
										}
									}	
								}
							}
						}
				}
			}
			List<ChikPeaO2B__Subscription__c> history_unord_l=[Select Id, Name, ChikPeaO2B__Account__c,
			 ChikPeaO2B__Item__c, ChikPeaO2B__Rate_Plan__c, ChikPeaO2B__Quantity__c, ChikPeaO2B__Billing_Start_Date__c,
			 ChikPeaO2B__Billing_Stop_Date__c, ChikPeaO2B__Next_Bill_Date__c, ChikPeaO2B__Recurring_Charge__c
			 from ChikPeaO2B__Subscription__c where Id IN:history_id_list];
			Map<Id, SubWrap>sub_history_map=new Map<Id, SubWrap>();
			for(ChikPeaO2B__Subscription__c history_unord:history_unord_l){
				sub_history_map.put(history_unord.Id, new SubWrap(history_unord));
			}
			//Creating order of Subscription history
			for(Id history_id_t:history_id_list){
				//sub_history.add(sub_history_map.get(history_id_t));
				sub_history_wrap.add(sub_history_map.get(history_id_t));
			}
			/*
			for(Integer i=0;i<history_id_list.size()-1;i=i+2){
				SubWrap sub_w1=sub_history_map.get(history_id_list[i]);
				SubWrap sub_w2=sub_history_map.get(history_id_list[i+1]);
				if(sub_w1.sub.Name!=sub_w2.sub.Name){
					sub_w1.name_chng=true;
					sub_w2.name_chng=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Next_Bill_Date__c!=sub_w2.sub.ChikPeaO2B__Next_Bill_Date__c){
					sub_w1.next_bill_date_chng=true;
					sub_w2.next_bill_date_chng=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Billing_Start_Date__c!=sub_w2.sub.ChikPeaO2B__Billing_Start_Date__c){
					sub_w1.start_bill_date_chng=true;
					sub_w2.start_bill_date_chng=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Rate_Plan__c!=sub_w2.sub.ChikPeaO2B__Rate_Plan__c){
					sub_w1.rate_plan_chang=true;
					sub_w2.rate_plan_chang=true;
				}
			}*/
			
			for(Integer i=history_id_list.size()-1;i>0;i--){
				SubWrap sub_w1=sub_history_map.get(history_id_list[i]);
				SubWrap sub_w2=sub_history_map.get(history_id_list[i-1]);
				if(sub_w1.sub.Name!=sub_w2.sub.Name){
					sub_w1.name_chng=true;
					sub_w2.name_chng=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Next_Bill_Date__c!=sub_w2.sub.ChikPeaO2B__Next_Bill_Date__c){
					sub_w1.next_bill_date_chng=true;
					sub_w2.next_bill_date_chng=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Billing_Start_Date__c!=sub_w2.sub.ChikPeaO2B__Billing_Start_Date__c){
					sub_w1.start_bill_date_chng=true;
					sub_w2.start_bill_date_chng=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Rate_Plan__c!=sub_w2.sub.ChikPeaO2B__Rate_Plan__c){
					sub_w1.rate_plan_chang=true;
					sub_w2.rate_plan_chang=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Quantity__c!=sub_w2.sub.ChikPeaO2B__Quantity__c){
					sub_w1.Quantity_chng=true;
					sub_w2.Quantity_chng=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Item__c!=sub_w2.sub.ChikPeaO2B__Item__c){
					sub_w1.item_chang=true;
					sub_w2.item_chang=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Billing_Stop_Date__c!=sub_w2.sub.ChikPeaO2B__Billing_Stop_Date__c){
					sub_w1.stop_date_chang=true;
					sub_w2.stop_date_chang=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Recurring_Charge__c!=sub_w2.sub.ChikPeaO2B__Recurring_Charge__c){
					sub_w1.recurring_c_chang=true;
					sub_w2.recurring_c_chang=true;
				}
				if(sub_w1.sub.ChikPeaO2B__Rate_Plan__c!=sub_w2.sub.ChikPeaO2B__Rate_Plan__c){
					sub_w1.Rate_Plan_chng=true;
					sub_w2.Rate_Plan_chng=true;
				}
			}
		}else{
			//No previous version Subscription
		} 
	}
	public class SubWrap{
		public ChikPeaO2B__Subscription__c sub{get;set;}
		public boolean name_chng{get;set;}
		public boolean Rate_Plan_chng{get;set;}
		public boolean Quantity_chng{get;set;}
		public boolean next_bill_date_chng{get;set;}
		public boolean start_bill_date_chng{get;set;}
		public boolean rate_plan_chang{get;set;}
		public boolean item_chang{get;set;}
		public boolean stop_date_chang{get;set;}
		public boolean recurring_c_chang{get;set;}
		public SubWrap(ChikPeaO2B__Subscription__c sub){
			this.sub=sub;
		}		
	}
}