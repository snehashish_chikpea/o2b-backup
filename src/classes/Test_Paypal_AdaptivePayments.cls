@isTest
private class Test_Paypal_AdaptivePayments {

    static testMethod void Test1() {
      
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Save_Failed_Payment__c=true,ChikPeaO2B__Delete_Credit_Cards_older_by__c=14);
        insert(o2bset);
        
        ChikPeaO2B__Bank_Account__c ba=new ChikPeaO2B__Bank_Account__c();
        ba.ChikPeaO2B__For_PayPal__c=true;
        ba.ChikPeaO2B__PayPal_PreApproval_Key__c='PA-0CK5250828868341B';
        insert ba;
        
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly',ChikPeaO2B__Bank_Account__c=ba.id);
        insert(acc);
        
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c itm=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        insert(itm);
        ChikPeaO2B__Rate_Plan__c rp=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itm.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        insert rp;
        
        ChikPeaO2B__Invoice__c inv=new ChikPeaO2B__Invoice__c();
        inv.ChikPeaO2B__Account__c=acc.id;
        insert inv;
        
        ChikPeaO2B__Invoice_Line__c il=new ChikPeaO2B__Invoice_Line__c();
        il.ChikPeaO2B__Invoice__c=inv.id;
        il.ChikPeaO2B__Item__c=itm.id;
        il.ChikPeaO2B__Qty__c=1;
        il.ChikPeaO2B__Unit_Rate__c=10;
        insert il;
        
        ChikPeaO2B__Payment__c p=new ChikPeaO2B__Payment__c();
        p.ChikPeaO2B__Account__c=acc.id;
        p.ChikPeaO2B__Invoice__c=inv.id;
        p.ChikPeaO2B__Payment_Amount__c=1;
        p.ChikPeaO2B__Status__c='Failed';
        insert p;

        ChikPeaO2B__Payment__c p2=new ChikPeaO2B__Payment__c();
        p2.ChikPeaO2B__Account__c=acc.id;
        p2.ChikPeaO2B__Invoice__c=inv.id;
        p2.ChikPeaO2B__Payment_Amount__c=5;
        p2.ChikPeaO2B__Status__c='Processed';
        p2.ChikPeaO2B__PayKey__c='AP-1MB38231201276316';
        p2.ChikPeaO2B__Has_Processed__c=true;
        insert p2;
        
        ChikPeaO2B__Gateway__c ppg=new ChikPeaO2B__Gateway__c();
        ppg.name='PayPal Adaptive Payment';
        ppg.ChikPeaO2B__Gateway_Type__c='PayPal';
        ppg.ChikPeaO2B__Gateway_Url_Prod__c='https://svcs.paypal.com/AdaptivePayments/';
        ppg.ChikPeaO2B__Gateway_Url_Test__c='https://svcs.sandbox.paypal.com/AdaptivePayments/';
        ppg.ChikPeaO2B__Login_Id__c='ud_api1.chikpea.com';
        ppg.ChikPeaO2B__Password__c='HPLX9W9HK75ST7YR';
        ppg.ChikPeaO2B__Merchant_Reference__c='AFcWxV21C7fd0v3bYYYRCpSSRl31A84xNfp0ges0scS48fr-5rz85HfZ';
        ppg.ChikPeaO2B__PayPal_Receiver_Email__c='jani@chikpea.com';
        ppg.ChikPeaO2B__Test_Mode__c=true;
        ppg.ChikPeaO2B__App_ID__c='APP-80W284485P519543T';
        insert ppg;
        
        Paypal_AdaptivePayments.pay_after_preapproval(ba.id,p.id,'jani@chikpea.com','');
        Paypal_AdaptivePayments.pay_after_preapproval_mp(ba,p,ppg);
        Paypal_AdaptivePayments.refund(p.id);
        //Paypal_AdaptivePayments.pay_after_preapproval_mp(ba,p2,ppg);
        Paypal_AdaptivePayments.partial_refund(p2.id,ppg.id,2);
    }
}