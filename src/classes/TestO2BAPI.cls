@isTest
private class TestO2BAPI{
    static testMethod void trackFedEx(){
        string request = '<InputDetails><Data type="TrackingNumber">12345</Data>'+
        '<Data type="devKey">GAjIHbCd2fGulTSV</Data>'+
        '<Data type="password">lw0WmJUJdQYc5ffJ5CTfpSJnZ</Data>'+
        '<Data type="accountNumber">510087607</Data>'+
        '<Data type="meterNumber">100234435</Data>'+
        '<Data type="url">https://wsbeta.fedex.com:443/web-services</Data>'+
        '</InputDetails>';
        O2BAPI.trackFedEx(request);
        request = '<InputDetails><Data type="TrackingNumber">98764532465</Data></InputDetails>';
        O2BAPI.trackFedEx(request);
        O2BAPI.trackFedEx('');
    }
    static testMethod void ItemPrice(){
        Account acc = new Account();
        acc.Name = 'Test_account';
        Insert acc;
        
        ChikPeaO2B__Invoice__c inv = new ChikPeaO2B__Invoice__c();
        inv.ChikPeaO2B__Account__c = acc.id;        
        insert inv;
        
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        ChikPeaO2B__Item__c recV=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        insert (recV);
        
        ChikPeaO2B__Item__c recV1=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        insert (recV1);
        
        ChikPeaO2B__Item__c recV2=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        insert (recV2);
        
        ChikPeaO2B__Item__c recV3=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        insert (recV3);
        
        ChikPeaO2B__Rate_Plan__c rp5=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV3.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp5);
        
        ChikPeaO2B__Rate_Plan__c rp6=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV2.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp6);
        
        ChikPeaO2B__Rate_Plan__c rp7=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp7);
        
        ChikPeaO2B__Rate_Plan__c rp8=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp8);
        
        ChikPeaO2B__Rate_Plan__c rp9=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp9);
        
        ChikPeaO2B__Rate_Plan__c rp10=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp10);
        
        ChikPeaO2B__Rate_Plan__c rp11=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Xyz',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rp11);
        
        ChikPeaO2B__Volume_Pricing__c vp = new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c = recV.id,ChikPeaO2B__Lower_Limit__c = 1, ChikPeaO2B__Non_Recurring_Charge__c = 100,ChikPeaO2B__Rate_Plan__c=rp10.id,ChikPeaO2B__Recurring_Charge__c=250,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Usage_Rate__c=12 );
        insert vp; 
        
        ChikPeaO2B__Volume_Pricing__c vp1 = new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c = recV.id,ChikPeaO2B__Lower_Limit__c = 101, ChikPeaO2B__Non_Recurring_Charge__c = 100,ChikPeaO2B__Rate_Plan__c=rp10.id,ChikPeaO2B__Recurring_Charge__c=250,ChikPeaO2B__Upper_Limit__c=200,ChikPeaO2B__Usage_Rate__c=12 );
        insert vp1;
        
        ChikPeaO2B__Tiered_Pricing__c tp = new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c = recV.id,ChikPeaO2B__Lower_Limit__c = 1, ChikPeaO2B__Non_Recurring_Charge__c = 100,ChikPeaO2B__Rate_Plan__c=rp9.id,ChikPeaO2B__Recurring_Charge__c=250,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Usage_Rate__c=12 );
        insert tp;
        
        ChikPeaO2B__Tiered_Pricing__c tp1 = new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c = recV.id,ChikPeaO2B__Lower_Limit__c = 101, ChikPeaO2B__Non_Recurring_Charge__c = 100,ChikPeaO2B__Rate_Plan__c=rp9.id,ChikPeaO2B__Recurring_Charge__c=250,ChikPeaO2B__Upper_Limit__c=200,ChikPeaO2B__Usage_Rate__c=12 );
        insert tp1;
            
        ChikPeaO2B__Subscription__c sub = new ChikPeaO2B__Subscription__c();
        sub.ChikPeaO2B__Account__c = acc.id;
        sub.ChikPeaO2B__Item__c = recV.id;
        sub.ChikPeaO2B__Rate_Plan__c = rp8.id;
        sub.ChikPeaO2B__Billing_Start_Date__c = date.today();
        insert sub;
        
        O2BAPI.GetItemPrice(rp9.id,1000);
        O2BAPI.GetItemPrice(rp10.id,1000);
        O2BAPI.GetItemPrice(rp9.id,-1000);
        O2BAPI.GetItemPrice(rp10.id,-1000);
        O2BAPI.GetItemPrice(rp9.id,100);
        O2BAPI.GetItemPrice(rp10.id,100);
        O2BAPI.GetItemPrice(rp9.id,200);
        O2BAPI.GetItemPrice(rp10.id,200);
        
        O2BAPI.GetItemPrice(rp11.id,10);
        O2BAPI.GetItemPrice(rp8.id,10);
        O2BAPI.GetItemPrice(rp7.id,10);
        O2BAPI.GetItemPrice(rp6.id,10);
        O2BAPI.GetItemPrice(rp5.id,10);
        O2BAPI.GetItemPrice(null,10);
        
        O2BAPI.calculateprice(null,0.0,null,null);
        
    }
}