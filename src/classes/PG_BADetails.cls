/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-20
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG_BADetails {
    
    global String baName { get; set; }
    global String baABANumber { get; set; }
    global String baAccountName { get; set; }
    global String baAccountNumber { get; set; }
    global String baAccountType { get; set; }
    //New Details for Tokenization
    global String baCustomerProfileID { get; set; }
    global String baCustomerPaymentProfileID { get; set; }
    global PG_BADetails(){
        
    }
    
    global PG_BADetails(Id objId){//@throws PG_Exception
        Schema.SObjectType token = objId.getSObjectType();
        //System.debug('---token='+token);
        Schema.DescribeSObjectResult dr = token.getDescribe();
        //System.debug('---dr='+dr);
        
        String queryString = 'Select '+
            's.Name, s.Id, s.ChikPeaO2B__Bank_Account__r.Id, s.ChikPeaO2B__Bank_Account__r.Name, s.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__ABA_Number__c, ' +
            's.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Account_Name__c, s.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Account_Number__c, ' +
            's.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Account_Type__c, ' +
            's.ChikPeaO2B__Customer_Profile_Id__c,s.ChikPeaO2B__Bank_Account__r.ChikPeaO2B__Customer_Payment_Profile_Id__c '+
            'From '+ dr.getName() + ' s '+
            //'where s.Id = \'' + objId + '\''; 
            'where s.Id = :objId';  
                                    
        System.debug('---queryString='+queryString);
            
        sObject[] objList = Database.query(queryString); 
        //System.debug('objlist--->'+objList);
        if(objList != null && !objList.isEmpty()){
            ChikPeaO2B__Bank_Account__c ba = (ChikPeaO2B__Bank_Account__c)objList[0].getSObject('ChikPeaO2B__Bank_Account__r');
            baName = ba.Name;
            baABANumber = ba.ChikPeaO2B__ABA_Number__c;
            baAccountName = ba.ChikPeaO2B__Account_Name__c;
            baAccountNumber = ba.ChikPeaO2B__Account_Number__c;
            baAccountType = ba.ChikPeaO2B__Account_Type__c;
            //New Details for Tokenization
            baCustomerProfileID=(String)objList[0].get('ChikPeaO2B__Customer_Profile_Id__c');
            baCustomerPaymentProfileID=ba.ChikPeaO2B__Customer_Payment_Profile_Id__c;
        }else{
            throw new PG_Exception('Gateway Exception::Not found '+token+'(Id='+objId+')');
        }       
    }
    /*
    public static void tryIt(){
        PG_BADetails details = new PG_BADetails('001i0000004AP7uAAG');
        //String Id =  '\' or \'1\'=1';
        //PG_BADetails details = new PG_BADetails(Id);
        System.debug('---details='+details);
    }
    */
}