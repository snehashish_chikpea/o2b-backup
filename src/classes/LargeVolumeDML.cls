/*
** Class         :  LargeVolumeDML
** Created by    :  Asitm9 (chikpea Inc.)
** Created date  :  22/08/2013
** Description   : 
*/

public with sharing class LargeVolumeDML {
	
	public static void bulkOperationHelper(list<sObject> slist,integer operation){
        BatchLargeVolumeOperation bbi = new BatchLargeVolumeOperation(slist,operation);
        Database.executeBatch(bbi,199);
    }
}