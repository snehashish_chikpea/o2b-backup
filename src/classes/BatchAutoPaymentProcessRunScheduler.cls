/*
** Class         :  BatchAutoPaymentProcessRunScheduler
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  10 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

global with sharing class BatchAutoPaymentProcessRunScheduler
{
    Webservice static String executeschedule()
    {
        String error;
        String sch;        
        BatchAutoPaymentProcessScheduler BAPPS=new BatchAutoPaymentProcessScheduler();
        list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Billing_Day__c,ChikPeaO2B__Schedule_Time__c,ChikPeaO2B__Schedule_Time_For_Credit_Card_Batch__c from ChikPeaO2B__O2B_Setting__c limit 1];
        if(O2BsetList!=null && O2BsetList.size()>0)
        {
            if(O2BsetList[0].ChikPeaO2B__Schedule_Time_For_Credit_Card_Batch__c!=null && O2BsetList[0].ChikPeaO2B__Schedule_Time_For_Credit_Card_Batch__c!='')
            {
                List<String> ScheduleTime=O2BsetList[0].ChikPeaO2B__Schedule_Time_For_Credit_Card_Batch__c.split(':');
                sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' * * ?';
            }    
            else
            {
                    sch='0 00 08 * * ?';
            }
            try{
                system.schedule('Auto Payment Process Batch',sch, BAPPS);
                error='success';
            }
            catch(Exception E)
            {
                error=E.getMessage();
            }
        }
        return error;  
    }
}