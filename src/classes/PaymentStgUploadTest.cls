/*
** Class         :  PaymentStgUploadTest
** Created by    :  Mehebub Hossain (chikpea Inc.)
** Description   :  Test class for PaymentStgUpload
** Created On    :  19-Dec-14
*/
@isTest
public with sharing class PaymentStgUploadTest {
	static testMethod void test1(){
		PaymentStgUpload psu=new PaymentStgUpload();
		psu.t_data='[[000,inv1,acc1,Credit Card,12-18-2014,100,true,chk-ref-000,btc-ref-000]]';
		psu.t_head='[Line No.,Invoice Ref,Account Reference,Payment Method,Payment Date,Amount,SPLIT TO LINES,CHECK REF,BATCH REF]';
		psu.submit();
	}
	//invalid date
	static testMethod void negative1(){
		PaymentStgUpload psu=new PaymentStgUpload();
		psu.t_data='[[0,inv1,acc1,Credit Card,12182014,100]]';
		psu.t_head='[Line No.,Invoice Ref,Account Reference,Payment Method,Payment Date,Amount]';
		psu.submit();
	}
	//invalid amount
	static testMethod void negative2(){
		PaymentStgUpload psu=new PaymentStgUpload();
		psu.t_data='[[0,inv1,acc1,Credit Card,12-18-2014,abc]]';
		psu.t_head='[Line No.,Invoice Ref,Account Reference,Payment Method,Payment Date,Amount]';
		psu.submit();
	}
	//no data entered
	static testMethod void negative4(){
		PaymentStgUpload psu=new PaymentStgUpload();
		psu.t_data='[["0","","","","",""]]';
		psu.t_head='[Line No.,Invoice Ref,Account Reference,Payment Method,Payment Date,Amount]';
		psu.submit();
	}
}