/*
** Class         :  BatchCollection
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by	 :  Asitm (chikpea Inc.)
** Reason		 :  Secuirty Issue fixed.(sharing)
** Description   :   
*/



global with sharing class BatchCollection implements Database.Batchable<Sobject>,Database.Stateful{
    global String query;
    global String errmsg;
    global string email;
    global BatchCollection()
    {
        errmsg='';
    }
    global database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<ChikPeaO2B__Invoice__c>invList=new List<ChikPeaO2B__Invoice__c>();
        for(sObject s:scope)
        {
            ChikPeaO2B__Invoice__c inv=(ChikPeaO2B__Invoice__c)s;
            inv.ChikPeaO2B__Collection_Email_Date__c=(inv.ChikPeaO2B__Collection_Email_Date__c!=null?(inv.ChikPeaO2B__Collection_Email_Date__c+','+date.today()):string.valueof(date.today()));
            inv.ChikPeaO2B__Reminder__c=true;
            invList.add(inv);
        }
        
        try{
            if(invList!=null && invList.size()>0)
                update(invList);
        }
        catch(DmlException e)
        {
            errmsg = e.getDmlMessage(0);
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo(email);
        mail.setSenderDisplayName('ChikPeaO2B Batch Process');
        mail.setSubject('Auto Payment Batch Process Completed');
        if (errmsg == null) errmsg = '';
        mail.setPlainTextBody('Collection Batch Process has completed ' + errmsg);
        try{
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception e){}
    }
}