global with sharing class BatchCreditCardStatusScheduler implements Schedulable
{
	global boolean test_exception=false;
    global void execute(SchedulableContext sc)
	{
		BatchCreditCardStatus bccs= new BatchCreditCardStatus();
		List<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c from ChikPeaO2B__O2B_Setting__c limit 1];
		bccs.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
		if(Test.isRunningTest())
		{
		  if(test_exception)
          {
             bccs.test_status_for_batchid=true;
          }
		  bccs.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet:Reporting\' AND ChikPeaO2B__Payment_Method__c=\'Credit Card\' AND ChikPeaO2B__Status__c=\'Processed\' AND ChikPeaO2B__Has_Processed__c=true';
		}
		else
		{
		  bccs.query='select Id,ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Status__c,ChikPeaO2B__Payment_Gateway__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Invoice__c ,ChikPeaO2B__Account__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Payment_Gateway__c=\'AuthNet\' AND ChikPeaO2B__Payment_Method__c=\'Credit Card\' AND ChikPeaO2B__Status__c=\'Processed\' AND ChikPeaO2B__Has_Processed__c=true';
        }
		ID batchprocessid = Database.executeBatch(bccs,1);
	 }
}