/*
** Class         :  LargeVolumeBillingRunScheduler
** Created by    :  Asitm9 (chikpea Inc.)
** Created date  :  22/08/2013
** Description   : 
*/

global with sharing class LargeVolumeBillingRunScheduler {
    Webservice static String executeschedule(string grpId){
        string error='';
        string sch='';
        list<ChikPeaO2B__Bill_Group__c>  billGrpList=[SELECT id,Name,ChikPeaO2B__Schedule_Time__c,
        ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Billing_Day__c,ChikPeaO2B__Maximum_Records__c,
        ChikPeaO2B__Bulk_Data__c
        FROM ChikPeaO2B__Bill_Group__c WHERE Id=:grpId];
        if(billGrpList!=null && billGrpList.size()>0){
            if(billGrpList[0].ChikPeaO2B__Billing_Type__c!='Calendar Month'){
                return 'Yuor Billing Type is Not Allowed to Process Large Volume Data. ';
            }
            string query='select id,name,(select id from Subscriptions__r),'+
            '(select id from Usages__r) ' +
            ' from Account where ChikPeaO2B__Active__c=\'Yes\' and ChikPeaO2B__Is_Billing__c=true';
            query+=' AND ChikPeaO2B__Bill_Group__c=\''+String.escapeSingleQuotes(grpId) +'\'';
            list<Account> AccList=database.query(query);
        
            Integer totalSubSize=0;
            list<Id> AccIDList= new list<Id>();
            for(Account acc : AccList){
                AccIDList.add(acc.Id);
                totalSubSize=totalSubSize+(acc.Subscriptions__r!=null? acc.Subscriptions__r.size() : 0);
                totalSubSize=totalSubSize+(acc.Usages__r!=null? acc.Usages__r.size() : 0);
            }
            if(totalSubSize>billGrpList.get(0).ChikPeaO2B__Maximum_Records__c){
                return 'Error :-This Bill Group Contains More Subscription Record Than The Permitted Size. ';
            }
            if(!billGrpList.get(0).ChikPeaO2B__Bulk_Data__c){
                return 'Please Permit The System To Process Large Volume Data. ';
            }
            if(billGrpList[0].ChikPeaO2B__Schedule_Time__c!=null && billGrpList[0].ChikPeaO2B__Schedule_Time__c!='')
            {
                List<String> ScheduleTime=billGrpList[0].ChikPeaO2B__Schedule_Time__c.split(':');
                if(billGrpList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
                    sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' '+(billGrpList[0].ChikPeaO2B__Billing_Day__c!=null?billGrpList[0].ChikPeaO2B__Billing_Day__c:'1')+' * ?';
                else
                    sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' * * ?';
            }    
            else
            {
                if(billGrpList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
                    sch='0 00 06 '+(billGrpList[0].ChikPeaO2B__Billing_Day__c!=null?billGrpList[0].ChikPeaO2B__Billing_Day__c:'1')+' * ?';
                else
                    sch='0 00 06 * * ?';
            }
            try{
                //sch='0 69 06 * * ?';
                LargeVolumeBillingScheduler lv= new LargeVolumeBillingScheduler();
                string grpName=billGrpList[0].Name;
                system.schedule('Create Large Volume Invoice Batch '+grpName,sch, lv);
                error='success';
            }
            catch(Exception e)
            {
                error=e.getMessage();
                //throw new BatchException('Some error occured in BatchCreateBillRunScheduler Class.',e);
            }
        }
        return error;
    }
    

}