global with sharing class BatchCreateBillRunScheduler
{
    Webservice static String executeschedule()
    {
        String error;
        String sch;        
        BatchCreateBillScheduler BCBS=new BatchCreateBillScheduler();
        list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Billing_Day__c,ChikPeaO2B__Schedule_Time__c from ChikPeaO2B__O2B_Setting__c limit 1];
        if(O2BsetList!=null && O2BsetList.size()>0)
        {
            if(O2BsetList[0].ChikPeaO2B__Schedule_Time__c!=null && O2BsetList[0].ChikPeaO2B__Schedule_Time__c!='')
            {
                List<String> ScheduleTime=O2BsetList[0].ChikPeaO2B__Schedule_Time__c.split(':');
                if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
                    sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' '+(O2BsetList[0].ChikPeaO2B__Billing_Day__c!=null?O2BsetList[0].ChikPeaO2B__Billing_Day__c:'1')+' * ?';
                else
                    sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' * * ?';
            }    
            else
            {
                if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
                    sch='0 00 06 '+(O2BsetList[0].ChikPeaO2B__Billing_Day__c!=null?O2BsetList[0].ChikPeaO2B__Billing_Day__c:'1')+' * ?';
                else
                    sch='0 00 06 * * ?';
            }
            try{
                //sch='0 69 06 * * ?';
                system.schedule('Create Invoice Batch',sch, BCBS);
                error='success';
            }
            catch(Exception e)
            {
                error=e.getMessage();
                //throw new BatchException('Some error occured in BatchCreateBillRunScheduler Class.',e);
            }
        }
        return error;  
    }
}