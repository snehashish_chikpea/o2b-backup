/*
** Class         :  BatchAutoPaymentProcessScheduler
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  10 march 14
** Modified by   :  S Pal (chikpea Inc.)
** Reason        :  extra filter in batch query payment date<=TODAY
** Description   :   
*/

global with sharing class BatchAutoPaymentProcessScheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        BatchAutoPaymentProcess BAPP = new BatchAutoPaymentProcess();
        List<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c, ChikPeaO2B__Enable_Gateway_Trx_Log__c, ChikPeaO2B__Enable_Gateway_Approved_Trx_Log__c from ChikPeaO2B__O2B_Setting__c limit 1];
        
        system.debug('---------------->'+o2bset[0].ChikPeaO2B__Card_Process_Frequency__c);
        Decimal freq=o2bset[0].ChikPeaO2B__Card_Process_Frequency__c==null?3
        :o2bset[0].ChikPeaO2B__Card_Process_Frequency__c;
        Boolean enableGWTrxLog = o2bset[0].ChikPeaO2B__Enable_Gateway_Trx_Log__c;
        Boolean enableGWApprovedTrxLog = o2bset[0].ChikPeaO2B__Enable_Gateway_Approved_Trx_Log__c;
        
        /*
        if(o2bset[0].ChikPeaO2B__Card_Process_Frequency__c==NULL){
            o2bset[0].ChikPeaO2B__Card_Process_Frequency__c=3;
        }
        
        try{
            Integer.valueOf(o2bset[0].ChikPeaO2B__Card_Process_Frequency__c);
        }catch(Exception e){o2bset[0].ChikPeaO2B__Card_Process_Frequency__c=3;}
        */
         /* and ChikPeaO2B__Payment_Method__c=\'Credit Card\' */
        BAPP.enableGWTrxLog = enableGWTrxLog;
        BAPP.enableGWApprovedTrxLog = enableGWApprovedTrxLog;
        BAPP.intervaldays=o2bset[0].ChikPeaO2B__Card_Process_Interval_days__c;
        BAPP.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
        BAPP.query='select id,name,ChikPeaO2B__Account__c,ChikPeaO2B__Account__r.name,ChikPeaO2B__Card_Failure__c,ChikPeaO2B__Credit_Card__c,ChikPeaO2B__Failure_Exception__c,ChikPeaO2B__Has_Processed__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Status__c,ChikPeaO2B__Trx_Message__c,ChikPeaO2B__Invoice__r.name,'+
                   'ChikPeaO2B__Credit_Card_Number__c,ChikPeaO2B__Expiry_Month__c,ChikPeaO2B__Expiry_Year__c,ChikPeaO2B__Last_Payment_Process_Date__c,ChikPeaO2B__Payment_Amount__c,ChikPeaO2B__Payment_Gateway__c, ChikPeaO2B__Transaction_Id__c,ChikPeaO2B__Merchant_Order_Number__c,ChikPeaO2B__Invoice_Line__c,ChikPeaO2B__Invoice_Line__r.name,ChikPeaO2B__Invoice_Line__r.ChikPeaO2B__Subscription__c'+
                   ' from ChikPeaO2B__Payment__c where ' +
                   '(ChikPeaO2B__Invoice__r.ChikPeaO2B__Invoice_Status__c=\'Open\' or ChikPeaO2B__Invoice__r.ChikPeaO2B__Invoice_Status__c=\'Paid Partial\') and ChikPeaO2B__Has_Processed__c=false and ChikPeaO2B__Status__c!=\'Processed\' and ChikPeaO2B__Payment_Date__c<=TODAY and ((ChikPeaO2B__Payment_Method__c=\'Credit Card\' and (ChikPeaO2B__Card_Failure__c<'+freq+' or ChikPeaO2B__Card_Failure__c=null)) or ChikPeaO2B__Payment_Method__c!=\'Credit Card\')';
        ID batchprocessid = Database.executeBatch(BAPP,1);  
    }
}