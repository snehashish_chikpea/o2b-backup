/*
** Class         :  BatchInvoiceMailSend
** Created by    :  Asitm (chikpea Inc.)
** Last Modified :  28 march 14
** Modified by	 :  
** Reason		 :  
** Description   :   
*/

global with sharing class BatchInvoiceMailSend implements Database.Batchable<Sobject>,Database.Stateful{
	global String query;
    global String errmsg;
    global string email;
    
    global database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<ChikPeaO2B__Invoice__c>invList=new List<ChikPeaO2B__Invoice__c>();
        ChikPeaO2B__O2B_Setting__c[] o2bSet = 
        	[Select Id, ChikPeaO2B__Email_blank_invoices__c from ChikPeaO2B__O2B_Setting__c limit 1];
        
        for(sObject s:scope)
        {
            ChikPeaO2B__Invoice__c inv=(ChikPeaO2B__Invoice__c)s;
            if(o2bSet[0].ChikPeaO2B__Email_blank_invoices__c&&inv.ChikPeaO2B__Account__r.ChikPeaO2B__Bill_To_Parent__c==false){
		            inv.ChikPeaO2B__Email_Sent__c=system.now();
		            invList.add(inv);
            }else if(inv.ChikPeaO2B__Amount_Due__c!=0&&inv.ChikPeaO2B__Account__r.ChikPeaO2B__Bill_To_Parent__c==false){
		            inv.ChikPeaO2B__Email_Sent__c=system.now();
		            invList.add(inv);
	        }                        
        }
        
        try{
            if(!invList.isEmpty())
                update invList;
        }catch(DmlException e){errmsg = e.getDmlMessage(0);}
    }
    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo(email);
        mail.setSenderDisplayName('ChikPeaO2B Batch Process');
        mail.setSubject('Auto Invoice E-mail Sending Batch Process Completed');
        if (errmsg == null) errmsg = '';
        mail.setPlainTextBody('Auto Invoice E-mail Sending Batch Process Completed' + errmsg);
        try{
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception e){}
    }
}