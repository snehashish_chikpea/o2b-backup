/*
** Class         :  BatchCreateBill
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

global with sharing class BatchCreateBill implements Database.Batchable<Sobject>, Database.AllowsCallouts, Database.Stateful{

    global String query;
    global String errmsg;
    global string email;
    global static string apexJobId;
    global BatchCreateBill()
    {
        errmsg='';
    }
    global database.querylocator start(Database.BatchableContext BC)
    {
        try{
        return Database.getQueryLocator(query);
        }catch(Exception e){throw new BatchException('Some error occured in BatchCreateBill Class.',e);}
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        string billType='';
        apexJobId=BC.getJobId();
        list<ChikPeaO2B__O2B_Setting__c> obStt=[SELECT Id,ChikPeaO2B__Billing_Type__c 
        FROM ChikPeaO2B__O2B_Setting__c WHERE ChikPeaO2B__Bill_Schedule_Job_Id__c=:BC.getJobId()];
        
        list<ChikPeaO2B__Bill_Group__c> billGrp=[SELECT Id,ChikPeaO2B__Billing_Type__c 
        FROM ChikPeaO2B__Bill_Group__c WHERE ChikPeaO2B__Bill_Schedule_Job_Id__c=:BC.getJobId()];
        
        if(!obStt.isEmpty()){
            billType=obStt[0].ChikPeaO2B__Billing_Type__c;
        }
        if(!billGrp.isEmpty()){
            billType=billGrp[0].ChikPeaO2B__Billing_Type__c;
        }
        
        List<id>AccIDList=new List<id>();
        for(sObject s:scope)
        {
             Account acc=(Account)s;
             AccIDList.add(acc.id);
        }
        try{
            //Invoicing.CreateInvoice(AccIDList,true);
            Invoicing.generateInvoice(AccIDList,true,billType);
        }catch(Exception e){
            
            errmsg+=e.getmessage();
            errmsg+='\n';
            errmsg+=e.getCause();
            errmsg+='\n';
            errmsg+=e.getLineNumber();
            errmsg+='\n';
            errmsg+=e.getStackTraceString();
            errmsg+='\n';
            /*
            errmsg+='Total number of Query'+Limits.getLimitQueries();
            errmsg+='\n';
            errmsg+='Total Number of records that can be queried  in this apex code context: '+Limits.getLimitDmlRows();
            errmsg+='\n';
            errmsg+='Total Number of DML statements allowed in this apex code context: ' +  Limits.getLimitDmlStatements();
            errmsg+='\n';
            */
            
            //errmsg+=e.
            ChikPeaO2B__BatchLog__c log= new ChikPeaO2B__BatchLog__c();
            log.Name='BatchCreateBill';
            log.ChikPeaO2B__Error_Log__c=errmsg;
            insert log;
            system.debug('---------->err1'+errmsg);
            system.debug('---------->err2'+log);
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo(email);
        mail.setSenderDisplayName('ChikPeaO2B Batch Process');
        mail.setSubject('Billing Batch Process Completed');
        if (errmsg == null) errmsg = '';
        mail.setPlainTextBody('Batch Process has completed ' + errmsg);
        try{
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception e){}
        
        //BatchParentBillScheduler.execute(); 
        //BatchParentBillRunScheduler.executeschedule();
    }
}