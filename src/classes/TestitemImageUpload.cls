@isTest
public class TestitemImageUpload{
    @isTest
    static void testMethod1()
    {
        Account acc=new Account(name='acc1');
        insert(acc);
    
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        insert(oneoff);
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert(rp1);
        
        Test.startTest();
        PageReference pageRef = Page.Image_Upload;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('itmid',oneoff.id);
        itemImageUpload IIU=new itemImageUpload();
        IIU.itemid=oneoff.id;
        IIU.nameFile='Image.png';
        IIU.sizeFile=2000;
        IIU.ImageUpload();
        IIU.nameFile='Image.xxx';
        IIU.ImageUpload();
        IIU.sizeFile=60*1024;
        IIU.ImageUpload();
        Test.stopTest();
    }
}