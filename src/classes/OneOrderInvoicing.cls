/*
** Class         :  OneOrderInvoicing
** Created by    :  Asitm9 (chikpea Inc.)
** Last Modified :  19 march 14
** Modified by	 :  
** Reason		 :  
** Description   :   
*/

global with sharing class OneOrderInvoicing {
	
	Webservice static id CreateInvoice(list<id> accidList,boolean isbatch,list<id>orderlineids,list<string>itmTypeList,list<id>oidList){
        System.debug('#****OneOrderInvoicing.CreateInvoice');
        list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Schedule_Time__c from ChikPeaO2B__O2B_Setting__c limit 1];
        List<id>ReturnInvoiceIds=new List<id>();
        if(O2BsetList!=null && O2BsetList.size()>0)
        {
            if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Anniversary Subscription')
            {
                ReturnInvoiceIds.add(OneOrderAnniversarySubsBilling.generateSubscriptionBill(accidList,orderlineids,itmTypeList,oidList));
                system.debug('###'+ReturnInvoiceIds );
            }
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Anniversary OneBill')
            {
                ReturnInvoiceIds.add(OneOrderAniversaryOneBill.aniversaryOneBillIvoicing(accidList,orderlineids,itmTypeList,oidList));
            }
            /*
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
            {
                ReturnInvoiceIds = CalenderMonthBill.generateCalenderMonthBill(accidList,isbatch);
            }
            */
            else if(O2BsetList[0].ChikPeaO2B__Billing_Type__c=='Calendar Month')
            {
            	ReturnInvoiceIds = CalenderMonthBill.generateCalenderMonthBillOnProcessorder(accidList,orderlineids,itmTypeList,oidList);
            }
        }
        
        if(ReturnInvoiceIds!=null && ReturnInvoiceIds.size()>0)
            return ReturnInvoiceIds[0];//bcoz,As Create bill button hit then only one invoice will be created
        else
            return null;
    }

}