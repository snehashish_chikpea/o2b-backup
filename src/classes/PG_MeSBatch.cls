/**
-----------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-11-17
@see 
-----------------------------------------------------------------------------------------------------
*/
public with sharing class PG_MeSBatch {

    public static final String TRXTYPE_SALE = 'D';//Authorization and capture

    public static final String RES_CODE_EMPTY = 'SF00';

    public static final String TRANSACTION_TRACKING_ID = 'trx_track_id';

    public static final String RES_CODE_OK = '000';
    public static final String RES_CODE_SYSTEM_ERROR = '001';
    public static final String RES_CODE_AUTHENTICATION_ERROR = '002';
    public static final String RES_CODE_INVALID_PARAMETER = '003';
    public static final String RES_CODE_MISSING_PARAMETER = '004';
    public static final String RES_CODE_INVALID_PROFILE_ID = '300';
    public static final String RES_CODE_INVALIS_RESPONSE_FILE_ID = '301';
    public static final String RES_CODE_INVALID_TEST_FLAG = '302';
    public static final String RES_CODE_BATCH_PROCESSING_NOT_ALLOWED = '303';
    
    public static String TEST_UPLOAD_RES = '';
    public static String TEST_DOWNLOAD_RES = '';
    
    PG_Config config = null;
    String uxtimestamp = '';
    String reqFileName = '';
    String resFileName = '';
    String reqFileBody = '';
    String resFileBody = '';
    String reqFileBodyMasked = '';
    String resUpload = '';
    String resDownload = '';
    
    List<Payment__c> payments;

    class BatchUploadResponse{

        String resCode{ get; set;}
        String resMessage{ get; set;}
        String reqFileId{ get; set;}

        BatchUploadResponse(String res){
            reqFileId = '';
            if(res == null || res.length() == 0){
                resCode = PG_MeSBatch.RES_CODE_EMPTY;
                resMessage = 'Empty or no response from server';
                return;
            }
            String[] t1 = res.split('&'), t2, t3, t4;
            t2 = t1[0].split('=');
            resCode = t2[1];
            t3 = t1[1].split('=');
            resMessage = EncodingUtil.urlDecode(t3[1], 'UTF-8');
            if(resCode == PG_MeSBatch.RES_CODE_OK){
                t4 = t1[2].split('=');
                reqFileId = t4[1];
            }
        }
    }

    class TransactionResponse{

        String transactionId{ get; set;}
        String errorCode{ get; set;}
        String authResponseText{ get; set;}
        String avsResult{ get; set;}
        String cvv2Result{ get; set;}
        String authCode{ get; set;}
        String trackingId{ 
            get; 
            set{
                trackingId = value.trim();
            }
        }

    }
    
    public void init(List<sObject> payments, String paymentGateway){
        uxtimestamp = String.valueOf(System.currentTimeMillis());
        //ensure in the caller payments is not empty
        this.payments = payments;
        config = new PG_Config(paymentGateway);
        System.debug('---'+config);
    }

    public PG_Config getConfig(){
        return config; 
    }
    
    public String uploadFile(String uploadPath){//@throws PG_Exception
        String expiryMonth = '', expiryYear = '', cardExpDate = '', trx, trxMasked;
        //Generate Batch request file body by iterating through each payment
        for(Payment__c payment : payments){
            try{
                System.debug('---p='+payment);
                trx = '';
                trxMasked = '';
                //generate upload file
                expiryMonth = payment.Credit_Card__r.Expiry_Month__c < 10?'0':'';
                expiryMonth += String.valueOf(payment.Credit_Card__r.Expiry_Month__c);
                expiryYear = String.valueOf(Math.mod(Integer.valueOf(payment.Credit_Card__r.Expiry_Year__c), 100));
                cardExpDate = expiryMonth + expiryYear;

                trx += 'transaction_type='+TRXTYPE_SALE+'&';
                trx += 'card_exp_date='+cardExpDate+'&';
                if(config.addressVerification){
                    trx += 'cardholder_zip='+payment.Credit_Card__r.CC_Postal_Code__c+'&';
                    if(!config.addressVerificationZipOnly) 
                        trx += 'cardholder_street_address='
                            +EncodingUtil.urlEncode(payment.Credit_Card__r.CC_Street2__c, 'UTF-8')+'&';                 
                }
                trx += 'transaction_amount='+payment.Payment_Amount__c+'&';
                trx += 'invoice_number='+payment.Invoice__r.Name+'&';
                trx += 'echo_'+TRANSACTION_TRACKING_ID+'='+payment.Id+'&';
                trxMasked = trx;
                trx += 'card_number='+payment.Credit_Card__r.Credit_Card_Number__c+'&';
                trxMasked += 'card_number='+PG.maskWord(payment.Credit_Card__r.Credit_Card_Number__c, '', -4, true)+'&';
                //trx += 'client_reference_number='+payment.Account__r.Name+'&';
                trx += 'client_reference_number='+payment.Merchant_Order_Number__c+'\n';
                trxMasked += 'client_reference_number='+payment.Merchant_Order_Number__c+'\n';
                reqFileBody += trx;
                reqFileBodyMasked += trxMasked;         
                //change included payment status to Submitted
                payment.Status__c = 'Submitted';
            }catch(Exception ex){
                //save excep and continue processing other payments
                payment.Has_Processed__c = false;
                payment.Status__c = 'Failed';
                payment.Last_Payment_Process_Date__c = Date.today();
                payment.Failure_Exception__c = ex.getMessage();
                //payment.Card_Failure__c = payment.Card_Failure__c!=null?payment.Card_Failure__c+1:1;
            }           
        }
        System.debug('---'+reqFileBody);
        //System.debug('---'+reqFileBodyMasked);
        //submit upload file
        String userId = config.loginId;
        String userPass = config.password;
        String profileId = config.merchantReference;
        String testFlag = config.testMode?'Y':'N';
        
        reqFileName = 'MeS_Batch_Request_'+uxtimestamp+'.txt';
        String url = config.testMode?config.gatewayUrlTest:config.gatewayUrlProd;
        url += uploadPath;
        String boundary = 'F9jBDELnfBLAVmLNbnLIYibT5Icp0h3VJ7mkI';
        String contentType = 'multipart/form-data; boundary='+boundary;

        Map<String, String> httpHeader = new Map<String, String>{
            'Content-Type' => contentType,
            'Cache-Control' => 'no-cache',
            'Pragma' => 'no-cache',
            'User-Agent' => 'Java/1.8.0_20',
            'Host' => 'www.merchante-solutions.com',
            'Accept' => 'text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2',
            'Connection' => 'keep-alive'
        };

        String body = '';
        body += '--' + boundary + '\n';
        body += 'Content-Disposition: form-data; name="userId"\n';
        body += '\n';
        body += userId + '\n';
        body += '\n';
        body += '--' + boundary + '\n';
        body += 'Content-Disposition: form-data; name="userPass"\n';
        body += '\n';
        body += userPass + '\n';
        body += '\n';
        body += '--' + boundary + '\n';
        body += 'Content-Disposition: form-data; name="profileId"\n';
        body += '\n';
        body += profileId + '\n';
        body += '\n';
        body += '--' + boundary + '\n';
        body += 'Content-Disposition: form-data; name="testFlag"\n';
        body += '\n';
        body += testFlag + '\n';
        body += '\n';
        body += '--' + boundary + '\n';
        body += 'Content-Disposition: form-data; name="reqFile"; filename="' + reqFileName + '"\n';
        body += 'Content-Type: text/plain; charset=ISO-8859-1\n';
        body += '\n';
        body += reqFileBody;
        //body += '\n';
        body += '--' + boundary + '--\n';
        
        System.debug('---req='+body);

        PG_Caller httpCaller = new PG_Caller(
            httpHeader,
            'POST',
            url,
            body,
            120*1000
        );
        
        resUpload = httpCaller.call();
        //resUpload = 'rspCode=002&rspMessage=Authentication+error';
        //resUpload = 'rspCode=000&rspMessage=OK&reqFileId=1533'; //Successfull submission
        if(Test.isRunningTest()){
            //resUpload = 'rspCode=000&rspMessage=OK&reqFileId=1533';
            resUpload = TEST_UPLOAD_RES;
        }
        System.debug('---res='+resUpload);
        BatchUploadResponse batRes = new BatchUploadResponse(resUpload);
        //save upload file as attachment to gateway object
        Attachment attach = new Attachment();
        attach.Body = Blob.valueOf(reqFileBodyMasked);
        //attach.Body = Blob.valueOf(reqFileBody);
        attach.Name = 'MeSReq-'+uxtimestamp;
        if(batRes.reqFileId != ''){
            attach.Name += '-' + batRes.reqFileId;
        }
        attach.Name += '.txt';
        attach.Description = resUpload; 
        attach.parentId = config.gatewayId;
        insert attach;
        
        String requsetFileId = null;
        if(batRes.resCode == PG_MeSBatch.RES_CODE_OK){//Submisson successfull
            requsetFileId = batRes.reqFileId;
            for(Payment__c payment : payments){
                payment.Status__c = 'In Process';
            }
            update payments;//with status set as In Process
        }else{//Submisson failed
            requsetFileId = null;
            update payments;// With Status set as Submitted
        }

        return requsetFileId;   
    }
    
    public void downloadFile(String downloadPath, String respFileId){
        
        String userId = config.loginId;
        String userPass = config.password;

        String url = config.testMode?config.gatewayUrlTest:config.gatewayUrlProd;
        url += downloadPath;
        
        Map<String, String> httpHeader = new Map<String, String>{
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Cache-Control' => 'no-cache',
            'Pragma' => 'no-cache',
            'User-Agent' => 'Java/1.8.0_20',
            'Host' => 'www.merchante-solutions.com',
            'Accept' => 'text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2',
            'Connection' => 'keep-alive'
        }; 
        
        String body = '';
        body += 'userId='+userId+'&userPass='+userPass+'&respFileId='+respFileId;
        
        
        PG_Caller httpCaller = new PG_Caller(
            httpHeader,
            'POST',
            url,
            body,
            120*1000
        );
        
        resDownload = httpCaller.call();
        if(Test.isRunningTest()){
            resDownload = '';
            //resDownload += 'transaction_id=a8c20e814c5734e0ba078dc105c4623d&error_code=000&auth_response_text=Exact Match&avs_result=Y&cvv2_result=M&auth_code=T3342H\n';
            resDownload += TEST_DOWNLOAD_RES;
        }

        /*resDownload = '';
        resDownload += 'transaction_id=7b616e0c6de036318747463969841e03&error_code=000&auth_response_text=Exact Match&avs_result=Y&cvv2_result=M&auth_code=T95361&eresp_trx_track_id=a0Ki000000BTvmDEAT\n';
        resDownload += 'transaction_id=bc482a315a7f355ca42baca8086d36f9&error_code=000&auth_response_text=Exact Match&avs_result=Y&cvv2_result=M&auth_code=T3341H&eresp_trx_track_id=a0Ki000000BznZnEAJ\n';*/
        
        System.debug('---resDownload='+resDownload);
        
        //save downloaded file as attachment to gateway object
        Attachment attach = new Attachment();
        attach.Body = Blob.valueOf(resDownload);
        attach.Name = 'MeSRes-' + uxtimestamp + '-' + respFileId + '.txt';
        attach.Description = 'Response File'; 
        attach.parentId = config.gatewayId;
        insert attach;

        //Update payments (Status, TrxId, Auth Code) based on result
        List<TransactionResponse> transactions = PG_MeSBatch.populateTransactionResponse(resDownload);
        Set<Id> paymentIds = new Set<Id>();
        for(TransactionResponse trx : transactions){
            paymentIds.add(trx.trackingId);
        }
        Map<Id, Payment__c> payments = new Map<Id, Payment__c>([Select c.Name, c.Id, c.Trx_Message__c, 
            c.Transaction_Id__c, c.Status__c, c.Failure_Exception__c, c.Comment__c, c.Card_Failure__c 
            From Payment__c c
            Where c.Id In :paymentIds ]);
        if(payments != null && !payments.isEmpty()){
            System.debug('---payments='+payments);
            Payment__c payment;
            for(TransactionResponse trx : transactions){
                payment = payments.get(trx.trackingId);
                payment.Transaction_Id__c = trx.transactionId;
                payment.Trx_Message__c = trx.authResponseText;
                payment.Last_Payment_Process_Date__c = Date.today();
                //Approved
                if(trx.errorCode == RES_CODE_OK){
                    payment.Has_Processed__c = true;
                    payment.Status__c = 'Processed';
                }else{//Declined or failed
                    payment.Has_Processed__c = false;
                    payment.Status__c = 'Failed';
                    payment.Card_Failure__c = payment.Card_Failure__c != null?(payment.Card_Failure__c + 1):1;
                }               
            }
            update payments.values();
        }
        System.debug('---updated payments='+payments);

    }

    //TODO: process by iterating to avoid Heap limit
    public static List<TransactionResponse> populateTransactionResponse(String resDownload){
        String[] lines = resDownload.split('\n');
        String[] pairs;
        String[] words;
        List<TransactionResponse> trList = new List<TransactionResponse>();
        for(String line : lines){
            TransactionResponse tr = new TransactionResponse();
            pairs = line.split('&');
            for(String pair : pairs){
                words = pair.split('=');
                if(words[0] == 'transaction_id')
                    tr.transactionId = words[1];
                else if(words[0] == 'error_code')
                    tr.errorCode = words[1];
                else if(words[0] == 'auth_response_text')
                    tr.authResponseText = words[1];
                else if(words[0] == 'avs_result')
                    tr.avsResult = words[1];
                else if(words[0] == 'cvv2_result')
                    tr.cvv2Result = words[1];
                else if(words[0] == 'auth_code')
                    tr.authCode = words[1];
                else if(words[0] == 'eresp_trx_track_id')
                    tr.trackingId = words[1];               
            }
            trList.add(tr);
        }
        return trList;
    }

    /*
    PG_MeSBatch.tryItUpload();
    */
    /*
    public static void tryItUpload(){

        String paymentGateway = 'MeS:Batch';
        String uploadPath = '/srv/api/bpUpload';
        List<Payment__c> payments = [Select Id,
            Payment_Gateway__c, Payment_Amount__c, Merchant_Order_Number__c, Invoice__r.Name, 
            Invoice__c, Credit_Card__r.CC_Street1__c, Credit_Card__r.CC_State__c, 
            Credit_Card__r.CC_Last_Name__c, Credit_Card__r.Expiry_Year__c, 
            Credit_Card__r.Expiry_Month__c, Credit_Card__r.Credit_Card_Type__c, 
            Credit_Card__r.Credit_Card_Number__c, Credit_Card__r.CC_Street2__c, Credit_Card__r.CC_Postal_Code__c, 
            Credit_Card__r.CC_First_Name__c, Credit_Card__r.CC_Country__c, 
            Credit_Card__r.CC_City__c, Credit_Card__c, Account__r.Name, Account__c 
            From Payment__c c
            Where Payment_Gateway__c = :paymentGateway
            and (Invoice__r.Invoice_Status__c = 'Open' or Invoice__r.Invoice_Status__c = 'Paid Partial') 
            and Has_Processed__c = false 
            and (Status__c = 'Not Processed' or Status__c = 'Failed')
            //and Payment_Date__c <= TODAY 
            //and (Payment_Method__c = 'Credit Card' and (Card_Failure__c < 2 or Card_Failure__c = null))
        ]; 
        PG_MeSBatch mesBat = new PG_MeSBatch();
        mesBat.init(payments, paymentGateway);
        String reqFileId = mesBat.uploadFile(uploadPath);
        System.debug('---reqFileId='+reqFileId);
    } 
    */
    /*
    PG_MeSBatch.tryItDownload();
    */
    /*
    public static void tryItDownload(){
        String paymentGateway = 'MeS:Batch';
        String downloadPath = '/srv/api/bpDownload';
        List<Payment__c> payments = null; //populate
        String resFileId = '1572';

        PG_MeSBatch mesBat = new PG_MeSBatch();
        mesBat.init(payments, paymentGateway);
        mesBat.downloadFile(downloadPath, resFileId);
    }
    */
}