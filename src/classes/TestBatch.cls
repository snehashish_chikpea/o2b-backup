@isTest
public class TestBatch
{
    @isTest
    static void testBatchCreateBillRunScheduler()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Schedule_Time__c='03:30',ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Is_Parent_Billing_Enabled__c=true);
        insert(o2bset);
        BatchCreateBillRunScheduler.executeschedule();
    }
    @isTest
    static void testBatchCreateBillRunScheduler1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Is_Parent_Billing_Enabled__c=true);
        insert(o2bset);
        BatchCreateBillRunScheduler.executeschedule();
    }
    @isTest
    static void testmethod3()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(/*ChikPeaO2B__Billing_Type__c='Anniversary Subscription',*/ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Is_Parent_Billing_Enabled__c=true);
        insert(o2bset);
        Account acc=new Account(name='acc1',ChikPeaO2B__Active__c='Yes');
        insert(acc);
        BatchCreateBillScheduler schd=new BatchCreateBillScheduler();
        BatchCreateBill BCB = new BatchCreateBill();
        BCB.email='o2b@chikpea.com';
        BCB.query='select id,name from Account where ChikPeaO2B__Active__c=\'Yes\'';
        Database.executeBatch(bcb,50);
    }
    @isTest
    static void testBatchCreateBillRunSchedulerForBillGroup()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Is_Parent_Billing_Enabled__c=true);
        insert(o2bset);
        ChikPeaO2B__Bill_Group__c billGrp= new ChikPeaO2B__Bill_Group__c();
        billGrp.ChikPeaO2B__Billing_Day__c='6';
        billGrp.ChikPeaO2B__Billing_Type__c='Calendar Month';
        billGrp.ChikPeaO2B__O2B_Setting__c=o2bset.Id;
        insert billGrp;
        BatchCreateBillRunSchedulerForBillGroup.executeschedule(billGrp.id);
    }
    @isTest
    static void testBatchCreateBillRunSchedulerForBillGroup1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Is_Parent_Billing_Enabled__c=true);
        insert(o2bset);
        ChikPeaO2B__Bill_Group__c billGrp= new ChikPeaO2B__Bill_Group__c();
        billGrp.ChikPeaO2B__Billing_Day__c='6';
        billGrp.ChikPeaO2B__Schedule_Time__c='6:06';
        billGrp.ChikPeaO2B__Billing_Type__c='Anniversary Subscription';
        billGrp.ChikPeaO2B__O2B_Setting__c=o2bset.Id;
        insert billGrp;
        BatchCreateBillRunSchedulerForBillGroup.executeschedule(billGrp.id);
    }
}