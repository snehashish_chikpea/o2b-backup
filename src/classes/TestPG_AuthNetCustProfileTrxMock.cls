/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2015-08-11
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
global class TestPG_AuthNetCustProfileTrxMock implements WebServiceMock {
    
    public String resultCode {  //Ok, Error 
        get {
            return (resultCode == null)?'Ok':resultCode;
        } 
        set;
    }
    public String code {  // I00001, E00014 
        get {
            return ( code == null)?'I00001':code;
        }
        set; 
    }
    public String text { // Successful., Transaction ID is required. 
        get {
            return ( text == null)?'Successful.':text;
        } 
        set; 
    } 
    public String directResponse { 
        get {
            return ( directResponse == null)?'1,1,1,This transaction has been approved.,4GWD6X,Y,2237225176,,,1.99,CC,auth_only,o2b001,Bhaskar,Roy,ChikPea Inc,One Market St,San Francisco,CA,94105,USA,4152616050,,bhaskar.roy@chikpea.com,,,,,,,,,,,,,,07BB530AA92B4979A398EE6ADAAE7D13,,2,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,':directResponse;
        }
        set; 
    }
    /*
    Auth---
    1,1,1,This transaction has been approved.,4GWD6X,Y,2237225176,,,1.99,CC,auth_only,o2b001,Bhaskar,Roy,ChikPea Inc,One Market St,San Francisco,CA,94105,USA,4152616050,,bhaskar.roy@chikpea.com,,,,,,,,,,,,,,07BB530AA92B4979A398EE6ADAAE7D13,,2,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,
    Auth Capture Success---
    1,1,1,This transaction has been approved.,Y3ARCO,Y,2237226396,,,1.89,CC,auth_capture,o2b001,Bhaskar,Roy,ChikPea Inc,One Market St,San Francisco,CA,94105,USA,4152616050,,bhaskar.roy@chikpea.com,,,,,,,,,,,,,,179D052E39B5D14A57880A29D4AC7949,,2,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,
    Capture Only---
    1,1,1,This transaction has been approved.,4GWD6Y,P,2237271512,,,1.09,CC,capture_only,o2b001,Bhaskar,Roy,ChikPea Inc,One Market St,San Francisco,CA,94105,USA,4152616050,,bhaskar.roy@chikpea.com,,,,,,,,,,,,,,6D55502B0D0572EB6A05738BBD31972F,,,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,
    Prior Auth Capture---
    1,1,1,This transaction has been approved.,4GWD6X,P,2237225176,,,1.00,CC,prior_auth_capture,o2b001,,,,,,,94105,,,,,,,,,,,,,,,,,,739726263E1DD7D3E86A1CFCCE394D89,,,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,
    Void---
    1,1,1,This transaction has been approved.,969IGH,P,2237338612,,,0.00,CC,void,o2b001,,,,,,,94105,,,,,,,,,,,,,,,,,,173917C70FFC2A4E0D50723520BE595B,,,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,
    Credit---
    1,1,1,This transaction has been approved.,,P,2237361601,,,1.89,CC,credit,o2b001,Bhaskar,Roy,ChikPea Inc,One Market St,San Francisco,CA,94105,USA,4152616050,,bhaskar.roy@chikpea.com,,,,,,,,,,,,,,C820463A5F200859C04F161BEDB0257D,,,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,
    */
    global void doInvoke(Object stub,
                     Object request,
                     Map<String, Object> response,
                     String endpoint,
                     String soapAction,
                     String requestName,
                     String responseNS,
                     String responseName,
                     String responseType
    ){
        PG_AuthNetSOAPService.MessagesTypeMessage messagesTypeMessage = new PG_AuthNetSOAPService.MessagesTypeMessage();
        messagesTypeMessage.code = code;
        messagesTypeMessage.text = text;
        //
        PG_AuthNetSOAPService.ArrayOfMessagesTypeMessage messages = new PG_AuthNetSOAPService.ArrayOfMessagesTypeMessage();
        messages.MessagesTypeMessage = new PG_AuthNetSOAPService.MessagesTypeMessage[]{messagesTypeMessage};
        //
        PG_AuthNetSOAPService.CreateCustomerProfileTransactionResponseType createCustomerProfileTransactionResult
            = new PG_AuthNetSOAPService.CreateCustomerProfileTransactionResponseType();
        createCustomerProfileTransactionResult.resultCode = resultCode;
        createCustomerProfileTransactionResult.directResponse = directResponse;
        createCustomerProfileTransactionResult.messages = messages;
        //
        PG_AuthNetSOAPService.CreateCustomerProfileTransactionResponse_element createCustomerProfileTransactionResponse_element 
            = new PG_AuthNetSOAPService.CreateCustomerProfileTransactionResponse_element();
        createCustomerProfileTransactionResponse_element.CreateCustomerProfileTransactionResult
            = createCustomerProfileTransactionResult;   
        response.put('response_x', createCustomerProfileTransactionResponse_element);
    }
}