@isTest
private class TestForBulkDataClasses {
	
    static testMethod void myUnitTest() {
        list<Account> accList=new list<Account>{new Account(Name='Asitm9')};
        LargeVolumeDML.bulkOperationHelper(accList, 1);
    }
    
    
    
    static testMethod void myUnitTest1() {
    	ChikPeaO2B__O2B_Setting__c ob= new ChikPeaO2B__O2B_Setting__c();
    	insert ob;
    	ChikPeaO2B__Bill_Group__c bg=new ChikPeaO2B__Bill_Group__c(
    	ChikPeaO2B__Schedule_Time__c='0:00',ChikPeaO2B__Billing_Type__c='Calendar Month',
    	ChikPeaO2B__Billing_Day__c='6',ChikPeaO2B__Maximum_Records__c=9,ChikPeaO2B__Bulk_Data__c=true,
    	ChikPeaO2B__O2B_Setting__c=ob.Id
    	);
    	insert bg;
    	LargeVolumeBillingRunScheduler.executeschedule(bg.Id);
    }
}