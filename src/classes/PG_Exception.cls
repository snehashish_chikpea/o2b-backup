/**
----------------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-23
@see 
----------------------------------------------------------------------------------------------------------
*/
global with sharing class PG_Exception extends Exception {
    
    private String errorCode = 'SF001';
    private String errorType = 'GatewayException';//SOAPFault
    private String errorMesssage = 'An error occured';
    //private String reqLog = '';
    //private String resLog = '';
    
    global String getErrorCode(){
        return errorCode;
    }
    
    global void setErrorCode(String errorCode){
        this.errorCode = errorCode;
    }
    
    global String getErrorType(){
        return errorType;
    }
    
    global void setErrorType(String errorType){
        this.errorType = errorType;
    }
    
    global String getErrorMesssage(){
        return errorMesssage;
    }
    
    global void setErrorMesssage(String errorMesssage){
        this.errorMesssage = errorMesssage;
    }
    /*
    global String getReqLog(){
        return reqLog;
    }
    
    global void setReqLog(String reqLog){
        this.reqLog = reqLog;
    }
    
    global String getResLog(){
        return resLog;
    }
    
    global void setResLog(String reqLog){
        this.resLog = resLog;
    }
    */
}