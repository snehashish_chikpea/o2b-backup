global with sharing class BatchCreateBillScheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        string errmsg;
        BatchCreateBill BCB = new BatchCreateBill();
        List<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, 
        ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Bill_Schedule_Job_Id__c 
        from ChikPeaO2B__O2B_Setting__c limit 1];
        BCB.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
        BCB.query='select id,name from Account where ChikPeaO2B__Active__c=\'Yes\' and ChikPeaO2B__Is_Billing__c=true';
        system.debug('------------------->billGroupId'+BatchCreateBillRunSchedulerForBillGroup.billGroupId);
        string billGroupId='';
        /*
        list<ChikPeaO2B__Bill_Group__c> billGrpLst=[SELECT id FROM ChikPeaO2B__Bill_Group__c WHERE ChikPeaO2B__isExecuting__c=true];
        if(!billGrpLst.isEmpty()){
            billGroupId=billGrpLst[0].Id;
            billGrpLst[0].ChikPeaO2B__isExecuting__c=false;
            update billGrpLst;
        }
        if(billGroupId!=null
        && billGroupId!=''){
            BCB.query+=' AND ChikPeaO2B__Bill_Group__c=\''+billGroupId +'\'';
        }
        
        
        
        
        */
        CronTrigger ct=[SELECT Id,CronJobDetail.Name FROM CronTrigger WHERE Id=:sc.getTriggerId()];
        string grpName=ct.CronJobDetail.Name.remove('Create Invoice Batch ');
        grpName=grpName.trim();
        list<ChikPeaO2B__Bill_Group__c> billGrpLst=[SELECT id,ChikPeaO2B__Bill_Schedule_Job_Id__c,ChikPeaO2B__Batch_Size__c FROM ChikPeaO2B__Bill_Group__c WHERE Name=:grpName];
        if(!billGrpLst.isEmpty()){
            billGroupId=billGrpLst[0].Id;
        }
        
        if(billGroupId!=null
        && billGroupId!=''){
            BCB.query+=' AND ChikPeaO2B__Bill_Group__c=\''+billGroupId +'\'';
        }
        
        system.debug('----------------->'+BCB.query);
        //BatchCreateBillRunSchedulerForBillGroup.billGroupId=null;
        try{
            ID batchprocessid;
            if(billGroupId!=null && billGroupId!='')//schedule from bill group
                batchprocessid = Database.executeBatch(BCB,integer.valueof((billGrpLst!=null && billGrpLst.size()>0 && billGrpLst[0].ChikPeaO2B__Batch_Size__c!=null)?billGrpLst[0].ChikPeaO2B__Batch_Size__c:'1'));
            else//schedule from o2bsettings 
                batchprocessid = Database.executeBatch(BCB,integer.valueof((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Batch_Size__c!=null)?o2bset[0].ChikPeaO2B__Batch_Size__c:'50'));
            if(billGroupId!=null
            && billGroupId!=''){
                billGrpLst[0].ChikPeaO2B__Bill_Schedule_Job_Id__c=batchprocessid;
                update billGrpLst;
            }
            else{
                o2bset[0].ChikPeaO2B__Bill_Schedule_Job_Id__c=batchprocessid;
                update o2bset;
            }
        }catch(Exception e){
            //throw new BatchException('Some error occured in BatchCreateBillScheduler Class.',e);
            errmsg+=e.getmessage();
            errmsg+=e.getCause();
            errmsg+=e.getLineNumber();
            errmsg+=e.getStackTraceString();
            ChikPeaO2B__BatchLog__c log= new ChikPeaO2B__BatchLog__c();
            log.Name='BatchCreateBillScheduler';
            log.ChikPeaO2B__Error_Log__c=errmsg;
            //=== CURD check [START] ===//
            Map<string,string>BatchLogInsertResultMap=new map<string,string>();
            BatchLogInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__BatchLog__c',FieldPermissionSet.BatchLogInsert,'insert');
            if(BatchLogInsertResultMap.get('AllowDML')!=null && BatchLogInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                insert log;
            }
            //=== CURD check [END] ===//
            system.debug('---------->err1'+errmsg);
            system.debug('---------->err2'+log);
            
        }  
    }
}