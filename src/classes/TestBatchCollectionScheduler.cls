@Istest
public class TestBatchCollectionScheduler{
    @isTest
    static void testMethod1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Save_Failed_Payment__c=true,ChikPeaO2B__Credit_Note_Default_Validity__c=2,ChikPeaO2B__Card_Process_Frequency__c=3,ChikPeaO2B__Card_Process_Interval_days__c=10);
        insert(o2bset);
        List<ChikPeaO2B__Dunning_Rule__c> DunList = new List<ChikPeaO2B__Dunning_Rule__c>();
        ChikPeaO2B__Dunning_Rule__c dun1 = new ChikPeaO2B__Dunning_Rule__c(ChikPeaO2B__Account_Due__c=12,ChikPeaO2B__Invoice_Due__c=10,ChikPeaO2B__O2B_Setting__c=o2bset.id,ChikPeaO2B__Past_Days__c=5);
        DunList.add(dun1);
        ChikPeaO2B__Dunning_Rule__c dun2 = new ChikPeaO2B__Dunning_Rule__c(ChikPeaO2B__Account_Due__c=12,ChikPeaO2B__Invoice_Due__c=10,ChikPeaO2B__O2B_Setting__c=o2bset.id,ChikPeaO2B__Past_Days__c=10);
        DunList.add(dun2);
        insert DunList;
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly',ChikPeaO2B__Auto_Payment__c='Yes');
        insert(acc);
        ChikPeaO2B__Credit_Card__c cc=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st1',ChikPeaO2B__CC_City__c='city1',ChikPeaO2B__CC_Country__c='country1',ChikPeaO2B__CC_First_Name__c='FN1',ChikPeaO2B__CC_Last_Name__c='LN1',ChikPeaO2B__CC_Postal_Code__c='700076',ChikPeaO2B__CC_State__c='wb1',ChikPeaO2B__CC_Street2__c='strt2',ChikPeaO2B__Credit_Card_Number__c='0004',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=02,ChikPeaO2B__Expiry_Year__c=2016);
        insert(cc);
        acc.ChikPeao2b__credit_card__c=cc.id;
        update(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        list<item__c>itemlist=new List<Item__c>();
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        List<ChikPeaO2B__Advance_payment__c> advpaylist=new List<ChikPeaO2B__Advance_payment__c>();
        ChikPeaO2B__Advance_payment__c advp=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=4,ChikPeaO2B__Used_Amount__c=0,ChikPeaO2B__Payment_Date__c=date.today());
        advpaylist.add(advp);
        ChikPeaO2B__Advance_payment__c advp1=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=2.5,ChikPeaO2B__Used_Amount__c=2.5,ChikPeaO2B__Payment_Date__c=date.today());
        advpaylist.add(advp1);
        insert(advpaylist);
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        List<ChikPeaO2B__Subscription__c>SubList=new List<ChikPeaO2B__Subscription__c>();
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1);
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-5),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub2);
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(15),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub3);
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(45),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub4);
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub5);
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(5),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub6);
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-3),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub7);
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(70),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub8);
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9);
        insert(SubList);
        List<id>AccList=new List<id>();
        AccList.add(acc.id);
        List<id>retinvid=AnniversaryOneBill.generateAnniversaryOneBill(AccList,false);
        List<ChikPeaO2B__Invoice__c>invlist=new List<ChikPeaO2B__Invoice__c>();
        for(ChikPeaO2B__Invoice__c inv:[Select id, Name, ChikPeaO2B__Bill_Date__c, ChikPeaO2B__Due_Date__c From ChikPeaO2B__Invoice__c Where Id in:retinvid])
        {
            inv.ChikPeaO2B__Bill_Date__c = date.today().adddays(-5);
            //inv.ChikPeaO2B__Payment_Term__c='Net10';
            invlist.add(inv);
        }
        update(invlist);
        for(ChikPeaO2B__Invoice__c inv:invlist)
        {
            system.debug('@@@==>'+inv.ChikPeaO2B__Payment_Term__c+'@@==>'+inv.ChikPeaO2B__Bill_Date__c+'@@==>'+inv.ChikPeaO2B__Due_Date__c+'@@==>'+inv.ChikPeaO2B__Invoice_Status__c+'@@@==>'+inv.ChikPeaO2B__Past_Days__c+'@@@'+date.today());
        }
        Test.startTest();
            BatchCollectionRunScheduler.executeschedule();
        Test.stopTest();
    }
    @isTest
    static void testMethod2()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Save_Failed_Payment__c=true,ChikPeaO2B__Credit_Note_Default_Validity__c=2,ChikPeaO2B__Card_Process_Frequency__c=3,ChikPeaO2B__Card_Process_Interval_days__c=10);
        insert(o2bset);
        List<ChikPeaO2B__Dunning_Rule__c> DunList = new List<ChikPeaO2B__Dunning_Rule__c>();
        ChikPeaO2B__Dunning_Rule__c dun1 = new ChikPeaO2B__Dunning_Rule__c(ChikPeaO2B__Account_Due__c=12,ChikPeaO2B__Invoice_Due__c=10,ChikPeaO2B__O2B_Setting__c=o2bset.id,ChikPeaO2B__Past_Days__c=5);
        DunList.add(dun1);
        ChikPeaO2B__Dunning_Rule__c dun2 = new ChikPeaO2B__Dunning_Rule__c(ChikPeaO2B__Account_Due__c=12,ChikPeaO2B__Invoice_Due__c=10,ChikPeaO2B__O2B_Setting__c=o2bset.id,ChikPeaO2B__Past_Days__c=10);
        DunList.add(dun2);
        insert DunList;
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly',ChikPeaO2B__Auto_Payment__c='Yes');
        insert(acc);
        ChikPeaO2B__Credit_Card__c cc=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st1',ChikPeaO2B__CC_City__c='city1',ChikPeaO2B__CC_Country__c='country1',ChikPeaO2B__CC_First_Name__c='FN1',ChikPeaO2B__CC_Last_Name__c='LN1',ChikPeaO2B__CC_Postal_Code__c='700076',ChikPeaO2B__CC_State__c='wb1',ChikPeaO2B__CC_Street2__c='strt2',ChikPeaO2B__Credit_Card_Number__c='0004',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=02,ChikPeaO2B__Expiry_Year__c=2016);
        insert(cc);
        acc.ChikPeao2b__credit_card__c=cc.id;
        update(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        list<item__c>itemlist=new List<Item__c>();
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        List<ChikPeaO2B__Advance_payment__c> advpaylist=new List<ChikPeaO2B__Advance_payment__c>();
        ChikPeaO2B__Advance_payment__c advp=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=4,ChikPeaO2B__Used_Amount__c=0,ChikPeaO2B__Payment_Date__c=date.today());
        advpaylist.add(advp);
        ChikPeaO2B__Advance_payment__c advp1=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=2.5,ChikPeaO2B__Used_Amount__c=2.5,ChikPeaO2B__Payment_Date__c=date.today());
        advpaylist.add(advp1);
        insert(advpaylist);
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        List<ChikPeaO2B__Subscription__c>SubList=new List<ChikPeaO2B__Subscription__c>();
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1);
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-5),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub2);
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(15),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub3);
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(45),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub4);
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub5);
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(5),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub6);
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-3),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub7);
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(70),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub8);
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9);
        insert(SubList);
        List<id>AccList=new List<id>();
        AccList.add(acc.id);
        List<id>retinvid=AnniversaryOneBill.generateAnniversaryOneBill(AccList,false);
        List<ChikPeaO2B__Invoice__c>invlist=new List<ChikPeaO2B__Invoice__c>();
        for(ChikPeaO2B__Invoice__c inv:[Select id, Name, ChikPeaO2B__Bill_Date__c, ChikPeaO2B__Due_Date__c From ChikPeaO2B__Invoice__c Where Id in:retinvid])
        {
              inv.ChikPeaO2B__Bill_Date__c = date.today().adddays(-5);
              //inv.ChikPeaO2B__Payment_Term__c = 'Net10';
            invlist.add(inv);
        }
        update(invlist);
        for(ChikPeaO2B__Invoice__c inv:invlist)
        {
            system.debug('@@@==>'+inv.ChikPeaO2B__Payment_Term__c+'@@==>'+inv.ChikPeaO2B__Bill_Date__c+'@@==>'+inv.ChikPeaO2B__Due_Date__c+'@@==>'+inv.ChikPeaO2B__Invoice_Status__c+'@@@==>'+inv.ChikPeaO2B__Past_Days__c+'@@@'+date.today());
        }
        Test.startTest();
            BatchCollection BC1 = new BatchCollection();
            BC1.email=((o2bset!=null && o2bset.ChikPeaO2B__Admin_Email__c!=null)?o2bset.ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
            BC1.query='select id,name,ChikPeaO2B__Collection_Email_Date__c,ChikPeaO2B__Past_Days__c'+
                       ' from ChikPeaO2B__Invoice__c where ';
            string wherecondition='';
            for(ChikPeaO2B__Dunning_Rule__c  dun: DunList)           
            {
                wherecondition+=('ChikPeaO2B__Past_Days__c='+dun.ChikPeaO2B__Past_Days__c+' or ');
            }
            wherecondition=wherecondition.trim().removeendignorecase('or').trim();
            BC1.query+=wherecondition;
            ID batchprocessid = Database.executeBatch(BC1);  
        Test.stopTest();
    }
}