@isTest
public with sharing class TestSubscriptionHistory {	 
    @isTest
    static void unitTest1(){
		Account acc=new Account(name='acc1');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        List<ChikPeaO2B__Item__c> item_list=new List<ChikPeaO2B__Item__c>();
        ChikPeaO2B__Item__c item1=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');
        item_list.add(item1);
        ChikPeaO2B__Item__c item2=new ChikPeaO2B__Item__c(name='Sample',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        item_list.add(item2);
        insert item_list;
        
        List<ChikPeaO2B__Rate_Plan__c>RPLIst=new List<ChikPeaO2B__Rate_Plan__c> ();
        ChikPeaO2B__Rate_Plan__c rp1_item1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=item1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPLIst.add(rp1_item1);
        
        ChikPeaO2B__Rate_Plan__c rp2_item2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Recurring_Charge__c=2,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPLIst.add(rp2_item2);        
        insert RPLIst;
        
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id);
        insert(ord);
        List<ChikPeaO2B__Order_Line__c> orl_list=new List<ChikPeaO2B__Order_Line__c>();
        
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item1.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1_item1.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl1);
        
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl2);
        
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl3);
        
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl4);
        
        ChikPeaO2B__Order_Line__c orl5=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl5);
        
        ChikPeaO2B__Order_Line__c orl6=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl6);
        
        ChikPeaO2B__Order_Line__c orl7=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl7);
        
        ChikPeaO2B__Order_Line__c orl8=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl8);
        
        ChikPeaO2B__Order_Line__c orl9=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl9);
        
        ChikPeaO2B__Order_Line__c orl10=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=item2.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2_item2.id,ChikPeaO2B__Unit_Price__c=2.5);
        orl_list.add(orl10);
        
        insert orl_list;
        
        
        
        List<ChikPeaO2B__Subscription__c>subList=new List<ChikPeaO2B__Subscription__c>();
        
        ChikPeaO2B__Subscription__c sub10=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub10.ChikPeaO2B__Order_Line__c=orl10.id;
        sub10.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub10.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub10);
        
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub9.ChikPeaO2B__Order_Line__c=orl9.id;
        sub9.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub9.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub9);
        
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub8.ChikPeaO2B__Order_Line__c=orl8.id;
        sub8.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub8.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub8);
        
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub7.ChikPeaO2B__Order_Line__c=orl7.id;
        sub7.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub7.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub7);
        
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub6.ChikPeaO2B__Order_Line__c=orl6.id;
        sub6.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub6.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub6);
        
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub5.ChikPeaO2B__Order_Line__c=orl5.id;
        sub5.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub5.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub5);
        
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub4.ChikPeaO2B__Order_Line__c=orl4.id;
        sub4.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub4.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub4);
        
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),
        ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(30),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub3.ChikPeaO2B__Order_Line__c=orl3.id;
        sub3.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub3.ChikPeaO2B__Item__c=item2.id;
        subList.add(sub3);
        
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id, ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-40),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today().adddays(-10),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Recurring_Charge__c=1);
        sub2.ChikPeaO2B__Order_Line__c=orl2.id;
        sub2.ChikPeaO2B__Rate_Plan__c=rp2_item2.id;
        sub2.ChikPeaO2B__Item__c=item2.id;
        //insert sub2;
        subList.add(sub2);
        
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=3,ChikPeaO2B__Recurring_Charge__c=2);
        sub1.ChikPeaO2B__Order_Line__c=orl1.id;
        sub1.ChikPeaO2B__Rate_Plan__c=rp1_item1.id;
        
        sub1.ChikPeaO2B__Item__c=item1.id;
        //insert sub1;
        subList.add(sub1);
      
        insert(subList);
        orl9.ChikPeaO2B__old_Subscription__c=sub10.Id;
        orl8.ChikPeaO2B__old_Subscription__c=sub9.Id; 
        orl7.ChikPeaO2B__old_Subscription__c=sub8.Id; 
        orl6.ChikPeaO2B__old_Subscription__c=sub7.Id; 
        orl5.ChikPeaO2B__old_Subscription__c=sub6.Id;  
        orl4.ChikPeaO2B__old_Subscription__c=sub5.Id;
        orl3.ChikPeaO2B__old_Subscription__c=sub4.Id;  
        orl2.ChikPeaO2B__old_Subscription__c=sub3.Id;     
        orl1.ChikPeaO2B__old_Subscription__c=sub2.Id;
        update orl_list;
        //System.debug();
		Apexpages.Standardcontroller stdc1 = new Apexpages.Standardcontroller(sub1);
        SubscriptionHistory SCE1=new SubscriptionHistory(stdc1);	
    }
}