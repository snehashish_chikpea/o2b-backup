/*
** Class         :  PaymentStgUpload
** Created by    :  Mehebub Hossain (chikpea Inc.)
** Description   :  
** Created On    :  9-Dec-14
*/
public with sharing class PaymentStgUpload {
    public String t_data{get;set;}
    public String t_head{get;set;}
    public List<xRow> xRowList{get;set;}
    public Map<String, xRow> xRowMap{get;set;}
    public Integer no_row{get;set;}
    public Integer no_col{get;set;}
    public String json_data{get;set;} 
    public PaymentStgUpload(){
        no_row=0;
        no_col=6;
        xRowMap=new Map<String,xRow>();
        xRowList=new List<xRow>();
        //queryData();
    }
    
    
    /*
    private void queryData(){
        xRowList=new List<xRow>();
        xRowMap=new Map<String,xRow>();
        List<ChikPeaO2B__Payment_Staging__c> payment_stgs=[select Id, Name,ChikPeaO2B__Invoice_Ref__c,
        ChikPeaO2B__Account_Reference__c, 
        ChikPeaO2B__Status__c,
            ChikPeaO2B__Amount__c,ChikPeaO2B__Used_Amount__c from ChikPeaO2B__Payment_Staging__c order by Name];
        
        no_row=payment_stgs.size();
        creatXRow(payment_stgs);
        
        
        creatXRowArray();
        
        t_data='['+
                    '[\"a1\", \"b1\", \"c1\", \"d1\"],'+
                    '[1, 2, 3, 4],'+
                    '[1.1, 1.2, 1.3, 1.4],'+
                    '[true, false, true, false]'+
                ']';
        
        t_data='[]';
        json_data= JSON.serializePretty(xRowList);
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,t_data));
    }*/
    
    public Pagereference submit(){
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'#****46'+t_data));
        
        //JSONParser parser1 = JSON.createParser(t_data);
        //Integer counter=0;
        
        String[] t_head_s=t_head.substring(1,t_head.length()-1).replaceAll('\"','').split(',');
        
        
        
        Map<String, Integer> header_map=new Map<String, Integer>();
        
        for(Integer count_i=0;count_i<t_head_s.size();count_i++){
            header_map.put(t_head_s[count_i].trim().toUpperCase(),count_i);
            system.debug(t_head_s[count_i].trim().toUpperCase()+'===DJ-->'+count_i);
        }
        
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'header_map='+header_map));
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'t_data='+t_data));

        System.debug('#****header_map='+header_map);
        if(dataValidation(t_data)&&headerValidation(header_map)){
            List<List<String>> t_data_arr = fromString(t_data);
        
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'t_data_arr='+t_data_arr));
            
            for(String[] t_data_d_d:t_data_arr){
                for(Integer count_j=0;count_j<t_data_d_d.size();count_j++){
                if(t_data_d_d.size()>0){
                    String payment_stg_id=t_data_d_d[0];
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'t_data_d_d[4]='+t_data_d_d[4]));
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'header_map='+header_map));
                    xRow xrow_m;
                    if(payment_stg_id!=null&&payment_stg_id!=''){
                            if(xRowMap.containsKey(payment_stg_id)){
                                //Existing Payment Stg
                                xrow_m=xRowMap.get(payment_stg_id);                 
                            }else{
                                //New Existing Payment Stg
                                xrow_m=new xRow(new ChikPeaO2B__Payment_Staging__c(ChikPeaO2B__Status__c='New'));
                                xRowList.add(xrow_m);   
                                xRowMap.put(payment_stg_id, xrow_m);                
                                                
                            }
                            
                            //t1-3-0, Inv-3, Acc-3, ACH, 17-12-14, 200
                            
                            
                            xrow_m.payment_stg.ChikPeaO2B__Invoice_Ref__c=t_data_d_d[header_map.get('INVOICE REF')].replaceAll('\"', '');
                            xrow_m.payment_stg.ChikPeaO2B__Account_Reference__c=t_data_d_d[header_map.get('ACCOUNT REFERENCE')].replaceAll('\"', '');
                            xrow_m.payment_stg.ChikPeaO2B__Payment_Method__c=t_data_d_d[header_map.get('PAYMENT METHOD')].replaceAll('\"', '');
                            xrow_m.payment_stg.ChikPeaO2B__Check_Ref__c=t_data_d_d[header_map.get('CHECK REF')].replaceAll('\"', '');
                            xrow_m.payment_stg.ChikPeaO2B__Split_to_Lines__c=Boolean.valueof(t_data_d_d[header_map.get('SPLIT TO LINES')].replaceAll('\"', ''));
                            xrow_m.payment_stg.ChikPeaO2B__Batch_Ref__c=t_data_d_d[header_map.get('BATCH REF')].replaceAll('\"', '');
                            
                            
                            System.debug('#****xrow_m.payment_stg.ChikPeaO2B__Payment_Method__c='+xrow_m.payment_stg.ChikPeaO2B__Split_to_Lines__c);
                            String date_s=t_data_d_d[header_map.get('PAYMENT DATE')].replaceAll('\"', '');
                            System.debug('#****date_s='+date_s);
                            if(date_s!=null&&date_s!=''){
                                String[] date_s_a=date_s.split('-');
                                
                                if(date_s_a.size()==3){
                                    xrow_m.payment_stg.ChikPeaO2B__Payment_Date__c=Date.valueOf(date_s_a[2]+'-'+
                                        date_s_a[0]+'-'+date_s_a[1]+' 0:0:0');
                                }else{
                                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'Invalid Date \"'+date_s+'\" at Line No. '
                                        +payment_stg_id));
                                    return redirectMsg();
                                    //throw new applicationException('Invalid Date '+date_s);
                                }
                            }
                            String amt_s=t_data_d_d[header_map.get('AMOUNT')];
                            
                            if(amt_s==null||amt_s==''){
                                amt_s='0';
                            }else{
                                amt_s=amt_s.replaceAll('\"', '');
                                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'amt_s='+amt_s));
                            }
                            try{
                             xrow_m.payment_stg.ChikPeaO2B__Amount__c=Decimal.valueOf(amt_s);
                            }catch(exception e){
                                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'Invalid Amount \"'+amt_s+'\" at Line No. '
                                    +payment_stg_id));
                            }
                    }
                }else{
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,
                        'Invalid Payment Stg Id'));
                }
                }
            }
            
            if(t_data_arr.size()==0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning,'You have not entered anything'));
            }else{
                return updateStg();
            }
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'#****xRowMap='+xRowMap));
            
        }
        return null;
    }
    
    private boolean headerValidation(Map<String, Integer> header_map){
        String [] h_arr =new String[] {'INVOICE REF', 'ACCOUNT REFERENCE', 'PAYMENT METHOD', 'PAYMENT DATE', 'AMOUNT','SPLIT TO LINES','CHECK REF','BATCH REF'};
        if(header_map.containsKey('LINE NO.')){
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Unexpected error '));
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'\"LINE NO.\" column does not exist'));
        }
        for(String header:h_arr){
            if(!header_map.containsKey(header)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'\"'+header+'\" column does not exist'));
                return false;
            }
        }
        return true;
    }
    
    private boolean dataValidation(String t_data){
        if(t_data=='[]'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'CSV does not contain any data'));
            return false;
        }
        return true;
    }
    
    private Pagereference updateStg(){
        List<ChikPeaO2B__Payment_Staging__c> paymt_stg_list=new List<ChikPeaO2B__Payment_Staging__c>();
        for(xRow xrw:xRowList){
            paymt_stg_list.add(xrw.payment_stg);
        }
        
        Savepoint sp = Database.setSavepoint();
        try{
              
              upsert paymt_stg_list;    
              //queryData();  
           //   executeBatch();
              return redirect();    
        }catch(Exception e){
             Database.rollback(sp);
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,''+e));
             //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Unexpected Error'));
        }
        return null;
    }
    public Pagereference redirect(){
        //Pagereference page_ref=new Pagereference('/apex/PaymentStgUploadInfo');
        Schema.DescribeSObjectResult dResult = ChikPeaO2B__Payment_Staging__c.sObjectType.getDescribe();        
        String keyPrefix = dResult.getKeyPrefix();
        Pagereference page_ref=new Pagereference('/'+keyPrefix);
        return page_ref;
    }
    
    public Pagereference redirectMsg(){
        Pagereference page_ref=new Pagereference('/apex/PaymentStgUploadInfo');
        return page_ref;
    }
    public void executeBatch(){
        Id btch_id= Database.executeBatch(new BatchPaymentStaging(), 200);
    }
    public Pagereference backToMain(){
        Pagereference page_ref=new Pagereference('/apex/PaymentStgUpload');
        return page_ref;
    }
    private static List<List<String>> fromString(String t_data) {
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'#***t_data='+t_data));
        List<List<String>> t_data_arr=new List<List<String>>();
        t_data = t_data.trim();
        t_data = t_data.subString(2,t_data.length()-2).replaceAll('(\\s+)', ' ');
        String []t_data_arr_f1=t_data.split('\\],\\[');
        
        for(String t_data_s:t_data_arr_f1){
            String []field_s=t_data_s.split(',');
            boolean data_found=false;
            List<String> field_s1=new List<String>();
            field_s1.add(field_s[0]);
            for(Integer count_k=1;count_k<field_s.size();count_k++){
                String field=field_s[count_k];
                field=field.substring(1,field.length()-1).trim();               
                if(field!=null&&field!=''){
                    data_found=true;
                }
                field_s1.add(field_s[count_k]);
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'#***field='+field)); 
            }
            if(data_found){
                t_data_arr.add(field_s1);
            }
                    
        }
        return t_data_arr;
    }
    public void creatXRow(List<ChikPeaO2B__Payment_Staging__c> payment_stgs){
        for(ChikPeaO2B__Payment_Staging__c payment_stg:payment_stgs){
            xRow xR=new xRow(payment_stg);  
            xRowList.add(xR);   
            xRowMap.put(xR.payment_stg_id, xR); 
        }
    }
    public void creatXRowArray(){
        t_data='[';
        for(xRow xR:xRowList){          
            t_data+=xR.rString+',';         
        }
        t_data=t_data.substring(0, t_data.length());
        //t_data+='["true", "false", "true", "false"]';
        t_data+=']';
    }
    //String payment_stg_id;
    Static Integer payment_stg_id_token=0;
    public class xRow{
        public String rString{get;set;}
        public String payment_stg_id{get;set;}
        public ChikPeaO2B__Payment_Staging__c payment_stg{get;set;}
        public xRow(ChikPeaO2B__Payment_Staging__c payment_stg){
            
            payment_stg_id_token++;
            payment_stg_id=String.valueof(payment_stg_id_token);
            
            rString='';
            this.payment_stg=payment_stg;
            if(payment_stg!=null){
                rString+='[';
                rString+='\"'+payment_stg_id+'\",';
                rString+='\"'+payment_stg.id+'\",';
                if(payment_stg.ChikPeaO2B__Invoice_Ref__c!=null)
                rString+='\"'+payment_stg.ChikPeaO2B__Invoice_Ref__c+'\",';
                else
                rString+='\"\",';
                rString+='\"'+payment_stg.ChikPeaO2B__Account_Reference__c+'\",';
                rString+='\"'+payment_stg.ChikPeaO2B__Status__c+'\",';
                rString+='\"'+payment_stg.ChikPeaO2B__Amount__c+'\",';
                rString+='\"'+payment_stg.ChikPeaO2B__Used_Amount__c+'\"';
                rString+='\"'+payment_stg.ChikPeaO2B__Check_Ref__c+'\"';
                rString+='\"'+payment_stg.ChikPeaO2B__Split_to_Lines__c+'\"';
                rString+='\"'+payment_stg.ChikPeaO2B__Batch_Ref__c+'\"';
                rString+=']';
            }           
        }
    }
    public class applicationException extends Exception {}
}