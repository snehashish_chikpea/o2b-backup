/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2015-11-10
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
global with sharing class TestPG_AuthNetCIMAPIMock implements WebServiceMock {
	
	global Object res { get; set;}
	
	global void doInvoke(Object stub,
                     Object request,
                     Map<String, Object> response,
                     String endpoint,
                     String soapAction,
                     String requestName,
                     String responseNS,
                     String responseName,
                     String responseType
    ){
    	
    	response.put('response_x', res);
    	
    }
	
}