/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-19
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG_Config {
        
    global String paymentGatewayName { get; set; }  
    global String paymentGatewayType { get; set; }  
    global String gatewayUrlTest { get; set; }  
    global String gatewayUrlProd { get; set; }  
    global String reportingGatewayUrlTest { get; set; } 
    global String reportingGatewayUrlProd { get; set; } 
    global String gatewayUrlTestFallback { get; set; }  
    global String gatewayUrlProdFallback { get; set; }  
    global String currencySupport { get; set; } 
    global String loginId { get; set; } 
    global String merchantReference { get; set; }   
    global String terminalId { get; set; }  
    global String password { get; set; }    
    global Integer timeoutLimit { get; set; }   
    global Boolean testMode { get; set; }   
    global Boolean tokenization { get; set; }   
    global Boolean addressVerification { get; set; }
    global Boolean addressVerificationZipOnly { get; set; } 
    global Boolean cardCodeVerification { get; set; }   
    global Boolean attachNote { get; set; }
    global Boolean captureCardDetails { get; set; }
    global Boolean primarySiteAvailable { get; set; }
    global Boolean partialAuthorization { get; set; }
    global String partnerCode { get; set; }
    global Boolean usePartnerCode { get; set; }
    global Integer retryCount { get; set; }
    global Integer retryInterval { get; set; }
    global Integer batchDownloadInterval { get; set; }
    global Integer batchUploadInterval { get; set; }
    global Integer batchSize { get; set; }
    global String batchResFieldId { get; set; }
    global Id gatewayId { get; set;}
    
    global PG_Config(String paymentGatewayName){//@throws PG_Exception
        
        this.paymentGatewayName = paymentGatewayName;
        List<Gateway__c> gateways = [Select g.Time_out_limit__c, g.Tokenization__c, g.Test_Mode__c, 
            g.Password__c, g.O2B_Setting__c, g.Name, g.Merchant_Reference__c, g.Login_Id__c, g.Id, 
            g.Gateway_Url_Test__c, g.Gateway_Url_Prod__c, g.Reporting_Gateway_Url_Prod__c,
            g.Reporting_Gateway_Url_Test__c, g.Gateway_Url_Test_Fallback__c, 
            g.Gateway_Url_Prod_Fallback__c, g.Currency__c, g.Capture_Card_Details__c, 
            g.Attach_Note__c, g.Address_Verification__c, g.Gateway_Type__c,
            g.Address_Verification_ZIP_Only__c, g.Card_Code_Verification__c, 
            g.Primary_Site_Available__c, g.Partial_Authorization__c, g.Partner_Code__c,
            g.Use_Partner_Code__c, g.Retry_Interval__c, 
            g.Retry_Count__c, g.Terminal_ID__c, g.Batch_Download_Interval__c, 
            g.Batch_Upload_Interval__c, g.Batch_Size__c, g.Batch_Res_File_Id__c
            From Gateway__c g 
            where g.Name = :paymentGatewayName limit 1];
        if(gateways != null && !gateways.isEmpty()){
            this.gatewayUrlTest = gateways[0].Gateway_Url_Test__c;
            this.gatewayUrlProd = gateways[0].Gateway_Url_Prod__c;
            this.reportingGatewayUrlTest = gateways[0].Reporting_Gateway_Url_Test__c;
            this.reportingGatewayUrlProd = gateways[0].Reporting_Gateway_Url_Prod__c;
            this.gatewayUrlTestFallback = gateways[0].Gateway_Url_Test_Fallback__c;//Orbital
            this.gatewayUrlProdFallback = gateways[0].Gateway_Url_Prod_Fallback__c;//Orbital
            this.currencySupport = gateways[0].Currency__c;
            this.loginId = gateways[0].Login_Id__c;
            this.merchantReference = gateways[0].Merchant_Reference__c;
            this.terminalId = gateways[0].Terminal_ID__c;
            this.password = gateways[0].Password__c;            
            this.timeoutLimit = gateways[0].Time_out_limit__c!=null?
                Integer.valueOf(gateways[0].Time_out_limit__c):120;//Default 120 sec
            this.testMode = gateways[0].Test_Mode__c;
            this.tokenization = gateways[0].Tokenization__c;
            this.addressVerification = gateways[0].Address_Verification__c;
            this.addressVerificationZipOnly = gateways[0].Address_Verification_ZIP_Only__c;
            this.cardCodeVerification = gateways[0].Card_Code_Verification__c;
            this.attachNote = gateways[0].Attach_Note__c;
            this.captureCardDetails = gateways[0].Capture_Card_Details__c;
            this.primarySiteAvailable = gateways[0].Primary_Site_Available__c;
            this.partialAuthorization = gateways[0].Partial_Authorization__c;
            this.partnerCode = gateways[0].Partner_Code__c;
            this.usePartnerCode = gateways[0].Use_Partner_Code__c;
            this.retryCount = gateways[0].Retry_Count__c!=null?
                Integer.valueOf(gateways[0].Retry_Count__c):14;
            this.retryInterval = gateways[0].Retry_Interval__c!=null?
                Integer.valueOf(gateways[0].Retry_Interval__c):2;
            this.paymentGatewayType = gateways[0].Gateway_Type__c;  
            this.gatewayId = gateways[0].Id;
            this.batchDownloadInterval = gateways[0].Batch_Download_Interval__c!=null? 
                Integer.valueOf(gateways[0].Batch_Download_Interval__c):1;
            this.batchUploadInterval = gateways[0].Batch_Upload_Interval__c!=null? 
                Integer.valueOf(gateways[0].Batch_Upload_Interval__c):1;
            this.batchSize = gateways[0].Batch_Size__c!=null?
                Integer.valueOf(gateways[0].Batch_Size__c):1;
            this.batchResFieldId = gateways[0].Batch_Res_File_Id__c!=null?
                gateways[0].Batch_Res_File_Id__c:'';
        }else{
            throw new PG_Exception('PG Exception::Payment Gateway '+paymentGatewayName
                +' not supported.');
        }               
    }
    
    /*
    public static void tryIt(){
        PG_Config config = new PG_Config('PayFlow');
        System.debug('---config='+config);
    }   
    */
}