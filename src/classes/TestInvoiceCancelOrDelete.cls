@isTest
public class TestInvoiceCancelOrDelete
{
    @isTest
    static void testMethod1()
    {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Account_based_tiering__c=false);
        insert(o2bset);
        Account acc=new Account(name='acc1');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        ChikPeaO2B__Item__c itm1=new ChikPeaO2B__Item__c(name='Item1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        insert(itm1);
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        insert (rp1);
        ChikPeaO2B__Item__c itm2=new ChikPeaO2B__Item__c(name='Item2',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        insert(itm2);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        insert (rp2);
        subscription__c sub=new subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Aggregate_Usage__c=400,ChikPeaO2B__Billing_Start_Date__c=date.today(),ChikPeaO2B__Free_usage__c=100,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Recurring_Charge__c=20,ChikPeaO2B__Billing_Stopped__c=true);
        insert(sub);
        ChikPeaO2B__Purchase__c pur=new ChikPeaO2B__Purchase__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Aggregate_Usage__c=300,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Max_Usage__c=100,ChikPeaO2B__Qty__c=1,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Sell_Price__c=1);
        insert(pur);
        List<ChikPeaO2B__Usage__c>usgList=new List<ChikPeaO2B__Usage__c>();
        ChikPeaO2B__Usage__c subusg=new ChikPeaO2B__Usage__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Free_Usage__c=2,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Rate__c=0.5,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Subscription__c=sub.id,ChikPeaO2B__Total_Usage__c=0,ChikPeaO2B__Type__c='Postpaid');
        usgList.add(subusg);
        ChikPeaO2B__Usage__c purusg=new ChikPeaO2B__Usage__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Rate__c=1.5,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Purchase__c=pur.id,ChikPeaO2B__Remaining_Usage__c=50,ChikPeaO2B__Total_Usage__c=0,ChikPeaO2B__Type__c='Prepaid');
        usgList.add(purusg);
        insert(usgList);
        ChikPeaO2B__Invoice__c inv=new ChikPeaO2B__Invoice__c(Account__c=acc.id,ChikPeaO2B__Open_Balance__c=0);
        insert(inv);
        List<ChikPeaO2B__Invoice_Line__c>InvlList=new list<ChikPeaO2B__Invoice_Line__c>();
        ChikPeaO2B__Invoice_Line__c invl1=new ChikPeaO2B__Invoice_Line__c(ChikPeaO2B__Invoice__c=inv.id,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Qty__c=1,ChikPeaO2B__Unit_Rate__c=10,ChikPeaO2B__Line_Type__c='Subscription',ChikPeaO2B__Subscription__c=sub.id);
        InvlList.add(invl1);
        ChikPeaO2B__Invoice_Line__c invl2=new ChikPeaO2B__Invoice_Line__c(ChikPeaO2B__Invoice__c=inv.id,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Qty__c=1,ChikPeaO2B__Unit_Rate__c=10,ChikPeaO2B__Line_Type__c='PostPaid Usage',ChikPeaO2B__Usage__c=subusg.id);
        InvlList.add(invl2);
        ChikPeaO2B__Invoice_Line__c invl3=new ChikPeaO2B__Invoice_Line__c(ChikPeaO2B__Invoice__c=inv.id,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Qty__c=1,ChikPeaO2B__Unit_Rate__c=10,ChikPeaO2B__Line_Type__c='Purchase',ChikPeaO2B__Purchase__c=pur.id);
        InvlList.add(invl3);
        ChikPeaO2B__Invoice_Line__c invl4=new ChikPeaO2B__Invoice_Line__c(ChikPeaO2B__Invoice__c=inv.id,ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Qty__c=1,ChikPeaO2B__Unit_Rate__c=10,ChikPeaO2B__Line_Type__c='PrePaid Usage',ChikPeaO2B__Usage__c=purusg.id);
        InvlList.add(invl4);
        insert(invlList);
        Test.startTest();
        InvoiceCancelOrDelete.SubscriptionValidationForCancelDelete(inv.id);
        InvoiceCancelOrDelete.cancelordel(inv.id);
        Test.stopTest();
    }
}