/**
----------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-12-04
@see 
----------------------------------------------------------------------------------------------------
*/
global with sharing class PG_MeSBatchUploaderSchedulable implements Schedulable {

    public static final String JOB_NAME = 'MeS Batch File Uploader';
    public static final String PAYMENT_GATEWAY = 'MeS:Batch';
    public static final String UPLOAD_PATH = '/srv/api/bpUpload';

    global void execute(System.SchedulableContext SC){
        String cronTriggerId = SC.getTriggerId();
        upload(cronTriggerId);      
    }

    @future(callout=true)
    static void upload(String cronTriggerId){
        //TODO: catch and log exceptions in log object
        List<ChikPeaO2B__Gateway__c> gateways = null;
        ChikPeaO2B__Gateway__c gateway = null;
        Integer batchSize = 0;
        Integer batchDownloadInterval = 0;
        Integer batchUploadInterval = 0;
        //String uploadJobId = '';
        gateways = [Select c.Name, c.Id, 
            c.ChikPeaO2B__Batch_Download_Interval__c, c.ChikPeaO2B__Batch_Upload_Interval__c, 
            c.ChikPeaO2B__Batch_Size__c, c.ChikPeaO2B__Batch_Res_File_Id__c
            From ChikPeaO2B__Gateway__c c 
            Where c.Name = :PAYMENT_GATEWAY Limit 1];
        if(gateways != null && !gateways.isEmpty()){
            gateway = gateways[0];
            batchSize = Integer.valueOf(gateway.ChikPeaO2B__Batch_Size__c);
            batchDownloadInterval = Integer.valueOf(gateway.ChikPeaO2B__Batch_Download_Interval__c);
            batchUploadInterval = Integer.valueOf(gateway.ChikPeaO2B__Batch_Upload_Interval__c);
            //uploadJobId = gateways[0].ChikPeaO2B__Upload_Job_Id__c;
        }
        List<Payment__c> payments = [Select Id,
            Payment_Gateway__c, Payment_Amount__c, Merchant_Order_Number__c, Invoice__r.Name, 
            Invoice__c, Credit_Card__r.CC_Street1__c, Credit_Card__r.CC_State__c, 
            Credit_Card__r.CC_Last_Name__c, Credit_Card__r.Expiry_Year__c, 
            Credit_Card__r.Expiry_Month__c, Credit_Card__r.Credit_Card_Type__c, 
            Credit_Card__r.Credit_Card_Number__c, Credit_Card__r.CC_Street2__c,
            Credit_Card__r.CC_Postal_Code__c, 
            Credit_Card__r.CC_First_Name__c, Credit_Card__r.CC_Country__c, 
            Credit_Card__r.CC_City__c, Credit_Card__c, Account__r.Name, Account__c 
            From Payment__c c
            Where Payment_Gateway__c = :PAYMENT_GATEWAY
            and (Invoice__r.Invoice_Status__c = 'Open' or Invoice__r.Invoice_Status__c = 'Paid Partial') 
            and Has_Processed__c = false 
            and (Status__c = 'Not Processed' or Status__c = 'Failed')
            and Payment_Date__c <= TODAY 
            and (Payment_Method__c = 'Credit Card' and (Card_Failure__c < 2 or Card_Failure__c = null))
            Limit :batchSize
        ];
        if(payments != null && payments.size() > 0){
            PG_MeSBatch mesBat = new PG_MeSBatch();
            mesBat.init(payments, PAYMENT_GATEWAY);
            String reqFileId = mesBat.uploadFile(UPLOAD_PATH);

            if(reqFileId != null){
                //set the response file Id
                String resFileId = String.valueOf(Integer.valueOf(reqFileId)+1);
                System.debug('---reqFileId='+reqFileId+', resFileId='+resFileId);
                gateway.ChikPeaO2B__Batch_Res_File_Id__c = resFileId;
                update gateway;

                //Abort scheduled upload job and schedule download job after download interval
                System.abortJob(cronTriggerId);
                //PG_MeSBatchDownloaderSchedulable.scheduleThisAfter(0, batchDownloadInterval, 0, resFileId);
                PG_MeSBatchDownloaderSchedulable.scheduleThisAfter(0, batchDownloadInterval, 0);                
            }else{//log and prepare next set of upload
                System.abortJob(cronTriggerId);
                scheduleThisAfter(0, batchUploadInterval, 0);
            }
        }else{
            //reset the response file Id
            gateway.ChikPeaO2B__Batch_Res_File_Id__c = null;
            update gateway;
            //Abort scheduled upload job and re-schedule after 24 hours
            System.abortJob(cronTriggerId);
            scheduleThisAfter(0, 0, 24);
        }
    }

    /*global void finish(Database.BatchableContext BC){
        System.debug('---done scheduling job = '+BC.getJobId());
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email 
            FROM AsyncApexJob 
            WHERE Id = :BC.getJobId()];
        //then use the active job id and abort it
        System.debug('---job id='+a.id);
        system.abortJob(a.id);
    }*/

    /*
        PG_MeSBatchUploaderSchedulable.scheduleThisAfter(0, 1, 0);//after 1 min
    */
    global static void scheduleThisAfter(Integer seconds, Integer minutes, Integer hours){
        
        Datetime later = Datetime.now();
        if(hours > 0) later = later.addHours(hours);
        if(minutes > 0) later = later.addMinutes(minutes);
        if(seconds > 0) later = later.addSeconds(seconds);
        String cronExpr = later.second() + ' ' + later.minute() + ' ' + later.hour() + ' * * ?';
        System.debug('---cronExpr='+cronExpr);
        PG_MeSBatchUploaderSchedulable uploader = new PG_MeSBatchUploaderSchedulable();
        String cronTriggerID = System.schedule(PG_MeSBatchUploaderSchedulable.JOB_NAME, cronExpr, uploader);
        System.debug('---cron trigger Id = '+cronTriggerID);

        //Save the cron Trigger Id in Gateway object Upload Job Id field
        /*List<ChikPeaO2B__Gateway__c> gateways = [Select c.Name, c.Id, 
            c.ChikPeaO2B__Upload_Job_Id__c
            From ChikPeaO2B__Gateway__c c 
            Where c.Name = :PAYMENT_GATEWAY Limit 1];
        if(gateways != null && !gateways.isEmpty()){
            ChikPeaO2B__Gateway__c gateway = gateways[0];
            gateway.ChikPeaO2B__Upload_Job_Id__c = cronTriggerID;
            update gateway;
        }*/
    }
}