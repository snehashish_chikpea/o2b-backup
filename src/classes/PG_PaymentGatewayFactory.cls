/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 2.0 2013-09-27 2013-11-29 2014-03-21
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG_PaymentGatewayFactory {
    
    global static PG_PaymentGateway create(String paymentGatewayType){
        
        String paymentGatewayTypeName = PG.getPaymentGatewayTypeName(paymentGatewayType);   
        //System.debug('Testing Class Name for paymentGatewayTypeName---->'+paymentGatewayTypeName);
        //Type t = Type.forName(paymentGatewayTypeName);
        Type t = Type.forName('',paymentGatewayTypeName);
        //System.debug('Testing Class Name for paymentGatewayTypeName Type---->'+t);
        PG_PaymentGateway paymentGateway = (PG_PaymentGateway)t.newInstance();
        
        return paymentGateway;
    }
    
    global static PG_PaymentGatewayACH createACH(String paymentGatewayType){
        
        String paymentGatewayTypeName = PG.getPaymentGatewayTypeName(paymentGatewayType);
        Type t = Type.forName(paymentGatewayTypeName);
        PG_PaymentGatewayACH paymentGatewayACH = (PG_PaymentGatewayACH)t.newInstance();
        
        return paymentGatewayACH;
    }
    //TODO: Uncomment before starting using handler
    /*
    global static PG_ResponseHandler createHandler(String paymentGatewayType){
        
        String paymentGatewayTypeName = PG.getResponseHandlerName(paymentGatewayType);      
        Type t = Type.forName(paymentGatewayTypeName);
        PG_ResponseHandler responseHandler = (PG_ResponseHandler)t.newInstance();
        
        return responseHandler;
    }
    */
}