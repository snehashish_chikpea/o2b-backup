/**
----------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-04-28
@see 
----------------------------------------------------------------------------------------------------
*/
global with sharing class PG_AuthorizeNetReporting {
    
    PG_AuthNetSOAPService.ServiceSoap report;
    
    global Integer R_TIMEOUT_MS{get; set;}
    global String R_ENDPOINT{get; set;}
    
    //TDOD: depricated, populate from Gateway config
    String loginName = '';
    String transactionKey = '';
    
    global void init(String paymentGatewayName){
        
        PG_Config config = new PG_Config(paymentGatewayName);
        System.debug('---config='+config);
        
        //loginName = '5GtpZ88g2';
        //transactionKey = '8dM6bR63KMwxs77P';
        loginName = config.loginId;
        transactionKey = config.password;
        
        //R_TIMEOUT_MS = 120*1000;
        //R_ENDPOINT = 'https://apitest.authorize.net/soap/v1/Service.asmx';
        R_TIMEOUT_MS = config.timeoutLimit*1000;
        R_ENDPOINT = config.testMode?config.reportingGatewayUrlTest:config.reportingGatewayUrlProd;
        if(R_ENDPOINT == null || R_ENDPOINT == '') 
            throw new PG_Exception('Gateway Exception::invalid gateway endpoint url.');
        
        report = new PG_AuthNetSOAPService.ServiceSoap(
            R_ENDPOINT,
            R_TIMEOUT_MS
        );
    }
    
    /**
    PG_AuthorizeNetReporting anReport = new PG_AuthorizeNetReporting();
    anReport.getTransactionDetails('2211101871');
    */
    public PG_AuthNetSOAPService.GetTransactionDetailsResponseType getTransactionDetails(String transId){
        
        PG_AuthNetSOAPService.MerchantAuthenticationType merchantAuthentication = 
            new PG_AuthNetSOAPService.MerchantAuthenticationType();
            
        merchantAuthentication.name = loginName;
        merchantAuthentication.transactionKey = transactionKey; 
        
        return report.GetTransactionDetails(
            merchantAuthentication, 
            transId
        );
            
    }
    
    /**
    PG_AuthorizeNetReporting anReport = new PG_AuthorizeNetReporting();
    anReport.init('AuthNet:Reporting');
    //Map<String, String> resMap = anReport.getTransactionDetailsMap('2211101871', PG.PAYMENT_TYPE_CC);
    Map<String, String> resMap = anReport.getTransactionDetailsMap('2211708431', PG.PAYMENT_TYPE_ACH);
    String res = '';
    for(String key : resMap.keySet()){
        res += key + '=' + resMap.get(key) + ', ';
    }
    System.debug('---res='+res);
    */
    //TODO:// 3 type of trx tested, CC (auth capture, refund ), ACH (Debit)
    global Map<String, String> getTransactionDetailsMap(String transId, 
        String paymentType
    ){
        
        //String transId = reqMap.get('transId');
        //String paymentType = reqMap.get('paymentType');
        
        if(paymentType == null 
            || paymentType != PG.PAYMENT_TYPE_CC 
            && paymentType != PG.PAYMENT_TYPE_ACH
        ) 
            throw new PG_Exception('Gateway Exception::invalid transction type. Valid \'CC\' or \'ACH\'.');
        if(transId == null) 
            throw new PG_Exception('Gateway Exception::transId is required.');
        
        Map<String, String> transactionDetail = null;
        
        PG_AuthNetSOAPService.GetTransactionDetailsResponseType transactionDetailsResult = getTransactionDetails(transId);
        
        System.debug('---'+transactionDetailsResult);
        
        transactionDetail = new Map<String, String>{
            'resultCode' => transactionDetailsResult.resultCode,            
            'MessagesTypeMessage.code' => transactionDetailsResult.messages.MessagesTypeMessage[0].code,            
            'MessagesTypeMessage.text' => transactionDetailsResult.messages.MessagesTypeMessage[0].text,            
            'transId' => transactionDetailsResult.transaction_x.transId,            
            'submitTimeUTC' => String.valueOf(transactionDetailsResult.transaction_x.submitTimeUTC),            
            'submitTimeLocal' => String.valueOf(transactionDetailsResult.transaction_x.submitTimeLocal),
            'transactionType' => transactionDetailsResult.transaction_x.transactionType,
            'transactionStatus' => transactionDetailsResult.transaction_x.transactionStatus,
            'responseCode' => String.valueOf(transactionDetailsResult.transaction_x.responseCode),
            'responseReasonCode' => String.valueOf(transactionDetailsResult.transaction_x.responseReasonCode),
            'responseReasonDescription' => transactionDetailsResult.transaction_x.responseReasonDescription,
            'AVSResponse' => transactionDetailsResult.transaction_x.AVSResponse,
            'authAmount' => String.valueOf(transactionDetailsResult.transaction_x.authAmount),
            'settleAmount' => String.valueOf(transactionDetailsResult.transaction_x.settleAmount),
            'taxExempt' => String.valueOf(transactionDetailsResult.transaction_x.taxExempt),
            'recurringBilling' => String.valueOf(transactionDetailsResult.transaction_x.recurringBilling),
            'marketType' => transactionDetailsResult.transaction_x.marketType,
            'product' => transactionDetailsResult.transaction_x.product         
        };
        // If order details, like invoice number not supplied
        if(transactionDetailsResult.transaction_x.order != null){
            transactionDetail.putAll(new Map<String, String>{
                'order.description' => transactionDetailsResult.transaction_x.order.description,
                'order.invoiceNumber' => transactionDetailsResult.transaction_x.order.invoiceNumber,
                'order.purchaseOrderNumber' => transactionDetailsResult.transaction_x.order.purchaseOrderNumber
            });
        }
        if(transactionDetailsResult.transaction_x.customer != null){
            transactionDetail.putAll(new Map<String, String>{
                'customer.id' => transactionDetailsResult.transaction_x.customer.id,
                'customer.email' => transactionDetailsResult.transaction_x.customer.email,
                'customer.taxId' => transactionDetailsResult.transaction_x.customer.taxId,
                'customer.type' => transactionDetailsResult.transaction_x.customer.type_x
            });
        }    
        if(transactionDetailsResult.transaction_x.batch != null){
                transactionDetail.put('batch.batchId', transactionDetailsResult.transaction_x.batch.batchId);
                //transactionDetail.put('batch.paymentMethod', transactionDetailsResult.transaction_x.batch.paymentMethod);
                transactionDetail.put('batch.settlementTimeUTC', String.valueOf(transactionDetailsResult.transaction_x.batch.settlementTimeUTC));
                transactionDetail.put('batch.settlementTimeLocal', String.valueOf(transactionDetailsResult.transaction_x.batch.settlementTimeLocal));
                transactionDetail.put('batch.settlementState', transactionDetailsResult.transaction_x.batch.settlementState);
            }  
        if(paymentType == PG.PAYMENT_TYPE_CC){
            transactionDetail.put('authCode', transactionDetailsResult.transaction_x.authCode);
            
            if(transactionDetailsResult.transaction_x.payment.creditCard != null){
                transactionDetail.put('creditCard.cardNumber', transactionDetailsResult.transaction_x.payment.creditCard.cardNumber);
                transactionDetail.put('creditCard.cardType', transactionDetailsResult.transaction_x.payment.creditCard.cardType);
                transactionDetail.put('creditCard.expirationDate', transactionDetailsResult.transaction_x.payment.creditCard.expirationDate);
            }
        }
        if(paymentType == PG.PAYMENT_TYPE_ACH){
            if(transactionDetailsResult.transaction_x.payment.bankAccount != null){
                transactionDetail.put('bankAccount.accountNumber', transactionDetailsResult.transaction_x.payment.bankAccount.accountNumber);
                transactionDetail.put('bankAccount.accountType', transactionDetailsResult.transaction_x.payment.bankAccount.accountType);
                transactionDetail.put('bankAccount.bankName', transactionDetailsResult.transaction_x.payment.bankAccount.bankName);
                transactionDetail.put('bankAccount.echeckType', transactionDetailsResult.transaction_x.payment.bankAccount.echeckType);
                transactionDetail.put('bankAccount.nameOnAccount', transactionDetailsResult.transaction_x.payment.bankAccount.nameOnAccount);
                transactionDetail.put('bankAccount.routingNumber', transactionDetailsResult.transaction_x.payment.bankAccount.routingNumber);
            }
        }
            
        return transactionDetail;
    }
    
    /**
    @Depricated - No use, since webservice invoke does not provide access to xml(SOAP) response
    //TODO: post in idea section
    //String res = '<GetTransactionDetailsResponse xmlns="https://api.authorize.net/soap/v1/"><GetTransactionDetailsResult><resultCode>Ok</resultCode><messages><MessagesTypeMessage><code>I00001</code><text>Successful.</text></MessagesTypeMessage></messages><transaction><transId>2211101871</transId><submitTimeUTC>2014-04-16T10:18:34.757Z</submitTimeUTC><submitTimeLocal>2014-04-16T04:18:34.757</submitTimeLocal><transactionType>authCaptureTransaction</transactionType><transactionStatus>settledSuccessfully</transactionStatus><responseCode>1</responseCode><responseReasonCode>1</responseReasonCode><responseReasonDescription>Approval</responseReasonDescription><authCode>OCZJE0</authCode><AVSResponse>Y</AVSResponse><batch><batchId>3348069</batchId><settlementTimeUTC>2014-04-16T10:29:21.4Z</settlementTimeUTC><settlementTimeLocal>2014-04-16T04:29:21.4</settlementTimeLocal><settlementState>settledSuccessfully</settlementState></batch><order><invoiceNumber>I-0000000113</invoiceNumber></order><authAmount>200.00</authAmount><settleAmount>200.00</settleAmount><taxExempt>false</taxExempt><payment><creditCard><cardNumber>XXXX8764</cardNumber><expirationDate>XXXX</expirationDate><cardType>Visa</cardType></creditCard></payment><customer><id>AB Corp</id></customer><recurringBilling>false</recurringBilling><marketType>eCommerce</marketType><product>Card Not Present</product></transaction></GetTransactionDetailsResult></GetTransactionDetailsResponse>';
    //Map<String, String> resMap = PG_AuthorizeNetReporting.parse(res);
    //System.debug('---'+resMap);
    
    public static Map<String, String> parse(String xmlStr){//@throws xmlException
        
        Map<String, String> resMap = new Map<String, String>(); 
        
        XmlStreamReader reader = new XmlStreamReader(xmlStr);
        String leafNodeName = '';
        String leafNodeValue = '';
        Boolean isLeafNode = false;
        
        
        while(reader.hasNext()){
            
            if(reader.getEventType() == Xmltag.START_ELEMENT){
                leafNodeName = reader.getLocalName();
                //System.debug('---'+reader.getLocalName());
                
            } else if(reader.getEventType() == Xmltag.END_ELEMENT){ 
                if(isLeafNode){
                    resMap.put(leafNodeName, leafNodeValue);                    
                    //System.debug('---'+leafNodeName+' = '+leafNodeValue);
                }
                isLeafNode = false;
                
            } else if(reader.getEventType() == Xmltag.CHARACTERS){
                leafNodeValue = reader.getText();
                isLeafNode = true;              
                //System.debug('--'+reader.getText());
            }
            reader.next();
        }
        
        return resMap;
    }
    */
}

/*
Help in integration
//Trx : AuthCapture

<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <soap:Body>
        <GetTransactionDetailsResponse xmlns="https://api.authorize.net/soap/v1/">
            <GetTransactionDetailsResult>
                <resultCode>Ok</resultCode>
                <messages>
                    <MessagesTypeMessage>
                        <code>I00001</code>
                        <text>Successful.</text>
                    </MessagesTypeMessage>
                </messages>
                <transaction>
                    <transId>2211101871</transId>
                    <submitTimeUTC>2014-04-16T10:18:34.757Z</submitTimeUTC>
                    <submitTimeLocal>2014-04-16T04:18:34.757</submitTimeLocal>
                    <transactionType>authCaptureTransaction</transactionType>
                    <transactionStatus>settledSuccessfully</transactionStatus>
                    <responseCode>1</responseCode>
                    <responseReasonCode>1</responseReasonCode>
                    <responseReasonDescription>Approval</responseReasonDescription>
                    <authCode>OCZJE0</authCode>
                    <AVSResponse>Y</AVSResponse>
                    <batch>
                        <batchId>3348069</batchId>
                        <settlementTimeUTC>2014-04-16T10:29:21.4Z</settlementTimeUTC>
                        <settlementTimeLocal>2014-04-16T04:29:21.4</settlementTimeLocal>
                        <settlementState>settledSuccessfully</settlementState>
                    </batch>
                    <order>
                        <invoiceNumber>I-0000000113</invoiceNumber>
                    </order>
                    <authAmount>200.00</authAmount>
                    <settleAmount>200.00</settleAmount>
                    <taxExempt>false</taxExempt>
                    <payment>
                        <creditCard>
                            <cardNumber>XXXX8764</cardNumber>
                            <expirationDate>XXXX</expirationDate>
                            <cardType>Visa</cardType>
                        </creditCard>
                    </payment>
                    <customer>
                        <id>AB Corp</id>
                    </customer>
                    <recurringBilling>false</recurringBilling>
                    <marketType>eCommerce</marketType>
                    <product>Card Not Present</product>
                </transaction>
            </GetTransactionDetailsResult>
        </GetTransactionDetailsResponse>
    </soap:Body>
</soap:Envelope>

//Trx : Refund

<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <soap:Body>
        <GetTransactionDetailsResponse xmlns="https://api.authorize.net/soap/v1/">
            <GetTransactionDetailsResult>
                <resultCode>Ok</resultCode>
                <messages>
                    <MessagesTypeMessage>
                        <code>I00001</code>
                        <text>Successful.</text>
                    </MessagesTypeMessage>
                </messages>
                <transaction>
                    <transId>2211761141</transId>
                    <submitTimeUTC>2014-04-26T10:41:47.627Z</submitTimeUTC>
                    <submitTimeLocal>2014-04-26T04:41:47.627</submitTimeLocal>
                    <transactionType>refundTransaction</transactionType>
                    <transactionStatus>refundSettledSuccessfully</transactionStatus>
                    <responseCode>1</responseCode>
                    <responseReasonCode>1</responseReasonCode>
                    <responseReasonDescription>Approval</responseReasonDescription>
                    <AVSResponse>P</AVSResponse>
                    <batch>
                        <batchId>3376991</batchId>
                        <settlementTimeUTC>2014-04-26T12:45:03.313Z</settlementTimeUTC>
                        <settlementTimeLocal>2014-04-26T06:45:03.313</settlementTimeLocal>
                        <settlementState>settledSuccessfully</settlementState>
                    </batch>
                    <order>
                        <invoiceNumber>I-0000000802</invoiceNumber>
                    </order>
                    <authAmount>0.34</authAmount>
                    <settleAmount>0.34</settleAmount>
                    <taxExempt>false</taxExempt>
                    <payment>
                        <creditCard>
                            <cardNumber>XXXX5114</cardNumber>
                            <expirationDate>XXXX</expirationDate>
                            <cardType>MasterCard</cardType>
                        </creditCard>
                    </payment>
                    <customer>
                        <id>Test A1</id>
                    </customer>
                    <recurringBilling>false</recurringBilling>
                    <marketType>eCommerce</marketType>
                    <product>Card Not Present</product>
                </transaction>
            </GetTransactionDetailsResult>
        </GetTransactionDetailsResponse>
    </soap:Body>
</soap:Envelope>

//Trx : ACH

<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <soap:Body>
        <GetTransactionDetailsResponse xmlns="https://api.authorize.net/soap/v1/">
            <GetTransactionDetailsResult>
                <resultCode>Ok</resultCode>
                <messages>
                    <MessagesTypeMessage>
                        <code>I00001</code>
                        <text>Successful.</text>
                    </MessagesTypeMessage>
                </messages>
                <transaction>
                    <transId>2211708431</transId>
                    <submitTimeUTC>2014-04-25T09:00:13.023Z</submitTimeUTC>
                    <submitTimeLocal>2014-04-25T03:00:13.023</submitTimeLocal>
                    <transactionType>authCaptureTransaction</transactionType>
                    <transactionStatus>capturedPendingSettlement</transactionStatus>
                    <responseCode>1</responseCode>
                    <responseReasonCode>1</responseReasonCode>
                    <responseReasonDescription>Approval</responseReasonDescription>
                    <AVSResponse>P</AVSResponse>
                    <order>
                        <invoiceNumber>I-0000000021</invoiceNumber>
                    </order>
                    <authAmount>2.00</authAmount>
                    <settleAmount>2.00</settleAmount>
                    <taxExempt>false</taxExempt>
                    <payment>
                        <bankAccount>
                            <accountType>businessChecking</accountType>
                            <nameOnAccount>Pritis Nayak</nameOnAccount>
                            <echeckType>CCD</echeckType>
                            <routingNumber>XXXX0549</routingNumber>
                            <accountNumber>XXXX5678</accountNumber>
                        </bankAccount>
                    </payment>
                    <customer>
                        <id>Test A7</id>
                    </customer>
                    <recurringBilling>false</recurringBilling>
                </transaction>
            </GetTransactionDetailsResult>
        </GetTransactionDetailsResponse>
    </soap:Body>
</soap:Envelope>
*/