public  class PG_IPPayments implements PG_PaymentGateway{
    
    public static final Boolean TOKENIZATION_SUPPORT = false;
    
    String gatewayName;
    Boolean primarySiteAvailable;
    PG_CCDetails details;
    PG_Config config;   
    PG_Response pgResponse;
    PG_VantivXmlParser ip_xml_parser=new PG_VantivXmlParser();
    
    public Integer R_TIMEOUT_MS{get; set;}
    public String R_ENDPOINT{get; set;}
    public String Amount{get; set;}//Required
    public String CVV{get; set;}
    public String TxRefNum{get; set;}//for Refund, MFC or void
    public String CustRefNum{get;set;}
    public String TxRefIdx{get; set;}
    public String PriorAuthCd{get; set;}//for TrxType = FC
    /*public String TransactionRef{get; set;}
    public String InvoiceRef{get; set;}
    public String CustomerRef{get; set;}
    public String firstName{get; set;}
    public String lastName{get; set;}*/
    
    
    public void init(PG_CCDetails details, PG_Config config, Map<String, String> transactionInfo)//inject variying gateway parameters
    {
        System.debug('---details='+details+', config='+config+', transactionInfo='+transactionInfo);
        gatewayName = config.paymentGatewayName;
        primarySiteAvailable = config.primarySiteAvailable;
        this.details = details;
        this.config = config;
        pgResponse = new PG_Response();
        pgResponse.paymentGatewayName = gatewayName;
        
        this.Amount = transactionInfo.get('Amount')!=null?
            transactionInfo.get('Amount'):'';//implied decimal, $100 as Amount = 10000
        if(Amount != '')
        {
            try
            {
                Decimal amnt = Decimal.valueOf(Amount);
                Amount = String.valueOf(Integer.valueOf(amnt*100));
            }
            catch(TypeException ex)
            {
                throw new PG_Exception('Gateway Exception::Invalid amount - '+Amount);
            }
        }
        this.CVV = transactionInfo.get('CVV')!=null?
            transactionInfo.get('CVV'):'';
        this.TxRefNum = transactionInfo.get('TransactionRef')!=null?
            transactionInfo.get('TransactionRef'):'';
        System.debug('&&&&&&&&'+TxRefNum);
        this.CustRefNum = transactionInfo.get('CustomerRef')!=null?
            transactionInfo.get('CustomerRef'):'';
        System.debug('&&&&&&&&'+CustRefNum);
        this.TxRefIdx = transactionInfo.get('TxRefIdx')!=null?
            transactionInfo.get('TxRefIdx'):'';
        this.PriorAuthCd = transactionInfo.get('priorAuthCd')!=null?
            transactionInfo.get('priorAuthCd'):'';
        
        R_TIMEOUT_MS = config.timeoutLimit*1000;
        R_ENDPOINT = config.testMode?
            (primarySiteAvailable?config.gatewayUrlTest:config.gatewayUrlTestFallback):
            (primarySiteAvailable?config.gatewayUrlProd:config.gatewayUrlProdFallback);
        System.debug('---init():PG name='+gatewayName+', CC details='+details.debug()+', PG config='+config);
        //+', transactionInfo='+transactionInfo);
    }
    
    public void processTransaction(PG.TransactionType transactionType){//@throws PG_Exception
        
        if(config.tokenization && !TOKENIZATION_SUPPORT)
            throw new PG_Exception('Gateway Exception::Tokenization not supported for '+gatewayName);
        
        String resBody = '';
        if(PG.TransactionType.AUTHORIZATION == transactionType){
            pgResponse.transactionType = PG.TransactionType.AUTHORIZATION.name();
            resBody = authorization();//Auth
        }else if(PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE == transactionType){
            pgResponse.transactionType = PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE.name();
            resBody = markForCapture();//Delayed Capture
        }else if(PG.TransactionType.AUTHORIZATION_CAPTURE == transactionType){
            pgResponse.transactionType = PG.TransactionType.AUTHORIZATION_CAPTURE.name();
            resBody = authorizationCapture();//Sale         
        }else if(PG.TransactionType.REVERSEAL == transactionType){
            pgResponse.transactionType = PG.TransactionType.REVERSEAL.name();
            resBody = reversal();//Void
        }else if(PG.TransactionType.REFUND == transactionType){
            pgResponse.transactionType = PG.TransactionType.REFUND.name();
            resBody = refund();//Credit
        }else{
            throw new PG_Exception('Gateway Exception::transaction '+transactionType.name()+
                ' not supported for '+gatewayName);
        }
        populateResponse(ip_xml_parser.processXML(resBody));
    }
    
    public PG_Response getPGResponse()
   {
        return pgResponse;
   }
   
   public String authorization()//@throws PG_Exception
   {
        System.debug('AUTHORIZATION PROCESS INVOKED************');
        String resBody='';
        String reqBody='<Transaction>'+
                     '<CustNumber></CustNumber>'+
                     '<CustRef>'+CustRefNum+'</CustRef>'+
                     '<Amount>'+amount+'</Amount>'+
                     '<TrnType>2</TrnType>'+
                     '<CreditCard Registered=\'False\'>'+
                     '<CardNumber>'+details.creditCardNumber +'</CardNumber>'+
                     '<ExpM>'+details.expiryMonth +'</ExpM>'+
                     '<ExpY>'+details.expiryYear+'</ExpY>'+
                     '<CVN></CVN>'+
                     '<CardHolderName>'+details.ccFirstName+' '+details.ccLastName+'</CardHolderName>'+
                     '</CreditCard>'+
                     '<Security>'+
                     '<UserName>'+config.loginId+'</UserName>'+
                     '<Password>'+config.password+'</Password>'+
                     '</Security>'+
                     '<TrnSource></TrnSource>'+
                     '</Transaction>'
                     ;
        /*if(Test.isRunningTest()){
            resBody = '<?xml version=\'1.0\' encoding=\'utf-8\'?><soap:Envelope xmlns:xsi=\'http://www.w3.org/2001/XMLSchema-instance\' xmlns:xsd=\'http://www.w3.org/2001/XMLSchema\' xmlns:soap=\'http://www.w3.org/2003/05/soap-envelope\'><soap:Body><SubmitSinglePaymentResponse xmlns=\'http://www.ippayments.com.au/interface/api/dts\'><SubmitSinglePaymentResult><Response> <ResponseCode>0</ResponseCode> <Timestamp>02-Jul-2014 11:07:08</Timestamp> <Receipt>10001197</Receipt> <SettlementDate>02-Jul-2014</SettlementDate> <DeclinedCode></DeclinedCode> <DeclinedMessage></DeclinedMessage> </Response></SubmitSinglePaymentResult></SubmitSinglePaymentResponse></soap:Body></soap:Envelope>';
        }
        else{*/
           resBody = submitAuthorizationSaleCall(reqBody);
       // }
         //resBody = submitAuthorizationSaleCall(reqBody);
         System.debug('Test.isRunningTest()---->'+Test.isRunningTest());
          /*if(Test.isRunningTest()){
            resBody = '<?xml version=\'1.0\' encoding=\'utf-8\'?><soap:Envelope xmlns:xsi=\'http://www.w3.org/2001/XMLSchema-instance\' xmlns:xsd=\'http://www.w3.org/2001/XMLSchema\' xmlns:soap=\'http://www.w3.org/2003/05/soap-envelope\'><soap:Body><SubmitSinglePaymentResponse xmlns=\'http://www.ippayments.com.au/interface/api/dts\'><SubmitSinglePaymentResult><Response> <ResponseCode>0</ResponseCode> <Timestamp>02-Jul-2014 11:07:08</Timestamp> <Receipt>10001197</Receipt> <SettlementDate>02-Jul-2014</SettlementDate> <DeclinedCode></DeclinedCode> <DeclinedMessage></DeclinedMessage> </Response></SubmitSinglePaymentResult></SubmitSinglePaymentResponse></soap:Body></soap:Envelope>';
        }*/
        return resBody;
        
    }
    
    public String markForCapture()//@throws PG_Exception
    {
        System.debug('CAPTURE PROCESS INVOKED************');
        String resBody='';
        String reqBody='<Capture>'+
                     '<Receipt>'+TxRefNum+'</Receipt>'+
                     '<Amount>'+amount+'</Amount>'+
                     '<Security>'+
                     '<UserName>'+config.loginId+'</UserName>'+
                     '<Password>'+config.password+'</Password>'+
                     '</Security>'+
                     '</Capture>'
                     ;
        /*if(Test.isRunningTest()){
            resBody = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><SubmitSingleCaptureResponse xmlns="http://www.ippayments.com.au/interface/api/dts"><SubmitSingleCaptureResult><Response> <ResponseCode>0</ResponseCode> <Timestamp>02-Jul-2014 11:07:08</Timestamp> <Receipt>88918350</Receipt> <SettlementDate>02-Jul-2014</SettlementDate> <DeclinedCode></DeclinedCode> <DeclinedMessage></DeclinedMessage> </Response></SubmitSingleCaptureResult></SubmitSingleCaptureResponse></soap:Body></soap:Envelope>';
        }
        else{*/
            resBody = submitCaptureCall(reqBody);
        //}
        return resBody;
        
    }
    
    public String refund()//@throws PG_Exception
    {
        System.debug('REFUND OR CREDIT PROCESS INVOKED************');
        String resBody='';
        String reqBody='<Refund>'+
                     '<Receipt>'+TxRefNum+'</Receipt>'+
                     '<Amount>'+amount+'</Amount>'+
                     '<Security>'+
                     '<UserName>'+config.loginId+'</UserName>'+
                     '<Password>'+config.password+'</Password>'+
                     '</Security>'+
                     '</Refund>'
                     ;
        /*if(Test.isRunningTest()){
            resBody='<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><SubmitSingleRefundResponse xmlns="http://www.ippayments.com.au/interface/api/dts"><SubmitSingleRefundResult><Response> <ResponseCode>0</ResponseCode> <Timestamp>02-Jul-2014 11:07:08</Timestamp> <Receipt>88918350</Receipt> <SettlementDate>02-Jul-2014</SettlementDate> <DeclinedCode></DeclinedCode> <DeclinedMessage></DeclinedMessage> </Response></SubmitSingleRefundResult></SubmitSingleRefundResponse></soap:Body></soap:Envelope>';
        }
        else{*/
            resBody = submitRefundCall(reqBody);
        //}
        return resBody;
        
    }
    
    public String authorizationCapture()//@throws PG_Exception
    {
        System.debug('SALE PROCESS INVOKED************');
        String resBody='';
        String reqBody='<Transaction>'+
                     '<CustNumber></CustNumber>'+
                     '<CustRef>'+CustRefNum+'</CustRef>'+
                     '<Amount>'+amount+'</Amount>'+
                     '<TrnType>1</TrnType>'+
                     '<CreditCard Registered=\'False\'>'+
                     '<CardNumber>'+details.creditCardNumber +'</CardNumber>'+
                     '<ExpM>'+details.expiryMonth +'</ExpM>'+
                     '<ExpY>'+details.expiryYear+'</ExpY>'+
                     '<CVN></CVN>'+
                     '<CardHolderName>'+details.ccFirstName+' '+details.ccLastName+'</CardHolderName>'+
                     '</CreditCard>'+
                     '<Security>'+
                     '<UserName>'+config.loginId+'</UserName>'+
                     '<Password>'+config.password+'</Password>'+
                     '</Security>'+
                     '<TrnSource></TrnSource>'+
                     '</Transaction>'
                     ;
        /*if(Test.isRunningTest()){
            resBody = '<?xml version=\'1.0\' encoding=\'utf-8\'?><soap:Envelope xmlns:xsi=\'http://www.w3.org/2001/XMLSchema-instance\' xmlns:xsd=\'http://www.w3.org/2001/XMLSchema\' xmlns:soap=\'http://www.w3.org/2003/05/soap-envelope\'><soap:Body><SubmitSinglePaymentResponse xmlns=\'http://www.ippayments.com.au/interface/api/dts\'><SubmitSinglePaymentResult><Response> <ResponseCode>0</ResponseCode> <Timestamp>02-Jul-2014 11:07:08</Timestamp> <Receipt>10001197</Receipt> <SettlementDate>02-Jul-2014</SettlementDate> <DeclinedCode></DeclinedCode> <DeclinedMessage></DeclinedMessage> </Response></SubmitSinglePaymentResult></SubmitSinglePaymentResponse></soap:Body></soap:Envelope>';
        }
        else{*/
            resBody = submitAuthorizationSaleCall(reqBody);
        //}
        return resBody;
        
    }
    
    public String reversal()//@throws PG_Exception
    {
        System.debug('VOID or CANCEL PROCESS INVOKED************');
        String resBody='';
        String reqBody='<Void>'+
                     '<Receipt>'+TxRefNum+'</Receipt>'+
                     '<Amount>'+amount+'</Amount>'+
                     '<Security>'+
                     '<UserName>'+config.loginId+'</UserName>'+
                     '<Password>'+config.password+'</Password>'+
                     '</Security>'+
                     '</Void>'
                     ;
        /*if(Test.isRunningTest()){
            resBody = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><SubmitSingleVoidResponse xmlns="http://www.ippayments.com.au/interface/api/dts"><SubmitSingleVoidResult><Response> <ResponseCode>0</ResponseCode> <Timestamp>02-Jul-2014 11:07:08</Timestamp> <Receipt>88918350</Receipt> <SettlementDate>02-Jul-2014</SettlementDate> <DeclinedCode></DeclinedCode> <DeclinedMessage></DeclinedMessage> </Response></SubmitSingleVoidResult></SubmitSingleVoidResponse></soap:Body></soap:Envelope>';
        }
        else{*/
            resBody = submitVoidCall(reqBody);
       // }
            return resBody;
        
    }
    
    public void populateResponse(Map<String,String> ip_response)
    {
        System.debug('populateResponse called--process>>'+ip_response);
        if(ip_response.get('ResponseCode').equals('0'))
        {
          pgResponse.processStatus =PG.Status.SUCCESS.name();
          pgResponse.transactionId = ip_response.get('Receipt');
          pgResponse.transactionTime =getTimeStamp(ip_response.get('Timestamp')) ;
          pgResponse.isException = false;
          //pgResponse.responseMessage = ip_response.get('DeclinedMessage');
          //pgResponse.responseCode = ip_response.get('DeclinedCode');
          //pgResponse.authcode=vantiv_response.get('authCode');
          pgResponse.approvalStatus = 'Approved';
        }
        else if(ip_response.get('ResponseCode').equals('1'))
        {
           pgResponse.processStatus=PG.Status.FAILURE.name();
           pgResponse.isException = true;
           pgResponse.responseMessage = ip_response.get('DeclinedMessage');
           pgResponse.responseCode = ip_response.get('DeclinedCode');
        }
        System.debug('pgResponse---'+pgResponse);
    }
    
    private Datetime getTimeStamp(String d)
    {
        string day1 = d.substring(0, 2);
        string month1 = d.substring(3, 6);
        string year1 = d.substring(7, 11);
        string hour1 = d.substring(12, 14);
        string minute1 = d.substring(15, 17);
        string second1 = d.substring(18,20);
        
        Map<String,Integer> month2=new Map<String,Integer>{'Jan' => 01, 'Feb' => 02, 'Mar' => 03, 'Apr' => 04, 'May' => 05,'Jun' => 06,'Jul' => 07,'Aug' => 08,'Sep' => 09,'Oct' => 10,'Nov' => 11,'Dec' => 12};
        
       
        Integer year, month, day, hour, minute, second;
        year = Integer.valueOf(year1);
        month = month2.get(month1);
        day = Integer.valueOf(day1);
        hour = Integer.valueOf(hour1);
        minute = Integer.valueOf(minute1);
        second = Integer.valueOf(second1);

        Datetime t1 = Datetime.newInstance(year, month, day, hour, minute, second);
        
        return t1;
    }
    
    
    private String submitAuthorizationSaleCall(String reqBody){//@throws PG_Exception
        String resBody = '';
        PG_IPPaymentsService.dtsSoap  obj=new PG_IPPaymentsService.dtsSoap();
        resBody=obj.SubmitSinglePayment(reqBody);
        pgResponse.resLog = resBody; 
        System.debug('resBody-->>'+resBody);   
        return resBody;
    }
    private String submitCaptureCall(String reqBody){//@throws PG_Exception
        String resBody = '';
        PG_IPPaymentsService.dtsSoap  obj=new PG_IPPaymentsService.dtsSoap();
        resBody=obj.SubmitSingleCapture(reqBody);
        pgResponse.resLog = resBody; 
        return resBody;
    }
    private String submitRefundCall(String reqBody){//@throws PG_Exception
        String resBody = '';
        System.Debug('$$$$$'+reqBody);
        PG_IPPaymentsService.dtsSoap  obj=new PG_IPPaymentsService.dtsSoap();
        resBody=obj.SubmitSingleRefund(reqBody);
        pgResponse.resLog = resBody; 
        return resBody;
    }
    private String submitVoidCall(String reqBody){//@throws PG_Exception
        String resBody = '';
        PG_IPPaymentsService.dtsSoap  obj=new PG_IPPaymentsService.dtsSoap();
        resBody=obj.SubmitSingleVoid(reqBody);
        pgResponse.resLog = resBody; 
        return resBody;
    }
}