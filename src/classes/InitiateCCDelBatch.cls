global with sharing class InitiateCCDelBatch
{ 
    webservice static string run_batch(integer batsize) 
    {
        try
        {
            list<ChikPeaO2B__O2B_Setting__c> o2bset=new list<ChikPeaO2B__O2B_Setting__c>();
            o2bset=[select ChikPeaO2B__Delete_Credit_Cards_older_by__c from ChikPeaO2B__O2B_Setting__c limit 1];
            if(o2bset.size()>0)
            {
                CCDelBatch btcdata=new CCDelBatch(o2bset[0].ChikPeaO2B__Delete_Credit_Cards_older_by__c);
                ID BatchId = Database.executeBatch(btcdata,batsize);
            }
        }
        catch(exception e)
        {
            return e.getmessage();
        }
        return 'Credit Card Deletion Batch Initiated. Please wait';
    }
}