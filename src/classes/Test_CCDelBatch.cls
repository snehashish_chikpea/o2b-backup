@isTest
private class Test_CCDelBatch {

    static testMethod void myUnitTest() {
      
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Save_Failed_Payment__c=true,ChikPeaO2B__Delete_Credit_Cards_older_by__c=14);
        insert(o2bset);
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly');
        insert(acc);
        List<ChikPeaO2B__Credit_Card__c>CCList=new List<ChikPeaO2B__Credit_Card__c>();
        ChikPeaO2B__Credit_Card__c cc=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st1',ChikPeaO2B__CC_City__c='city1',ChikPeaO2B__CC_Country__c='country1',ChikPeaO2B__CC_First_Name__c='FN1',ChikPeaO2B__CC_Last_Name__c='LN1',ChikPeaO2B__CC_Postal_Code__c='700076',ChikPeaO2B__CC_State__c='wb1',ChikPeaO2B__CC_Street2__c='strt2',ChikPeaO2B__Credit_Card_Number__c='5114',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=02,ChikPeaO2B__Expiry_Year__c=2016);
        CCList.add(cc);
        ChikPeaO2B__Credit_Card__c cc1=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st2',ChikPeaO2B__CC_City__c='city2',ChikPeaO2B__CC_Country__c='country2',ChikPeaO2B__CC_First_Name__c='FN2',ChikPeaO2B__CC_Last_Name__c='LN2',ChikPeaO2B__CC_Postal_Code__c='700077',ChikPeaO2B__CC_State__c='wb2',ChikPeaO2B__CC_Street2__c='strt3',ChikPeaO2B__Credit_Card_Number__c='0004',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=03,ChikPeaO2B__Expiry_Year__c=2013);
        CCList.add(cc1);
        ChikPeaO2B__Credit_Card__c cc2=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st3',ChikPeaO2B__CC_City__c='city3',ChikPeaO2B__CC_Country__c='country3',ChikPeaO2B__CC_First_Name__c='FN3',ChikPeaO2B__CC_Last_Name__c='LN3',ChikPeaO2B__CC_Postal_Code__c='700078',ChikPeaO2B__CC_State__c='wb3',ChikPeaO2B__CC_Street2__c='strt4',ChikPeaO2B__Credit_Card_Number__c='4444',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=12,ChikPeaO2B__Expiry_Year__c=2015);
        CCList.add(cc2);
        ChikPeaO2B__Credit_Card__c cc3=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st4',ChikPeaO2B__CC_City__c='city4',ChikPeaO2B__CC_Country__c='country4',ChikPeaO2B__CC_First_Name__c='FN4',ChikPeaO2B__CC_Last_Name__c='LN4',ChikPeaO2B__CC_Postal_Code__c='700079',ChikPeaO2B__CC_State__c='wb4',ChikPeaO2B__CC_Street2__c='strt5',ChikPeaO2B__Credit_Card_Number__c='5100',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=05,ChikPeaO2B__Expiry_Year__c=2020);
        CCList.add(cc3);
        insert(CCList);
        
        InitiateCCDelBatch.run_batch(200);
    }
    static testMethod void myUnitTest1() {
      
        InitiateCCDelBatch.run_batch(null);
    }
}