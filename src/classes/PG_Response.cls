/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-25
@see 
---------------------------------------------------------------------------------------------------
*/
global with sharing class PG_Response {

    global String paymentGatewayName { get; set; }
    global String transactionType { get; set; }
    global String reqLog { get; set; }
    global String resLog { get; set; }
    global Map<String, String> transactionRequestMap { get; set; }
    global Map<String, String> transactionResponseMap { get; set; }
    global String processStatus { get; set; }
    global String approvalStatus { get; set; }
    global String transactionId { get; set; }
    global Datetime transactionTime { get; set; }
    global String responseCode { get; set; }
    global String responseMessage { get; set; }
    global String exceptionCode { get; set; }
    global String exceptionMessage { get; set; }
    global Boolean isException { get; set; }
    
    global PG_Response(){
        reqLog = '';
        resLog = '';
        isException = false;
        exceptionMessage = '';
        transactionResponseMap = new Map<String, String>();
    }
    
    global String getRequestParamValue(String paramName){
        return transactionRequestMap.get(paramName);
    }
        
    global String getResponseParamValue(String paramName){
        return transactionResponseMap.get(paramName);
    }   

}