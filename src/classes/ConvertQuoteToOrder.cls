/*
** Class         :  ConvertQuoteToOrder
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason     :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

global with sharing class ConvertQuoteToOrder{
    Webservice static id quoteToOrder(List<id> qidList){
        system.debug('++++++++'+qidList);
        List<quote__c>qList=new List<quote__c>();
        List<Order__c>NewOrderList=new List<order__c>();
        Map<id,order__c>OrderMap=new Map<id,order__c>();
        List<order_line__c>NewOrderLineList=new List<order_line__c>();
        List<quote__c>UpdateQuote=new List<quote__c>();
        if(qidList!=null && qidList.size()>0){
            for(quote__c qt:[SELECT id,name,Account__c,PO_Number__c,Price_Book__c,Status__c,
            Tax_Rate__c,Contact__c,ChikPeaO2B__Contract_Term_Months__c,
            (SELECT id,name,Description__c,Item__c,Item_Type__c,Package__c,Quantity__c,Quote__c,
            ChikPeaO2B__Subscription_Term_Months__c,ChikPeaO2B__Subscription_start_date__c,
            Rate_Plan__c,Non_recurring_Charge__c,Recurring_Charge__c,Tax_Charge__c,Tax_Rate__c,
            Unit_Price__c,ChikPeaO2B__Free_Usage__c,ChikPeaO2B__Max_Usage__c,ChikPeaO2B__Min_Usage__c,ChikPeaO2B__Usage_Rate__c,ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Pricing_Type__c,ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Usage_Rate__c from quote_lines__r) from quote__c where id in:qidList])
                qList.add(qt);
        }
        if(qList!=null && qList.size()>0){
            for(quote__c qt:qList){
                order__c ord=new order__c(Account__c=qt.account__c,Contact__c=qt.Contact__c,PO_Number__c=qt.PO_Number__c,Price_Book__c=qt.Price_Book__c,Quote__c=qt.id,Status__c='Open',Tax_Rate__c=qt.Tax_Rate__c);
                NewOrderList.add(ord);        
                qt.Status__c='Accepted';
                UpdateQuote.add(qt);
            }
            try{
                if(NewOrderList!=null && NewOrderList.size()>0){
                    insert(NewOrderList);// Insert New Order
                if(UpdateQuote!=null && UpdateQuote.size()>0)
                    update(UpdateQuote);
                }
            }
            catch(Exception e){}    
            
            for(order__c ord:NewOrderList){
                OrderMap.put(ord.Quote__c,ord);    
            }
            
            for(quote__c qt:qList){
                if(qt.Quote_Lines__r!=null && qt.Quote_Lines__r.size()>0){
                    for(Quote_Line__c qtl:qt.Quote_Lines__r){
                        order_line__c orl=new order_line__c(Description__c=qtl.Description__c,Item__c=qtl.item__c,Item_Type__c=qtl.Item_Type__c,Order__c=OrderMap.get(qt.id).id,Quantity__c=qtl.Quantity__c,Quote_Line__c=qtl.id,Rate_Plan__c=qtl.Rate_Plan__c,Unit_Price__c=qtl.Unit_Price__c,Package__c=qtl.Package__c);
                        orl.ChikPeaO2B__Subscription_Term_Months__c=qtl.ChikPeaO2B__Subscription_Term_Months__c;
                        orl.ChikPeaO2B__Subscription_start_date__c=qtl.ChikPeaO2B__Subscription_start_date__c;
                        orl.ChikPeaO2B__Free_Usage__c=qtl.ChikPeaO2B__Free_Usage__c;
                        orl.ChikPeaO2B__Max_Usage__c=qtl.ChikPeaO2B__Max_Usage__c;
                        orl.ChikPeaO2B__Min_Usage__c=qtl.ChikPeaO2B__Min_Usage__c;
                        if(qtl.ChikPeaO2B__Usage_Rate__c!=null)
                        orl.ChikPeaO2B__Usage_Rate__c=qtl.ChikPeaO2B__Usage_Rate__c;
                        else
                        {
                             if(qtl.ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Pricing_Type__c=='Flat')
                             orl.ChikPeaO2B__Usage_Rate__c=qtl.ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Usage_Rate__c;
                        }
                        NewOrderLineList.add(orl);    
                    }    
                }
            }
            try{
                if(NewOrderLineList!=null && NewOrderLineList.size()>0)
                    insert(NewOrderLineList);// Insert New OrerLines
            }
            catch(Exception e){}
        }
        if(NewOrderList!=null && NewOrderList.size()==1)
            return NewOrderList[0].id;
        else
            return null;    
    }
}