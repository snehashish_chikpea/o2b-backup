/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-02-28
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
private class TestPG_WSOribitalElements {

    static testMethod void testWSOribitalElements() {
        
        Test.startTest();
        
        PG_WSOribitalElements.NewOrderRequestElement nReq = new PG_WSOribitalElements.NewOrderRequestElement();
        
        PG_WSOribitalElements.NewOrder_element nReqE = new PG_WSOribitalElements.NewOrder_element();
        nReqE.newOrderRequest = nReq; 
        
        PG_WSOribitalElements.NewOrderResponseElement nRes = new PG_WSOribitalElements.NewOrderResponseElement();
        
        PG_WSOribitalElements.NewOrderResponse_element nResE = new PG_WSOribitalElements.NewOrderResponse_element();
        nResE.return_x = nRes;
        
        PG_WSOribitalElements.MFCElement mReq= new PG_WSOribitalElements.MFCElement();
        
        PG_WSOribitalElements.MFC_element mReqE = new PG_WSOribitalElements.MFC_element();
        mReqE.mfcRequest = mReq;
        
        PG_WSOribitalElements.MFCResponseElement mRes = new PG_WSOribitalElements.MFCResponseElement();
        
        PG_WSOribitalElements.MFCResponse_element mResE = new PG_WSOribitalElements.MFCResponse_element();
        mResE.return_x = mRes;
        
        PG_WSOribitalElements.ReversalElement rReq= new PG_WSOribitalElements.ReversalElement();
        
        PG_WSOribitalElements.Reversal_element rReqE = new PG_WSOribitalElements.Reversal_element();
        rReqE.reversalRequest = rReq;
        
        PG_WSOribitalElements.ReversalResponseElement rRes = new PG_WSOribitalElements.ReversalResponseElement();
        
        PG_WSOribitalElements.ReversalResponse_element rResE = new PG_WSOribitalElements.ReversalResponse_element();
        rResE.return_x = rRes;
    
        Test.stopTest();
    }

}