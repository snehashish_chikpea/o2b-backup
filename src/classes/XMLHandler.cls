/*
** Class         :  XMLHandler
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

global with sharing class XMLHandler
{
    class Data
    {
        String type;
        String value;
        
        public Data()
        {}
    }
    
    //************************ XML Reader ****************************
    
    public static Map<String, String> parseDetails(String argDetails) 
    {
        Map<String, String> returnDetails = new Map<String, String>();
        try{
            XmlStreamReader reader = new XmlStreamReader(argDetails);
            
            while(reader.hasNext()) 
            {
                if(reader.getEventType() == XmlTag.START_ELEMENT) 
                {
                    if(reader.getLocalName() == 'Data') 
                    {
                        Data dt = parseData(reader);
                        returnDetails.put(dt.type, dt.value);
                    }
                }
                reader.next();
            }
        }
        catch(Exception ex)
        {
            
        }
        return returnDetails;
    }
    
    public static Data parseData(XmlStreamReader reader) 
    {
        Data dt = new Data();
        try
        {
            dt.type = reader.getAttributeValue(null, 'type');
            dt.value = '';
            while(reader.hasNext()) 
            {
                if(reader.getEventType() == XmlTag.END_ELEMENT && reader.getLocalName() == 'Data') 
                {
                    break;
                } 
                else if(reader.getEventType() == XmlTag.CHARACTERS) 
                {
                    dt.value += reader.getText();
                }
                else
                {
                    if(reader.getEventType() == XmlTag.END_ELEMENT)
                        dt.value += '</' + reader.getLocalName() + '>';
                    else if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() != 'Data')
                        dt.value += '<' + reader.getLocalName() + '>';
                }
                try
                {
                    reader.next();
                }
                catch(Exception ex)
                {
                    reader.next();
                }
            }
        }
        catch(Exception ex)
        {
            
        }
        return dt;
    }
    
    
    //************************ XML Writer ****************************
   
    public static String writeDetails(Map<String, String> contentMap)
    {
        System.debug('-------contentMap> '+contentMap);
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartDocument(null, '1.0');
            w.writeStartElement(null, 'OutputDetails', null);
                for(String type : contentMap.keySet())
                {
                    w.writeStartElement(null, 'Data', null);
                    w.writeAttribute(null, null, 'type', type);
                    if(type != null && contentMap.get(type) != null)
                    {
                    w.writeCharacters(contentMap.get(type));
                    }
                    w.writeEndElement();
                }
            w.writeEndElement();
        w.writeEndDocument();
        String xmlOutput = w.getXmlString();
        w.close();
        return xmlOutput;
    }
    
}