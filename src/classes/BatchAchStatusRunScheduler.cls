/*
** Class         :  BatchAchStatusRunScheduler
** Created by    :  Satya (chikpea Inc.)
** Last Modified :  25 July 2016
** Modified by   :  Satya (chikpea Inc.)
*/

global with sharing class BatchAchStatusRunScheduler
{
    Webservice static String executeschedule()
    {
        String error;
        String sch;        
        BatchAchStatusScheduler BASS=new BatchAchStatusScheduler();
        list<ChikPeaO2B__O2B_Setting__c>O2BsetList=[SELECT id,ChikPeaO2B__Scheduled_Time_for_Reporting_Batch__c from ChikPeaO2B__O2B_Setting__c limit 1];
        if(O2BsetList!=null && O2BsetList.size()>0)
        {
            if(O2BsetList[0].ChikPeaO2B__Scheduled_Time_for_Reporting_Batch__c!=null && O2BsetList[0].ChikPeaO2B__Scheduled_Time_for_Reporting_Batch__c!='')
            {
                List<String> ScheduleTime=O2BsetList[0].ChikPeaO2B__Scheduled_Time_for_Reporting_Batch__c.split(':');
                sch='0 '+ScheduleTime[1].trim()+' '+ScheduleTime[0].trim()+' * * ?';
                try
                {
                    system.schedule('BatchAchStatusScheduler',sch, BASS);
                    error='success';
                }
                catch(Exception E)
                {
                    error=E.getMessage();
                }
            }    
            else
            {
                    //sch='0 00 08 * * ?';
                    System.debug('Scheduled Time for Reporting Batch was not selected!');
                    error='Scheduled Time for Reporting Batch was not selected';
            }
            
            
        }
        return error;  
    }
}