global with sharing class BatchCollectionScheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        BatchCollection BC = new BatchCollection();
        List<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c from ChikPeaO2B__O2B_Setting__c limit 1];
        BC.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
        BC.query='select id,name,ChikPeaO2B__Collection_Email_Date__c,ChikPeaO2B__Past_Days__c'+
                   ' from ChikPeaO2B__Invoice__c where ';
        string wherecondition='';
        for(ChikPeaO2B__Dunning_Rule__c  dun: [select id,name,ChikPeaO2B__Account_Due__c,ChikPeaO2B__Invoice_Due__c,ChikPeaO2B__O2B_Setting__c,ChikPeaO2B__Past_Days__c from ChikPeaO2B__Dunning_Rule__c limit 99])           
        {
            wherecondition+=('ChikPeaO2B__Past_Days__c='+dun.ChikPeaO2B__Past_Days__c+' or ');
        }
        wherecondition=wherecondition.trim().removeendignorecase('or').trim();
        BC.query+=wherecondition;
        ID batchprocessid = Database.executeBatch(BC);  
    }
}