/*
** Class         :  CCGatewayConnector
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  24 march 14
** Modified by	 :  Asitm (chikpea Inc.)
** Reason		 :  Secuirty Issue fixed.(sharing)
** Description   :  .
*/


global with sharing class CCGatewayConnector
{
    /*
    public static PG_Response invoke(String paymentGatewayName,ChikPeaO2B__Credit_Card__c cc,PG.TransactionType transactionType,Map<String, String> tranParams)    
    {
         PG_Response pgResponse = null;
         try{
      		
      	  System.debug( '---paymentGatewayName='+paymentGatewayName);
      	  
          PG_PaymentGateway paymentGateway = PG_PaymentGatewayFactory.create(paymentGatewayName);
          PG_CCDetails details = new PG_CCDetails();
          details.ccCity = cc.CC_City__c;    
          details.ccCountry = cc.CC_Country__c;    
          details.ccFirstName = cc.CC_First_Name__c;    
          details.ccLastName = cc.CC_Last_Name__c;    
          details.ccPostalCode = cc.CC_Postal_Code__c;   
          details.ccState = cc.CC_State__c;    
          details.ccStreet1 = cc.CC_Street1__c;    
          details.ccStreet2 = cc.CC_Street2__c;    
          details.creditCardNumber = cc.Credit_Card_Number__c;    
          details.creditCardType = cc.Credit_Card_Type__c;
          Decimal expMon = cc.Expiry_Month__c;
          details.expiryMonth = expMon < 10?'0':'';  
          details.expiryMonth += String.valueOf(cc.Expiry_Month__c);    
          details.expiryYear = String.valueOf(cc.Expiry_Year__c); 
          
          System.debug( '---details='+details);    
          
          PG_Config config = new PG_Config(paymentGatewayName);
          
          System.debug( '---config='+config);
          
          paymentGateway.init(details, config, tranParams);
              
          PG_PaymentProcessor paymentProcessor = new PG_PaymentProcessor(paymentGateway);
          try{
            paymentProcessor.process(transactionType);
          }catch(PG_Exception ex){
            throw ex;
          }catch(Exception ex){
            throw ex;
          }finally{
            pgResponse = paymentProcessor.fetchResponse();
          }      
        }catch(PG_Exception ex){
          if(pgResponse == null){
            pgResponse = new PG_Response();
          }
          pgResponse.isException = true;
          if(ex.getErrorType()=='SOAPFault'){
            pgResponse.exceptionCode = ex.getErrorCode();
            pgResponse.exceptionMessage = ex.getErrorMesssage();
          }else{
            pgResponse.exceptionCode = 'SF001';
            pgResponse.exceptionMessage = ex.getMessage();
          }
        }catch(Exception ex){
          if(pgResponse == null){
            pgResponse = new PG_Response();
          }
          pgResponse.isException = true;
          pgResponse.exceptionMessage = ex.getMessage();
        }          
        System.debug('---pgResponse='+pgResponse);
        return pgResponse;    
      }
      */
      
      public static void SubscriptionUpdate(List<ChikPeaO2B__payment__c>paylist)
      {
          ChikPeaO2B__O2B_Setting__c[] o2bSet = [Select Id,ChikPeaO2B__Line_Payment__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Subscription_Payment_Status__c from ChikPeaO2B__O2B_Setting__c limit 1];
          list<ChikPeaO2B__Subscription__c>UpdatesubList=new list<ChikPeaO2B__Subscription__c>();
          if(o2bSet[0].ChikPeaO2B__Line_Payment__c)
          {
              set<id>invoicelineid=new set<id>();
              for(ChikPeaO2B__payment__c pay : paylist)
              {
                  if(pay.ChikPeaO2B__Card_Failure__c>=o2bset[0].ChikPeaO2B__Card_Process_Frequency__c && pay.ChikPeaO2B__Invoice_Line__c!=null)
                     invoicelineid.add(pay.ChikPeaO2B__Invoice_Line__c); 
              }
              set<id>subid=new set<id>();
              for(ChikPeaO2B__Invoice_Line__c invl : [SELECT id,name,ChikPeaO2B__Subscription__c from ChikPeaO2B__Invoice_Line__c where id in:invoicelineid and ChikPeaO2B__Subscription__c!=null])
              {
                  subid.add(invl.ChikPeaO2B__Subscription__c);    
              }
              for(ChikPeaO2B__Subscription__c sub : [SELECT id,name,ChikPeaO2B__Payment_Status__c from ChikPeaO2B__Subscription__c where id in:subid])
              {
                  sub.ChikPeaO2B__Payment_Status__c=o2bSet[0].ChikPeaO2B__Subscription_Payment_Status__c;
                  UpdatesubList.add(sub);
              }
          }
          else//line payment false
          {
              set<id>invoiceid=new set<id>();
              for(ChikPeaO2B__payment__c pay : paylist)
              {
                  if(pay.ChikPeaO2B__Card_Failure__c>=o2bset[0].ChikPeaO2B__Card_Process_Frequency__c && pay.ChikPeaO2B__Invoice__c!=null)
                     invoiceid.add(pay.ChikPeaO2B__Invoice__c); 
              }
              set<id>subid=new set<id>();
              for(ChikPeaO2B__Invoice_Line__c invl : [SELECT id,name,ChikPeaO2B__Subscription__c from ChikPeaO2B__Invoice_Line__c where ChikPeaO2B__Invoice__c in:invoiceid and ChikPeaO2B__Subscription__c!=null])
              {
                  subid.add(invl.ChikPeaO2B__Subscription__c);    
              }
              for(ChikPeaO2B__Subscription__c sub : [SELECT id,name,ChikPeaO2B__Payment_Status__c from ChikPeaO2B__Subscription__c where id in:subid])
              {
                  sub.ChikPeaO2B__Payment_Status__c=o2bSet[0].ChikPeaO2B__Subscription_Payment_Status__c;
                  UpdatesubList.add(sub);
              }
          }
          try{
              if(UpdatesubList!=null && UpdatesubList.size()>0)
                  update(UpdatesubList);
          }
          catch(exception e){}
      }
}