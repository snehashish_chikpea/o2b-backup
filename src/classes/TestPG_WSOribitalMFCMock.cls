/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-05-03
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
global class TestPG_WSOribitalMFCMock implements WebServiceMock {
  
  global void doInvoke(Object stub,
                 Object request,
                 Map<String, Object> response,
                 String endpoint,
                 String soapAction,
                 String requestName,
                 String responseNS,
                 String responseName,
                 String responseType
  ){
       PG_WSOribitalElements.MFCResponse_element respElement =  
           new PG_WSOribitalElements.MFCResponse_element();
    
      PG_WSOribitalElements.MFCResponseElement mfcRespElement =
        new PG_WSOribitalElements.MFCResponseElement();
        
      mfcRespElement.version = '2.7';
      mfcRespElement.bin = '000001';
      mfcRespElement.merchantID = '206734';
      mfcRespElement.terminalID = '001';
      mfcRespElement.orderID = 'O-61B2';
      mfcRespElement.txRefNum = '5267C51034CCEC321A2AEB9F089477A7D35654B1';
      mfcRespElement.txRefIdx = '1';
      mfcRespElement.splitTxRefIdx = '';
      mfcRespElement.amount = '10000';
      mfcRespElement.respDateTime = '20131023092201';
      mfcRespElement.procStatus = '0';
      mfcRespElement.procStatusMessage = '';
      mfcRespElement.approvalStatus = '1';
      mfcRespElement.respCode = '00';
      mfcRespElement.avsRespCode = 'B ';
      mfcRespElement.authorizationCode = 'tst43E';
      mfcRespElement.respCodeMessage = '';
      mfcRespElement.hostRespCode = '100';
      mfcRespElement.hostAVSRespCode = 'I3';
      mfcRespElement.retryTrace = '';
      mfcRespElement.retryAttempCount = '';
      mfcRespElement.lastRetryDate = '';
        
      respElement.return_x = mfcRespElement;
           
           response.put('response_x', respElement); 
     }
  
}