/*
** Class            :  ClearBatchLogBatch
** Created by       :  Snehashish Roy (Chikpea Inc.)
** Last Modified    :  23 February 2016 (Created)
** Description      :  Batch to clear all the BatchLogs
*/
global with sharing class ClearBatchLogBatch implements Database.Batchable<Sobject>,Database.Stateful{
    global String query;
    global ClearBatchLogBatch(){
        if(!Test.isRunningTest())
            query = 'select id from ChikPeaO2B__BatchLog__c where createddate!=Today';
        else
            query = 'select id from ChikPeaO2B__BatchLog__c';
    }
    global database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Sobject> scope){
        Savepoint sp = Database.setSavepoint();
        list<ChikPeaO2B__BatchLog__c> btchlogs=new list<ChikPeaO2B__BatchLog__c>();
        System.debug('scope size='+scope.size());
        for(sObject s:scope)
        {
            ChikPeaO2B__BatchLog__c btchlog = (ChikPeaO2B__BatchLog__c) s;
            if(btchlog.id!=null)
                btchlogs.add(btchlog);
        }
        if(btchlogs.size()>0)
        {   
            try
            {
                delete btchlogs;
            }
            catch(Exception e){}
        }
    }
    global void finish(Database.BatchableContext BC){
       
    }
}