global with sharing class PaymentStaging{
    public static boolean hasError;
    /*
        This method process Paymment Staging with Account ref. 
        Does not create any adavance payment if ammount is more than due. Extra amount remains in Payment Stg. 
    */
    public static void PaymentStagingToInvoicePayment(List<ChikPeaO2B__Invoice__c>InvoiceList,boolean linepay)
    {
        hasError=false;
        List<ChikPeaO2B__Payment__c>InsertPay=new List<ChikPeaO2B__Payment__c>();
        if(linepay)
        {
            Map<id,List<ChikPeaO2B__Payment__c>>NonLinePayMap=new Map<id,List<ChikPeaO2B__Payment__c>>();
            Map<id,set<id>>InvLinePayMap=new Map<id,set<id>>();
            for(ChikPeaO2B__Payment__c LinePayList : [select id,name,ChikPeaO2B__Invoice__c,ChikPeaO2B__Invoice_Line__c from ChikPeaO2B__Payment__c where ChikPeaO2B__Invoice__c in : InvoiceList])//Line Paymet
            {
                System.debug('#****LinePayList.ChikPeaO2B__Invoice_Line__c='+LinePayList.ChikPeaO2B__Invoice_Line__c);
                if(LinePayList.ChikPeaO2B__Invoice_Line__c==null)
                {
                    if(!NonLinePayMap.containskey(LinePayList.ChikPeaO2B__Invoice__c))
                    {
                        List<ChikPeaO2B__Payment__c>temp=new List<ChikPeaO2B__Payment__c>();
                        temp.add(LinePayList);
                        NonLinePayMap.put(LinePayList.ChikPeaO2B__Invoice__c,temp);
                    }
                    else
                    {
                        List<ChikPeaO2B__Payment__c>temp=NonLinePayMap.get(LinePayList.ChikPeaO2B__Invoice__c);
                        temp.add(LinePayList);
                        NonLinePayMap.put(LinePayList.ChikPeaO2B__Invoice__c,temp);
                    }
                }
                else
                {
                    if(!InvLinePayMap.containskey(LinePayList.ChikPeaO2B__Invoice__c))
                    {
                        set<id>invlid=new set<id>();
                        invlid.add(LinePayList.ChikPeaO2B__Invoice_Line__c);
                        InvLinePayMap.put(LinePayList.ChikPeaO2B__Invoice__c,invlid);
                    }
                    else
                    {
                        set<id>invlid=InvLinePayMap.get(LinePayList.ChikPeaO2B__Invoice__c);
                        invlid.add(LinePayList.ChikPeaO2B__Invoice_Line__c);
                        InvLinePayMap.put(LinePayList.ChikPeaO2B__Invoice__c,invlid);
                    }    
                }
            }
            
            Map<id,List<ChikPeaO2B__Invoice_Line__c>>InvlLineMap=new Map<id,List<ChikPeaO2B__Invoice_Line__c>>();
            set<string>accnumber=new set<string>();
            for(ChikPeaO2B__Invoice_Line__c invl : [Select id,name,ChikPeaO2B__Invoice__c,ChikPeaO2B__Invoice__r.ChikPeaO2B__Account__r.AccountNumber,Chikpeao2b__Line_Rate__c from ChikPeaO2B__Invoice_Line__c where ChikPeaO2B__Invoice__c in : InvoiceList])
            {
                if(!InvlLineMap.containskey(invl.ChikPeaO2B__Invoice__c))
                {
                    List<ChikPeaO2B__Invoice_Line__c>temp=new List<ChikPeaO2B__Invoice_Line__c>();
                    temp.add(invl);
                    InvlLineMap.put(invl.ChikPeaO2B__Invoice__c,temp);
                }
                else
                {
                    List<ChikPeaO2B__Invoice_Line__c>temp=InvlLineMap.get(invl.ChikPeaO2B__Invoice__c);
                    temp.add(invl);
                    InvlLineMap.put(invl.ChikPeaO2B__Invoice__c,temp);
                }       
                accnumber.add(invl.ChikPeaO2B__Invoice__r.ChikPeaO2B__Account__r.AccountNumber);
            }
            
            List<ChikPeaO2B__Payment_Staging__c>UpdateStagingList=new List<ChikPeaO2B__Payment_Staging__c>();
            Map<string,List<ChikPeaO2B__Payment_Staging__c>>PayStgMap=new Map<string,List<ChikPeaO2B__Payment_Staging__c>>();
            for(ChikPeaO2B__Payment_Staging__c paystg : [select id,name,ChikPeaO2B__Status__c,ChikPeaO2B__Account_Reference__c,ChikPeaO2B__Amount__c,ChikPeaO2B__Used_Amount__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Payment_Date__c,ChikPeaO2B__Check_Ref__c from ChikPeaO2B__Payment_Staging__c where ChikPeaO2B__Status__c!='Error' and (ChikPeaO2B__Account_Reference__c in : accnumber or ChikPeaO2B__Account_Reference__c=null)])
            {
                if(paystg.ChikPeaO2B__Account_Reference__c!=null)
                {
                    if(paystg.ChikPeaO2B__Used_Amount__c<paystg.ChikPeaO2B__Amount__c)    
                    {
                        if(!PayStgMap.containskey(paystg.ChikPeaO2B__Account_Reference__c))
                        {
                            List<ChikPeaO2B__Payment_Staging__c>temp=new List<ChikPeaO2B__Payment_Staging__c>();
                            temp.add(paystg);
                            PayStgMap.put(paystg.ChikPeaO2B__Account_Reference__c,temp);
                        }
                        else
                        {
                            List<ChikPeaO2B__Payment_Staging__c>temp=PayStgMap.get(paystg.ChikPeaO2B__Account_Reference__c);
                            temp.add(paystg);
                            PayStgMap.put(paystg.ChikPeaO2B__Account_Reference__c,temp);
                        }    
                    }
                }
                else
                {
                    paystg.ChikPeaO2B__Status__c='Error';
                    UpdateStagingList.add(paystg);
                }
            }
            
            for(ChikPeaO2B__Invoice__c inv: InvoiceList)
            {
                if((NonLinePayMap.get(inv.id)==null || NonLinePayMap.get(inv.id).size()==0) && (InvlLineMap.get(inv.id)!=null && InvlLineMap.get(inv.id).size()>0) && (inv.ChikPeaO2B__Account__r.AccountNumber!=null && PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber)!=null && PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber).size()>0))//no non line payment exist,eligible for line payment
                {
                    //List<ChikPeaO2B__Payment_Staging__c>RelatedPayStg=PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber);
                    for(ChikPeaO2B__Invoice_Line__c invl : InvlLineMap.get(inv.id))
                    {
                        if((InvLinePayMap.get(inv.id)==null || (!InvLinePayMap.get(inv.id).contains(invl.id))) && invl.Chikpeao2b__Line_Rate__c>0)//invoice line Not already paid and line rate>0
                        {
                            decimal lineratedue=invl.Chikpeao2b__Line_Rate__c;
                            if(inv.ChikPeaO2B__Account__r.AccountNumber!=null && PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber)!=null){
                            for(ChikPeaO2B__Payment_Staging__c paystg : PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber)/*RelatedPayStg*/)
                            {
                                system.debug('~~~~'+invl.name+'~~~'+paystg.name+'~~~'+paystg.ChikPeaO2B__Amount__c+'~~~~'+paystg.ChikPeaO2B__Used_Amount__c);
                                if(lineratedue>0)
                                {
                                    if((paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c)>0)
                                    {
                                        system.debug('####'+invl.name+'###'+paystg.name+'###'+(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c));
                                        if((paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c)>lineratedue)
                                        {
                                            ChikPeaO2B__Payment__c payment=new ChikPeaO2B__Payment__c();
                                            payment.ChikPeaO2B__Account__c=inv.ChikPeaO2B__Account__c;
                                            payment.ChikPeaO2B__Has_Processed__c=true;
                                            payment.ChikPeaO2B__Invoice__c=inv.id;
                                            payment.ChikPeaO2B__Payment_Amount__c=lineratedue;
                                            payment.ChikPeaO2B__payment_method__c = paystg.ChikPeaO2B__Payment_Method__c;
                                            if(paystg.ChikPeaO2B__Payment_Method__c=='Check')
                                            {
                                               payment.ChikPeaO2B__Check_Date__c= paystg.ChikPeaO2B__Payment_Date__c;
                                               payment.ChikPeaO2B__Check_Number__c=paystg.ChikPeaO2B__Check_Ref__c;
                                            }
                                            payment.ChikPeaO2B__status__c = 'Processed';
                                            //payment.ChikPeaO2B__payment_gateway__c = gateway;
                                            payment.ChikPeaO2B__Invoice_Line__c = invl.Id; 
                                            payment.ChikPeaO2B__Payment_Date__c =date.today(); //inv.ChikPeaO2B__Due_Date__c;
                                            payment.ChikPeaO2B__Payment_Staging__c=paystg.id;
                                            InsertPay.add(payment);
                                            paystg.ChikPeaO2B__Used_Amount__c+=lineratedue;
                                            lineratedue=0;
                                            paystg.ChikPeaO2B__Status__c='Uploaded';
                                        }
                                        else//(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c)<lineratedue
                                        {
                                            ChikPeaO2B__Payment__c payment=new ChikPeaO2B__Payment__c();
                                            payment.ChikPeaO2B__Account__c=inv.ChikPeaO2B__Account__c;
                                            payment.ChikPeaO2B__Has_Processed__c=true;
                                            payment.ChikPeaO2B__Invoice__c=inv.id;
                                            payment.ChikPeaO2B__Payment_Amount__c=(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c);
                                            payment.ChikPeaO2B__payment_method__c = paystg.ChikPeaO2B__Payment_Method__c;
                                            if(paystg.ChikPeaO2B__Payment_Method__c=='Check')
                                            {
                                               payment.ChikPeaO2B__Check_Date__c= paystg.ChikPeaO2B__Payment_Date__c;
                                               payment.ChikPeaO2B__Check_Number__c=paystg.ChikPeaO2B__Check_Ref__c;
                                            }
                                            payment.ChikPeaO2B__status__c = 'Processed';
                                            //payment.ChikPeaO2B__payment_gateway__c = gateway;
                                            payment.ChikPeaO2B__Invoice_Line__c = invl.Id; 
                                            payment.ChikPeaO2B__Payment_Date__c = date.today();//inv.ChikPeaO2B__Due_Date__c;
                                            payment.ChikPeaO2B__Payment_Staging__c=paystg.id;
                                            InsertPay.add(payment);
                                            lineratedue-=(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c);
                                            paystg.ChikPeaO2B__Used_Amount__c+=(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c);
                                            paystg.ChikPeaO2B__Status__c='Uploaded';
                                        }
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                            }
                        }//invoice line not already paid and line rate>0 if end    
                    }//invoice line loop end 
                    //PayStgMap.put(inv.ChikPeaO2B__Account__r.AccountNumber,RelatedPayStg);   
                }//no non line payment does not exist if end
            }//invoice loop end
            if(InsertPay!=null && InsertPay.size()>0)
            {
                list<Database.SaveResult> InserPaymentResult=database.insert(InsertPay,false);
                ExceptionLogger.insertLog(InserPaymentResult, InsertPay, 'Dml Exception in Class PaymentStaging for list InsertPay','ChikPeaO2B__Invoice__c');                
                for (Database.SaveResult sr : InserPaymentResult) {
                    if (!sr.isSuccess()) {
                        hasError=true;
                        break;    
                    }
                }
            }
            for(List<ChikPeaO2B__Payment_Staging__c> pstglist : PayStgMap.values())
            {
                UpdateStagingList.addAll(pstglist);
            }
            if(UpdateStagingList!=null && UpdateStagingList.size()>0)
            {
                list<Database.SaveResult> UpdatePaymentStagingResult=database.update(UpdateStagingList,false);
                ExceptionLogger.insertLog(UpdatePaymentStagingResult, UpdateStagingList, 'Dml Exception in Class PaymentStaging for list UpdateStagingList','id');
            }
        }//line pay if end 
        else//##### not line pay
        {
            set<string>accnumber=new set<string>();
            for(ChikPeaO2B__Invoice__c inv: InvoiceList)
            {
                accnumber.add(inv.ChikPeaO2B__Account__r.AccountNumber);    
            }
            
            List<ChikPeaO2B__Payment_Staging__c>UpdateStagingList=new List<ChikPeaO2B__Payment_Staging__c>();
            Map<string,List<ChikPeaO2B__Payment_Staging__c>>PayStgMap=new Map<string,List<ChikPeaO2B__Payment_Staging__c>>();
            for(ChikPeaO2B__Payment_Staging__c paystg : [select id,name,ChikPeaO2B__Status__c,ChikPeaO2B__Account_Reference__c,ChikPeaO2B__Amount__c,ChikPeaO2B__Used_Amount__c,ChikPeaO2B__Payment_Method__c,ChikPeaO2B__Payment_Date__c,ChikPeaO2B__Check_Ref__c from ChikPeaO2B__Payment_Staging__c where ChikPeaO2B__Status__c!='Error' and (ChikPeaO2B__Account_Reference__c in : accnumber or ChikPeaO2B__Account_Reference__c=null)])
            {
                if(paystg.ChikPeaO2B__Account_Reference__c!=null)
                {
                    if(paystg.ChikPeaO2B__Used_Amount__c<paystg.ChikPeaO2B__Amount__c)    
                    {
                        if(!PayStgMap.containskey(paystg.ChikPeaO2B__Account_Reference__c))
                        {
                            List<ChikPeaO2B__Payment_Staging__c>temp=new List<ChikPeaO2B__Payment_Staging__c>();
                            temp.add(paystg);
                            PayStgMap.put(paystg.ChikPeaO2B__Account_Reference__c,temp);
                        }
                        else
                        {
                            List<ChikPeaO2B__Payment_Staging__c>temp=PayStgMap.get(paystg.ChikPeaO2B__Account_Reference__c);
                            temp.add(paystg);
                            PayStgMap.put(paystg.ChikPeaO2B__Account_Reference__c,temp);
                        }    
                    }
                }
                else
                {
                    paystg.ChikPeaO2B__Status__c='Error';
                    UpdateStagingList.add(paystg);
                }
            }
            
            for(ChikPeaO2B__Invoice__c inv: InvoiceList)
            {
                if(inv.ChikPeaO2B__Amount_Due_Remaining__c>0)
                {
                    decimal lnvdue=inv.ChikPeaO2B__Amount_Due_Remaining__c;
                    //List<ChikPeaO2B__Payment_Staging__c>RelatedPayStg=PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber);
                    system.debug('!!!'+inv.ChikPeaO2B__Account__r.AccountNumber+'!!!!'+PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber));
                    if(inv.ChikPeaO2B__Account__r.AccountNumber!=null && PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber)!=null){
                    for(ChikPeaO2B__Payment_Staging__c paystg : PayStgMap.get(inv.ChikPeaO2B__Account__r.AccountNumber)/*RelatedPayStg*/)
                    {
                        system.debug('~~~~'+inv.name+'~~~'+paystg.name+'~~~'+paystg.ChikPeaO2B__Amount__c+'~~~~'+paystg.ChikPeaO2B__Used_Amount__c);
                        if(lnvdue>0)
                        {
                            if((paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c)>0)
                            {
                                system.debug('####'+inv.name+'###'+paystg.name+'###'+(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c));
                                if((paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c)>lnvdue)
                                {
                                    ChikPeaO2B__Payment__c payment=new ChikPeaO2B__Payment__c();
                                    payment.ChikPeaO2B__Account__c=inv.ChikPeaO2B__Account__c;
                                    payment.ChikPeaO2B__Has_Processed__c=true;
                                    payment.ChikPeaO2B__Invoice__c=inv.id;
                                    payment.ChikPeaO2B__Payment_Amount__c=lnvdue;
                                    payment.ChikPeaO2B__payment_method__c = paystg.ChikPeaO2B__Payment_Method__c;
                                    if(paystg.ChikPeaO2B__Payment_Method__c=='Check')
                                    {
                                            payment.ChikPeaO2B__Check_Date__c= paystg.ChikPeaO2B__Payment_Date__c;
                                            payment.ChikPeaO2B__Check_Number__c=paystg.ChikPeaO2B__Check_Ref__c;
                                    }
                                    payment.ChikPeaO2B__status__c = 'Processed';
                                    //payment.ChikPeaO2B__payment_gateway__c = gateway;
                                    //payment.ChikPeaO2B__Invoice_Line__c = invl.Id; 
                                    payment.ChikPeaO2B__Payment_Date__c = date.today();//inv.ChikPeaO2B__Due_Date__c;
                                    payment.ChikPeaO2B__Payment_Staging__c=paystg.id;
                                    InsertPay.add(payment);
                                    paystg.ChikPeaO2B__Used_Amount__c+=lnvdue;
                                    lnvdue=0;
                                    paystg.ChikPeaO2B__Status__c='Uploaded';
                                }
                                else//(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c)<lnvdue
                                {
                                    ChikPeaO2B__Payment__c payment=new ChikPeaO2B__Payment__c();
                                    payment.ChikPeaO2B__Account__c=inv.ChikPeaO2B__Account__c;
                                    payment.ChikPeaO2B__Has_Processed__c=true;
                                    payment.ChikPeaO2B__Invoice__c=inv.id;
                                    payment.ChikPeaO2B__Payment_Amount__c=(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c);
                                    payment.ChikPeaO2B__payment_method__c = paystg.ChikPeaO2B__Payment_Method__c;
                                    if(paystg.ChikPeaO2B__Payment_Method__c=='Check')
                                    {
                                               payment.ChikPeaO2B__Check_Date__c= paystg.ChikPeaO2B__Payment_Date__c;
                                               payment.ChikPeaO2B__Check_Number__c=paystg.ChikPeaO2B__Check_Ref__c;
                                    }
                                    payment.ChikPeaO2B__status__c = 'Processed';
                                    //payment.ChikPeaO2B__payment_gateway__c = gateway;
                                    //payment.ChikPeaO2B__Invoice_Line__c = invl.Id; 
                                    payment.ChikPeaO2B__Payment_Date__c = date.today();//inv.ChikPeaO2B__Due_Date__c;
                                    payment.ChikPeaO2B__Payment_Staging__c=paystg.id;
                                    InsertPay.add(payment);
                                    lnvdue-=(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c);
                                    paystg.ChikPeaO2B__Used_Amount__c+=(paystg.ChikPeaO2B__Amount__c-paystg.ChikPeaO2B__Used_Amount__c);
                                    paystg.ChikPeaO2B__Status__c='Uploaded';
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }//payment staging loop end
                    }
                    //PayStgMap.put(inv.ChikPeaO2B__Account__r.AccountNumber,RelatedPayStg);
                }//amount due remaining>0 if end   
            }//Invoice loop end
            if(InsertPay!=null && InsertPay.size()>0)
            {
                list<Database.SaveResult> InserPaymentResult=database.insert(InsertPay,false);
                ExceptionLogger.insertLog(InserPaymentResult, InsertPay, 'Dml Exception in Class PaymentStaging for list InsertPay','ChikPeaO2B__Invoice__c');                
                for (Database.SaveResult sr : InserPaymentResult) {
                    if (!sr.isSuccess()) {
                        hasError=true;
                        break;    
                    }
                }
            }
            for(List<ChikPeaO2B__Payment_Staging__c> pstglist : PayStgMap.values())
            {
                UpdateStagingList.addAll(pstglist);
            }
            if(UpdateStagingList!=null && UpdateStagingList.size()>0)
            {
                list<Database.SaveResult> UpdatePaymentStagingResult=database.update(UpdateStagingList,false);
                ExceptionLogger.insertLog(UpdatePaymentStagingResult, UpdateStagingList, 'Dml Exception in Class PaymentStaging for list UpdateStagingList','id');
            }
        }//non line payment else end      
    }//method end
    
   
    /*
        Method added by Mehebub Hossain (chikpea Inc.)
        On 23-Feb-15    
        *This method process Paymment Staging with Invoice ref. 
        *If ammount is more than due, it create extra amount as Advance Payment
        *linepay is don't care 
    */ 
    
      public static void PaymentStagingToInvoicePayment(List<ChikPeaO2B__Payment_Staging__c> stg_list, boolean linepay){
        Set<String> inf_ref_set=new Set<String>();
        System.debug('#****stg_list='+stg_list);
        for(ChikPeaO2B__Payment_Staging__c stg:stg_list){
            inf_ref_set.add(stg.ChikPeaO2B__Invoice_Ref__c);
        }
        
        List<ChikPeaO2B__Invoice_Line__c> inv_line_list = new List<ChikPeaO2B__Invoice_Line__c>();
                
        inv_line_list=[select Id, Name, ChikPeaO2B__Line_Rate__c,ChikPeaO2B__Line_tax__c,
            ChikPeaO2B__Invoice__c,ChikPeaO2B__Invoice__r.ChikPeaO2B__Account__c,
            (select Id, Name, ChikPeaO2B__Payment_Amount__c from ChikPeaO2B__Payments__r)
                from ChikPeaO2B__Invoice_Line__c where Name IN:inf_ref_set];
        
        Map<String, ChikPeaO2B__Invoice_Line__c> inv_line_ref_inv=new Map<String, ChikPeaO2B__Invoice_Line__c>();
        for(ChikPeaO2B__Invoice_Line__c inv_line:inv_line_list){
            inv_line_ref_inv.put(inv_line.Name, inv_line);
        }
            
        List<ChikPeaO2B__Payment__c> pmt_list_inv_line=new List<ChikPeaO2B__Payment__c>();
        List<ChikPeaO2B__Advance_payment__c> adv_pmt_list=new List<ChikPeaO2B__Advance_payment__c>();
        //attempt to shearch invoice line and create payment
        for(ChikPeaO2B__Payment_Staging__c stg:stg_list){
            if(inv_line_ref_inv.containsKey(stg.ChikPeaO2B__Invoice_Ref__c)){   
                ChikPeaO2B__Invoice_Line__c inv_line=inv_line_ref_inv.get(stg.ChikPeaO2B__Invoice_Ref__c);
                Decimal stg_amt_remaining=stg.ChikPeaO2B__Amount__c-stg.ChikPeaO2B__Used_Amount__c;
                Decimal inv_line_due=0;
                Decimal existing_pmt_sum=0;
                for(ChikPeaO2B__Payment__c existing_pmt:inv_line.ChikPeaO2B__Payments__r){
                    existing_pmt_sum+=existing_pmt.ChikPeaO2B__Payment_Amount__c;
                }
                inv_line_due=inv_line.ChikPeaO2B__Line_Rate__c - existing_pmt_sum + (inv_line.ChikPeaO2B__Line_tax__c != null ? inv_line.ChikPeaO2B__Line_tax__c : 0.00);
                system.debug('DJ==>Invlines Name->'+inv_line.name+' '+inv_line_due);
                if(inv_line_due>0&&stg_amt_remaining>0){
                    ChikPeaO2B__Payment__c payment=createPayment(
                        new ChikPeaO2B__Invoice__c(Id=inv_line.ChikPeaO2B__Invoice__c, 
                                ChikPeaO2B__Account__c=inv_line.ChikPeaO2B__Invoice__r.ChikPeaO2B__Account__c), stg);                   
                    if(inv_line_due<stg_amt_remaining){ 
                        payment.ChikPeaO2B__Payment_Amount__c=inv_line_due; 
                        payment.ChikPeaO2B__Invoice_Line__c=inv_line.Id; 
                        stg.ChikPeaO2B__Used_Amount__c+=inv_line_due; 
                        stg_amt_remaining=stg.ChikPeaO2B__Amount__c-stg.ChikPeaO2B__Used_Amount__c;
                        ChikPeaO2B__Advance_payment__c adv_payment
                                =createAdvancePayment(new ChikPeaO2B__Invoice__c(Id=inv_line.ChikPeaO2B__Invoice__c, 
                                ChikPeaO2B__Account__c=inv_line.ChikPeaO2B__Invoice__r.ChikPeaO2B__Account__c), stg, 
                                    stg_amt_remaining);
                        stg.ChikPeaO2B__Used_Amount__c+=stg_amt_remaining;
                        adv_pmt_list.add(adv_payment);                      
                    }else if(inv_line_due>stg_amt_remaining){
                        payment.ChikPeaO2B__Payment_Amount__c=stg_amt_remaining; 
                        payment.ChikPeaO2B__Invoice_Line__c=inv_line.Id;
                        stg.ChikPeaO2B__Used_Amount__c+=stg_amt_remaining; 
                    }else{
                        payment.ChikPeaO2B__Payment_Amount__c=stg_amt_remaining; 
                        payment.ChikPeaO2B__Invoice_Line__c=inv_line.Id;
                        stg.ChikPeaO2B__Used_Amount__c+=stg_amt_remaining; 
                    }
                    pmt_list_inv_line.add(payment);
                }
                else if(inv_line_due <=0 && stg_amt_remaining>0){
                    ChikPeaO2B__Advance_payment__c adv_payment =createAdvancePayment(new ChikPeaO2B__Invoice__c(Id=inv_line.ChikPeaO2B__Invoice__c, 
                                ChikPeaO2B__Account__c=inv_line.ChikPeaO2B__Invoice__r.ChikPeaO2B__Account__c), stg, 
                                    stg_amt_remaining);
                    stg.ChikPeaO2B__Used_Amount__c+=stg_amt_remaining;
                    adv_pmt_list.add(adv_payment); 
                    system.debug('DJ Full advance'+stg+' '+adv_payment); 
                }
            }
        }
        //System.debug('#****pmt_list_inv_line='+pmt_list_inv_line);
        if(pmt_list_inv_line.size() > 0){
            insert pmt_list_inv_line;
        }
        
        List<ChikPeaO2B__Payment__c> pmt_list_inv=new List<ChikPeaO2B__Payment__c>();
        
        
        List<ChikPeaO2B__Invoice__c> inv_list=[select Id, Name, ChikPeaO2B__Amount_Due__c,ChikPeaO2B__Account__c,ChikPeaO2B__Amount_Due_Remaining__c,
        ChikPeaO2B__Invoice_Status__c,
            (select Id, Name, ChikPeaO2B__Line_Rate__c,ChikPeaO2B__Line_tax__c  from ChikPeaO2B__Invoice_Lines__r),
            (select id,name,ChikPeaO2B__Invoice_Line__c,ChikPeaO2B__Payment_Amount__c from ChikPeaO2B__Payments__r where ChikPeaO2B__Has_Processed__c = true and ChikPeaO2B__Status__c = 'Processed'  )
                from ChikPeaO2B__Invoice__c where Name IN:inf_ref_set];

        Map<String, ChikPeaO2B__Invoice__c> inv_ref_inv=new Map<String, ChikPeaO2B__Invoice__c>();
        for(ChikPeaO2B__Invoice__c inv:inv_list){
            inv_ref_inv.put(inv.Name, inv);
        }   
        
        for(ChikPeaO2B__Payment_Staging__c stg:stg_list){
            if(stg.ChikPeaO2B__Used_Amount__c==null)
            stg.ChikPeaO2B__Used_Amount__c=0;
            
            Decimal stg_amt_remaining=stg.ChikPeaO2B__Amount__c-stg.ChikPeaO2B__Used_Amount__c;
            ChikPeaO2B__Invoice__c inv = inv_ref_inv.get(stg.ChikPeaO2B__Invoice_Ref__c);
            System.debug('#****stg_amt_remaining='+stg_amt_remaining);
            if(inv_ref_inv.containsKey(stg.ChikPeaO2B__Invoice_Ref__c)&&inv.ChikPeaO2B__invoice_status__c!='Paid'&&stg_amt_remaining>0 && inv.ChikPeaO2B__Amount_Due__c>0 ){
                System.debug('------1');
                if(!stg.ChikPeaO2B__Split_to_Lines__c){
                    ChikPeaO2B__Payment__c payment=createPayment(inv, stg);
                    
                    if(inv.ChikPeaO2B__Amount_Due_Remaining__c>stg_amt_remaining){
                        //No advance payment
                        payment.ChikPeaO2B__Payment_Amount__c=stg_amt_remaining;
                        stg.ChikPeaO2B__Used_Amount__c+=stg_amt_remaining;
                    }else if(inv.ChikPeaO2B__Amount_Due_Remaining__c<stg_amt_remaining){
                        //Advance payment
                        payment.ChikPeaO2B__Payment_Amount__c=inv.ChikPeaO2B__Amount_Due_Remaining__c;
                        stg.ChikPeaO2B__Used_Amount__c+=inv.ChikPeaO2B__Amount_Due_Remaining__c;
                        stg_amt_remaining=stg_amt_remaining-inv.ChikPeaO2B__Amount_Due_Remaining__c;
                        ChikPeaO2B__Advance_payment__c adv_payment=createAdvancePayment(inv, stg, 
                                    stg_amt_remaining);
                        stg.ChikPeaO2B__Used_Amount__c+=stg_amt_remaining;
                        adv_pmt_list.add(adv_payment);          
                    }else{
                        payment.ChikPeaO2B__Payment_Amount__c=inv.ChikPeaO2B__Amount_Due_Remaining__c;
                        stg.ChikPeaO2B__Used_Amount__c+=inv.ChikPeaO2B__Amount_Due_Remaining__c;
                    }
                    pmt_list_inv.add(payment);
                }
                else{
                    for(ChikPeaO2B__Invoice_Line__c invl : inv.ChikPeaO2B__Invoice_Lines__r){
                        
                        decimal payable = (invl.ChikPeaO2B__Line_Rate__c != null ? invl.ChikPeaO2B__Line_Rate__c : 0.00 )+ (invl.ChikPeaO2B__Line_tax__c != null ? invl.ChikPeaO2B__Line_tax__c : 0.00);
                        if(payable > 0.00){
                            for(ChikPeaO2B__Payment__c pt : inv.ChikPeaO2B__payments__r){
                                if(pt.ChikPeaO2B__Invoice_Line__c == invl.id){
                                    pt.ChikPeaO2B__Payment_Amount__c = pt.ChikPeaO2B__Payment_Amount__c != null ? pt.ChikPeaO2B__Payment_Amount__c : 0.00 ;
                                    payable = payable - pt.ChikPeaO2B__Payment_Amount__c;    
                                }
                            }
                            if(payable > 0.00 && stg.ChikPeaO2B__Amount__c - stg.ChikPeaO2B__Used_Amount__c > 0.00){
                                
                                if(stg.ChikPeaO2B__Amount__c - stg.ChikPeaO2B__Used_Amount__c >= payable){
                                    ChikPeaO2B__Payment__c payment=createPayment(inv, stg);
                                    payment.ChikPeaO2B__Payment_Amount__c = payable ;
                                    payment.ChikPeaO2B__Invoice_Line__c = invl.id;
                                    stg.ChikPeaO2B__Used_Amount__c = stg.ChikPeaO2B__Used_Amount__c + payable;
                                    pmt_list_inv.add(payment);      
                                }
                                else{
                                    ChikPeaO2B__Payment__c payment=createPayment(inv, stg);
                                    payment.ChikPeaO2B__Payment_Amount__c = (stg.ChikPeaO2B__Amount__c - stg.ChikPeaO2B__Used_Amount__c) ;
                                    payment.ChikPeaO2B__Invoice_Line__c = invl.id;
                                    stg.ChikPeaO2B__Used_Amount__c = stg.ChikPeaO2B__Used_Amount__c + payment.ChikPeaO2B__Payment_Amount__c;
                                    pmt_list_inv.add(payment);   
                                }    
                            }
                        }
                        
                    }
                    if(stg.ChikPeaO2B__Amount__c - stg.ChikPeaO2B__Used_Amount__c > 0.0){
                        ChikPeaO2B__Advance_payment__c adv_payment=createAdvancePayment(inv, stg,stg.ChikPeaO2B__Amount__c - stg.ChikPeaO2B__Used_Amount__c);    
                        stg.ChikPeaO2B__Used_Amount__c+=stg.ChikPeaO2B__Amount__c - stg.ChikPeaO2B__Used_Amount__c;
                        adv_pmt_list.add(adv_payment); 
                    }
                }
                
            }else if(inv_ref_inv.containsKey(stg.ChikPeaO2B__Invoice_Ref__c)&&stg_amt_remaining>0){
                //Full advance payment
                System.debug('------2');
                stg_amt_remaining=stg.ChikPeaO2B__Amount__c-stg.ChikPeaO2B__Used_Amount__c;
                ChikPeaO2B__Advance_payment__c adv_payment=createAdvancePayment(inv, stg, 
                                stg_amt_remaining);
                stg.ChikPeaO2B__Used_Amount__c+=stg_amt_remaining;
                adv_pmt_list.add(adv_payment);  
            }
        }
        
        //Invoice or Invoice line reference not found
        for(ChikPeaO2B__Payment_Staging__c stg:stg_list){
            if(!(inv_ref_inv.containsKey(stg.ChikPeaO2B__Invoice_Ref__c)||
                    inv_line_ref_inv.containsKey(stg.ChikPeaO2B__Invoice_Ref__c))){
                stg.ChikPeaO2B__Status__c='Error';
            }
        }
        //System.debug('#****pmt_list_inv='+pmt_list_inv);
        //System.debug('#****adv_pmt_list='+adv_pmt_list);
        System.debug('#****stg_list='+stg_list);
        insert pmt_list_inv;
        insert adv_pmt_list;
        update stg_list;
      }
      private static ChikPeaO2B__Payment__c createPayment(ChikPeaO2B__Invoice__c inv, ChikPeaO2B__Payment_Staging__c stg){
        ChikPeaO2B__Payment__c payment=new ChikPeaO2B__Payment__c();
        payment.ChikPeaO2B__Account__c=inv.ChikPeaO2B__Account__c;
        payment.ChikPeaO2B__Has_Processed__c=true;
        payment.ChikPeaO2B__Invoice__c=inv.id;
        payment.ChikPeaO2B__payment_method__c =  stg.ChikPeaO2B__Payment_Method__c;
        payment.ChikPeaO2B__status__c = 'Processed';
        payment.ChikPeaO2B__Payment_Date__c = Date.today();// date.today();//inv.ChikPeaO2B__Due_Date__c;
        payment.ChikPeaO2B__Payment_Staging__c = stg.id;
        payment.ChikPeaO2B__Batch_ID__c = stg.ChikPeaO2B__Batch_Ref__c;
        if(stg.ChikPeaO2B__Payment_Method__c=='Check')
        {
            payment.ChikPeaO2B__Check_Date__c= stg.ChikPeaO2B__Payment_Date__c;
            payment.ChikPeaO2B__Check_Number__c=stg.ChikPeaO2B__Check_Ref__c;
        }
        
        stg.ChikPeaO2B__Status__c='Uploaded';
        return payment;
      }
     private static ChikPeaO2B__Advance_payment__c createAdvancePayment(ChikPeaO2B__Invoice__c inv, 
        ChikPeaO2B__Payment_Staging__c stg, Decimal amt){
        ChikPeaO2B__Advance_payment__c adv_payment=new ChikPeaO2B__Advance_payment__c();
        adv_payment.ChikPeaO2B__Account__c=inv.ChikPeaO2B__Account__c;
        adv_payment.ChikPeaO2B__Status__c='Processed';
        System.debug('#****stg.ChikPeaO2B__Payment_Method__c='+stg.ChikPeaO2B__Payment_Method__c);
        adv_payment.ChikPeaO2B__Payment_Method__c=stg.ChikPeaO2B__Payment_Method__c;
        adv_payment.ChikPeaO2B__Advanced_Payment__c=amt;        
        adv_payment.ChikPeaO2B__Transaction_Id__c =stg.Name;
        adv_payment.ChikPeaO2B__Payment_Date__c = stg.ChikPeaO2B__Payment_Date__c;
        adv_payment.ChikPeaO2B__Check_Number__c = stg.ChikPeaO2B__Check_Ref__c;
        adv_payment.ChikPeaO2B__Batch_ID__c = stg.ChikPeaO2B__Batch_Ref__c ;
        stg.ChikPeaO2B__Status__c='Uploaded';
        System.debug('#****stg.ChikPeaO2B__Status__c='+stg.ChikPeaO2B__Status__c);
        return adv_payment;
     }
}