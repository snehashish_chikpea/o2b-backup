/*
** Class         :  BatchInvoiceMailSend
** Created by    :  Asitm (chikpea Inc.)
** Last Modified :  28 march 14
** Modified by	 :  
** Reason		 :  
** Description   :   
*/

global with sharing class BatchInvoiceMailSendScheduler implements Schedulable{
	global void execute(SchedulableContext sc)
    {
        BatchInvoiceMailSend BC = new BatchInvoiceMailSend();
        List<ChikPeaO2B__O2B_Setting__c> o2bset=[SELECT id,name,ChikPeaO2B__Admin_Email__c, ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c from ChikPeaO2B__O2B_Setting__c limit 1];
        BC.email=((o2bset!=null && o2bset.size()>0 && o2bset[0].ChikPeaO2B__Admin_Email__c!=null)?o2bset[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
        BC.query='select id,ChikPeaO2B__Email_Sent__c,ChikPeaO2B__Amount_Due__c,ChikPeaO2B__Account__r.ChikPeaO2B__Bill_To_Parent__c'+
                   ' from ChikPeaO2B__Invoice__c where ChikPeaO2B__Invoice_Status__c=\'Open\' AND ChikPeaO2B__Email_Sent__c=Null';
        ID batchprocessid = Database.executeBatch(BC,199);  
    }
}