/*

** Class         :  API_class
** Created by    :  Debajyoti Mukherjee (chikpea Inc.)
** Last Modified :  14_05_2014(Created)
** Description   :  API definations for O2B

*/
global with sharing class O2BAPI{
    WebService static String runPaymentstagingBatch(integer size){
        if(size == null || size == 0){
            size = 200;
        }
        try{
            database.executebatch(new BatchPaymentStaging(),size);
            return 'success';
        }
        catch(exception e){
            return e.getmessage();
        }
        return null;
    }
    global static string UsageCalculation(list<ChikPeaO2B__Usage__c> ulist){
        //[SELECT id,name,ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Rating_Rule__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Total_Usage__c,(select id,name,ChikPeaO2B__Usage__c,ChikPeaO2B__Usage_unit__c  from ChikPeaO2B__Usage_Stagings__r) from ChikPeaO2B__Usage__c where id in : uid]
        string remsg = '';
        for(ChikPeaO2B__Usage__c u : ulist){
            Type t;
            t = Type.forName('',u.ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Rating_Rule__c);//ChikPeaO2B
            if(t == null){
                t = Type.forName('ChikPeaO2B',u.ChikPeaO2B__Rate_Plan__r.ChikPeaO2B__Rating_Rule__c);//ChikPeaO2B
            }
            if(t != null){
                try{
                    Calculation c = (Calculation)t.newInstance();
                    u.ChikPeaO2B__Total_Usage__c=c.calculate(u.ChikPeaO2B__Usage_Stagings__r);
                }
                catch(exception e){
                    u.ChikPeaO2B__Total_Usage__c= 0.00;
                    system.debug(e.getmessage());
                    remsg = remsg + e.getmessage();
                }
            }
            
        }
        if(ulist.size() > 0){
            update ulist;
        }
        return remsg ;
    }
    // #8
    //------------------------------------------------------------------------------------------------
    // Program          trackFedEx
    // Author           ChikPea 08/09/2012
    // Purpose          This method calls FedEx tracking service to get status of a tracking number.
    //                            
    // Inputs           <InputDetails>
    //                      <Data type="TrackingNumber">12345</Data>         // The traccking number of FedEx
    //                      <Data type="devKey">GAjIHbCd2fGulTSV</Data>
    //                      <Data type="password">lw0WmJUJdQYc5ffJ5CTfpSJnZ</Data>
    //                      <Data type="accountNumber">510087607</Data>
    //                      <Data type="meterNumber">100234435</Data>
    //                      <Data type="url">https://wsbeta.fedex.com:443/web-services</Data>  
    //                  </InputDetails>
    // Outputs          <OutputDetails>
    //                      <Data type="TrackingNumber">12345</Data>                         // FedEx tracking number.
    //                      <Data type="StatusCode">Response Status Code</Data>              // FedEx Status code.
    //                      <Data type="StatusDescription">Status Description</Data>         // What the FedEx status code means.
    //                      <Data type="LastStatusUpdateTime">Date Time</Data>               // Date of update of the last status in FedEx.
    //                      <Data type="ResponseCode">CODE</Data>                            // FedEx response code.
    //                      <Data type="ResponseMessage">Response_Message</Data>             // FedEx response message.
    //                      <Data type="Result">SUCCESS or FAILURE</Data>                    // System processed result.
    //                      <Data type="ErrorCode">000(success) or other(error)</Data>       // System generated error code.
    //                      <Data type="ErrorMessage">message</Data>                         // System generated error message.
    //                  </OutputDetails>
    //------------------------------------------------------------------------------------------------ 
    Webservice static String trackFedEx( String request )
    {
        // -------------------------------------------
        // Initializing the exception handler class
        // -------------------------------------------
       // IVRAPIExceptionHandler iaeH = new IVRAPIExceptionHandler();
        
        // ---------------------------------------------------------
        // Parsing request xml and store it in the following map
        // ---------------------------------------------------------
        Map<String, String> inputDetails;
        if(request != null && request.trim() != '')
        {
            try
            {
                inputDetails = XMLHandler.parseDetails( request );
                System.debug('------reqIPDet>' + inputDetails);
            }
            catch(Exception ex)
            {
                // ------------------------------------
                // Handle error for invalid xml sent
                // ------------------------------------
                Map<String, String> retCont = new Map<String, String>{'TrackingNumber' => '','StatusCode' => '','StatusDescription' => '','LastStatusUpdateTime' => '','ResponseCode' => '','ResponseMessage' => '','Result' => 'FAILURE','ErrorCode' => '902','ErrorMessage' => 'Request contains an invalid xml.'}; 
               
                return XMLHandler.writeDetails(retCont);
            }
        }
        else
        {
            // ---------------------------------
            // Handle error for no data sent
            // ---------------------------------
            Map<String, String> retCont = new Map<String, String>();
            retCont.put('TrackingNumber', '');
            retCont.put('StatusCode', '');
            retCont.put('StatusDescription', '');
            retCont.put('LastStatusUpdateTime', '');
            retCont.put('ResponseCode', '');
            retCont.put('ResponseMessage', '');
            retCont.put('Result', 'FAILURE');
            retCont.put('ErrorCode', '901');
            retCont.put('ErrorMessage', 'Invalid request is found.');

            return XMLHandler.writeDetails(retCont);
        }
        
        // ------------------------------
        // Validating the request data
        // ------------------------------
        Set<String> fieldsToBeChecked = new Set<String>();
        fieldsToBeChecked.add('TrackingNumber');
        /*List<String> errorReport = iaeH.checkException( inputDetails, fieldsToBeChecked );
        System.debug('-------errorReport> '+errorReport);
        if(errorReport != null && errorReport.size() > 0)
        {
            Map<String, String> retCont = new Map<String, String>();
            retCont.put('TrackingNumber', '');
            retCont.put('StatusCode', '');
            retCont.put('StatusDescription', '');
            retCont.put('LastStatusUpdateTime', '');
            retCont.put('ResponseCode', '');
            retCont.put('ResponseMessage', '');
            retCont.put('Result', 'FAILURE');
            retCont.put('ErrorCode', errorReport[0]);
            retCont.put('ErrorMessage', errorReport[1]);

            return XMLHandler.writeDetails(retCont);
        }*/
        
        // --------------------
        // FedEx Information
        // --------------------
        String devKey = inputDetails.get('devKey');     //'VkZaT7kWRqfN59dE';
        String password = inputDetails.get('password');     //'SwzAH5YgwIk7FTOQpM7jKGxDf';
        String accountNumber = inputDetails.get('accountNumber');     //'510087682';
        String meterNumber = inputDetails.get('meterNumber');     //'118562418';
        String url = inputDetails.get('url');     //'https://wsbeta.fedex.com:443/web-services/track';
        String trackingNumber = inputDetails.get('TrackingNumber');
        boolean isinputok = true;
        if(devKey == null || password == null || accountNumber == null || meterNumber == null || url == null || trackingNumber == null ){
            isinputok = false;    
        }
        // ------------------------------------------------------
        // Preparing xml request to call FedEx tracking service
        // ------------------------------------------------------
        String requestXml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v5="http://fedex.com/ws/track/v5">'+
                               '<soapenv:Header/>'+
                               '<soapenv:Body>'+
                                  '<v5:TrackRequest>'+
                                     '<v5:WebAuthenticationDetail>'+
                                        '<v5:UserCredential>'+
                                           '<v5:Key>'+devKey+'</v5:Key>'+
                                           '<v5:Password>'+password+'</v5:Password>'+
                                        '</v5:UserCredential>'+
                                     '</v5:WebAuthenticationDetail>'+
                                     '<v5:ClientDetail>'+
                                        '<v5:AccountNumber>'+accountNumber+'</v5:AccountNumber>'+
                                        '<v5:MeterNumber>'+meterNumber+'</v5:MeterNumber>'+   
                                        '<v5:IntegratorId>12345</v5:IntegratorId>'+   
                                        '<v5:Localization>'+
                                           '<v5:LanguageCode>EN</v5:LanguageCode>'+      
                                        '</v5:Localization>'+
                                     '</v5:ClientDetail>'+
                                     '<v5:TransactionDetail>'+   
                                        '<v5:CustomerTransactionId>Track Ground Shipment By Tracking Number</v5:CustomerTransactionId>'+   
                                        '<v5:Localization>'+
                                           '<v5:LanguageCode>EN</v5:LanguageCode>'+      
                                        '</v5:Localization>'+
                                     '</v5:TransactionDetail>'+
                                     '<v5:Version>'+
                                        '<v5:ServiceId>trck</v5:ServiceId>'+
                                        '<v5:Major>5</v5:Major>'+
                                        '<v5:Intermediate>0</v5:Intermediate>'+
                                        '<v5:Minor>0</v5:Minor>'+
                                     '</v5:Version>'+
                                     '<v5:CarrierCode>FXSP</v5:CarrierCode>'+
                                     '<v5:PackageIdentifier>'+
                                        '<v5:Value>'+trackingNumber+'</v5:Value>'+
                                        '<v5:Type>TRACKING_NUMBER_OR_DOORTAG</v5:Type>'+
                                     '</v5:PackageIdentifier>'+
                                  '</v5:TrackRequest>'+
                               '</soapenv:Body>'+
                            '</soapenv:Envelope>';
        
        // -------------------------------
        // Sending xml request to FedEx
        // -------------------------------
        if(isinputok){
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(url);
            req.setBody(requestXml);
            req.setHeader('Content-Type', 'application/soap+xml; charset=utf-8');
            req.setTimeout(60000);
            String Storedresponse = '';
            System.debug('------> '+req);
            try
            {
                Http http = new Http();                 //Creating new Http.
                if(!Test.isRunningTest() )
                {
                    HTTPResponse Res = http.send(req);      //Send request and get the response.
                    System.debug('------> '+Res);
                    Storedresponse = Res.getBody();
                    System.debug('------> '+Storedresponse);
                }
                else
                {
                    Storedresponse = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><env:Header xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><env:Body xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><v5:TrackReply xmlns:v5="http://fedex.com/ws/track/v5"><v5:HighestSeverity>SUCCESS</v5:HighestSeverity><v5:Notifications><v5:Code>0</v5:Code><v5:Message>Request was successfully processed.</v5:Message></v5:Notifications><v5:TrackDetails><v5:StatusCode>DL</v5:StatusCode><v5:StatusDescription>Delivered</v5:StatusDescription><v5:Events><v5:Timestamp>2011-06-02T12:46:16-07:00</v5:Timestamp><v5:EventType>DL</v5:EventType><v5:EventDescription>Delivered</v5:EventDescription></v5:Events></v5:TrackDetails></v5:TrackReply></env:Body></soapenv:Envelope>';
                    if(testNegetive)
                        Storedresponse = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><env:Header xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/><env:Body xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><v5:TrackReply xmlns:v5="http://fedex.com/ws/track/v5"><v5:HighestSeverity>ERROR</v5:HighestSeverity><v5:Notifications><v5:Code>0</v5:Code><v5:Message>Request was successfully processed.</v5:Message></v5:Notifications><v5:TrackDetails><v5:StatusCode>DL</v5:StatusCode><v5:StatusDescription>Delivered</v5:StatusDescription><v5:Events><v5:Timestamp>2011-06-02T12:46:16-07:00</v5:Timestamp><v5:EventType>DL</v5:EventType><v5:EventDescription>Delivered</v5:EventDescription></v5:Events></v5:TrackDetails></v5:TrackReply></env:Body></soapenv:Envelope>';
                    if(testNegetive2)
                        Storedresponse = '<soapenv:Envelope>';
                    if(testNegetive3)
                        Integer a = 2/0;
                }
                System.debug('------FedEx response> '+Storedresponse);
                
                // -------------------------------
                // Parsing verizon response xml
                // -------------------------------
                DOM.Document doc = new DOM.Document();     
                String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
                String trackV5 = 'http://fedex.com/ws/track/v5';
                try 
                {
                    doc.load(Storedresponse);   
                    DOM.XMLNode root = doc.getRootElement();
                    String FedExResult = root.getChildElement('Body', soapNS).getChildElement('TrackReply', trackV5).getChildElement('HighestSeverity', trackV5).getText();
                    String FedExResponseCode = root.getChildElement('Body', soapNS).getChildElement('TrackReply', trackV5).getChildElement('Notifications', trackV5).getChildElement('Code', trackV5).getText();
                    String FedExResponseMessage = root.getChildElement('Body', soapNS).getChildElement('TrackReply', trackV5).getChildElement('Notifications', trackV5).getChildElement('Message', trackV5).getText();
                    
                    if(FedExResult == 'SUCCESS' || FedExResult == 'NOTE' || FedExResult == 'WARNING')
                    {
                        String FedExStatusCode = root.getChildElement('Body', soapNS).getChildElement('TrackReply', trackV5).getChildElement('TrackDetails', trackV5).getChildElement('StatusCode', trackV5).getText();
                        String FedExStatusDesc = root.getChildElement('Body', soapNS).getChildElement('TrackReply', trackV5).getChildElement('TrackDetails', trackV5).getChildElement('StatusDescription', trackV5).getText();
                        String FedExLastStatusUpdateTime = '';
                        Dom.XmlNode[] FedExAllTrackDetials = root.getChildElement('Body', soapNS).getChildElement('TrackReply', trackV5).getChildElement('TrackDetails', trackV5).getChildElements();
                        if(FedExAllTrackDetials != null && FedExAllTrackDetials.size() > 0)
                        {
                            for(Dom.XmlNode nod : FedExAllTrackDetials)
                            {
                                if(nod.getName() == 'Events' && nod.getChildElement('EventType', trackV5).getText() == FedExStatusCode)
                                {
                                    FedExLastStatusUpdateTime = nod.getChildElement('Timestamp', trackV5).getText();
                                    break;
                                }
                            }
                        }
                        
                        Map<String, String> retCont = new Map<String, String>();
                        retCont.put('TrackingNumber', inputDetails.get('TrackingNumber'));
                        retCont.put('StatusCode', FedExStatusCode);
                        retCont.put('StatusDescription', FedExStatusDesc);
                        retCont.put('LastStatusUpdateTime', FedExLastStatusUpdateTime);
                        retCont.put('ResponseCode', FedExResponseCode);
                        retCont.put('ResponseMessage', FedExResponseMessage);
                        retCont.put('Result', 'SUCCESS');
                        retCont.put('ErrorCode', '000');
                        retCont.put('ErrorMessage', '');
            
                        return XMLHandler.writeDetails(retCont);
                    }
                    else
                    {
                        Map<String, String> retCont = new Map<String, String>{
                             'TrackingNumber' => inputDetails.get('TrackingNumber'), 'StatusCode' => '','StatusDescription' => '','LastStatusUpdateTime' => '','ResponseCode' => FedExResponseCode,'ResponseMessage' => FedExResponseMessage,'Result' => 'SUCCESS','ErrorCode' => '000','ErrorMessage' => ''};                  
                        return XMLHandler.writeDetails(retCont);
                    }
                }
                catch(Exception ex)
                {
                   
                    Map<String, String> retCont = new Map<String, String>{
                     'TrackingNumber' => inputDetails.get('TrackingNumber'),'StatusCode' => '','StatusDescription' => '','LastStatusUpdateTime' => '','ResponseCode' => '','ResponseMessage' => '','Result' => 'FAILURE','ErrorCode' => '912','ErrorMessage' => 'FedEx response is not well-formatted'}; 
                    return XMLHandler.writeDetails(retCont);
                }
            }
            // ---------------------------------------------------------
            // This exception handler is to handle any error occured.
            // ---------------------------------------------------------
            catch(Exception ex)
            {
                Map<String, String> retCont = new Map<String, String>{
                     'TrackingNumber' => inputDetails.get('TrackingNumber'),'StatusCode' => '','StatusDescription' => '','LastStatusUpdateTime' => '','ResponseCode' => '','ResponseMessage' => '','Result' => 'FAILURE','ErrorCode' => '911','ErrorMessage' => 'FedEx connection can not be built.'};
                return XMLHandler.writeDetails(retCont);
            }
        }
        else{
            Map<String, String> retCont = new Map<String, String>();
            retCont.put('TrackingNumber', '');
            retCont.put('StatusCode', '');
            retCont.put('StatusDescription', '');
            retCont.put('LastStatusUpdateTime', '');
            retCont.put('ResponseCode', '');
            retCont.put('ResponseMessage', '');
            retCont.put('Result', 'FAILURE');
            retCont.put('ErrorCode', '901');
            retCont.put('ErrorMessage', 'Invalid request is found.');

            return XMLHandler.writeDetails(retCont);
                       
        }
     
    }
    WebService static String getItemPrice(String RatePlanId, Decimal Quantity){
       
        ChikPeaO2B__Rate_Plan__c rp = new ChikPeaO2B__Rate_Plan__c();         
        string price = '';
        map<string,string> outputmap = new map<string,string>();
        if(RatePlanId == null || RatePlanId == ''){
            outputmap.put('code','O2B_01');outputmap.put('message','Rate Plan Id is Null');
            return XMLHandler.writeDetails(outputmap);              
        }
        try{
            rp = [select id,ChikPeaO2B__Item__c,ChikPeaO2B__Non_Recurring_Charge__c,ChikPeaO2B__Pricing_Type__c,
            ChikPeaO2B__Recurring_Charge__c,ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c,
            (select id,ChikPeaO2B__Item__c,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Non_Recurring_Charge__c,
            ChikPeaO2B__Recurring_Charge__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from ChikPeaO2B__Volume_Pricings__r order by ChikPeaO2B__Lower_Limit__c),
            (select id,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Non_Recurring_Charge__c,
            ChikPeaO2B__Recurring_Charge__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from ChikPeaO2B__Tiered_Pricing__r order by ChikPeaO2B__Lower_Limit__c)
             from ChikPeaO2B__Rate_Plan__c where id =: RatePlanId];
        }
        catch(exception e){
            outputmap.put('code','O2B_02');outputmap.put('message',e.getmessage());
            return XMLHandler.writeDetails(outputmap);               
        }
        try{
           /* if(rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c == 'Pre-Paid Usage'){
                return string.valueof(Quantity * rp.ChikPeaO2B__Non_Recurring_Charge__c);
            }
            else if (rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c == 'Post-Paid Usage'){
                return string.valueof(Quantity * rp.ChikPeaO2B__Recurring_Charge__c);
            }*/
            if(rp.ChikPeaO2B__Pricing_Type__c == 'Flat'){
                if(rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c == 'One-Off' || rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c == 'Pre-Paid Usage'){
                    decimal val = Quantity * rp.ChikPeaO2B__Non_Recurring_Charge__c;
                    return string.valueof(val);       
                }
                else if(rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c == 'Recurring' || rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c == 'Post-Paid Usage'){
                    decimal val = Quantity * rp.ChikPeaO2B__Recurring_Charge__c;
                    return string.valueof(val);
                }
            }
            else if(rp.ChikPeaO2B__Pricing_Type__c == 'Tiered'){
                system.debug('O2B_Pricing Type Tired');                
                return calculateprice(rp.ChikPeaO2B__Tiered_Pricing__r,Quantity,'Tiered',rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c);        
            }
            else if(rp.ChikPeaO2B__Pricing_Type__c == 'Volume'){
                system.debug('O2B_Pricing Type Volume');
                return calculateprice(rp.ChikPeaO2B__Volume_Pricings__r,Quantity,'Volume',rp.ChikPeaO2B__Item__r.ChikPeaO2B__Item_Type__c);    
            }
            else{
                outputmap.put('code','O2B_05');outputmap.put('message','Invalid Pricing Type');
                return XMLHandler.writeDetails(outputmap);   
            }
        }
        catch(exception e){
            outputmap.put('code','O2B_06');outputmap.put('message',e.getmessage());
            return XMLHandler.writeDetails(outputmap);   
        }
        return '0.00';   
    } 
    // method 5 sub module for method 4 
    WebService static String calculateprice(List<Sobject> pricingList, decimal Quantity,String type,string Itype){
        decimal rate = 0.00;
        string price = '';
        if(Itype == 'Recurring'){
            price = 'ChikPeaO2B__Recurring_Charge__c';    
        }
        else if(Itype == 'One-Off'){
            price = 'ChikPeaO2B__Non_Recurring_Charge__c';
        }
        else{
            price = 'ChikPeaO2B__Usage_Rate__c';
        }
        //if(pricingList != null){
       //     pricingList.sort();
       // }
        if(type == 'Volume'){
            for(Sobject Sobj :  pricingList ){                
               // ChikPeaO2B__Volume_Pricing__c vp = (ChikPeaO2B__Volume_Pricing__c) Sobj;
                
                String tmp = Sobj.get(price) != null ? String.valueof(Sobj.get(price)) : '0.00';
                
                if((Decimal)Sobj.get('ChikPeaO2B__Lower_Limit__c') <= Quantity && (Decimal)Sobj.get('ChikPeaO2B__Upper_Limit__c') >= Quantity){
                    return tmp;
                }
                else if((Decimal)Sobj.get('ChikPeaO2B__Lower_Limit__c') >= Quantity ){
                    return string.valueof(rate);
                }                
                rate = Decimal.valueof(tmp);
            }
            return string.valueof(rate);
        }
        else if (type == 'Tiered'){
            decimal TotalAmt = 0.00;
            decimal uplmt = 0.00;
            decimal qty = Quantity;
            for(Sobject Sobj :  pricingList ){                
               // ChikPeaO2B__Tiered_Pricing__c tp = (ChikPeaO2B__Tiered_Pricing__c) Sobj;
                
                String tmp = Sobj.get(price) != null ? String.valueof(Sobj.get(price)) : '0.00';
                
                TotalAmt = TotalAmt + Decimal.valueof(tmp) * ( (Decimal)Sobj.get('ChikPeaO2B__Upper_Limit__c') - (Decimal)Sobj.get('ChikPeaO2B__lower_Limit__c') + 1 <= qty ? (Decimal)Sobj.get('ChikPeaO2B__Upper_Limit__c') - (Decimal)Sobj.get('ChikPeaO2B__lower_Limit__c') +1 : qty );              
                
                system.debug('Dj=>'+TotalAmt+' == '+Sobj.get('ChikPeaO2B__Upper_Limit__c')+'  ='+tmp+'=  '+qty);
                
                qty = qty - ((Decimal)Sobj.get('ChikPeaO2B__Upper_Limit__c') - (Decimal)Sobj.get('ChikPeaO2B__lower_Limit__c') +1); 
                
                rate =  Decimal.valueof(tmp);
                //uplmt = Sobj.ChikPeaO2B__Upper_Limit__c;
                if(qty <= 0)
                    return string.valueof(TotalAmt);
            }
            system.debug('DJ==>'+qty+' rate=>'+rate);
            TotalAmt = TotalAmt + rate * qty;
            return string.valueof(TotalAmt);
        }
        else{
            return '0.00';    
        }        
    } 
     
    public static Boolean testNegetive = false;
    public static Boolean testNegetive2 = false;
    public static Boolean testNegetive3 = false;
    public static Boolean testNegetive4 = false; 
    
    // Search Account Root
     /*
    
    */
    Webservice static String searchAccountRoot(String ac_ids)
    {
        List<Account> ac_list=[Select Id, Name, ParentId, Parent.Id, Parent.Parent.Id, Parent.Parent.Parent.Id, 
                    Parent.Parent.Parent.Parent.Id, Parent.Parent.Parent.Parent.Parent.Id  From Account  where Id =:ac_ids];
        if(ac_list.size()>0)
        {
            
            Account acc=ac_list[0];
            if(acc.Parent.Id==null)
            {
                return acc.Id;
            }
            else if(acc.Parent.Parent.Id==null)
            {
                return acc.Parent.Id;
            }
            else if(acc.Parent.Parent.Parent.Id==null)
            {
                return acc.Parent.Parent.Id;
            }
            else if(acc.Parent.Parent.Parent.Parent.Id==null)
            {
                return acc.Parent.Parent.Parent.Id;
            }
            else if(acc.Parent.Parent.Parent.Parent.Parent.Id==null)
            {
                return acc.Parent.Parent.Parent.Parent.Id;
            }
            else
            {
                return searchAccountRoot(acc.Parent.Parent.Parent.Parent.Parent.Id);
            }
        }
        return null;
    }
}