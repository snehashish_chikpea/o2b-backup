public with sharing class ExceptionLogger {
    public static void  insertLog(Exception e,string clsName){
            string errmsg='';
            errmsg+=e.getmessage();
            errmsg+='\n';
            errmsg+=e.getCause();
            errmsg+='\n';
            errmsg+=e.getLineNumber();
            errmsg+='\n';
            errmsg+=e.getStackTraceString();
            ChikPeaO2B__BatchLog__c log= new ChikPeaO2B__BatchLog__c();
            //log.Name='BatchAutoPaymentProcess';
            log.Name=clsName;
            log.ChikPeaO2B__Error_Log__c=errmsg;
            //=== CURD check [START] ===//
            Map<string,string>BLogInsertResultMap=new map<string,string>();
            BLogInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__BatchLog__c',FieldPermissionSet.BatchLogInsert,'insert');
            if(BLogInsertResultMap.get('AllowDML')!=null && BLogInsertResultMap.get('AllowDML').equalsignorecase('true'))
            {
                insert log;
            }
            else
            {        
                return;
            }
            //=== CURD check [END] ===//
            system.debug('---------->err1'+errmsg);
            system.debug('---------->err2'+log);
    }
    
    public static void insertLog(list<Exception> exList,string clsName){
        list<ChikPeaO2B__BatchLog__c> blogList= new list<ChikPeaO2B__BatchLog__c>();
        if(exList!=null && exList.size()>0){
	        for(Exception e : exList){
	            string errmsg='';
	            errmsg+=e.getmessage();
	            errmsg+='\n';
	            errmsg+=e.getCause();
	            errmsg+='\n';
	            errmsg+=e.getLineNumber();
	            errmsg+='\n';
	            errmsg+=e.getStackTraceString();
	            ChikPeaO2B__BatchLog__c log= new ChikPeaO2B__BatchLog__c();
	            //log.Name='BatchAutoPaymentProcess';
	            log.Name=clsName;
	            log.ChikPeaO2B__Error_Log__c=errmsg;
	            blogList.add(log);
	        }
	        //=== CURD check [START] ===//
	        Map<string,string>B1LogInsertResultMap=new map<string,string>();
	        B1LogInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__BatchLog__c',FieldPermissionSet.BatchLogInsert,'insert');
	        if(B1LogInsertResultMap.get('AllowDML')!=null && B1LogInsertResultMap.get('AllowDML').equalsignorecase('true'))
	        {
	            insert blogList;
	        }
        }
        //=== CURD check [END] ===//
    }
    public static void insertLog(list<Database.SaveResult> srList,list<Sobject> soList,string clsName,string parentFld){
        Integer i=0;
        list<ChikPeaO2B__BatchLog__c> errLogList= new list<ChikPeaO2B__BatchLog__c>();
        for(Database.SaveResult sr : srList) {
            system.debug('----------->sr-->'+sr);
            if(!sr.isSuccess()){
                ChikPeaO2B__BatchLog__c log= new ChikPeaO2B__BatchLog__c();
                log.Name=clsName;
                string errmsg='';
                for(Database.Error err : sr.getErrors()){
                    errmsg+='ErrMsg--->'+err.getMessage()+'\n';
                    if(parentFld.equalsignorecase('id'))
                        errmsg+='reference Id -->'+soList[i].get(parentFld)+'\n';
                    else
                        errmsg+='parent Id -->'+soList[i].get(parentFld)+'\n';
                    for(string fld : err.getFields()){
                        errmsg+=fld+'--->'+soList[i].get(fld)+'\n';
                    }
                }
                log.ChikPeaO2B__Error_Log__c=errmsg;
                if(soList[i].get(parentFld)!=null){
                    errLogList.add(log);
                }
            }
            i++;
        }
        
        if(!errLogList.isEmpty()){
            if(System.isBatch() || System.isScheduled() || System.isFuture() ){
                //=== CURD check [START] ===//
                Map<string,string>B2LogInsertResultMap=new map<string,string>();
                B2LogInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__BatchLog__c',FieldPermissionSet.BatchLogInsert,'insert');
                if(B2LogInsertResultMap.get('AllowDML')!=null && B2LogInsertResultMap.get('AllowDML').equalsignorecase('true'))
                {
                    insert errLogList;
                }
                else
                {        
                    return;
                }
                //=== CURD check [END] ===//    
            }
        }
        
    }
    
    
    
    public static void insertLog(list<Database.UpsertResult> srList,list<Sobject> soList,string clsName,string parentFld){
        Integer i=0;
        list<ChikPeaO2B__BatchLog__c> errLogList= new list<ChikPeaO2B__BatchLog__c>();
        for(Database.UpsertResult sr : srList) {
            system.debug('----------->sr-->'+sr);
            if(!sr.isSuccess()){
                ChikPeaO2B__BatchLog__c log= new ChikPeaO2B__BatchLog__c();
                log.Name=clsName;
                string errmsg='';
                for(Database.Error err : sr.getErrors()){
                    errmsg+='ErrMsg--->'+err.getMessage()+'\n';
                    if(parentFld.equalsignorecase('id'))
                        errmsg+='reference Id -->'+soList[i].get(parentFld)+'\n';
                    else
                        errmsg+='parent Id -->'+soList[i].get(parentFld)+'\n';
                    for(string fld : err.getFields()){
                        errmsg+=fld+'--->'+soList[i].get(fld)+'\n';
                    }
                }
                log.ChikPeaO2B__Error_Log__c=errmsg;
                if(soList[i].get(parentFld)!=null){
                    errLogList.add(log);
                }
            }
            i++;
        }
        
        if(!errLogList.isEmpty()){
            if(System.isBatch() || System.isScheduled() || System.isFuture() ){
                //=== CURD check [START] ===//
                Map<string,string>B2LogInsertResultMap=new map<string,string>();
                B2LogInsertResultMap=CRUD_FLS_EnforcementVulnerability.CRUD_FLS_EnforcementVulnerabilitycheck('ChikPeaO2B__BatchLog__c',FieldPermissionSet.BatchLogInsert,'insert');
                if(B2LogInsertResultMap.get('AllowDML')!=null && B2LogInsertResultMap.get('AllowDML').equalsignorecase('true'))
                {
                    insert errLogList;
                }
                else
                {        
                    return;
                }
                //=== CURD check [END] ===//    
            }
        }
        
    }
}