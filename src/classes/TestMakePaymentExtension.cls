@isTest
private class TestMakePaymentExtension {

    static testMethod void myUnitTest() {
      
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Save_Failed_Payment__c=true,ChikPeaO2B__Supported_Gateways__c='AuthNet');
        insert(o2bset);
        ChikPeaO2B__Gateway__c gw=new ChikPeaO2B__Gateway__c(Name='AuthNet',ChikPeaO2B__Currency__c='USD',ChikPeaO2B__Gateway_Type__c='AuthNet',ChikPeaO2B__Tokenization__c=true,ChikPeaO2B__O2B_Setting__c=o2bset.id);
        insert gw;
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        List<ChikPeaO2B__Credit_Card__c>CCList=new List<ChikPeaO2B__Credit_Card__c>();
        ChikPeaO2B__Credit_Card__c cc=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st1',ChikPeaO2B__CC_City__c='city1',ChikPeaO2B__CC_Country__c='country1',ChikPeaO2B__CC_First_Name__c='FN1',ChikPeaO2B__CC_Last_Name__c='LN1',ChikPeaO2B__CC_Postal_Code__c='700076',ChikPeaO2B__CC_State__c='wb1',ChikPeaO2B__CC_Street2__c='strt2',ChikPeaO2B__Credit_Card_Number__c='5114',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=02,ChikPeaO2B__Expiry_Year__c=2016);
        CCList.add(cc);
        ChikPeaO2B__Credit_Card__c cc1=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st2',ChikPeaO2B__CC_City__c='city2',ChikPeaO2B__CC_Country__c='country2',ChikPeaO2B__CC_First_Name__c='FN2',ChikPeaO2B__CC_Last_Name__c='LN2',ChikPeaO2B__CC_Postal_Code__c='700077',ChikPeaO2B__CC_State__c='wb2',ChikPeaO2B__CC_Street2__c='strt3',ChikPeaO2B__Credit_Card_Number__c='0004',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=03,ChikPeaO2B__Expiry_Year__c=2013);
        CCList.add(cc1);
        ChikPeaO2B__Credit_Card__c cc2=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st3',ChikPeaO2B__CC_City__c='city3',ChikPeaO2B__CC_Country__c='country3',ChikPeaO2B__CC_First_Name__c='FN3',ChikPeaO2B__CC_Last_Name__c='LN3',ChikPeaO2B__CC_Postal_Code__c='700078',ChikPeaO2B__CC_State__c='wb3',ChikPeaO2B__CC_Street2__c='strt4',ChikPeaO2B__Credit_Card_Number__c='4444',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=12,ChikPeaO2B__Expiry_Year__c=2015);
        CCList.add(cc2);
        ChikPeaO2B__Credit_Card__c cc3=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st4',ChikPeaO2B__CC_City__c='city4',ChikPeaO2B__CC_Country__c='country4',ChikPeaO2B__CC_First_Name__c='FN4',ChikPeaO2B__CC_Last_Name__c='LN4',ChikPeaO2B__CC_Postal_Code__c='700079',ChikPeaO2B__CC_State__c='wb4',ChikPeaO2B__CC_Street2__c='strt5',ChikPeaO2B__Credit_Card_Number__c='5100',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=05,ChikPeaO2B__Expiry_Year__c=2020);
        CCList.add(cc3);
        insert(CCList);
        List<ChikPeaO2B__Advance_payment__c> advpaylist=new List<ChikPeaO2B__Advance_payment__c>();
        ChikPeaO2B__Advance_payment__c advp=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=4,ChikPeaO2B__Used_Amount__c=0,ChikPeaO2B__Payment_Date__c=date.today(),ChikPeaO2B__Status__c='Processed');
        advpaylist.add(advp);
        ChikPeaO2B__Advance_payment__c advp1=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=2.5,ChikPeaO2B__Used_Amount__c=2.5,ChikPeaO2B__Payment_Date__c=date.today(),ChikPeaO2B__Status__c='Processed');
        advpaylist.add(advp1);
        insert(advpaylist);
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        List<ChikPeaO2B__Subscription__c>SubList=new List<ChikPeaO2B__Subscription__c>();
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1);
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-5),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub2);
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(15),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub3);
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(45),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub4);
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub5);
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(5),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub6);
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-3),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub7);
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(70),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub8);
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9);
        insert(SubList);
        List<id>AccList=new List<id>();
        AccList.add(acc.id);
        AnniversaryOneBill.generateAnniversaryOneBill(AccList,false);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(acc);
        MakePaymentExtension payext = new MakePaymentExtension(stdc);
        payext.useAdvPayment=true;
        payext.useCN=true;
        payext.payment.Payment_Method__c='Credit Card';
        payext.payment.Payment_Gateway__c='AuthNet';
        
        PageReference pageRef = Page.Make_Payment;
        Test.setCurrentPage(pageRef);
        List<MakePaymentExtension.UnpaidInvoice>UPIList=payext.getUnpaidInvoices();
        payext.selectAll();
        payext.oneunselect();
        for(MakePaymentExtension.UnpaidInvoice unpi : UPIList)
        {
            unpi.selected=true;
        }
        UPIList=payext.getUnpaidInvoices();
        List<MakePaymentExtension.CreditCard>cclist1=payext.getCreditCards();
        for(MakePaymentExtension.CreditCard ccc : cclist1)
        {
            ccc.selected=true;
            break;
        }
        cclist1=payext.getCreditCards();
        
        payext.PayamntValidation();
        
        
        
        payext.DisableDoPaymentBtn();
        payext.processPayment();
        payext.useExistingCC();
        payext.useNewCcard();
        payext.getsaveOptions();
        payext.selectedoption='Save Card';
        payext.SaveCard();
        payext.ctype('6011456789012346');//discover
        payext.ctype('4011456789012346');//visa
        payext.ctype('5111456789012346');//master
        payext.ctype('371145678901234');//AE
        
        payext.payment.Payment_Method__c='Credit Card';
        payext.payment.Payment_Gateway__c='AuthNet';
        payext.gatewaySelected();
        payext.useNewCcard();
        
        payext.cc_number='4111111111111111';
        payext.cc_token_email='sroytest12@chikpea.com';
        payext.cc_secode='123';
        payext.NewCC.CC_First_Name__c='Snehashish';
        payext.NewCC.CC_Last_Name__c='R';
        payext.NewCC.Expiry_Year__c=2020;
        payext.NewCC.Expiry_Month__c=12;
        
        payext.validateCcard();
        payext.saveCcardToken();
    }
    static testMethod void myUnitTest1() {
      
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Save_Failed_Payment__c=true,ChikPeaO2B__Supported_Gateways__c='AuthNet');
        insert(o2bset);
        ChikPeaO2B__Gateway__c gw=new ChikPeaO2B__Gateway__c(Name='AuthNet',ChikPeaO2B__Currency__c='USD',ChikPeaO2B__Gateway_Type__c='AuthNet',ChikPeaO2B__Tokenization__c=true,ChikPeaO2B__O2B_Setting__c=o2bset.id);
        insert gw;
        Account acc=new Account(name='acc1',ChikPeaO2B__Next_Bill_Date__c=date.today(),Bill_Cycle__c='Monthly',Customer_Profile_Id__c='123456');
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        List<ChikPeaO2B__Credit_Card__c>CCList=new List<ChikPeaO2B__Credit_Card__c>();
        ChikPeaO2B__Credit_Card__c cc=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st1',ChikPeaO2B__CC_City__c='city1',ChikPeaO2B__CC_Country__c='country1',ChikPeaO2B__CC_First_Name__c='FN1',ChikPeaO2B__CC_Last_Name__c='LN1',ChikPeaO2B__CC_Postal_Code__c='700076',ChikPeaO2B__CC_State__c='wb1',ChikPeaO2B__CC_Street2__c='strt2',ChikPeaO2B__Credit_Card_Number__c='5114',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=02,ChikPeaO2B__Expiry_Year__c=2016);
        CCList.add(cc);
        ChikPeaO2B__Credit_Card__c cc1=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st2',ChikPeaO2B__CC_City__c='city2',ChikPeaO2B__CC_Country__c='country2',ChikPeaO2B__CC_First_Name__c='FN2',ChikPeaO2B__CC_Last_Name__c='LN2',ChikPeaO2B__CC_Postal_Code__c='700077',ChikPeaO2B__CC_State__c='wb2',ChikPeaO2B__CC_Street2__c='strt3',ChikPeaO2B__Credit_Card_Number__c='0004',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=03,ChikPeaO2B__Expiry_Year__c=2013);
        CCList.add(cc1);
        ChikPeaO2B__Credit_Card__c cc2=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st3',ChikPeaO2B__CC_City__c='city3',ChikPeaO2B__CC_Country__c='country3',ChikPeaO2B__CC_First_Name__c='FN3',ChikPeaO2B__CC_Last_Name__c='LN3',ChikPeaO2B__CC_Postal_Code__c='700078',ChikPeaO2B__CC_State__c='wb3',ChikPeaO2B__CC_Street2__c='strt4',ChikPeaO2B__Credit_Card_Number__c='4444',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=12,ChikPeaO2B__Expiry_Year__c=2015);
        CCList.add(cc2);
        ChikPeaO2B__Credit_Card__c cc3=new ChikPeaO2B__Credit_Card__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__CC_Street1__c='st4',ChikPeaO2B__CC_City__c='city4',ChikPeaO2B__CC_Country__c='country4',ChikPeaO2B__CC_First_Name__c='FN4',ChikPeaO2B__CC_Last_Name__c='LN4',ChikPeaO2B__CC_Postal_Code__c='700079',ChikPeaO2B__CC_State__c='wb4',ChikPeaO2B__CC_Street2__c='strt5',ChikPeaO2B__Credit_Card_Number__c='5100',ChikPeaO2B__Credit_Card_Type__c='Master',ChikPeaO2B__Expiry_Month__c=05,ChikPeaO2B__Expiry_Year__c=2020);
        CCList.add(cc3);
        insert(CCList);
        List<ChikPeaO2B__Advance_payment__c> advpaylist=new List<ChikPeaO2B__Advance_payment__c>();
        ChikPeaO2B__Advance_payment__c advp=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=4,ChikPeaO2B__Used_Amount__c=0,ChikPeaO2B__Payment_Date__c=date.today(),ChikPeaO2B__Status__c='Processed');
        advpaylist.add(advp);
        ChikPeaO2B__Advance_payment__c advp1=new ChikPeaO2B__Advance_payment__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Advanced_Payment__c=2.5,ChikPeaO2B__Used_Amount__c=2.5,ChikPeaO2B__Payment_Date__c=date.today(),ChikPeaO2B__Status__c='Processed');
        advpaylist.add(advp1);
        insert(advpaylist);
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        ProcessOrder.OrderProcessor(oidList);
        List<ChikPeaO2B__Subscription__c>SubList=new List<ChikPeaO2B__Subscription__c>();
        ChikPeaO2B__Subscription__c sub1=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-30),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub1);
        ChikPeaO2B__Subscription__c sub2=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-5),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub2);
        ChikPeaO2B__Subscription__c sub3=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(15),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub3);
        ChikPeaO2B__Subscription__c sub4=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-20),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(45),ChikPeaO2B__Billing_Started__c=false,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub4);
        ChikPeaO2B__Subscription__c sub5=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub5);
        ChikPeaO2B__Subscription__c sub6=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(5),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub6);
        ChikPeaO2B__Subscription__c sub7=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(-3),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub7);
        ChikPeaO2B__Subscription__c sub8=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(-50),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(70),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub8);
        ChikPeaO2B__Subscription__c sub9=new ChikPeaO2B__Subscription__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Billing_Start_Date__c=date.today().adddays(5),ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Next_Bill_Date__c=date.today(),ChikPeaO2B__Billing_Stop_Date__c=date.today().adddays(10),ChikPeaO2B__Billing_Started__c=true,ChikPeaO2B__Order_Line__c=orl3.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=1);
        SubList.add(sub9);
        insert(SubList);
        List<id>AccList=new List<id>();
        AccList.add(acc.id);
        AnniversaryOneBill.generateAnniversaryOneBill(AccList,false);
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(acc);
        MakePaymentExtension payext = new MakePaymentExtension(stdc);
        payext.useAdvPayment=true;
        payext.useCN=true;
        payext.payment.Payment_Method__c='Credit Card';
        payext.payment.Payment_Gateway__c='AuthNet';
        
        PageReference pageRef = Page.Make_Payment;
        Test.setCurrentPage(pageRef);
        List<MakePaymentExtension.UnpaidInvoice>UPIList=payext.getUnpaidInvoices();
        payext.selectAll();
        payext.oneunselect();
        for(MakePaymentExtension.UnpaidInvoice unpi : UPIList)
        {
            unpi.selected=true;
        }
        UPIList=payext.getUnpaidInvoices();
        List<MakePaymentExtension.CreditCard>cclist1=payext.getCreditCards();
        for(MakePaymentExtension.CreditCard ccc : cclist1)
        {
            ccc.selected=true;
            break;
        }
        cclist1=payext.getCreditCards();
        
        payext.PayamntValidation();
        
        
        
        payext.DisableDoPaymentBtn();
        payext.processPayment();
        payext.useExistingCC();
        payext.useNewCcard();
        payext.getsaveOptions();
        payext.selectedoption='Save Card';
        payext.SaveCard();
        payext.ctype('6011456789012346');//discover
        payext.ctype('4011456789012346');//visa
        payext.ctype('5111456789012346');//master
        payext.ctype('371145678901234');//AE
        
        payext.payment.Payment_Method__c='Credit Card';
        payext.payment.Payment_Gateway__c='AuthNet';
        payext.gatewaySelected();
        payext.useNewCcard();
        
        payext.cc_number='4111111111111111';
        payext.cc_token_email='sroytest12@chikpea.com';
        payext.cc_secode='123';
        payext.NewCC.CC_First_Name__c='Snehashish';
        payext.NewCC.CC_Last_Name__c='R';
        payext.NewCC.Expiry_Year__c=2020;
        payext.NewCC.Expiry_Month__c=12;
        
        payext.validateCcard();
        payext.saveCcardToken();
    }
}