@isTest
public class TestAddOrderLineExtension
{
    @isTest
    static void testMethod1()
    {
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        List<ChikPeaO2B__Tiered_Pricing__c>tieredlist=new List<ChikPeaO2B__Tiered_Pricing__c>();
        List<ChikPeaO2B__Volume_Pricing__c>volumelist=new list<ChikPeaO2B__Volume_Pricing__c>();
        
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false);
        insert(o2bset);
        
        Account acc=new Account(name='acc1');
        insert(acc);
    
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c rec1=new ChikPeaO2B__Item__c(name='rec1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec1);
        ChikPeaO2B__Item__c recV=new ChikPeaO2B__Item__c(name='recV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(recV);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        itemlist.add(pre);
        ChikPeaO2B__Item__c oneoffT=new ChikPeaO2B__Item__c(name='oneoffT',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoffT);
        ChikPeaO2B__Item__c oneoffV=new ChikPeaO2B__Item__c(name='oneoffV',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoffV);
        ChikPeaO2B__Item__c oneofford=new ChikPeaO2B__Item__c(name='oneofford',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneofford);        
        ChikPeaO2B__Item__c record=new ChikPeaO2B__Item__c(name='record',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(record);
        ChikPeaO2B__Item__c postord=new ChikPeaO2B__Item__c(name='postord',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(postord);
        ChikPeaO2B__Item__c preord=new ChikPeaO2B__Item__c(name='preord',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage');    
        itemlist.add(preord);
        ChikPeaO2B__Item__c itm1=new ChikPeaO2B__Item__c(name='Item1',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(itm1);
        ChikPeaO2B__Item__c itmx=new ChikPeaO2B__Item__c(name='Itemx',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(itmx);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp4);
        ChikPeaO2B__Rate_Plan__c rp5=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp5);
        ChikPeaO2B__Rate_Plan__c rp6=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Tiered',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='GB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp6);
        ChikPeaO2B__Rate_Plan__c rp7=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Non_Recurring_Charge__c=35,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='KB',ChikPeaO2B__Usage_Rate__c=1.5);
        RPlist.add(rp7);
        ChikPeaO2B__Rate_Plan__c rp8=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Annual',ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Volume',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp8);
        ChikPeaO2B__Rate_Plan__c rp9=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneofford.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp9);
        ChikPeaO2B__Rate_Plan__c rp10=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=record.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp10);
        ChikPeaO2B__Rate_Plan__c rp11=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=postord.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp11);
        ChikPeaO2B__Rate_Plan__c rp12=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=preord.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp12);
        ChikPeaO2B__Rate_Plan__c rpp=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rpp);
        ChikPeaO2B__Rate_Plan__c rppx=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=itmx.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rppx);
        insert(RPlist);
             
        ChikPeaO2B__Tiered_Pricing__c tr1=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr1);
        ChikPeaO2B__Tiered_Pricing__c tr2=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr2);
        ChikPeaO2B__Tiered_Pricing__c tr3=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr3);
        ChikPeaO2B__Tiered_Pricing__c tr4=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=200,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr4);
        ChikPeaO2B__Tiered_Pricing__c tr5=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Lower_Limit__c=201,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Recurring_Charge__c=11,ChikPeaO2B__Usage_Rate__c=4);
        tieredlist.add(tr5);
        ChikPeaO2B__Tiered_Pricing__c tr6=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Non_Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2.5);
        tieredlist.add(tr6);
        ChikPeaO2B__Tiered_Pricing__c tr7=new ChikPeaO2B__Tiered_Pricing__c(ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Non_Recurring_Charge__c=10,ChikPeaO2B__Usage_Rate__c=2);
        tieredlist.add(tr7);
        insert(tieredlist);
        
        ChikPeaO2B__Volume_Pricing__c vp1=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp1);
        ChikPeaO2B__Volume_Pricing__c vp2=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Non_Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp5.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp2);
        ChikPeaO2B__Volume_Pricing__c vp3=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Non_Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp3);
        ChikPeaO2B__Volume_Pricing__c vp4=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Non_Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp4);
        ChikPeaO2B__Volume_Pricing__c vp5=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Lower_Limit__c=1,ChikPeaO2B__Upper_Limit__c=100,ChikPeaO2B__Recurring_Charge__c=40,ChikPeaO2B__Rate_Plan__c=rp8.id,ChikPeaO2B__Usage_Rate__c=3);
        volumelist.add(vp5);
        ChikPeaO2B__Volume_Pricing__c vp6=new ChikPeaO2B__Volume_Pricing__c(ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Lower_Limit__c=101,ChikPeaO2B__Upper_Limit__c=1000,ChikPeaO2B__Recurring_Charge__c=45,ChikPeaO2B__Rate_Plan__c=rp8.id,ChikPeaO2B__Usage_Rate__c=2.5);
        volumelist.add(vp6);
        insert(volumelist);
        
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id);
        insert(ord);
        ChikPeaO2B__Order_Line__c ol1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Rate_Plan__c=rpp.id,ChikPeaO2B__Item__c=itm1.id);
        insert(ol1);
        
        ChikPeaO2B__Package__c pkg = new ChikPeaO2B__Package__c(name='pkg1',ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Package_Type__c='Post-Paid Usage',ChikPeaO2B__Price_Book__c=ord.ChikPeaO2B__Price_Book__c,ChikPeaO2B__Bill_On_Package__c=false);
        insert(pkg);
        ChikPeaO2B__Bundled_Item__c bi=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=itm1.id,ChikPeaO2B__Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rpp.id,ChikPeaO2B__Usage_Rate__c=1.2);
        insert(bi);
        ChikPeaO2B__Package__c pkgx = new ChikPeaO2B__Package__c(name='pkgx',ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Package_Type__c='Post-Paid Usage',ChikPeaO2B__Price_Book__c=ord.ChikPeaO2B__Price_Book__c,ChikPeaO2B__Bill_On_Package__c=true);
        insert(pkgx);
        ChikPeaO2B__Bundled_Item__c bi1=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=itmx.id,ChikPeaO2B__Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkgx.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rppx.id,ChikPeaO2B__Usage_Rate__c=1.2);
        insert(bi1);
        ChikPeaO2B__Item__c itm2=new ChikPeaO2B__Item__c(name='pkgx',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Is_package_Item__c=true);    
        insert(itm2);
        ChikPeaO2B__Rate_Plan__c rpp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=itm2.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        insert (rpp1);
        pkgx.ChikPeaO2B__Package_Item__c=itm2.id;
        update(pkgx);
        list<ChikPeaO2B__Bundled_Item__c>BList=new list<ChikPeaO2B__Bundled_Item__c>();
        ChikPeaO2B__Package__c pkg_1 = new ChikPeaO2B__Package__c(name='pkg_1',ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Package_Type__c='Post-Paid Usage',ChikPeaO2B__Price_Book__c=ord.ChikPeaO2B__Price_Book__c,ChikPeaO2B__Bill_On_Package__c=false);
        insert(pkg_1);
        ChikPeaO2B__Bundled_Item__c bi2=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg_1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Usage_Rate__c=1.2);
        BList.add(bi2);
        ChikPeaO2B__Bundled_Item__c bi3=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=oneoffT.id,ChikPeaO2B__Non_Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg_1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp6.id,ChikPeaO2B__Usage_Rate__c=1.2);
        BList.add(bi3);
        ChikPeaO2B__Bundled_Item__c bi4=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=oneoffV.id,ChikPeaO2B__Non_Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg_1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp7.id,ChikPeaO2B__Usage_Rate__c=1.2);
        BList.add(bi4);
        ChikPeaO2B__Bundled_Item__c bi5=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=rec1.id,ChikPeaO2B__Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg_1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Usage_Rate__c=1.2);
        BList.add(bi5);
        ChikPeaO2B__Bundled_Item__c bi6=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg_1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Usage_Rate__c=1.2);
        BList.add(bi6);
        ChikPeaO2B__Bundled_Item__c bi7=new ChikPeaO2B__Bundled_Item__c(ChikPeaO2B__Item__c=recV.id,ChikPeaO2B__Recurring_Charge__c=12,ChikPeaO2B__Package__c=pkg_1.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp8.id,ChikPeaO2B__Usage_Rate__c=1.2);
        BList.add(bi7);
        insert(BList);
        Test.startTest();
        Apexpages.Standardcontroller stdc = new Apexpages.Standardcontroller(ord);
        AddOrderLineExtension addext = new AddOrderLineExtension(stdc);
        PageReference pageRef = Page.Add_orderLine;
        Test.setCurrentPage(pageRef);
        //========================= one off [START] =============================//
        list<ListOfItem.Item>OneList=addext.getOneOffItems();
        addext.oneoffgetmethod();
        for(ListOfItem.Item item : OneList)
        {
            if(item.itm.name!='oneofford')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)//only one rp
                {
                    item.selectedRP.put(rp.id,true);
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)//only one rp
                {
                    item.orderedRP.put(rp.id,true);
                }
            }
        }
        addext.currentitm=Onelist[0].itm.id;
        list<ListOfItem.Item>OneList1=addext.getOneOffItems();
        //========================= one off [END] =============================//
        
        //========================= PrePaid [START] =============================//
        list<ListOfItem.Item>PreList=addext.getPreItems();
        addext.pregetmethod();
        list<ListOfItem.Item>PreList1=addext.getPreItems();
        addext.pregetmethod();
        for(ListOfItem.Item item : PreList1)
        {
            if(item.itm.name!='preord')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.selectedRP.put(rp.id,true);
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.orderedRP.put(rp.id,true);
                }
            }
        }
        addext.currentitm=PreList1[0].itm.id;
        list<ListOfItem.Item>PreList2=addext.getPreItems();
        //========================= PrePaid [END] =============================//
        
        //========================= Recurring [START] =============================//
        list<ListOfItem.Item>RecList=addext.getRecItems();
        addext.recgetmethod();
        list<ListOfItem.Item>RecList1=addext.getRecItems();
        addext.recgetmethod();
        for(ListOfItem.Item item : RecList1)
        {
            if(item.itm.name!='record')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.selectedRP.put(rp.id,true);
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.orderedRP.put(rp.id,true);
                }
            }
        }
        addext.currentitm=RecList1[0].itm.id;
        list<ListOfItem.Item>RecList2=addext.getRecItems();
        //========================= Recurring [END] =============================//
        
        //========================= Postpaid [START] =============================//
        list<ListOfItem.Item>PostList=addext.getPostItems();
        addext.postgetmethod();
        list<ListOfItem.Item>PostList1=addext.getPostItems();
        addext.postgetmethod();
        for(ListOfItem.Item item : PostList1)
        {
            if(item.itm.name!='postord')
            {
                item.selected=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.selectedRP.put(rp.id,true); 
                }
            }
            else
            {
                item.ordered=true;
                for(rate_plan__c rp : item.itm.Rate_Plans__r)
                {
                    item.orderedRP.put(rp.id,true); 
                }
            }
        }
        addext.currentitm=PostList1[0].itm.id;
        list<ListOfItem.Item>PostList2=addext.getPostItems();
        //========================= Postpaid [END] =============================//
        
        //======================== Package [START] =======================//
        List<ListOfItem.o2b2Package> pkglist=addext.geto2b2Packages();
        addext.pkggetmethod();
        List<ListOfItem.o2b2Package> pkglist1=addext.geto2b2Packages();

        for(ListOfItem.o2b2Package o2b2pkg : pkglist1)
        {
            o2b2pkg.selected=true;
            o2b2pkg.pkgqty=3;
            addext.PackageQChange();
        
            for(item__c pitm:o2b2pkg.ItemList)
            {
                o2b2pkg.itemqty.put(string.valueof(o2b2pkg.pkg.id)+string.valueof(pitm.id),4);    
                addext.itemQChange();
                o2b2pkg.selectedRPprice.put(string.valueof(o2b2pkg.pkg.id)+string.valueof(pitm.id),2.9);
                addext.SelectedPriceChange();
            }
        }
        List<ListOfItem.o2b2Package> pkglist2=addext.geto2b2Packages();
        //======================== Package [END] =======================//
        
        addext.Additem();
        addext.ClickedRP=rp4.id;
        addext.showPopup();
        addext.closePopup();
        addext.finish();
        
        addext.UnitPriceCalculation(12,'Annual','Daily');
        addext.UnitPriceCalculation(12,'Annual','Monthly');
        addext.UnitPriceCalculation(12,'Annual','Quarterly');
        addext.UnitPriceCalculation(12,'Annual','Weekly');
        addext.UnitPriceCalculation(12,'Annual','Half Yearly');
        
        addext.UnitPriceCalculation(12,'Daily','Annual');
        addext.UnitPriceCalculation(12,'Daily','Monthly');
        addext.UnitPriceCalculation(12,'Daily','Quarterly');
        addext.UnitPriceCalculation(12,'Daily','Weekly');
        addext.UnitPriceCalculation(12,'Daily','Half Yearly');
        
        addext.UnitPriceCalculation(12,'Monthly','Annual');
        addext.UnitPriceCalculation(12,'Monthly','Daily');
        addext.UnitPriceCalculation(12,'Monthly','Quarterly');
        addext.UnitPriceCalculation(12,'Monthly','Weekly');
        addext.UnitPriceCalculation(12,'Monthly','Half Yearly');
        
        addext.UnitPriceCalculation(12,'Quarterly','Annual');
        addext.UnitPriceCalculation(12,'Quarterly','Monthly');
        addext.UnitPriceCalculation(12,'Quarterly','Daily');
        addext.UnitPriceCalculation(12,'Quarterly','Weekly');
        addext.UnitPriceCalculation(12,'Quarterly','Half Yearly');
        
        addext.UnitPriceCalculation(12,'Weekly','Annual');
        addext.UnitPriceCalculation(12,'Weekly','Monthly');
        addext.UnitPriceCalculation(12,'Weekly','Daily');
        addext.UnitPriceCalculation(12,'Weekly','Quarterly');
        addext.UnitPriceCalculation(12,'Weekly','Half Yearly');
        
        addext.UnitPriceCalculation(12,'Half Yearly','Annual');
        addext.UnitPriceCalculation(12,'Half Yearly','Monthly');
        addext.UnitPriceCalculation(12,'Half Yearly','Daily');
        addext.UnitPriceCalculation(12,'Half Yearly','Quarterly');
        addext.UnitPriceCalculation(12,'Half Yearly','Weekly');
        
        addext.UnitPriceCalculation(12,'2','Annual');
        addext.UnitPriceCalculation(12,'2','Monthly');
        addext.UnitPriceCalculation(12,'2','Daily');
        addext.UnitPriceCalculation(12,'2','Quarterly');
        addext.UnitPriceCalculation(12,'2','Weekly');
        addext.UnitPriceCalculation(12,'2','Half Yearly');
        
        boolean onehn=addext.OneOffhasNext;
        boolean onehp=addext.OneOffhasPrevious;
        integer onepgn = addext.OneOffpageNumber;
        integer onetotpg = addext.Oneoffpages;
        addext.oneoffnxt();
        addext.oneoffprv();
        addext.Oneofffirst();
        addext.Oneofflast();
        addext.OneOffRPSelPriceChangeMethod();
        addext.OneOffQtyChangeMethod();
        
        boolean prehn=addext.PrehasNext;
        boolean prehp=addext.PrehasPrevious;
        integer prepgn = addext.prepageNumber;
        integer pretotpg = addext.prepages;
        addext.prenxt();
        addext.preprv();
        addext.Preffirst();
        addext.prelast();
        addext.preRPSelPriceChangeMethod();
        addext.PreQtyChangeMethod();
        
        boolean rechn=addext.RechasNext;
        boolean rechp=addext.RechasPrevious;
        integer recpgn = addext.recpageNumber;
        integer rectotpg = addext.recpages;
        addext.recnxt();
        addext.recprv();
        addext.recffirst();
        addext.reclast();
        addext.recRPSelPriceChangeMethod();
        addext.RecQtyChangeMethod();
        
        boolean posthn=addext.PosthasNext;
        boolean posthp=addext.PosthasPrevious;
        integer postpgn = addext.postpageNumber;
        integer posttotpg = addext.postpages;
        addext.postnxt();
        addext.postprv();
        addext.postffirst();
        addext.postlast();
        addext.postRPSelPriceChangeMethod();
        addext.PostQtyChangeMethod();
        
        boolean pkghn=addext.PkghasNext;
        boolean pkghp=addext.PkghasPrevious;
        integer pkgpgn=addext.PkgpageNumber;
        integer pkgpg=addext.Pkgpages;
        addext.Pkgfirst();
        addext.Pkglast();
        addext.Pkgnxt();
        addext.pkgprv();
        
        addext.getSeachOptions();
        addext.SelectedSearch='';
        addext.searchVal='';
        addext.searchitem();
        addext.SelectedSearch='ChikPeaO2B__Category__c';
        addext.searchVal='software';
        addext.searchitem();
        addext.SelectedSearch='Name';
        addext.searchVal='Item1';
        addext.searchitem();
        Test.stopTest();
    }    
}