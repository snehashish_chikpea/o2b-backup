/*
** Class         :  ParentBillProcessor
** Created by    :  Asitm9(chikpea Inc.)
** Rewritten by  :  Mehebub Hossain(chikpea Inc.)(9-Apr-15)
** Description   :  

*Support Hierarchy billing upto any level
*Support intermediate Parent. 
*Delete empty Invoice Lines of child Account if 'Delete 0 invoice after parent billing' is checked in O2B Setting
Use recursion to create hierarchy data structure.  Search reverse from any Account to Parent Account and stops when found 
a Account with Bill to Parent is false and Parent Billing is true. 
*/

global with sharing class ParentBillingProcessor { 
    //contains hierarchy data structure of Account
    private static Map<Id,List<AccountWrap>>tree_map;
    private static Map<Id,AccountWrap>all_ac_wrap_map;
    private static List<AccountWrap>all_ac_wrap;
    private static List<AccountWrap> parent_billing_list{get;set;}
    private static Map<Id,List<ChikPeaO2B__Invoice__c>> acId_inv;
    private static Map<Id,ChikPeaO2B__Invoice__c> invId_inv;
    private static Set<Id>inv_id{get;set;}
    private static Set<Id>all_acc_id{get;set;}

    webservice static void processAccounts(list<id> ac_ids){
        //initilize tree_map, all_ac_wrap
        tree_map=new Map<Id,List<AccountWrap>>();
        all_ac_wrap=new List<AccountWrap>();
        parent_billing_list=new List<AccountWrap>();
        all_ac_wrap_map=new Map<Id,AccountWrap>();
        invId_inv=new Map<Id,ChikPeaO2B__Invoice__c>();
        inv_id=new Set<Id>();
        all_acc_id=new Set<Id>();
        acId_inv=new Map<Id,List<ChikPeaO2B__Invoice__c>>();
        //querying setting
        List<ChikPeaO2B__O2B_Setting__c> o2bsetting = [select ChikPeaO2B__Delete_blank_invoice_after_parent_billin__c 
            from ChikPeaO2B__O2B_Setting__c limit 1 ];
        //checking if setting found
        if(o2bsetting.size()>0){
            //querying root account
            List<Account> ac_root_list=[SELECT Id,ParentId, 
                    ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Amount_Remaining__c, BillingStreet,ChikPeaO2B__Bill_To_Parent__c,
                    BillingState,BillingPostalCode, BillingCountry, BillingCity, Bill_Cycle__c,Amount_Due__c,
                    Active__c, Activation_Date__c,Payment_Term__c,ChikPeaO2B__Parent_Billing__c,
                    (SELECT Id,Name,CreatedDate FROM Contacts where ChikPeaO2B__Role__c='Billing' order by CreatedDate desc limit 1),
                    (SELECT Id,ChikPeaO2B__Other_Charges__c,Createddate,ChikPeaO2B__Email_Sent__c
                    FROM ChikPeaO2B__Invoices__r 
                    WHERE ChikPeaO2B__Invoice_Status__c='Open' and ChikPeaO2B__Email_Sent__c=null ORDER BY Createddate desc LIMIT 1 )
                    FROM Account WHERE Id IN :ac_ids];
            //creating hierarchy data structure  
            List<AccountWrap> ac_wrap_root=new List<AccountWrap>();
            for(Account ac:ac_root_list){
                all_acc_id.add(ac.Id);
                AccountWrap ac_wrap=new AccountWrap(ac);
                all_ac_wrap.add(ac_wrap);
                all_ac_wrap_map.put(ac.id,ac_wrap);
                ac_wrap_root.add(ac_wrap);
            } 
            tree_map.put(null, ac_wrap_root);
            buildTree(ac_ids);   
            //System.debug('#****tree_map='+tree_map); 
            //setting up parrent invoice
            for(AccountWrap ac_wrap:all_ac_wrap){
                if(ac_wrap.ac.ChikPeaO2B__Parent_Billing__c){
                    parent_billing_list.add(ac_wrap);
                    if(ac_wrap.ac.ChikPeaO2B__Invoices__r!=null && !ac_wrap.ac.ChikPeaO2B__Invoices__r.isEmpty()){
                        ChikPeaO2B__Invoice__c existing_inv=ac_wrap.ac.ChikPeaO2B__Invoices__r;
                        Date d_prev_month=Date.today().addMonths(-1);
                        if(existing_inv.ChikPeaO2B__Email_Sent__c!=null||existing_inv.Createddate<d_prev_month){
                            //creating new invoice
                            createInvoice(ac_wrap);
                        }else{
                            //using existing invoice
                            ac_wrap.inv=existing_inv;
                        }
                    }else{
                        //Creating Invoice for Accounts which have no Invoice yet 
                        createInvoice(ac_wrap);
                    }
                }
            }   
            //querying Invoice and Invoice Line 
            List<ChikPeaO2B__Invoice__c> all_invoice=[SELECT Id,ChikPeaO2B__Other_Charges__c,ChikPeaO2B__Account__r.ParentId, 
            ChikPeaO2B__Account__c,
            (SELECT Id,ChikPeaO2B__Invoice__c FROM ChikPeaO2B__Invoice_Lines__r),
            (SELECT Id,ChikPeaO2B__Account__c,ChikPeaO2B__Invoice__c FROM ChikPeaO2B__Payments__r
                WHERE ChikPeaO2B__Status__c='Not Processed')
                FROM ChikPeaO2B__Invoice__c
                WHERE ChikPeaO2B__Account__c IN:all_acc_id and ChikPeaO2B__Invoice_Status__c='Open' and ChikPeaO2B__Email_Sent__c=null];
                    
           for(ChikPeaO2B__Invoice__c inv:all_invoice){
                invId_inv.put(inv.Id, inv);
                if(acId_inv.containsKey(inv.ChikPeaO2B__Account__c)){
                    List<ChikPeaO2B__Invoice__c> inv_list=acId_inv.get(inv.ChikPeaO2B__Account__c);
                    inv_list.add(inv);
                    acId_inv.put(inv.ChikPeaO2B__Account__c,inv_list);
                }else{
                    List<ChikPeaO2B__Invoice__c> inv_list=new List<ChikPeaO2B__Invoice__c>();
                    inv_list.add(inv);
                    acId_inv.put(inv.ChikPeaO2B__Account__c,inv_list);
                }
           }  
           
           //move Invoice Lines
           for(AccountWrap ac_wrap :all_ac_wrap){
                moveInvoiceLine(ac_wrap, ac_wrap);
           }
           // 
           
           //starting line movement DML
            try{ 
                 List<ChikPeaO2B__Invoice__c> parent_invoice=new List<ChikPeaO2B__Invoice__c>();
                 for(AccountWrap ac_wrap:parent_billing_list ){
                    if(ac_wrap.invl_to_move!=null&&ac_wrap.invl_to_move.size()>0){
                    	parent_invoice.add(ac_wrap.inv);
                    }
                 }   
                 //upsert parent_invoice; 
                 list<Database.UpsertResult> upsertInv_rs=Database.upsert(parent_invoice,false);
                 ExceptionLogger.insertLog(upsertInv_rs,parent_invoice, 'ParentBillingProcessor','id');
            
                 List<ChikPeaO2B__Invoice_Line__c> inv_line_to_move=new List<ChikPeaO2B__Invoice_Line__c>();
                 List<ChikPeaO2B__Payment__c> pmt_to_move=new List<ChikPeaO2B__Payment__c>();
                 for(AccountWrap ac_wrap:parent_billing_list ){
                    for(ChikPeaO2B__Invoice_Line__c invl:ac_wrap.invl_to_move){
                        //moving Invoice Line   
                        invl.ChikPeaO2B__Invoice__c=ac_wrap.inv.Id;
                    }
                    for(ChikPeaO2B__Payment__c pmt:ac_wrap.pmt_to_move){
                        //moving payment
                        pmt.ChikPeaO2B__Invoice__c=ac_wrap.inv.Id;
                        pmt.ChikPeaO2B__Account__c=ac_wrap.ac.Id;
                    }
                    inv_line_to_move.addAll(ac_wrap.invl_to_move);
                    pmt_to_move.addAll(ac_wrap.pmt_to_move);
                 }
                 
                 
                 //update inv_line_to_move;
                 
                 list<Database.SaveResult> updateInv_line_rs=Database.update(inv_line_to_move,false);
                 ExceptionLogger.insertLog(updateInv_line_rs,inv_line_to_move, 'ParentBillingProcessor','id');
            
                 
                 //update pmt_to_move;
                 list<Database.SaveResult> updatePayment_rs=Database.update(pmt_to_move,false);
                 ExceptionLogger.insertLog(updatePayment_rs, pmt_to_move, 'ParentBillingProcessor','id');
            
                 
                 
                 //delete empty line after Hierarchy Billing
                 //checking O2B Settings
                
                 if(o2bsetting[0].ChikPeaO2B__Delete_blank_invoice_after_parent_billin__c){
                    List<ChikPeaO2B__Invoice__c> del_invoice=new List<ChikPeaO2B__Invoice__c>();
                    List<ChikPeaO2B__Invoice__c> query_del_invoice=[SELECT Id,ChikPeaO2B__Cancel_Delete_Allowed__c,
                        (SELECT Id,ChikPeaO2B__Invoice__c FROM ChikPeaO2B__Invoice_Lines__r)                       
                           from ChikPeaO2B__Invoice__c
                            WHERE ChikPeaO2B__Account__c IN:all_acc_id];
                     for(ChikPeaO2B__Invoice__c inv:query_del_invoice){
                        if(inv.ChikPeaO2B__Invoice_Lines__r.isEmpty()){
                            inv.ChikPeaO2B__Cancel_Delete_Allowed__c=true;
                            del_invoice.add(inv);
                        }
                     } 
                     update del_invoice;
                     delete del_invoice;      
                 }
                
            }catch(Exception e){
                 System.debug('#****Exception:'+e);
            } 
        }else{
            //no setting found exit program
            System.debug('Error: O2B Settings not found'); 
            return;
        }   
    }
    public static void moveInvoiceLine(AccountWrap ac_wrap,AccountWrap ac_wrap_p){
        //System.debug('#****ac_wrap_p='+ac_wrap_p);
        if(ac_wrap_p!=null){
            if(ac_wrap_p.ac.ChikPeaO2B__Bill_To_Parent__c!=null&&ac_wrap_p.ac.ChikPeaO2B__Bill_To_Parent__c){
                ac_wrap_p=all_ac_wrap_map.get(ac_wrap_p.ac.ParentId);
                moveInvoiceLine(ac_wrap,ac_wrap_p);
            }else{          
                if(acId_inv.containsKey(ac_wrap.ac.Id)){
                    List<ChikPeaO2B__Invoice__c> inv_list=acId_inv.get(ac_wrap.ac.Id);
                    for(ChikPeaO2B__Invoice__c inv:inv_list){
                        ac_wrap_p.invl_to_move.addAll(inv.ChikPeaO2B__Invoice_Lines__r);
                        ac_wrap_p.pmt_to_move.addAll(inv.ChikPeaO2B__Payments__r);
                    }
                }
            }
        }
    }
    public static void buildTree(List<Id> ac_ids){
        //querying child account
        List<Account> ac_root_list=[SELECT Id,ParentId, 
                    ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Amount_Remaining__c, BillingStreet,ChikPeaO2B__Bill_To_Parent__c,
                    BillingState,BillingPostalCode, BillingCountry, BillingCity, Bill_Cycle__c,Amount_Due__c,
                    Active__c, Activation_Date__c,Payment_Term__c,ChikPeaO2B__Parent_Billing__c,
                    (SELECT Id,ChikPeaO2B__Other_Charges__c,Createddate,ChikPeaO2B__Email_Sent__c
                    FROM ChikPeaO2B__Invoices__r 
                    WHERE ChikPeaO2B__Invoice_Status__c='Open' ORDER BY Createddate desc LIMIT 1)
                    FROM Account WHERE ParentId IN :ac_ids];
        List<Id> child_id_list=new List<Id>();
        for(Account ac:ac_root_list){
            all_acc_id.add(ac.Id);
            child_id_list.add(ac.id);
            AccountWrap ac_wrap=new AccountWrap(ac);
            all_ac_wrap.add(ac_wrap);
            all_ac_wrap_map.put(ac.id,ac_wrap);
            //creating tree_map
            if(tree_map.containsKey(ac.ParentId)){
                //checking if map already contain a list of child 
                List<AccountWrap> ac_wrap_ch=tree_map.get(ac.ParentId);
                ac_wrap_ch.add(ac_wrap);
                tree_map.put(ac.ParentId, ac_wrap_ch);
            }else{
                List<AccountWrap> ac_wrap_ch=new List<AccountWrap>();
                ac_wrap_ch.add(ac_wrap);
                tree_map.put(ac.ParentId, ac_wrap_ch);
            }   
        }
        if(child_id_list.size()>0){
            buildTree(child_id_list);
        }           
    }
    //create an Invoice header 
    public static void createInvoice(AccountWrap ac_wrap){
        Invoice__c inv = new Invoice__c();
        inv.Account__c = ac_wrap.ac.Id;
        inv.Bill_Date__c = date.today();
        inv.Billing_Street__c = ac_wrap.ac.BillingStreet;
        inv.Billing_City__c = ((ac_wrap.ac.BillingCity == null || ac_wrap.ac.BillingCity.length()<30) ?
             ac_wrap.ac.BillingCity : ac_wrap.ac.BillingCity.substring(0,30) );
        inv.ChikPeaO2B__Billing_State_Province__c= ac_wrap.ac.BillingState;
        inv.ChikPeaO2B__Billing_Zip_Postal_Code__c= ac_wrap.ac.BillingPostalCode;
        inv.ChikPeaO2B__Billing_Country__c= ac_wrap.ac.BillingCountry;
        for(Contact cntct:ac_wrap.ac.Contacts){
            inv.ChikPeaO2B__Billing_Contact__c=cntct.Id;
        }
        ac_wrap.inv=inv;
    }
    //Wrapper class for data collection of Account, Invoice, Payment, Invoice Line
    //Helps finding master  of an Invoice Line
    public class AccountWrap{
        public Account ac{get;set;}//Parent Account
        //public List<AccountWrap> ac_wrap_ch{get;set;}//Child wrapper class
        public ChikPeaO2B__Invoice__c inv{get;set;}//Parent Invoice
        public List<ChikPeaO2B__Invoice_Line__c> invl_to_move{get;set;}//Invoice line need to be reparent   
        public List<ChikPeaO2B__Payment__c> pmt_to_move{get;set;}   
        public AccountWrap(Account ac){
            this.ac=ac;
            //ac_wrap_ch=new List<AccountWrap>();
            //inv=new ChikPeaO2B__Invoice__c();
            invl_to_move=new List<ChikPeaO2B__Invoice_Line__c>();
            pmt_to_move=new List<ChikPeaO2B__Payment__c>();
        }
    }
}