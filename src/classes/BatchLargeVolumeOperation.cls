/*
** Class         :  BatchLargeVolumeOperation
** Created by    :  Asitm9 (chikpea Inc.)
** Created date  :  22/08/2013
** Description   : 
*/

global with sharing class BatchLargeVolumeOperation implements Database.Batchable<sObject>,Database.Stateful{
    global list<sObject> sObjectList;
    global Integer op;
    global BatchLargeVolumeOperation(list<sObject>al,Integer o){
        sObjectList=al;
        op=o;
    }
    
    global Iterable<sObject> start(Database.BatchableContext BC){
        return sObjectList;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        map<string,string> ObjMasterMap= new map<string,string>();
        ObjMasterMap.put('ChikPeaO2B__Invoice_Line__c','ChikPeaO2B__Invoice__c');
        ObjMasterMap.put('ChikPeaO2B__Subscription__c','ChikPeaO2B__Account__c');
        string objType=string.valueOf(scope.getSObjectType());
        list<Database.SaveResult> insertSo;
        if(op==1){
            insertSo=Database.insert(scope,false);
        }
        else if(op==2){
            insertSo=Database.update(scope,false);
            system.debug('----------->'+scope);
        }
        
        if(ObjMasterMap.get(objType)!=null){
            ExceptionLogger.insertLog(insertSo, scope, 'Dml Exception in Class BatchBulkInsert :',ObjMasterMap.get(objType));
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}