/**
--------------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-20
@see 
----------------------------------------------------------------------------------------------------------
*/
public class PG_PayFlowPro implements PG_PaymentGateway, PG_PaymentGatewayACH{
    
    public static final Boolean TOKENIZATION_SUPPORT = false;
    
    public static final String TRXTYPE_AUTHORIZATION = 'A';//Authorization
    //public static final String TRXTYPE_BALANCEINQUIRY = 'B';//Balance Inquiry
    public static final String TRXTYPE_CREDIT = 'C';//Refund
    public static final String TRXTYPE_DELAYEDCAPTURE = 'D';//Delayed Capture
    //public static final String TRXTYPE_VOICEAUTH = 'F';//Voice Authorization
    public static final String TRXTYPE_INQUIRY = 'I';//Inquiry; useful for ACH
    //public static final String TRXTYPE_RATELOOKUP = 'K';//Rate Lookup
    //public static final String TRXTYPE_DATAUPLOAD = 'L';//Data Upload
    //public static final String TRXTYPE_DUPLICATE = 'N';//Duplicate Transaction
    public static final String TRXTYPE_SALE = 'S';//Authorization and capture
    public static final String TRXTYPE_VOID = 'V';//Void
    
    
    public static final String TENDER_ACH = 'A';//Automated clearinghouse (ACH)
    public static final String TENDER_CREDITCARD = 'C';//Credit card
    //public static final String TENDER_PINLESSDEBT = 'D';//Pinless debit
    //public static final String TENDER_TELECHECK = 'K';//Telecheck
    //public static final String TENDER_PAYPAL = 'P';//PayPal
    
    //Based on the Account Type list in Bank Account object
    public static final String ECHECK_BANK_ACC_TYPE_BUSINESSCHECKING = 'BusinessChecking';//C
    public static final String ECHECK_BANK_ACC_TYPE_CHECKING = 'Checking';//C
    public static final String ECHECK_BANK_ACC_TYPE_SAVINGS = 'Savings';//S
    
    public static final String INQUIRY_BY_PNREF = 'P';
    public static final String INQUIRY_BY_CUSTREF = 'C';
    
    String gatewayName;
    PG_CCDetails details;
    PG_BADetails acDetails;
    PG_Config config;   
    PG_Response pgResponse;
    public static Map<String, String> bankAccountType;
    
    public String Amount{get; set;}//Required
    public String CVV{get; set;}
    public String TransactionRef{get; set;}//PNREF, To be send as ORGID
    public String InvoiceRef{get; set;}
    public String CustomerRef{get; set;}//12 alnum, Payement Identifier for Payflow, unique even for retry
    public String COMMENT1{get; set;}
    public String COMMENT2{get; set;}
    public String EcheckType{get; set;}
    public String InquiryType{get; set;}//P for PINREF search, C for CUSTREF search
    public String StartTime{get; set;}//yyyymmddhhmmss
    public String EndTime{get; set;}//yyyymmddhhmmss, less than 30 days after start time
    
    public Integer R_TIMEOUT_MS{get; set;}
    public String R_ENDPOINT{get; set;}
    
    //@override(PG_PaymentGateway)
    public void init(PG_CCDetails details, 
        PG_Config config, 
        Map<String, String> transactionInfo
    ){
        gatewayName = config.paymentGatewayName;
        this.details = details;
        this.config = config;
        
        pgResponse = new PG_Response();
        pgResponse.paymentGatewayName = gatewayName;
        
        this.Amount = transactionInfo.get('Amount')!=null?
            transactionInfo.get('Amount'):'';
        this.CVV = transactionInfo.get('CVV')!=null?
            transactionInfo.get('CVV'):'';
        this.TransactionRef = transactionInfo.get('TransactionRef')!=null?
            transactionInfo.get('TransactionRef'):'';
        this.InvoiceRef = transactionInfo.get('InvoiceRef')!=null?
            transactionInfo.get('InvoiceRef'):'';
        this.CustomerRef = transactionInfo.get('CustomerRef')!=null?
            transactionInfo.get('CustomerRef'):'';
        this.COMMENT1 = transactionInfo.get('Comment')!=null?
            transactionInfo.get('Comment'):'';
        this.COMMENT2 = transactionInfo.get('Comment2')!=null?
            transactionInfo.get('Comment2'):'';
        this.InquiryType = transactionInfo.get('InquiryType')!=null?
            transactionInfo.get('InquiryType'):'';
        
        R_TIMEOUT_MS = config.timeoutLimit*1000;
        R_ENDPOINT = config.testMode?config.gatewayUrlTest:config.gatewayUrlProd;
        if(R_ENDPOINT == null || R_ENDPOINT == '') 
            throw new PG_Exception('Gateway Exception::invalid gateway endpoint url.');
        
        System.debug('---init():PG name='+gatewayName+', CC details='+details.debug()+', PG config='+config);
            //+', transactionInfo='+transactionInfo); 
    }
    
    //@override(PG_PaymentGateway)
    public void processTransaction(PG.TransactionType transactionType){//@throws PG_Exception
        
        if(config.tokenization && !TOKENIZATION_SUPPORT)
            throw new PG_Exception('Gateway Exception::Tokenization not supported for '+gatewayName);
        
        String resBody = '';
        
        if(PG.TransactionType.VERIFY == transactionType){
            pgResponse.transactionType = PG.TransactionType.VERIFY.name();
            resBody = verify();//Verify
        }else if(PG.TransactionType.AUTHORIZATION == transactionType){
            pgResponse.transactionType = PG.TransactionType.AUTHORIZATION.name();
            resBody = authorization();//Auth
        }else if(PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE == transactionType){
            pgResponse.transactionType = PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE.name();
            resBody = markForCapture();//Delayed Capture
        }else if(PG.TransactionType.AUTHORIZATION_CAPTURE == transactionType){
            pgResponse.transactionType = PG.TransactionType.AUTHORIZATION_CAPTURE.name();
            resBody = authorizationCapture();//Sale         
        }else if(PG.TransactionType.REVERSEAL == transactionType){
            pgResponse.transactionType = PG.TransactionType.REVERSEAL.name();
            resBody = reversal();//Void
        }else if(PG.TransactionType.REFUND == transactionType){
            pgResponse.transactionType = PG.TransactionType.REFUND.name();
            resBody = refund();//Credit
        }else if(PG.TransactionType.INQUIRY == transactionType){
            pgResponse.transactionType = PG.TransactionType.INQUIRY.name();
            resBody = inquiry(TENDER_CREDITCARD);//Inquiry
        }else{
            throw new PG_Exception('Gateway Exception::transaction '+transactionType.name()+
                ' not supported for '+gatewayName);
        }
        
        populateResponse(resBody);
        
    }
    
    //@override(PG_PaymentGatewayACH)
    public void initACHSubmission(PG_BADetails details, 
        PG_Config config, 
        Map<String, String> transactionInfo
    ){
        //gatewayName = 'PayFlow:ACH';
        gatewayName = config.paymentGatewayName;
        this.acDetails = details;
        this.config = config;
        bankAccountType = new Map<string,string>{
            ECHECK_BANK_ACC_TYPE_BUSINESSCHECKING => 'C',
            ECHECK_BANK_ACC_TYPE_CHECKING => 'C',
            ECHECK_BANK_ACC_TYPE_SAVINGS => 'S'
        };
        
        pgResponse = new PG_Response();
        pgResponse.paymentGatewayName = gatewayName;
        
        this.Amount = transactionInfo.get('Amount')!=null?
            transactionInfo.get('Amount'):'';
        this.TransactionRef = transactionInfo.get('TransactionRef')!=null?
            transactionInfo.get('TransactionRef'):'';
        this.EcheckType = transactionInfo.get('EcheckType')!=null?
            transactionInfo.get('EcheckType'):'';
        this.InvoiceRef = transactionInfo.get('InvoiceRef')!=null?
            transactionInfo.get('InvoiceRef'):'';
        this.CustomerRef = transactionInfo.get('CustomerRef')!=null?
            transactionInfo.get('CustomerRef'):'';
        this.COMMENT1 = transactionInfo.get('Comment')!=null?
            transactionInfo.get('Comment'):'Transaction Amount ' + Amount;
        this.COMMENT2 = transactionInfo.get('Comment2')!=null?
            transactionInfo.get('Comment2'):'Transaction Type Echeck';
        this.InquiryType = transactionInfo.get('InquiryType')!=null?
            transactionInfo.get('InquiryType'):'';
        this.StartTime = transactionInfo.get('StartTime')!=null?
            transactionInfo.get('StartTime'):'';
        this.EndTime = transactionInfo.get('EndTime')!=null?
            transactionInfo.get('EndTime'):'';
        
        R_TIMEOUT_MS = config.timeoutLimit*1000;
        R_ENDPOINT = config.testMode?config.gatewayUrlTest:config.gatewayUrlProd;
        if(R_ENDPOINT == null || R_ENDPOINT == '') 
            throw new PG_Exception('Gateway Exception::invalid gateway endpoint url.');
        
        System.debug('---initACHSubmission():PG name='+gatewayName+', BA details='+details+', PG config='+config
            +', transactionInfo='+transactionInfo); 
    }
    
    //@override(PG_PaymentGatewayACH)
    public void submitTransactionACH(PG.TransactionTypeACH transactionType){
        String resBody = '';
                
        if(PG.TransactionTypeACH.DEBIT == transactionType){
            pgResponse.transactionType = PG.TransactionTypeACH.DEBIT.name();
            resBody = debit();
        }else if(PG.TransactionTypeACH.CREDIT == transactionType){
            pgResponse.transactionType = PG.TransactionTypeACH.CREDIT.name();
            resBody = credit();
        }else if(PG.TransactionTypeACH.INQUIRY == transactionType){
            pgResponse.transactionType = PG.TransactionTypeACH.INQUIRY.name();
            resBody = inquiry(TENDER_ACH);
        }else if(PG.TransactionTypeACH.CANCEL == transactionType){
            pgResponse.transactionType = PG.TransactionTypeACH.CANCEL.name();
            resBody = cancel();
        }else if(PG.TransactionTypeACH.PRENOTE == transactionType){
            pgResponse.transactionType = PG.TransactionTypeACH.PRENOTE.name();
            resBody = prenote();
        }else{
            throw new PG_Exception('Gateway Exception::transaction '+transactionType.name()+
                ' not supported for '+gatewayName);
        }
        
        populateResponse(resBody);
    }
    
    //@override(PG_PaymentGateway)
    public PG_Response getPGResponse(){
        return pgResponse;
    }
    
    //CC
    public String verify(){//@throws PG_Exception   
        String reqBody = '';    
        String resBody = '';
        Map<String, String> reqMap = prepareCC();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_AUTHORIZATION,
            'ACCT' => details.creditCardNumber,         
            'AMT' => '0.00',            
            'EXPDATE' => details.expiryMonth + details.expiryYear.substring(details.expiryYear.length()-2),
            'COMMENT1[' + String.valueOf(COMMENT1.length()) + ']' => COMMENT1,
            'COMMENT2[' + String.valueOf(COMMENT2.length()) + ']' => COMMENT2,
            'INVNUM' => InvoiceRef,
            'CUSTREF' => CustomerRef
        });
        if(config.usePartnerCode) reqMap.put('BUTTONSOURCE', config.partnerCode);
        //if(config.testMode) reqMap.put('VERBOSITY', 'HIGH');
        if(config.cardCodeVerification) reqMap.put('CVV2', CVV);
        if(config.addressVerification){
            reqMap.put('BILLTOZIP', details.ccPostalCode);//Res-AVSADDR
            if(!config.addressVerificationZipOnly){
                String BILLTOSTREET = 'BILLTOSTREET[' + String.valueOf(details.ccStreet2.length()) + ']';
                reqMap.put(BILLTOSTREET, details.ccStreet2);//Res-AVSZIP
            }
        }
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        reqMapMasked.put('ACCT', PG.maskWord(reqMapMasked.get('ACCT'), '', -4, true));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=A70H6A0B9136&RESPMSG=Verified&AUTHCODE=010101&AVSADDR=Y&AVSZIP=Y&CVV2MATCH=Y&HOSTCODE=A&PROCAVS=Y&PROCCVV2=M&TRANSTIME=2014-03-10 05:19:56&AMT=0.00&ACCT=1881&EXPDATE=0520&CARDTYPE=0&IAVS=N';
        }
        return resBody;
    }
    
    //CC
    public String authorization(){//@throws PG_Exception    
        String reqBody = '';    
        String resBody = '';
        Map<String, String> reqMap = prepareCC();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_AUTHORIZATION,
            'ACCT' => details.creditCardNumber,         
            'AMT' => Amount,            
            'EXPDATE' => details.expiryMonth + details.expiryYear.substring(details.expiryYear.length()-2),
            'COMMENT1[' + String.valueOf(COMMENT1.length()) + ']' => COMMENT1,
            'COMMENT2[' + String.valueOf(COMMENT2.length()) + ']' => COMMENT2,
            'INVNUM' => InvoiceRef,
            'CUSTREF' => CustomerRef
        });
        if(config.usePartnerCode) reqMap.put('BUTTONSOURCE', config.partnerCode);
        //if(config.testMode) reqMap.put('VERBOSITY', 'HIGH');
        if(config.cardCodeVerification) reqMap.put('CVV2', CVV);
        if(config.addressVerification){
            reqMap.put('BILLTOZIP', details.ccPostalCode);//Res-AVSADDR
            if(!config.addressVerificationZipOnly){
                String BILLTOSTREET = 'BILLTOSTREET[' + String.valueOf(details.ccStreet2.length()) + ']';
                reqMap.put(BILLTOSTREET, details.ccStreet2);//Res-AVSZIP
            }
        }
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        reqMapMasked.put('ACCT', PG.maskWord(reqMapMasked.get('ACCT'), '', -4, true));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=A70H6A0BDABC&RESPMSG=Approved&AUTHCODE=010101&AVSADDR=Y&AVSZIP=Y&CVV2MATCH=Y&HOSTCODE=A&PROCAVS=Y&PROCCVV2=M&TRANSTIME=2014-03-10 07:05:14&AMT=1.89&ACCT=1881&EXPDATE=0520&CARDTYPE=0&IAVS=N';
        }
        return resBody;
    }
    
    //CC
    public String markForCapture(){//@throws PG_Exception   
        String reqBody = '';    
        String resBody = '';
        Map<String, String> reqMap = prepareCC();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_DELAYEDCAPTURE,
            'ORIGID' => TransactionRef
        });
        //if(config.testMode) reqMap.put('VERBOSITY', 'HIGH');
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=A70H6A0EA81F&RESPMSG=Approved&AUTHCODE=010101&AVSADDR=Y&AVSZIP=Y&CVV2MATCH=Y&HOSTCODE=A&PROCAVS=Y&PROCCVV2=M&TRANSTIME=2014-03-11 04:58:28&IAVS=N';
        }
        return resBody;
    }
    
    //CC
    public String authorizationCapture(){//@throws PG_Exception 
        String reqBody = '';    
        String resBody = '';
        Map<String, String> reqMap = prepareCC();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_SALE,
            'ACCT' => details.creditCardNumber,         
            'AMT' => Amount,            
            'EXPDATE' => details.expiryMonth + details.expiryYear.substring(details.expiryYear.length()-2),
            'COMMENT1[' + String.valueOf(COMMENT1.length()) + ']' => COMMENT1,
            'COMMENT2[' + String.valueOf(COMMENT2.length()) + ']' => COMMENT2,
            'INVNUM' => InvoiceRef,
            'CUSTREF' => CustomerRef
        });
        if(config.usePartnerCode) reqMap.put('BUTTONSOURCE', config.partnerCode);
        //if(config.testMode) reqMap.put('VERBOSITY', 'HIGH');
        if(config.cardCodeVerification) reqMap.put('CVV2', CVV);
        if(config.addressVerification){
            reqMap.put('BILLTOZIP', details.ccPostalCode);//Res-AVSADDR
            if(!config.addressVerificationZipOnly && details.ccStreet2 != null){
                String BILLTOSTREET = 'BILLTOSTREET[' + String.valueOf(details.ccStreet2.length()) + ']';
                reqMap.put(BILLTOSTREET, details.ccStreet2);//Res-AVSZIP
            }
        }
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        reqMapMasked.put('ACCT', PG.maskWord(reqMapMasked.get('ACCT'), '', -4, true));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=A10H6BC5A75B&RESPMSG=Approved&AUTHCODE=010101&AVSADDR=Y&AVSZIP=Y&CVV2MATCH=Y&HOSTCODE=A&PROCAVS=Y&PROCCVV2=M&TRANSTIME=2014-03-11 03:37:35&AMT=1.85&ACCT=1881&EXPDATE=0520&CARDTYPE=0&IAVS=N';
        }
        return resBody;
    }
    
    //CC
    public String reversal(){//@throws PG_Exception
        String reqBody = '';    
        String resBody = '';
        Map<String, String> reqMap = prepareCC();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_VOID,
            'ORIGID' => TransactionRef
        });
        //if(config.testMode) reqMap.put('VERBOSITY', 'HIGH');
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=A10H6BC9D66E&RESPMSG=Approved&HOSTCODE=A&TRANSTIME=2014-03-12 23:44:33';
        }
        return resBody;
    }
    
    //CC
    public String refund(){//@throws PG_Exception
        String reqBody = '';    
        String resBody = '';
        Map<String, String> reqMap = prepareCC();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_CREDIT,      
            'AMT' => Amount,
            'ORIGID' => TransactionRef
        });
        //if(config.testMode) reqMap.put('VERBOSITY', 'HIGH');
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=A10H6BC9DD59&RESPMSG=Approved&HOSTCODE=A&TRANSTIME=2014-03-12 23:59:01';
        }
        return resBody;
    }
    
    //ECHECK
    public String debit(){//@throws PG_Exception    
        String reqBody = '';    
        String resBody = '';
        //user input validations
        if(Amount == null || Amount == '' || Decimal.valueOf(Amount) < 0)
            throw new PG_Exception('Gateway Exception::Invalid amount - '+Amount);
        if(EcheckType != 'CCD' && EcheckType != 'WEB')
            throw new PG_Exception('Gateway Exception::Echeck Type not supported - '+EcheckType);
        if(EcheckType == 'CCD' && acDetails.baAccountType != ECHECK_BANK_ACC_TYPE_BUSINESSCHECKING)     
            throw new PG_Exception('Gateway Exception::Account Type should be '+ECHECK_BANK_ACC_TYPE_BUSINESSCHECKING
            +' for Echeck Type '+EcheckType);
        if(EcheckType == 'WEB' && (acDetails.baAccountType != ECHECK_BANK_ACC_TYPE_CHECKING &&
            acDetails.baAccountType != ECHECK_BANK_ACC_TYPE_SAVINGS)
        )       
            throw new PG_Exception('Gateway Exception::Account Type should be either '+ECHECK_BANK_ACC_TYPE_CHECKING
            +' or '+ECHECK_BANK_ACC_TYPE_SAVINGS+' for Echeck Type '+EcheckType);
        Map<String, String> reqMap = prepareACH();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_SALE,
            'AUTHTYPE' => EcheckType,//Only CCD and WEB supported
            'ABA' => acDetails.baABANumber,         
            'ACCT' => acDetails.baAccountNumber,            
            'ACCTTYPE' => bankAccountType.get(acDetails.baAccountType),         
            'FIRSTNAME' => acDetails.baAccountName,         
            'PRENOTE' => 'N',//Default          
            'AMT' => Amount,
            'COMMENT1[' + String.valueOf(COMMENT1.length()) + ']' => COMMENT1,
            'COMMENT2[' + String.valueOf(COMMENT2.length()) + ']' => COMMENT2,
            'INVNUM' => InvoiceRef,
            'CUSTREF' => CustomerRef
        });
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        reqMapMasked.put('ABA', PG.maskWord(reqMapMasked.get('ABA'), '', -4, true));
        reqMapMasked.put('ACCT', PG.maskWord(reqMapMasked.get('ACCT'), '', -4, true));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=V63D15597698&RESPMSG=Approved';
        }
        return resBody;
    }
    
    //ECHECK
    public String credit(){//@throws PG_Exception   
        String reqBody = '';    
        String resBody = '';
        //user input validations
        if(TransactionRef == '')
            throw new PG_Exception('Gateway Exception::Original Transaction Id required');
        Map<String, String> reqMap = prepareACH();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_CREDIT,
            'AUTHTYPE' => EcheckType,//Only CCD and WEB supported
            'ORIGID' => TransactionRef
        });
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=V63D15597698&RESPMSG=Approved';
        }
        return resBody;
    }
    
    //ECHECK and CC
    public String inquiry(String tender){//@throws PG_Exception 
        String reqBody = '';    
        String resBody = '';
        //user input validations
        Map<String, String> reqMap = prepare();
        reqMap.put('TENDER', tender);
        reqMap.put('VERBOSITY', 'HIGH');
        //System.debug('---InquiryType='+InquiryType);
        if(InquiryType == '' || InquiryType == INQUIRY_BY_PNREF){           
            if(TransactionRef == '')
                throw new PG_Exception('Gateway Exception::Original Transaction Id required.');
            reqMap.putAll(new Map<String, String>{              
                'TRXTYPE' => PG_PayFlowPro.TRXTYPE_INQUIRY,
                'ORIGID' => TransactionRef
            }); 
        }else if(InquiryType == INQUIRY_BY_CUSTREF){
            if(CustomerRef == '')
                throw new PG_Exception('Gateway Exception::CustomerRef required.');
            reqMap.putAll(new Map<String, String>{              
                'TRXTYPE' => PG_PayFlowPro.TRXTYPE_INQUIRY,
                'CUSTREF' => CustomerRef
            });
            //TODO: EndDtae not be less than 30 days from StartDate
            if(StartTime != '' && EndTime != ''){
                reqMap.put('STARTTIME', StartTime);
                reqMap.put('ENDTIME', EndTime);
            }
        }else throw new PG_Exception('Gateway Exception::Wrong InquiryType. Only P(PINREF) or C(CUSTREF) Supported.');
        
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=A70H6A6E2EE7&TRANSSTATE=9&CUSTREF=C-101&ORIGRESULT=0&ORIGPNREF=A70H6A0BDABC&RESPMSG=Approved&AUTHCODE=010101&AVSADDR=Y&AVSZIP=Y&CVV2MATCH=Y&IAVS=N';
        }
        return resBody;
    }
    
    //ECHECK
    public String cancel(){//@throws PG_Exception   
        String reqBody = '';    
        String resBody = '';
        //user input validations
        if(TransactionRef == '')
            throw new PG_Exception('Gateway Exception::Original Transaction Id required');
        Map<String, String> reqMap = prepareACH();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_VOID,                        
            'ABA' => acDetails.baABANumber,         
            'ACCT' => acDetails.baAccountNumber,            
            'ACCTTYPE' => bankAccountType.get(acDetails.baAccountType),
            'ORIGID' => TransactionRef
        });
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        reqMapMasked.put('ABA', PG.maskWord(reqMapMasked.get('ABA'), '', -4, true));
        reqMapMasked.put('ACCT', PG.maskWord(reqMapMasked.get('ACCT'), '', -4, true));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=V63D15597698&RESPMSG=Approved';
        }
        return resBody;
    }
    
    //ECHECK
    public String prenote(){//@throws PG_Exception  
        String reqBody = '';    
        String resBody = '';
        //user input validations
        if(EcheckType != 'CCD' && EcheckType != 'WEB')
            throw new PG_Exception('Gateway Exception::Echeck Type not supported - '+EcheckType);
        if(EcheckType == 'CCD' && acDetails.baAccountType != ECHECK_BANK_ACC_TYPE_BUSINESSCHECKING)     
            throw new PG_Exception('Gateway Exception::Account Type should be '+ECHECK_BANK_ACC_TYPE_BUSINESSCHECKING
            +' for Echeck Type '+EcheckType);
        if(EcheckType == 'WEB' && (acDetails.baAccountType != ECHECK_BANK_ACC_TYPE_CHECKING &&
            acDetails.baAccountType != ECHECK_BANK_ACC_TYPE_SAVINGS)
        )       
            throw new PG_Exception('Gateway Exception::Account Type should be either '+ECHECK_BANK_ACC_TYPE_CHECKING
            +' or '+ECHECK_BANK_ACC_TYPE_SAVINGS+' for Echeck Type '+EcheckType);
        Map<String, String> reqMap = prepareACH();
        reqMap.putAll(new Map<String, String>{              
            'TRXTYPE' => PG_PayFlowPro.TRXTYPE_SALE,
            'AUTHTYPE' => EcheckType,//Only CCD and WEB supported
            'ABA' => acDetails.baABANumber,         
            'ACCT' => acDetails.baAccountNumber,            
            'ACCTTYPE' => bankAccountType.get(acDetails.baAccountType),         
            'FIRSTNAME' => acDetails.baAccountName,         
            'PRENOTE' => 'Y',//Default N            
            'AMT' => '0.00',
            'COMMENT1[' + String.valueOf(COMMENT1.length()) + ']' => COMMENT1,
            'COMMENT2[' + String.valueOf(COMMENT2.length()) + ']' => COMMENT2,
            'INVNUM' => InvoiceRef,
            'CUSTREF' => CustomerRef
        });
        //System.debug('---reqMap='+reqMap);
        Map<String, String> reqMapMasked = reqMap.clone();
        reqMapMasked.put('USER', PG.maskWord(reqMapMasked.get('USER'), '', 4, false));
        reqMapMasked.put('PWD', PG.maskWord(reqMapMasked.get('PWD'), '', 4, false));
        reqMapMasked.put('ABA', PG.maskWord(reqMapMasked.get('ABA'), '', -4, true));
        reqMapMasked.put('ACCT', PG.maskWord(reqMapMasked.get('ACCT'), '', -4, true));
        pgResponse.transactionRequestMap = reqMapMasked;
        pgResponse.reqLog = PG.mapAsString(reqMapMasked);
        reqBody = PG_Caller.generateQueryStringComplete(reqMap);
        resBody = submitCall(reqBody);
        if(Test.isRunningTest()){
            resBody = 'RESULT=0&PNREF=V63D15597698&RESPMSG=Approved';
        }
        return resBody;
    }
    
    private Map<String, String> prepare(){
        return new Map<String, String>{
            //User paramters
            'USER' => config.loginId,
            'VENDOR' => config.loginId,
            'PARTNER' => config.merchantReference,
            'PWD' => config.password,
            //
            'CURRENCY' => config.currencySupport
        };
    }
    
    //CC
    private Map<String, String> prepareCC(){
        Map<String, String> reqMap = prepare();
        reqMap.put('TENDER', PG_PayFlowPro.TENDER_CREDITCARD);
        return reqMap;
    }
    
    //ECHECK
    private Map<String, String> prepareACH(){
        Map<String, String> reqMap = prepare();
        reqMap.put('TENDER', PG_PayFlowPro.TENDER_ACH);
        return reqMap;
    }
    
    private String submitCall(String reqBody){//@throws PG_Exception
        String resBody = '';
        //pgResponse.reqLog = reqBody;
        PG_Caller caller = new PG_Caller(
            new Map<string,string>{
                'CURLOPT_TIMEOUT' => String.valueOf(R_TIMEOUT_MS),
                'CURLOPT_RETURNTRANSFER' => 'TRUE',
                'Connection' => 'close',
                'Content-Length' => String.valueOf(reqBody.length()),
                'Content-Type' => 'text/name',//text/xml in case of xml
                'Host' => R_ENDPOINT,
                //TODO: Replace it using Trx Id
                'X-VPS-REQUEST-ID' => String.valueOf(System.now().format('YYMMddHHmmssSSS')),
                'X-VPS-CLIENT-TIMEOUT' => String.valueOf(R_TIMEOUT_MS) //Default 45 Sec
            },
            PG.HTTP_METHOD_POST,
            R_ENDPOINT,
            reqBody,
            R_TIMEOUT_MS
        );  
        resBody = caller.call();
        pgResponse.resLog = resBody;    
        return resBody;
    }
    
    private void populateResponse(String resBody){
        if(resBody != '' && resBody.length()>0 ){
            String[] resArr = resBody.split('&', 0);
            String[] nvmArr;
            Map<String, String> resMap = new Map<string,string>();
            for(String nvStr : resArr){
                nvmArr = nvStr.split('=', -1);
                resMap.put(nvmArr[0], nvmArr[1]);               
            }
            System.debug('---resMap='+resMap);
            pgResponse.transactionResponseMap = resMap;
            pgResponse.processStatus = PG.Status.SUCCESS.name();
            pgResponse.approvalStatus = resMap.get('RESULT') == '0'?
                PG.Status.APPROVED.name():PG.Status.DECLINED.name();
            pgResponse.responseCode = resMap.get('RESULT');
            pgResponse.responseMessage = resMap.get('RESPMSG');
            pgResponse.transactionId = resMap.get('PNREF');
            pgResponse.transactionTime = System.now();
            pgResponse.isException = false;
            pgResponse.exceptionMessage = '';
        }else{
            throw new PG_Exception('Gateway Exception::Invalid Response received. Response='+resBody);
        }
    }

}