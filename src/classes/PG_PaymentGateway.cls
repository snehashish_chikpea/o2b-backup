/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-19
@see 
---------------------------------------------------------------------------------------------------
*/
global interface PG_PaymentGateway {    
 
    void init(PG_CCDetails details, 
        PG_Config config, 
        Map<String, String> transactionInfo
    );
     
    void processTransaction(PG.TransactionType transactionType);
    
    PG_Response getPGResponse();
    
}