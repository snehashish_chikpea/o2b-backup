@isTest
private class TestBatchInvoiceMail {

    static testMethod void myUnitTest() {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Billing_Type__c='Anniversary Subscription',Create_Invoice_on_Process_Order__c=true);
        insert(o2bset);
        Account acc=new Account(name='acc1',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today());
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        Invoicing.CreateInvoice(new list<Id>{acc.Id}, false);
        BatchInvoiceMailSendRunScheduler.executeschedule();
    }
    
    static testMethod void myUnitTest1() {
        ChikPeaO2B__O2B_Setting__c o2bset=new ChikPeaO2B__O2B_Setting__c(ChikPeaO2B__Usage_on_orderline_quantity__c=true,ChikPeaO2B__Account_based_tiering__c=false,ChikPeaO2B__Billing_Type__c='Anniversary Subscription',Create_Invoice_on_Process_Order__c=true);
        insert(o2bset);
        Account acc=new Account(name='acc1',Bill_Cycle__c='Monthly',ChikPeaO2B__Next_Bill_Date__c=system.today());
        insert(acc);
        ChikPeaO2B__Price_Book__c pb=new ChikPeaO2B__Price_Book__c(Name='PB1',ChikPeaO2B__Active__c=true);
        insert(pb);
        
        list<item__c>itemlist=new List<Item__c>();
        List<ChikPeaO2B__Rate_Plan__c>RPlist=new List<ChikPeaO2B__Rate_Plan__c>();
        
        ChikPeaO2B__Item__c oneoff=new ChikPeaO2B__Item__c(name='oneoff',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='One-Off');    
        itemlist.add(oneoff);
        ChikPeaO2B__Item__c rec=new ChikPeaO2B__Item__c(name='rec',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Recurring');    
        itemlist.add(rec);
        ChikPeaO2B__Item__c post=new ChikPeaO2B__Item__c(name='post',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Post-Paid Usage');    
        itemlist.add(post);
        ChikPeaO2B__Item__c pre=new ChikPeaO2B__Item__c(name='pre',ChikPeaO2B__Active__c=true,ChikPeaO2B__Category__c='Software',ChikPeaO2B__Item_Type__c='Pre-Paid Usage',Is_Shipping__c=true);    
        itemlist.add(pre);
        insert(itemlist);
        
        ChikPeaO2B__Rate_Plan__c rp1=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Monthly',ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp1);
        ChikPeaO2B__Rate_Plan__c rp2=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Quarterly',ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5);
        RPlist.add(rp2);
        ChikPeaO2B__Rate_Plan__c rp3=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Daily',ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=0.5,ChikPeaO2B__Free_Usage__c=30);
        RPlist.add (rp3);
        ChikPeaO2B__Rate_Plan__c rp4=new ChikPeaO2B__Rate_Plan__c(ChikPeaO2B__Bill_Cycle__c='Weekly',ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Non_Recurring_Charge__c=1,ChikPeaO2B__Period_Off_Set__c=6,ChikPeaO2B__Period_Off_Set_Unit__c='Month(s)',ChikPeaO2B__Price_Book__c=pb.id,ChikPeaO2B__Pricing_Type__c='Flat',ChikPeaO2B__Start_Off_set__c=0,ChikPeaO2B__Start_Off_Set_Unit__c='Day(s)',ChikPeaO2B__UOM__c='MB',ChikPeaO2B__Usage_Rate__c=1.5,ChikPeaO2B__Max_Usage__c=50);
        RPlist.add(rp4);
        insert(RPlist);
        ChikPeaO2B__Order__c ord=new ChikPeaO2B__Order__c(ChikPeaO2B__Account__c=acc.id,ChikPeaO2B__Price_Book__c=pb.id,Shipping_Required__c='Yes');
        insert(ord);
        List<ChikPeaO2B__Order_Line__c>OLlist=new List<ChikPeaO2B__Order_Line__c>();
        ChikPeaO2B__Order_Line__c orl1=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=oneoff.id,ChikPeaO2B__Item_Type__c='One-Off',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp1.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl1);
        ChikPeaO2B__Order_Line__c orl2=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=rec.id,ChikPeaO2B__Item_Type__c='Recurring',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp2.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl2);
        ChikPeaO2B__Order_Line__c orl3=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=post.id,ChikPeaO2B__Item_Type__c='Post-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp3.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl3);
        ChikPeaO2B__Order_Line__c orl4=new ChikPeaO2B__Order_Line__c(ChikPeaO2B__Item__c=pre.id,ChikPeaO2B__Item_Type__c='Pre-Paid Usage',ChikPeaO2B__Order__c=ord.id,ChikPeaO2B__Quantity__c=2,ChikPeaO2B__Rate_Plan__c=rp4.id,ChikPeaO2B__Unit_Price__c=2.5);
        OLlist.add(orl4);
        insert(OLlist);
        List<id>oidList=new List<id>();
        oidList.add(ord.id);
        string invStr=ProcessOrder.OrderProcessor(oidList);
        system.debug('---------->'+invStr);
      
        BatchInvoiceMailSend BC = new BatchInvoiceMailSend();
        List<ChikPeaO2B__O2B_Setting__c> o2bset1=[SELECT id,name,ChikPeaO2B__Admin_Email__c, ChikPeaO2B__Batch_Size__c,ChikPeaO2B__Card_Process_Frequency__c,ChikPeaO2B__Card_Process_Interval_days__c from ChikPeaO2B__O2B_Setting__c limit 1];
        BC.email=((o2bset1!=null && o2bset1.size()>0 && o2bset1[0].ChikPeaO2B__Admin_Email__c!=null)?o2bset1[0].ChikPeaO2B__Admin_Email__c:'o2b@chikpea.com');
        BC.query='select id,ChikPeaO2B__Email_Sent__c,ChikPeaO2B__Amount_Due__c,ChikPeaO2B__Account__r.ChikPeaO2B__Bill_To_Parent__c'+
                   ' from ChikPeaO2B__Invoice__c where ChikPeaO2B__Invoice_Status__c=\'Open\' AND ChikPeaO2B__Email_Sent__c=Null';
        ID batchprocessid = Database.executeBatch(BC,199);
       
        //BatchInvoiceMailSendRunScheduler.executeschedule();
    }
}