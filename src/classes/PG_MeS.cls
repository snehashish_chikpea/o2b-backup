/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-03-13
@see 
---------------------------------------------------------------------------------------------------
*/
public class PG_MeS implements PG_PaymentGateway{
	
	public static final Boolean TOKENIZATION_SUPPORT = false;
	
	public static final String TRXTYPE_SALE = 'D';//Authorization and capture
	//public static final String TRXTYPE_CREDIT = 'C';//;Domestic only
	public static final String TRXTYPE_PREAUTH = 'P';//Authorization
	//public static final String TRXTYPE_REAUTH = 'J';//previously declined trx
	//public static final String TRXTYPE_OFFLINE = 'O';//;Domestic only
	public static final String TRXTYPE_VOID = 'V';//Void
	public static final String TRXTYPE_SETTLE_PREAUTH = 'S';//Delayed capture
	public static final String TRXTYPE_REFUND = 'U';//;multiple domestic only
	//public static final String TRXTYPE_STORE_CARD_DATA = 'T';//
	//public static final String TRXTYPE_DEL_STORE_CARD_DATA = 'X';//
	public static final String TRXTYPE_VERIFICATION = 'A';//Verify;Domestic only
	//public static final String TRXTYPE_INQUIRY = 'I';//
	//public static final String TRXTYPE_BATCH_CLOSE = 'Z';//
	
	String gatewayName;
    PG_CCDetails details;
    PG_Config config;   
    PG_Response pgResponse;
    
    public String Amount{get; set;}//Required
    public String CVV{get; set;}
	public String TransactionRef{get; set;}
	public String InvoiceRef{get; set;}
	public String CustomerRef{get; set;}
	
	public Integer R_TIMEOUT_MS{get; set;}
	public String R_ENDPOINT{get; set;}
	
	 public void init(PG_CCDetails details, 
        PG_Config config, 
        Map<String, String> transactionInfo
    ){
    	gatewayName = config.paymentGatewayName;
        this.details = details;
        this.config = config;
        
        pgResponse = new PG_Response();
		pgResponse.paymentGatewayName = gatewayName;
		
		this.Amount = transactionInfo.get('Amount')!=null?
			transactionInfo.get('Amount'):'';
        this.CVV = transactionInfo.get('CVV')!=null?
            transactionInfo.get('CVV'):'';
		this.TransactionRef = transactionInfo.get('TransactionRef')!=null?
			transactionInfo.get('TransactionRef'):'';
		this.InvoiceRef = transactionInfo.get('InvoiceRef')!=null?
			transactionInfo.get('InvoiceRef'):'';
		this.CustomerRef = transactionInfo.get('CustomerRef')!=null?
			transactionInfo.get('CustomerRef'):'';
			
		R_TIMEOUT_MS = config.timeoutLimit*1000;
		R_ENDPOINT = config.testMode?config.gatewayUrlTest:config.gatewayUrlProd;
		if(R_ENDPOINT == null || R_ENDPOINT == '') 
			throw new PG_Exception('Gateway Exception::invalid gateway endpoint url.');
		
		System.debug('---init():PG name='+gatewayName+', CC details='+details.debug()+', PG config='+config);
			//+', transactionInfo='+transactionInfo);
			
    }
    
    public void processTransaction(PG.TransactionType transactionType){//@throws PG_Exception
    	
    	if(config.tokenization && !TOKENIZATION_SUPPORT)
    		throw new PG_Exception('Gateway Exception::Tokenization not supported for '+gatewayName);
        
        String resBody = '';
        
		if(PG.TransactionType.VERIFY == transactionType){
			pgResponse.transactionType = PG.TransactionType.VERIFY.name();
			resBody = verify();//Verify
		}else if(PG.TransactionType.AUTHORIZATION == transactionType){
			pgResponse.transactionType = PG.TransactionType.AUTHORIZATION.name();
			resBody = authorization();//Auth
		}else if(PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE == transactionType){
			pgResponse.transactionType = PG.TransactionType.PRIOR_AUTHORIZATION_CAPTURE.name();
			resBody = markForCapture();//Delayed Capture
		}else if(PG.TransactionType.AUTHORIZATION_CAPTURE == transactionType){
			pgResponse.transactionType = PG.TransactionType.AUTHORIZATION_CAPTURE.name();
			resBody = authorizationCapture();//Sale			
		}else if(PG.TransactionType.REVERSEAL == transactionType){
			pgResponse.transactionType = PG.TransactionType.REVERSEAL.name();
			resBody = reversal();//Void
		}else if(PG.TransactionType.REFUND == transactionType){
			pgResponse.transactionType = PG.TransactionType.REFUND.name();
			resBody = refund();//Credit
		}else{
			throw new PG_Exception('Gateway Exception::transaction '+transactionType.name()+
				' not supported for '+gatewayName);
		}
		
		populateResponse(resBody);
        
    }
    
    public PG_Response getPGResponse(){
        return pgResponse;
    }
    
    public String verify(){//@throws PG_Exception	
		String reqBody = '';	
		String resBody = '';
		Map<String, String> reqMap = prepare();
		reqMap.putAll(new Map<String, String>{				
			'transaction_type' => TRXTYPE_VERIFICATION,
			'card_number' => details.creditCardNumber,
			'card_exp_date' => details.expiryMonth + details.expiryYear.substring(details.expiryYear.length()-2),
			'transaction_amount' => Amount,
			'invoice_number' => InvoiceRef,
			'client_reference_number' => CustomerRef
		});
		if(config.cardCodeVerification) reqMap.put('cvv2', CVV);
		if(config.addressVerification){
			reqMap.put('cardholder_zip', details.ccPostalCode);//Res-AVSADDR
			if(!config.addressVerificationZipOnly){
				reqMap.put('cardholder_street_address', EncodingUtil.urlEncode(details.ccStreet2, 'UTF-8'));//Res-AVSZIP
			}
		}
		//System.debug('---reqMap='+reqMap);
		Map<String, String> reqMapMasked = reqMap.clone();
		reqMapMasked.put('profile_id', PG.maskWord(reqMapMasked.get('profile_id'), '', 4, false));
		reqMapMasked.put('profile_key', PG.maskWord(reqMapMasked.get('profile_key'), '', 4, false));
		reqMapMasked.put('card_number', PG.maskWord(reqMapMasked.get('card_number'), '', -4, true));
		pgResponse.transactionRequestMap = reqMapMasked;
		pgResponse.reqLog = PG.mapAsString(reqMapMasked);
		reqBody = PG_Caller.generateQueryStringComplete(reqMap);
		resBody = submitCall(reqBody);
		if(Test.isRunningTest()){
			resBody = 'transaction_id=5c81b2c566863028be4086b2247ba760&error_code=085&auth_response_text=Exact Match&avs_result=Y&auth_code=T2870H';
		}
		return resBody;
	}
    
    public String authorization(){//@throws PG_Exception	
		String reqBody = '';	
		String resBody = '';
		Map<String, String> reqMap = prepare();
		reqMap.putAll(new Map<String, String>{				
			'transaction_type' => TRXTYPE_PREAUTH,
			'card_number' => details.creditCardNumber,
			'card_exp_date' => details.expiryMonth + details.expiryYear.substring(details.expiryYear.length()-2),
			'transaction_amount' => Amount,
			'invoice_number' => InvoiceRef,
			'client_reference_number' => CustomerRef
		});
		if(config.cardCodeVerification) reqMap.put('cvv2', CVV);
		if(config.addressVerification){
			reqMap.put('cardholder_zip', details.ccPostalCode);//Res-AVSADDR
			if(!config.addressVerificationZipOnly){
				reqMap.put('cardholder_street_address', EncodingUtil.urlEncode(details.ccStreet2, 'UTF-8'));//Res-AVSZIP
			}
		}
		//System.debug('---reqMap='+reqMap);
		Map<String, String> reqMapMasked = reqMap.clone();
		reqMapMasked.put('profile_id', PG.maskWord(reqMapMasked.get('profile_id'), '', 4, false));
		reqMapMasked.put('profile_key', PG.maskWord(reqMapMasked.get('profile_key'), '', 4, false));
		reqMapMasked.put('card_number', PG.maskWord(reqMapMasked.get('card_number'), '', -4, true));
		pgResponse.transactionRequestMap = reqMapMasked;
		pgResponse.reqLog = PG.mapAsString(reqMapMasked);
		reqBody = PG_Caller.generateQueryStringComplete(reqMap);
		resBody = submitCall(reqBody);
		if(Test.isRunningTest()){
			resBody = 'transaction_id=b7a4f6b2c9463710b82c3d9c6560a058&error_code=000&auth_response_text=Exact Match&avs_result=Y&auth_code=T3053H';
		}
		return resBody;
	}
    
    public String markForCapture(){//@throws PG_Exception	
		String reqBody = '';	
		String resBody = '';
		Map<String, String> reqMap = prepare();
		reqMap.putAll(new Map<String, String>{				
			'transaction_type' => TRXTYPE_SETTLE_PREAUTH,
			'transaction_id' => TransactionRef,
			'transaction_amount' => Amount,
			'invoice_number' => InvoiceRef,
			'client_reference_number' => CustomerRef
		});
		//System.debug('---reqMap='+reqMap);
		Map<String, String> reqMapMasked = reqMap.clone();
		reqMapMasked.put('profile_id', PG.maskWord(reqMapMasked.get('profile_id'), '', 4, false));
		reqMapMasked.put('profile_key', PG.maskWord(reqMapMasked.get('profile_key'), '', 4, false));
		pgResponse.transactionRequestMap = reqMapMasked;
		pgResponse.reqLog = PG.mapAsString(reqMapMasked);
		reqBody = PG_Caller.generateQueryStringComplete(reqMap);
		resBody = submitCall(reqBody);
		if(Test.isRunningTest()){
			resBody = 'transaction_id=b7a4f6b2c9463710b82c3d9c6560a058&error_code=000&auth_response_text=Settle Request Accepted';
		}
		return resBody;
	}
    
    public String authorizationCapture(){//@throws PG_Exception	
		String reqBody = '';	
		String resBody = '';
		Map<String, String> reqMap = prepare();
		reqMap.putAll(new Map<String, String>{				
			'transaction_type' => TRXTYPE_SALE,
			'card_number' => details.creditCardNumber,
			'card_exp_date' => details.expiryMonth + details.expiryYear.substring(details.expiryYear.length()-2),
			'transaction_amount' => Amount,
			'invoice_number' => InvoiceRef,
			'client_reference_number' => CustomerRef
		});
		if(config.cardCodeVerification) reqMap.put('cvv2', CVV);
		if(config.addressVerification){
			reqMap.put('cardholder_zip', details.ccPostalCode);//Res-AVSADDR
			if(!config.addressVerificationZipOnly){
				reqMap.put('cardholder_street_address', EncodingUtil.urlEncode(details.ccStreet2, 'UTF-8'));//Res-AVSZIP
			}
		}
		//System.debug('---reqMap='+reqMap);
		Map<String, String> reqMapMasked = reqMap.clone();
		reqMapMasked.put('profile_id', PG.maskWord(reqMapMasked.get('profile_id'), '', 4, false));
		reqMapMasked.put('profile_key', PG.maskWord(reqMapMasked.get('profile_key'), '', 4, false));
		reqMapMasked.put('card_number', PG.maskWord(reqMapMasked.get('card_number'), '', -4, true));
		pgResponse.transactionRequestMap = reqMapMasked;
		pgResponse.reqLog = PG.mapAsString(reqMapMasked);
		reqBody = PG_Caller.generateQueryStringComplete(reqMap);
		resBody = submitCall(reqBody);
		if(Test.isRunningTest()){
			resBody = 'transaction_id=47506f03922a3f9e9f82da42c29784e6&error_code=000&auth_response_text=Exact Match&avs_result=Y&auth_code=T3191H';
		}
		return resBody;
	}
    
    public String reversal(){//@throws PG_Exception	
		String reqBody = '';	
		String resBody = '';
		Map<String, String> reqMap = prepare();
		reqMap.putAll(new Map<String, String>{				
			'transaction_type' => TRXTYPE_VOID,
			'transaction_id' => TransactionRef
		});
		//System.debug('---reqMap='+reqMap);
		Map<String, String> reqMapMasked = reqMap.clone();
		reqMapMasked.put('profile_id', PG.maskWord(reqMapMasked.get('profile_id'), '', 4, false));
		reqMapMasked.put('profile_key', PG.maskWord(reqMapMasked.get('profile_key'), '', 4, false));
		pgResponse.transactionRequestMap = reqMapMasked;
		pgResponse.reqLog = PG.mapAsString(reqMapMasked);
		reqBody = PG_Caller.generateQueryStringComplete(reqMap);
		resBody = submitCall(reqBody);
		if(Test.isRunningTest()){
			resBody = 'transaction_id=44269c56d8c334a380eb3193fc5f9ae0&error_code=000&auth_response_text=Void Request Accepted';
		}
		return resBody;
	}
    
    public String refund(){//@throws PG_Exception	
		String reqBody = '';	
		String resBody = '';
		Map<String, String> reqMap = prepare();
		reqMap.putAll(new Map<String, String>{				
			'transaction_type' => TRXTYPE_REFUND,
			'transaction_id' => TransactionRef
		});
		if(Amount != '') reqMap.put('transaction_amount', Amount);
		//System.debug('---reqMap='+reqMap);
		Map<String, String> reqMapMasked = reqMap.clone();
		reqMapMasked.put('profile_id', PG.maskWord(reqMapMasked.get('profile_id'), '', 4, false));
		reqMapMasked.put('profile_key', PG.maskWord(reqMapMasked.get('profile_key'), '', 4, false));
		pgResponse.transactionRequestMap = reqMapMasked;
		pgResponse.reqLog = PG.mapAsString(reqMapMasked);
		reqBody = PG_Caller.generateQueryStringComplete(reqMap);
		resBody = submitCall(reqBody);
		if(Test.isRunningTest()){
			resBody = 'transaction_id=7c68c44ba0943e629f85584c35e27b55&error_code=000&auth_response_text=Refund Request Accepted - Void';
		}
		return resBody;
	}
	
	private Map<String, String> prepare(){
		return new Map<String, String>{
			//User paramters
			'profile_id' => config.loginId,
			'profile_key' => config.password	
		};
	}
	
	private String submitCall(String reqBody){//@throws PG_Exception
		String resBody = '';
		//pgResponse.reqLog = reqBody;
		PG_Caller caller = new PG_Caller(
			new Map<string,string>{
				'CURLOPT_TIMEOUT' => String.valueOf(R_TIMEOUT_MS),
				'CURLOPT_RETURNTRANSFER' => 'TRUE'
			},
			PG.HTTP_METHOD_POST,
			R_ENDPOINT,
			reqBody,
			R_TIMEOUT_MS
		);	
		resBody = caller.call();
		pgResponse.resLog = resBody;	
		return resBody;
	}
	
	private void populateResponse(String resBody){
    	if(resBody != '' && resBody.length()>0 ){
    		String[] resArr = resBody.split('&', 0);
			String[] nvmArr;
			Map<String, String> resMap = new Map<string,string>();
			for(String nvStr : resArr){
				nvmArr = nvStr.split('=', 0);
				resMap.put(nvmArr[0], nvmArr[1]);				
			}
			System.debug('---resMap='+resMap);
			pgResponse.transactionResponseMap = resMap;
			pgResponse.processStatus = PG.Status.SUCCESS.name();
			pgResponse.approvalStatus = resMap.get('error_code') == '000'?
				PG.Status.APPROVED.name():PG.Status.DECLINED.name();
			if(pgResponse.transactionType == PG.TransactionType.VERIFY.name()){
				pgResponse.approvalStatus = resMap.get('error_code') == '085'?
					PG.Status.APPROVED.name():PG.Status.DECLINED.name();
			}
			pgResponse.responseCode = resMap.get('error_code');
			pgResponse.responseMessage = resMap.get('auth_response_text');
			pgResponse.transactionId = resMap.get('transaction_id');
			pgResponse.transactionTime = System.now();
			pgResponse.isException = false;
			pgResponse.exceptionMessage = '';
		}else{
			throw new PG_Exception('Gateway Exception::Invalid Response received. Response='+resBody);
		}
    }

}