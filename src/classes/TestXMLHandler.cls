/**
---------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2014-04-08
@see 
---------------------------------------------------------------------------------------------------
*/
@isTest
private class TestXMLHandler {

    static testMethod void parseDetailsTest() {       
		
		Test.startTest();
		
		String argDetails = '<?xml version="1.0"?>'+
		'<InputDetails>'+
			'<Data type="paymentGatewayName">AuthNet</Data>'+
			'<Data type="accountId">001i0000004AP7uAAG</Data>'+
			'<Data type="TransactionTypeACH">DEBIT</Data>'+
			'<Data type="InvoiceRef">I-1001</Data>'+
			'<Data type="CustomerRef">C-1001</Data>'+
			'<Data type="Amount">10.0</Data>'+
			'<Data type="EcheckType">CCD</Data>'+
		'</InputDetails>';
		
		Map<String, String> returnDetails = new Map<String, String>{
			'paymentGatewayName' => 'AuthNet',
			'accountId' => '001i0000004AP7uAAG',
			'TransactionTypeACH' => 'DEBIT',
			'InvoiceRef' => 'I-1001',
			'CustomerRef' => 'C-1001',
			'Amount' => '10.0',
			'EcheckType' => 'CCD'
		};
		
		System.assertEquals(returnDetails, XMLHandler.parseDetails(argDetails));
		
		Test.stopTest();
    }
    
    static testMethod void writeDetailsTest() {       
		
		Test.startTest();
		
		Map<String, String> contentMap = new Map<String, String>{
			'paymentGatewayName' => 'AuthNet',
			'accountId' => '001i0000004AP7uAAG',
			'TransactionTypeACH' => 'DEBIT',
			'InvoiceRef' => 'I-1001',
			'CustomerRef' => 'C-1001',
			'Amount' => '10.0',
			'EcheckType' => 'CCD'
		};
		
		String xmlOutput = '<?xml version="1.0"?>'+
		'<OutputDetails>'+
			'<Data type="paymentGatewayName">AuthNet</Data>'+
			'<Data type="accountId">001i0000004AP7uAAG</Data>'+
			'<Data type="TransactionTypeACH">DEBIT</Data>'+
			'<Data type="InvoiceRef">I-1001</Data>'+
			'<Data type="CustomerRef">C-1001</Data>'+
			'<Data type="Amount">10.0</Data>'+
			'<Data type="EcheckType">CCD</Data>'+
		'</OutputDetails>';
		
		System.assertEquals(contentMap, XMLHandler.parseDetails(XMLHandler.writeDetails(contentMap)));
		
		Test.stopTest();
    }
}