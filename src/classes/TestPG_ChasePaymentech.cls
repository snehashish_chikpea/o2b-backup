/**
--------------------------------------------------------------------------------------------------
@desc

@author PN-Chikpea
@version 1.0 2013-09-19
@see 
--------------------------------------------------------------------------------------------------
*/
@isTest
private class TestPG_ChasePaymentech {
    
    static testMethod void chaseTransactionsSuccess() {
        
        Account acc = new Account();
        acc.Name = 'TestAccount1';
        acc.ChikPeaO2B__SLA__c = 'Bronze';
        acc.ChikPeaO2B__SLAExpirationDate__c = Date.today();
        acc.ChikPeaO2B__SLASerialNumber__c = '1234';
        
        insert acc;
        
        ChikPeaO2B__Credit_Card__c ccVisa = new ChikPeaO2B__Credit_Card__c();
        ccVisa.ChikPeaO2B__CC_First_Name__c = 'Visa';
        ccVisa.ChikPeaO2B__CC_Last_Name__c = 'ChasePT';
        ccVisa.ChikPeaO2B__Credit_Card_Type__c = 'Visa';
        ccVisa.ChikPeaO2B__Credit_Card_Number__c = '4113';// 4112344112344113
        ccVisa.ChikPeaO2B__Expiry_Month__c = 5;
        ccVisa.ChikPeaO2B__Expiry_Year__c = 2017;
        ccVisa.ChikPeaO2B__CC_Street1__c = 'Apt 2';
        ccVisa.ChikPeaO2B__CC_Street2__c = '1 Northeastern Blvd';
        ccVisa.ChikPeaO2B__CC_Postal_Code__c = '03109-1234';
        ccVisa.ChikPeaO2B__CC_City__c = 'Bedford';
        ccVisa.ChikPeaO2B__CC_State__c = 'NH';
        ccVisa.ChikPeaO2B__CC_Country__c = 'US';
        ccVisa.ChikPeaO2B__Account__c = acc.Id;
        
        insert ccVisa;
        
        ChikPeaO2B__Item__c item1 = new ChikPeaO2B__Item__c();
        item1.Name = 'Item1';
        item1.ChikPeaO2B__Item_Type__c = 'Recurring';
        item1.ChikPeaO2B__Active__c = true;
        
        insert item1;
        
        ChikPeaO2B__Price_Book__c pbGold = new ChikPeaO2B__Price_Book__c(ChikPeaO2B__Active__c=true);
        pbGold.Name = 'Gold';
        
        insert pbGold;
        
        ChikPeaO2B__Rate_Plan__c ratePlan1 = new ChikPeaO2B__Rate_Plan__c();
        ratePlan1.ChikPeaO2B__Item__c = item1.Id;
        ratePlan1.ChikPeaO2B__Bill_Cycle__c = 'Monthly';
        ratePlan1.ChikPeaO2B__Price_Book__c = pbGold.Id;
        ratePlan1.ChikPeaO2B__Pricing_Type__c = 'Tiered';
        
        insert ratePlan1;
        
        Date t = Date.today();
        Date t1 = t.addMonths(-3);
        Date t2 = t.addMonths(2);
        
        ChikPeaO2B__Subscription__c sub = new ChikPeaO2B__Subscription__c();
        sub.ChikPeaO2B__Credit_Card__c = ccVisa.Id;
        sub.ChikPeaO2B__Account__c = acc.Id;
        sub.ChikPeaO2B__Billing_Start_Date__c = t1;
        sub.ChikPeaO2B__Billing_Stop_Date__c = t2;
        sub.ChikPeaO2B__Item__c = item1.Id;
        sub.ChikPeaO2B__Rate_Plan__c = ratePlan1.Id;
        
        insert sub;
        
        ChikPeaO2B__Gateway__c gwChasePT = new ChikPeaO2B__Gateway__c();
        gwChasePT.Name = PG.GATEWAY_CHASE_ORBITAL;
        gwChasePT.ChikPeaO2B__Address_Verification__c = true;
        gwChasePT.ChikPeaO2B__Attach_Note__c = true;
        gwChasePT.ChikPeaO2B__Capture_Card_Details__c = true;
        gwChasePT.ChikPeaO2B__Card_Code_Verification__c = true;
        gwChasePT.ChikPeaO2B__Currency__c = 'USD';
        gwChasePT.ChikPeaO2B__Gateway_Url_Prod__c = 'https://ws.paymentech.net/PaymentechGateway';
        gwChasePT.ChikPeaO2B__Gateway_Url_Prod_Fallback__c = 'https://ws2.paymentech.net/PaymentechGateway';
        gwChasePT.ChikPeaO2B__Gateway_Url_Test__c = 'https://wsvar.paymentech.net/PaymentechGateway';
        gwChasePT.ChikPeaO2B__Gateway_Url_Test_Fallback__c = 'https://wsvar2.paymentech.net/PaymentechGateway';
        gwChasePT.ChikPeaO2B__Login_Id__c = 'ORBITALCONNECTIONUSERNAME';
        gwChasePT.ChikPeaO2B__Merchant_Reference__c = '206736';
        gwChasePT.ChikPeaO2B__Terminal_ID__c = '001';
        //gwChasePT.ChikPeaO2B__O2B_Setting__c = '';
        gwChasePT.ChikPeaO2B__Partial_Authorization__c = false;
        gwChasePT.ChikPeaO2B__Password__c = 'ORBITALCONNECTIONPASSWORD';
        gwChasePT.ChikPeaO2B__Primary_Site_Available__c = false;
        gwChasePT.ChikPeaO2B__Retry_Count__c = 14;
        gwChasePT.ChikPeaO2B__Retry_Interval__c = 2;
        gwChasePT.ChikPeaO2B__Test_Mode__c = true;
        gwChasePT.ChikPeaO2B__Time_out_limit__c = 120;    
        
        insert gwChasePT;
        
        Test.startTest();
        
        String paymentGatewayName;
        String subsciptionId;
        Map<String, String> tranParams;
        PG_ChasePaymentech cp;
        PG_CCDetails details;
        PG_Config config;
        
        //authorization - NO
        Test.setMock(WebServiceMock.class, new TestPG_WSOribitalNewOrderAuthMock());
        paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        subsciptionId = sub.Id;
        tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-3003',
            'Comment'=>'Authorization  - MC',  
            //'CardVerifyPresenceInd'=>'1',//for di and vi only, 1 = card code present
            'Amount'=>'10.0'//$10  
        };
        cp = new PG_ChasePaymentech();
        details = new PG_CCDetails(subsciptionId);
        config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res1 = cp.authorization();
        //System.debug('---res1='+res1);
        System.assertEquals('1', res1.approvalStatus);      
        
        //capture - MFC
        Test.setMock(WebServiceMock.class, new TestPG_WSOribitalMFCMock());
        paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        subsciptionId = sub.Id;
        tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-0006',
            'Amount'=>'10.0',//$10 
            'TransactionRef'=>'527C8EDCFB165E734526A17F0B7686534D2254C2'  
        };
        cp = new PG_ChasePaymentech();
        details = new PG_CCDetails(subsciptionId);
        config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.MFCResponseElement res2 = cp.markForCapture();
        //System.debug('---res2='+res2);
        System.assertEquals('1', res2.approvalStatus);
        
        //authorization capture - NO
        Test.setMock(WebServiceMock.class, new TestPG_WSOribitalNewOrderAuthMock());
        paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        subsciptionId = sub.Id;
        tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-3003',
            'Comment'=>'Auth/Capture Testing -  MC',
            //'CardVerifyPresenceInd'=>'1',//for di and vi only
            'Amount'=>'10.0'//$15      
        };
        cp = new PG_ChasePaymentech();
        details = new PG_CCDetails(subsciptionId);
        config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res3 = cp.authorizationCapture();
        //System.debug('---res3='+res3);
        System.assertEquals('1', res3.approvalStatus);
        
        //refund - NO   
        Test.setMock(WebServiceMock.class, new TestPG_WSOribitalRefundMock());
        paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        subsciptionId = sub.Id;
        tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-0009R',
            //'TransactionRef'=>'527C9764C3E27FF5A7A8C3E4F2128E7492E654AA',
            'Comment'=>'Refund Testing using a Credit Card Number - Vi',
            'Amount'=>'10.0'//$4        
        };
        cp = new PG_ChasePaymentech();
        details = new PG_CCDetails(subsciptionId);
        config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res4 = cp.refund();
        //System.debug('---res4='+res4);
        System.assertEquals('1', res4.approvalStatus);
        
        //Force capture - NO
        Test.setMock(WebServiceMock.class, new TestPG_WSOribitalNewOrderAuthMock());
        paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        subsciptionId = sub.Id;
        tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'O-104',
            'PriorAuthCd'=>'tst535',
            'Comment'=>'Test Web Service Force Capture Transaction',
            'Amount'=>'5.0'//$5     
        };
        cp = new PG_ChasePaymentech();
        details = new PG_CCDetails(subsciptionId);
        config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.NewOrderResponseElement res5 = cp.forceCapture();
        //System.debug('---res5='+res5);
        System.assertEquals('1', res5.approvalStatus);
        
        //Revrsal - void/ auth reversal
        Test.setMock(WebServiceMock.class, new TestPG_WSOribitalReversalMock());
        paymentGatewayName = PG.GATEWAY_CHASE_ORBITAL;
        subsciptionId = sub.Id;
        tranParams = new Map<string,string>{
            //'MerchantID'=>'206734',
            //'TerminalID'=>'001',
            'OrderID'=>'ON-3001',
            'TransactionRef'=>'528DD8D97BD111C51F5F481074EBB885C610544E'
            //'TxRefIdx'=>''//Should be numeric, not req for first time void/auth reversal
        };
        cp = new PG_ChasePaymentech();
        details = new PG_CCDetails(subsciptionId);
        config = new PG_Config(paymentGatewayName);
        cp.init(details, config, tranParams);
        PG_WSOribitalElements.ReversalResponseElement res6 = cp.authorizationReversal();
        PG_WSOribitalElements.ReversalResponseElement res = cp.reversal();
        //System.debug('---res6='+res6);
        System.assertEquals('', res6.approvalStatus);
        
        
        Test.stopTest();
        
    }
    
    static testMethod void testFaultHandler() {
        
        PG_ChasePaymentech cp = new PG_ChasePaymentech();
        String faultString = 'Web service callout failed: WebService returned a SOAP Fault: 20412: Precondition Failed: Security Information is missing faultcode=SOAP-ENV:Server faultactor="|0x52d8a810';
        PG_ChasePaymentech.Fault fault = cp.getFaultCodeAndMessage(faultString);
        System.assertEquals(fault.code, '20412');
        
    }
    
}