/*
** Class         :  Agreementset
** Created by    :  S Pal (chikpea Inc.)
** Last Modified :  10 march 14
** Modified by   :  Asitm (chikpea Inc.)
** Reason        :  Secuirty Issue fixed.(sharing)
** Description   :   
*/

public with sharing class Agreementset{
    public id orderid{get;set;}
    private list<order_line__c> OrderLineAgreementUpdateList=new list<order_line__c>();
    public agreement__c agree{get;set;}
    public string exagreename{get;set;}
    public id exagreeid{get;set;}
    public string existingORnew{get;set;} 
    public string SearchAgreementName{get;set;}
    private boolean AgreementSearch=false;
    
    public Agreementset()
    {
        if(ApexPages.currentPage().getparameters().get('ordid')!=null){
            orderid=ApexPages.currentPage().getparameters().get('ordid');
            existingORnew='New';
            agree=new agreement__c();
            agree.account__c=[select account__c from order__c where id=:orderid].account__c;
        }
    }
    public List<SelectOption> getOptions()
    {
        List<SelectOption>Options=new List<SelectOption>();

        Options.add(new SelectOption('New','New'));
        Options.add(new SelectOption('Existing','Existing'));
        Options.add(0,new SelectOption('--SELECT--','--SELECT--'));    
        return Options;
    }
    public List<Agreement__c> getActiveAgreementList()
    {       
        List<Agreement__c> agreeList = new List<Agreement__c>();
        ID AccountID=Apexpages.currentPage().getParameters().get('accid');
        agreeList.clear();
        Date CurrentDate=Date.today();
        if(AgreementSearch)
        {
            String searchLike = '%' + SearchAgreementName + '%';
            agreeList = [select Id, Name,Account__c,Account__r.name,Start_Date__c,End_Date__c,ChikPeaO2B__Bill_Cycle__c,ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Status__c,ChikPeaO2B__Term_Period__c from Agreement__c where Account__c=:AccountID and Name like:searchLike];
        }    
        else
            agreeList = [select Id, Name,Account__c,Account__r.name,Start_Date__c,End_Date__c,ChikPeaO2B__Bill_Cycle__c,ChikPeaO2B__Next_Bill_Date__c,ChikPeaO2B__Status__c,ChikPeaO2B__Term_Period__c from Agreement__c where Account__c=:AccountID];
        return agreeList;
    }
    public PageReference searchAgreement()
    {
        if(SearchAgreementName.equals(''))
        {
            AgreementSearch = false;
        }
        else
        {
            AgreementSearch = true;
        } 
        return null;   
    }
    public void setAgreement()
    {
        if(existingORnew=='New'){
        if(agree.name!=null && agree.name!='')
        {
            if(agree.start_date__c!=null)
            {
                if(agree.ChikPeaO2B__Term_Period__c!=null)
                {
                    try{
                        insert(agree);
                        for(order_line__c orl:[SELECT id,name,Agreement__c from order_line__c where Order__c=:orderid])
                        {
                            orl.Agreement__c=agree.id;
                            OrderLineAgreementUpdateList.add(orl);
                        }
                        update(OrderLineAgreementUpdateList);
                        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.CONFIRM,'New Agreement '+agree.name+' created and all orderline assigned the same agreement');
                        ApexPages.addMessage(errormsg);
                    }
                    catch(exception e)
                    {
                        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,e.getmessage());
                        ApexPages.addMessage(errormsg);
                    }    
                }
                else
                {
                    ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Please provide term period');
                    ApexPages.addMessage(errormsg);
                }
            }
            else
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Please provide start date');
                ApexPages.addMessage(errormsg);
            }
        }
        else
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Please Provide Agreement name');
            ApexPages.addMessage(errormsg);
        } 
        }
        else//existing agreement
        {
            try{
                for(order_line__c orl:[SELECT id,name,Agreement__c from order_line__c where Order__c=:orderid])
                {
                    orl.Agreement__c=exagreeid;
                    OrderLineAgreementUpdateList.add(orl);
                }
                update(OrderLineAgreementUpdateList);
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.CONFIRM,exagreename+' agreement assigned to all the order lines');
                ApexPages.addMessage(errormsg);
            }
            catch(exception e)
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,e.getmessage());
                ApexPages.addMessage(errormsg);
            }    
        }  
    }
}