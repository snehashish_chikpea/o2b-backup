trigger DelInvoice on Invoice__c (before delete) {
    for(Invoice__c inv : trigger.old)
    {
        if(!inv.ChikPeaO2B__Cancel_Delete_Allowed__c){
            if(inv.ChikPeaO2B__Invoice_Status__c=='Paid Partial'||inv.ChikPeaO2B__Invoice_Status__c=='Paid')
            {
                try{
                inv.adderror(inv.ChikPeaO2B__Invoice_Status__c+' invoice can not be deleted');
                }catch(exception e){}
                
            }
            /*
            else
            {
                //Ok, for delete
            }
            */
            
        }
    }
}