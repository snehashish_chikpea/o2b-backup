trigger calculateInvoiceDueRemaining on Payment__c (before insert,before update) {    
    Set<id> inv_ids=new Set<id>();
    for(Payment__c pmt:trigger.new){
        if(pmt.ChikPeaO2B__Has_Processed__c){
        	
        	inv_ids.add(pmt.ChikPeaO2B__Invoice__c);        	
        }
    }
    List<ChikPeaO2B__Invoice__c> inv_list=[select Id, name,ChikPeaO2B__Amount_Due__c,
    	ChikPeaO2B__Other_Charges__c,ChikPeaO2B__Amount_Due_Remaining__c 
    	from ChikPeaO2B__Invoice__c where id in:inv_ids];
    Map<Id, ChikPeaO2B__Invoice__c>	 inv_map=new Map<Id, ChikPeaO2B__Invoice__c>();
    for(ChikPeaO2B__Invoice__c inv:inv_list){
    	inv_map.put(inv.id, inv);
    }
    for(Payment__c pmt:trigger.new){
        if(pmt.ChikPeaO2B__Has_Processed__c){
        	if(inv_map.containsKey(pmt.ChikPeaO2B__Invoice__c)){
        		ChikPeaO2B__Invoice__c inv=inv_map.get(pmt.ChikPeaO2B__Invoice__c);
	        	//Decimal total_due=inv.ChikPeaO2B__Amount_Due__c+inv.ChikPeaO2B__Other_Charges__c;
	        	//System.debug('#***total_due='+total_due);
	        	pmt.Invoice_Due_Remaining__c=0;
	        	if(pmt.ChikPeaO2B__Payment_Amount__c<=inv.ChikPeaO2B__Amount_Due_Remaining__c){
	        		pmt.Invoice_Due_Remaining__c=
	        			inv.ChikPeaO2B__Amount_Due_Remaining__c-pmt.ChikPeaO2B__Payment_Amount__c;
	        	}    
        	} 	
        }
    }
}