trigger UpdataOrder on Order__c (before update) {
     if(trigger.isBefore){
        set<id>ordid=new set<id>();
        integer i=0;
        for(order__c ord : trigger.new)
        {
            order__c oord=trigger.old[i++];
            if(oord.Price_Book__c!=ord.Price_Book__c)
                ordid.add(ord.id);
        }
        map<id,integer>ordorlnumberMap=new map<id,integer>();
        for(order__c ord : [SELECT id,name,(select id,name from order_lines__r) from order__c where id in : ordid])
        {
            ordorlnumberMap.put(ord.id,ord.order_lines__r.size());    
        }
        integer j=0;
        for(order__c ord : trigger.new)
        {
            order__c oord=trigger.old[j++];
            if(oord.Price_Book__c!=null && oord.Price_Book__c!=ord.Price_Book__c && ordorlnumberMap.get(ord.id)>0)
                ord.addError('order pricebook can not be change, as order already have '+ordorlnumberMap.get(ord.id)+' order lines.');
        }
    }
}