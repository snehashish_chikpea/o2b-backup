trigger UpdatePackage on Package__c (after Update,before update) {
    // ************ Package Bill cycle change [START] *****************//
    set<id>PkgIds=new set<id>();
    map<id,string>NewBillcycle=new map<id,string>();
    map<id,string>OldBillcycle=new map<id,string>();
    List<Bundled_Item__c>bitmList=new List<Bundled_Item__c>();
    if(trigger.isAfter)
    {
        integer i=0;
        for(Package__c pkg:trigger.new)
        {
            Package__c opkg=trigger.old[i++];
            if(opkg.Bill_Cycle__c!=pkg.Bill_Cycle__c)
            {
                PkgIds.add(pkg.id);  
                NewBillcycle.put(pkg.id,pkg.Bill_Cycle__c);
                OldBillcycle.put(pkg.id,opkg.Bill_Cycle__c);  
            }   
        }
        if(PkgIds!=null && PkgIds.size()>0){
        for(Package__c  pkg:[SELECT id,name,Bill_Cycle__c,Bill_On_Package__c,Bundle_Non_Recurring_Price__c,Bundle_Recurring_Price__c,Description__c,Package_Discount__c,Package_Item__c,Package_Type__c,Price_Book__c,Price_Book__r.name,
                             (SELECT id,name,Item__c,Non_Recurring_Charge__c,Recurring_Charge__c,Unit_Price__c,Usage_Rate__c,Quantity__c,Rate_Plan__c from Bundled_Items__r)
                             from Package__c where id in : PkgIds])
        {
            if(pkg.Bundled_Items__r!=null && pkg.Bundled_Items__r.size()>0)
            {
                for(Bundled_Item__c bi:pkg.Bundled_Items__r)
                {
                    decimal multiplier=priceconvertion(OldBillcycle.get(pkg.id),NewBillcycle.get(pkg.id));
                    system.debug('Multiplierrrrrr'+multiplier+'#####n'+bi.Non_Recurring_Charge__c+'####r'+bi.Recurring_Charge__c);
                    bi.Non_Recurring_Charge__c=bi.Non_Recurring_Charge__c* multiplier;
                    System.debug('nonnn'+bi.Non_Recurring_Charge__c);
                    bi.Recurring_Charge__c=bi.Recurring_Charge__c*multiplier;                   
                    System.debug('recccc'+bi.Recurring_Charge__c);
                    bitmList.add(bi);
                }
            }
        }
        }
        System.debug('&&&&&&'+bitmList.size());
        if(bitmList!=null && bitmList.size()>0)
            update(bitmList);
        system.debug('%%%%'+bitmList.size());

        // ************ Package Bill cycle change [END] *****************//
        
        // ************ Bundled Item's value modification [START] *****************//
        integer j=0;
        map<id,decimal>PkgItemUpdateMap=new map<id,decimal>();
        for(Package__c pkg:trigger.new)
        {
            Package__c opkg=trigger.old[j++];
            if(pkg.Bill_On_Package__c && (pkg.Package_Type__c=='One-Off' || pkg.Package_Type__c=='Pre-Paid Usage')&&(opkg.Bundle_Non_Recurring_Price__c!=pkg.Bundle_Non_Recurring_Price__c))
            {
                if(pkg.Package_Item__c!=null)
                    PkgItemUpdateMap.put(pkg.Package_Item__c,pkg.Bundle_Non_Recurring_Price__c);
            }
            else if(pkg.Bill_On_Package__c && (pkg.Package_Type__c=='Recurring' || pkg.Package_Type__c=='Post-Paid Usage')&&(opkg.Bundle_Recurring_Price__c!=pkg.Bundle_Recurring_Price__c))
            {
                if(pkg.Package_Item__c!=null)
                    PkgItemUpdateMap.put(pkg.Package_Item__c,pkg.Bundle_Recurring_Price__c);
            }
        }
        List<Rate_Plan__c>rpUpdateList=new list<Rate_Plan__c>();
        for(Item__c itm:[SELECT id,name,Item_Type__c,(SELECT id,Non_Recurring_Charge__c,Recurring_Charge__c from Rate_Plans__r) from item__c where id in:PkgItemUpdateMap.keyset()])
        {
            system.debug('inside trg===>'+itm.id);
            Rate_Plan__c rp=itm.Rate_Plans__r[0];
            if(itm.Item_Type__c=='One-Off' || itm.Item_Type__c=='Pre-Paid Usage')
                rp.Non_Recurring_Charge__c=PkgItemUpdateMap.get(itm.id);
            if(itm.Item_Type__c=='Recurring' || itm.Item_Type__c=='Post-Paid Usage')
                rp.Recurring_Charge__c=PkgItemUpdateMap.get(itm.id);
            rpUpdateList.add(rp);        
        }
        if(rpUpdateList!=null && rpUpdateList.size()>0)
        {
            try{
                update(rpUpdateList);
            }
            catch(Exception e){}
        }
        // ************ Bundled Item's value modification [END] *****************//
    }// ######## Trigger.isAfter [END] ###########//
    
    // ######## Price Convertion method [START] ############### //
    private decimal priceconvertion(string oldbill,string newbill)
    {
        system.debug('$$$$$OLD'+oldbill+'NEW'+newbill);
        decimal multiplier=0;
        if(oldbill=='Annual')
        {
            System.debug('@@@@A');
            if(newbill=='Monthly')
                multiplier=(30/decimal.valueof(365));
            else if(newbill=='Quarterly')
                multiplier=(90/decimal.valueof(365));    
            else if(newbill=='Weekly')
                multiplier=(7/decimal.valueof(365));
            else if(newbill=='Daily')
            {
                multiplier=(1/decimal.valueof(365));        
                system.debug('======'+multiplier);
            }
            else if(newbill=='Half Yearly')
            {
                multiplier=(180/decimal.valueof(365));        
            }
            else
                multiplier=(integer.valueof(newbill)/decimal.valueof(365));        
        }  
        else if(oldbill=='Monthly')
        {
            System.debug('@@@@M');
            if(newbill=='Annual')
                multiplier=(365/decimal.valueof(30));
            else if(newbill=='Quarterly')
                multiplier=(90/decimal.valueof(30));    
            else if(newbill=='Weekly')
                multiplier=(7/decimal.valueof(30));
            else if(newbill=='Daily')
                multiplier=(1/decimal.valueof(30));
            else if(newbill=='Half Yearly')
            {
                multiplier=(180/decimal.valueof(30));        
            }
            else
                multiplier=(integer.valueof(newbill)/decimal.valueof(30));                    
        }
        else if(oldbill=='Quarterly')
        {
            System.debug('@@@@q');
            if(newbill=='Annual')
                multiplier=(365/decimal.valueof(90));
            else if(newbill=='Monthly')
                multiplier=(30/decimal.valueof(90));    
            else if(newbill=='Weekly')
                multiplier=(7/decimal.valueof(90));
            else if(newbill=='Daily')
                multiplier=(1/decimal.valueof(90));
            else if(newbill=='Half Yearly')
            {
                multiplier=(180/decimal.valueof(90));        
            }
            else
                multiplier=(integer.valueof(newbill)/decimal.valueof(90));                    
        }
        else if(oldbill=='Weekly')
        {
            System.debug('@@@@w');
            if(newbill=='Annual')
                multiplier=(365/decimal.valueof(7));
            else if(newbill=='Monthly')
                multiplier=(30/decimal.valueof(7));    
            else if(newbill=='Quarterly')
                multiplier=(90/decimal.valueof(7));
            else if(newbill=='Daily')
                multiplier=(1/decimal.valueof(7));
            else if(newbill=='Half Yearly')
            {
                multiplier=(180/decimal.valueof(7));        
            }
            else
                multiplier=(integer.valueof(newbill)/decimal.valueof(7));                    
        }
        else if(oldbill=='Daily')
        {
            System.debug('@@@@D');
            if(newbill=='Annual')
            {    
                multiplier=(365);
                system.debug('~~~~'+multiplier);
            }
            else if(newbill=='Monthly')
                multiplier=(30);    
            else if(newbill=='Quarterly')
                multiplier=(90);
            else if(newbill=='Weekly')
                multiplier=(7);
            else if(newbill=='Half Yearly')
            {
                multiplier=180;        
            }
            else
                multiplier=integer.valueof(newbill);
        }
        else if(oldbill=='Half Yearly')
        {
            if(newbill=='Annual')
                multiplier=(365/decimal.valueof(180));
            else if(newbill=='Monthly')
                multiplier=(30/decimal.valueof(180));    
            else if(newbill=='Quarterly')
                multiplier=(90/decimal.valueof(180));
            else if(newbill=='Daily')
                multiplier=(1/decimal.valueof(180));
            else if(newbill=='Weekly')
            {
                multiplier=(7/decimal.valueof(180));        
            }
            else
                multiplier=(integer.valueof(newbill)/decimal.valueof(180));
        }
        else// for oldbill =2,3,4 etc
        {
            if(newbill=='Annual')
                multiplier=(365/decimal.valueof(integer.valueof(oldbill)));
            else if(newbill=='Monthly')
                multiplier=(30/decimal.valueof(integer.valueof(oldbill)));    
            else if(newbill=='Quarterly')
                multiplier=(90/decimal.valueof(integer.valueof(oldbill)));
            else if(newbill=='Daily')
                multiplier=(1/decimal.valueof(integer.valueof(oldbill)));
            else if(newbill=='Weekly')
            {
                multiplier=(7/decimal.valueof(integer.valueof(oldbill)));        
            }
        }
        return  multiplier;
    }
    // ######## Price Convertion method [END] ###############//
    
    if(trigger.isBefore){
        set<id>pkgid=new set<id>();
        integer i=0;
        for(package__c pkg : trigger.new)
        {
            package__c opkg=trigger.old[i++];
            if(opkg.Price_Book__c!=pkg.Price_Book__c)
                pkgid.add(pkg.id);
        }
        map<id,integer>pkgBundlednumberMap=new map<id,integer>();
        for(package__c pkg : [SELECT id,name,(select id,name from bundled_items__r) from package__c where id in : pkgid])
        {
            pkgBundlednumberMap.put(pkg.id,pkg.bundled_items__r.size());    
        }
        integer j=0;
        for(package__c pkg : trigger.new)
        {
            package__c opkg=trigger.old[j++];
            if(opkg.Price_Book__c!=null && opkg.Price_Book__c!=pkg.Price_Book__c && pkgBundlednumberMap.get(pkg.id)>0)
                pkg.addError('Package pricebook can not be change, as Package already have '+pkgBundlednumberMap.get(pkg.id)+' bundled items.');
        }
    }
}