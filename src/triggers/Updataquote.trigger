trigger Updataquote on quote__c (before update) {
     if(trigger.isBefore)
     {
        set<id>qtid=new set<id>();
        integer i=0;
        for(quote__c qt : trigger.new)
        {
            quote__c oqt=trigger.old[i++];
            if(oqt.Price_Book__c!=qt.Price_Book__c)
                qtid.add(qt.id);
        }
        map<id,integer>qtqtlnumberMap=new map<id,integer>();
        for(quote__c qt : [SELECT id,name,(select id,name from quote_lines__r) from quote__c where id in : qtid])
        {
            qtqtlnumberMap.put(qt.id,qt.quote_lines__r.size());    
        }
        integer j=0;
        for(quote__c qt : trigger.new)
        {
            quote__c oqt=trigger.old[j++];
            if(oqt.Price_Book__c!=null && oqt.Price_Book__c!=qt.Price_Book__c && qtqtlnumberMap.get(qt.id)>0)
                qt.addError('quote pricebook can not be change, as quote already have '+qtqtlnumberMap.get(qt.id)+' quote lines.');
        }
    }
}