trigger DeletePackage on Package__c (before delete) {
    
    set<id>DeletePackageItem=new set<id>();
    
    for(Package__c pkg : trigger.old)
    {
        if(pkg.ChikPeaO2B__Bill_On_Package__c && pkg.ChikPeaO2B__Package_Item__c!=null)
        {
            DeletePackageItem.add(pkg.ChikPeaO2B__Package_Item__c);
        }    
    }
    
    List<ChikPeaO2B__Item__c> DelItemList=[SELECT id,name from ChikPeaO2B__Item__c where id in : DeletePackageItem];
    if(DelItemList!=null && DelItemList.size()>0)
        delete(DelItemList);
}