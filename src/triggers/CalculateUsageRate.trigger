/*
Trigger Name    : CalculateUsageRate
Created By      : Snehashish Roy (ChikPea Inc.)
Created Date    : 4 January, 2017
Description     : Populate Rate from Rate Plan if OrderLine is not present.
*/
trigger CalculateUsageRate on Usage__c (before insert) {
    set<id> rpids=new set<id>();
    list<ChikPeaO2B__Rate_Plan__c> rplist=new list<ChikPeaO2B__Rate_Plan__c>();
    map<id,decimal> rpratemap=new map<id,decimal>();
    for(Usage__c usg:trigger.new)
    {
        if(usg.ChikPeaO2B__Order_Line__c==null)
        {
            if(usg.ChikPeaO2B__Rate_Plan__c!=null)
                rpids.add(usg.ChikPeaO2B__Rate_Plan__c);
        }
    }
    if(rpids.size()>0)
        rplist=[select id,ChikPeaO2B__Usage_Rate__c,ChikPeaO2B__Pricing_Type__c from ChikPeaO2B__Rate_Plan__c where id in :rpids];
    if(rplist.size()>0)
    {
        for(ChikPeaO2B__Rate_Plan__c rp:rplist)
        {
            if(rp.ChikPeaO2B__Pricing_Type__c=='Flat')
                rpratemap.put(rp.id,rp.ChikPeaO2B__Usage_Rate__c!=null?rp.ChikPeaO2B__Usage_Rate__c:0);
            else
                rpratemap.put(rp.id,0);
        }
    }
    for(Usage__c usg:trigger.new)
    {
        if(usg.ChikPeaO2B__Order_Line__c==null && usg.ChikPeaO2B__Rate__c==null)
        {
            if(rpratemap.containskey(usg.ChikPeaO2B__Rate_Plan__c))
                usg.ChikPeaO2B__Rate__c=rpratemap.get(usg.ChikPeaO2B__Rate_Plan__c);
            else
                usg.ChikPeaO2B__Rate__c=0;
        }
    }
}