trigger EndDateCalculate on Agreement__c (before insert,before update) {
    if(trigger.isupdate){
        integer i=0;
        for(Agreement__c agg : trigger.new)
        {
            Agreement__c oagg=trigger.old[i++];
            if(agg.Term_Period__c!=null && ((oagg.Term_Period__c!=agg.Term_Period__c) || (oagg.start_date__c!=agg.start_date__c)))
            {
                agg.end_date__c=agg.start_date__c.addmonths(integer.valueof(agg.Term_Period__c))-1;
            }
        }
    }
    if(trigger.isinsert)
    {
        for(Agreement__c agg : trigger.new)
        {
            if(agg.Term_Period__c!=null)
            {
                agg.end_date__c=agg.start_date__c.addmonths(integer.valueof(agg.Term_Period__c))-1;
            }
        }
    }
}