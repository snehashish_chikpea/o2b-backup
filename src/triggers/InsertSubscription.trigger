/*
** Trigger       :  InsertSubscription
** Created by    :  Mehebub Hossain (chikpea Inc.)
** Created On    :  Feb-15  
** Modified by   :  Jani Das(Chikpea Inc)       
** Description   :  Subscription next bill date change for billing day(O2B setting or Bill group). 
*/
trigger InsertSubscription on ChikPeaO2B__Subscription__c (before insert) 
{
 set<id>sub_id_set=new set<id>();
 //List<Subscription__c> sub_list=new List<>
 List<ChikPeaO2B__O2B_Setting__c> o2b_settings=[select Id,ChikPeaO2B__Billing_Type__c,ChikPeaO2B__Billing_Day__c 
 from ChikPeaO2B__O2B_Setting__c limit 1];
  System.debug('#InsertSubscripton trigger----o2b_settings='+o2b_settings);
  
 if(o2b_settings.size()>0)
 {
     ChikPeaO2B__O2B_Setting__c o2b_setting=o2b_settings[0];
     
     set<Id> ac_id=new set<Id>();
     
     for(ChikPeaO2B__Subscription__c sub : trigger.new)
     {
        ac_id.add(sub.ChikPeaO2B__Account__c);
        System.debug('Bill Cycle Test--->'+sub.ChikPeaO2B__Bill_Cycle__c);
        
     }
     
     Map<Id,Account> ac_id_ac=new Map<Id, Account>();
     
     List<Account> ac_list=[select Id, Name, ChikPeaO2B__Bill_Group__c, ChikPeaO2B__Bill_Group__r.ChikPeaO2B__Billing_Type__c,
     ChikPeaO2B__Bill_Group__r.ChikPeaO2B__Billing_Day__c from Account where Id IN:ac_id];
     for(Account ac:ac_list)
     {
     
        ac_id_ac.put(ac.Id, ac);
     }
     
     
    for(ChikPeaO2B__Subscription__c sub : trigger.new)
    {
     
         System.debug('#InsertSubscripton trigger----1sub.ChikPeaO2B__Bill_Cycle__c='
                        +sub.ChikPeaO2B__Bill_Cycle__c);
        
        if(sub.ChikPeaO2B__Bill_Cycle__c!='Weekly' && sub.ChikPeaO2B__Bill_Cycle__c!='Daily')
        {
            if(sub.ChikPeaO2B__Next_Bill_Date__c!=null)
            {            
                if(ac_id_ac.containsKey(sub.ChikPeaO2B__Account__c))
                {
                    if(ac_id_ac.get(sub.ChikPeaO2B__Account__c).ChikPeaO2B__Bill_Group__c!=null)
                    {
                        if(ac_id_ac.get(sub.ChikPeaO2B__Account__c).ChikPeaO2B__Bill_Group__r.ChikPeaO2B__Billing_Type__c=='Calendar Month' && ac_id_ac.get(sub.ChikPeaO2B__Account__c).ChikPeaO2B__Bill_Group__r.ChikPeaO2B__Billing_Day__c!=null)
                        {
                            Date today_date=Date.today();
                            Date billing_date=Date.newInstance(Date.today().year(),Date.today().month(),Integer.valueOf(ac_id_ac.get(sub.ChikPeaO2B__Account__c).ChikPeaO2B__Bill_Group__r.ChikPeaO2B__Billing_Day__c));
                           if(sub.ChikPeaO2B__Calendar_month__c=='Previous Month')
                           {
                               Date firstdate_thismnth=Date.newInstance(Date.today().year(),Date.today().month(),1);
                               if(sub.ChikPeaO2B__Billing_Start_Date__c.month()==sub.ChikPeaO2B__Next_Bill_Date__c.month() && sub.ChikPeaO2B__Billing_Start_Date__c.year()==sub.ChikPeaO2B__Next_Bill_Date__c.year() && sub.ChikPeaO2B__Next_Bill_Date__c>=firstdate_thismnth)
                               {
                                    sub.ChikPeaO2B__Next_Bill_Date__c=sub.ChikPeaO2B__Next_Bill_Date__c.addMonths(1);
                               }
                               else if(sub.ChikPeaO2B__Billing_Start_Date__c.month()==sub.ChikPeaO2B__Next_Bill_Date__c.month() && sub.ChikPeaO2B__Billing_Start_Date__c.year()==sub.ChikPeaO2B__Next_Bill_Date__c.year() && sub.ChikPeaO2B__Next_Bill_Date__c<firstdate_thismnth)
                               {
                                    if(today_date>=billing_date)
                                     {
                                        Date nxtmnt=Date.today().addmonths(1);
                                        sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                     }
                                     else
                                     {
                                         sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                     }
                               }
                               else if(sub.ChikPeaO2B__Billing_Start_Date__c.month()!=sub.ChikPeaO2B__Next_Bill_Date__c.month() || sub.ChikPeaO2B__Billing_Start_Date__c.year()!=sub.ChikPeaO2B__Next_Bill_Date__c.year())
                               {
                                 if(sub.ChikPeaO2B__Next_Bill_Date__c>sub.ChikPeaO2B__Billing_Start_Date__c)
                                 {
                                     if((sub.ChikPeaO2B__Next_Bill_Date__c>=firstdate_thismnth && sub.ChikPeaO2B__Next_Bill_Date__c.month()==Date.today().month() && sub.ChikPeaO2B__Next_Bill_Date__c.year()==Date.today().year()) || (sub.ChikPeaO2B__Next_Bill_Date__c<firstdate_thismnth))
                                     {
                                         if(today_date>=billing_date)
                                         {
                                            Date nxtmnt=Date.today().addmonths(1);
                                            sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                         }
                                         else
                                         {
                                             sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                         }
                                     }
                                  }
                                  else
                                  {
                                       if(sub.ChikPeaO2B__Billing_Start_Date__c>=firstdate_thismnth)
                                       {
                                            Date nxtmnt=sub.ChikPeaO2B__Billing_Start_Date__c.addMonths(1);
                                            sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                       }
                                       else
                                       {
                                            if(today_date>=billing_date)
                                            {
                                                Date nxtmnt=Date.today().addmonths(1);
                                                sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                            }
                                            else
                                            {
                                                 sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                            }
                                       }
                                  }
                               }
                           }
                          else
                          {   /*if(sub.ChikPeaO2B__Next_Bill_Date__c.day()>=Integer.valueOf(ac_id_ac.get(sub.ChikPeaO2B__Account__c).ChikPeaO2B__Bill_Group__r.ChikPeaO2B__Billing_Day__c))
                                {
                                    sub.ChikPeaO2B__Next_Bill_Date__c=sub.ChikPeaO2B__Next_Bill_Date__c.addmonths(1);
                                    System.debug('#InsertSubscripton trigger----2sub.ChikPeaO2B__Billing_Start_Date__c='
                                        +sub.ChikPeaO2B__Next_Bill_Date__c);
                                }*/
                                if(today_date>=billing_date && today_date.month()==sub.ChikPeaO2B__Next_Bill_Date__c.month() && today_date.year()==sub.ChikPeaO2B__Next_Bill_Date__c.year()) // current month subscription
                                {
                                     sub.ChikPeaO2B__Next_Bill_Date__c=sub.ChikPeaO2B__Next_Bill_Date__c.addmonths(1);
                                }
                                else if(billing_date>sub.ChikPeaO2B__Next_Bill_Date__c && billing_date.month()!=sub.ChikPeaO2B__Next_Bill_Date__c.month()) // Previous month subscription
                                {
                                     if(today_date>=billing_date)
                                     {
                                        Date nxtmnt=Date.today().addmonths(1);
                                        sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                     }
                                     else
                                     {
                                         sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                     }
                                }
                            }
                        }
                    }
                    else if(o2b_setting.ChikPeaO2B__Billing_Type__c=='Calendar Month')
                    {
                        System.debug('#InsertSubscripton trigger----1sub.ChikPeaO2B__Billing_Start_Date__c='
                                +sub.ChikPeaO2B__Next_Bill_Date__c);
                        Date today_date=Date.today();
                        Date billing_date=Date.newInstance(Date.today().year(),Date.today().month(),Integer.valueOf(o2b_setting.ChikPeaO2B__Billing_Day__c));       
                        if(sub.ChikPeaO2B__Calendar_month__c=='Previous Month') 
                        {
                               Date firstdate_thismnth=Date.newInstance(Date.today().year(),Date.today().month(),1);
                               if(sub.ChikPeaO2B__Billing_Start_Date__c.month()==sub.ChikPeaO2B__Next_Bill_Date__c.month() && sub.ChikPeaO2B__Billing_Start_Date__c.year()==sub.ChikPeaO2B__Next_Bill_Date__c.year() && sub.ChikPeaO2B__Next_Bill_Date__c>=firstdate_thismnth)
                               {
                                    sub.ChikPeaO2B__Next_Bill_Date__c=sub.ChikPeaO2B__Next_Bill_Date__c.addMonths(1);
                               }
                               else if(sub.ChikPeaO2B__Billing_Start_Date__c.month()==sub.ChikPeaO2B__Next_Bill_Date__c.month() && sub.ChikPeaO2B__Billing_Start_Date__c.year()==sub.ChikPeaO2B__Next_Bill_Date__c.year() && sub.ChikPeaO2B__Next_Bill_Date__c<firstdate_thismnth)
                               {
                                    if(today_date>=billing_date)
                                     {
                                        Date nxtmnt=Date.today().addmonths(1);
                                        sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                     }
                                     else
                                     {
                                         sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                     }
                               }
                               else if(sub.ChikPeaO2B__Billing_Start_Date__c.month()!=sub.ChikPeaO2B__Next_Bill_Date__c.month() || sub.ChikPeaO2B__Billing_Start_Date__c.year()!=sub.ChikPeaO2B__Next_Bill_Date__c.year())
                               {
                                   if(sub.ChikPeaO2B__Next_Bill_Date__c>sub.ChikPeaO2B__Billing_Start_Date__c)
                                   {
                                     if((sub.ChikPeaO2B__Next_Bill_Date__c>=firstdate_thismnth && sub.ChikPeaO2B__Next_Bill_Date__c.month()==Date.today().month() && sub.ChikPeaO2B__Next_Bill_Date__c.year()==Date.today().year()) || (sub.ChikPeaO2B__Next_Bill_Date__c<firstdate_thismnth))
                                     {
                                         if(today_date>=billing_date)
                                         {
                                            Date nxtmnt=Date.today().addmonths(1);
                                            sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                         }
                                         else
                                         {
                                             sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                         }
                                     }
                                   }
                                   else
                                   {
                                       if(sub.ChikPeaO2B__Billing_Start_Date__c>=firstdate_thismnth)
                                       {
                                            Date nxtmnt=sub.ChikPeaO2B__Billing_Start_Date__c.addMonths(1);
                                            sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                       }
                                       else
                                       {
                                            if(today_date>=billing_date)
                                            {
                                                Date nxtmnt=Date.today().addmonths(1);
                                                sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                            }
                                            else
                                            {
                                                 sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                            }
                                       }
                                   }
                               }
                        
                        }
                        else
                        {
                                if(today_date>=billing_date && today_date.month()==sub.ChikPeaO2B__Next_Bill_Date__c.month() && today_date.year()==sub.ChikPeaO2B__Next_Bill_Date__c.year()) //current month subscription
                                {
                                         sub.ChikPeaO2B__Next_Bill_Date__c=sub.ChikPeaO2B__Next_Bill_Date__c.addmonths(1);
                                }
                                else if(billing_date>sub.ChikPeaO2B__Next_Bill_Date__c && billing_date.month()!=sub.ChikPeaO2B__Next_Bill_Date__c.month()) // Previous month subscription
                                {
                                         if(today_date>=billing_date)
                                         {
                                            Date nxtmnt=Date.today().addmonths(1);
                                            sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(nxtmnt.year(),nxtmnt.month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                         }
                                         else
                                         {
                                             sub.ChikPeaO2B__Next_Bill_Date__c=Date.newInstance(Date.today().year(),Date.today().month(),sub.ChikPeaO2B__Next_Bill_Date__c.day());
                                         }
                                } 
                        }
                    }
                
                }
            }
            else
            {
                System.debug('#InsertSubscripton trigger Error next bill date can not be null');
            }
        }
    }
    
 }

}