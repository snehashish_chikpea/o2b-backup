trigger Inserto2bset on O2B_Setting__c (before Insert) {
    List<O2B_Setting__c> osetList = [SELECT id,name from O2B_Setting__c limit 1];
    for(O2B_Setting__c oset : trigger.new)
    {
        if(osetList.size()>0)
            oset.adderror('One O2b settings is already exists'); 
    }
}