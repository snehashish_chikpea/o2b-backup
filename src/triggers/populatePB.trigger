trigger populatePB on ChikPeaO2B__Bundled_Item__c (after insert) 
{
    list<ChikPeaO2B__Bundled_Item__c> blist=new list<ChikPeaO2B__Bundled_Item__c>();
    blist=[select id,ChikPeaO2B__Price_Book__c,ChikPeaO2B__Package__r.ChikPeaO2B__Price_Book__c from ChikPeaO2B__Bundled_Item__c where id in :trigger.newmap.keyset()];
    if(blist.size()>0)
    {
        for(ChikPeaO2B__Bundled_Item__c bi:blist)
        {
            bi.ChikPeaO2B__Price_Book__c=bi.ChikPeaO2B__Package__r.ChikPeaO2B__Price_Book__c;
        }
        update blist;
    }
}