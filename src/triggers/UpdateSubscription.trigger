trigger UpdateSubscription on Subscription__c (before Update) {
    
    set<id>RPid=new set<id>();
    set<id>Itemid=new set<id>();
    MAP<id,Rate_Plan__c>RPMAP=new MAP<id,Rate_Plan__c>();
    MAP<id,Item__c>ItemMap=new Map<id,Item__c>();
    Map<id,List<ChikPeaO2B__Tiered_Pricing__c>>TieredMap=new Map<id,List<ChikPeaO2B__Tiered_Pricing__c>>();
    Map<id,List<ChikPeaO2B__Volume_Pricing__c>>VolumeMap=new Map<id,List<ChikPeaO2B__Volume_Pricing__c>>();
    integer i=0;
    for(Subscription__c s : trigger.new)
    {
        Subscription__c os=trigger.old[i++];
        RPid.add(s.ChikPeaO2B__Rate_Plan__c);
        Itemid.add(s.ChikPeaO2B__Item__c);
        if(s.ChikPeaO2B__Billing_Started__c && s.ChikPeaO2B__Billing_Start_Date__c!=os.ChikPeaO2B__Billing_Start_Date__c)
            s.adderror('Can not change Start date after billing is started for the subscription');
        else if(s.ChikPeaO2B__Billing_Start_Date__c!=os.ChikPeaO2B__Billing_Start_Date__c && s.ChikPeaO2B__Billing_Start_Date__c>s.ChikPeaO2B__Next_Bill_Date__c)
        {
            s.ChikPeaO2B__Next_Bill_Date__c=s.ChikPeaO2B__Billing_Start_Date__c;
        }
    }
    for(item__c itm : [SELECT id,name,ChikPeaO2B__Item_Type__c from item__c where id in : Itemid])
    {
        ItemMap.put(itm.id,itm);
    }
    for(Rate_Plan__c rp : [SELECT id,name,ChikPeaO2B__Pricing_Type__c from Rate_Plan__c where id in : RPid])
    {
        RPMAP.put(rp.id,rp);
    }
    for(ChikPeaO2B__Tiered_Pricing__c tr : [SELECT id,name,Lower_Limit__c,Upper_Limit__c,Non_Recurring_Charge__c,Recurring_Charge__c,Usage_Rate__c,Rate_Plan__c from Tiered_Pricing__c where Rate_Plan__c in:RPid order by Lower_Limit__c asc])
    {
        if(!TieredMap.containskey(tr.Rate_Plan__c))
        {
            List<ChikPeaO2B__Tiered_Pricing__c>templist=new List<ChikPeaO2B__Tiered_Pricing__c>();
            templist.add(tr);
            TieredMap.put(tr.Rate_Plan__c,templist);
        }
        else
        {
            List<ChikPeaO2B__Tiered_Pricing__c>templist=TieredMap.get(tr.Rate_Plan__c);
            templist.add(tr);
            TieredMap.put(tr.Rate_Plan__c,templist);            
        }
    }
    for(ChikPeaO2B__Volume_Pricing__c vp : [SELECT id,name,Lower_Limit__c,Upper_Limit__c,Non_Recurring_Charge__c,Recurring_Charge__c,Usage_Rate__c,Rate_Plan__c from Volume_Pricing__c where Rate_Plan__c in:RPid order by Lower_Limit__c asc])
    {
        if(!VolumeMap.containskey(vp.Rate_Plan__c))
        {
            List<ChikPeaO2B__Volume_Pricing__c>templist=new List<ChikPeaO2B__Volume_Pricing__c >();
            templist.add(vp);
            VolumeMap.put(vp.Rate_Plan__c,templist);
        }
        else
        {
            List<ChikPeaO2B__Volume_Pricing__c>templist=VolumeMap.get(vp.Rate_Plan__c);
            templist.add(vp);
            VolumeMap.put(vp.Rate_Plan__c,templist);            
        }
    }
    public decimal PriceReCalculation(List<ChikPeaO2B__Tiered_Pricing__c>trlist,List<ChikPeaO2B__Volume_Pricing__c>vplist,string itemtype,string pricingtype,decimal reccharge,decimal qty)
    {
        decimal returnval;
        if(itemtype.equalsignorecase('Recurring'))
        {
            //#### ====== PRICE CALCULATION RECURRING [START] ===== ####//
            if(pricingtype.equalsignorecase('Flat')){
                returnval=(reccharge!=null?reccharge:0);   
            }    
            else if(pricingtype.equalsignorecase('Tiered'))
            {    
                integer trc=1;
                
                if(trlist!=null && trlist.size()>0)
                {
                    decimal remitmqty=qty;
                    decimal itm_TotalPrice=0;
                   
                    for(Tiered_Pricing__c tr:trlist)
                    {          
                        if(remitmqty<=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1))
                        {
                            itm_TotalPrice+=(tr.Recurring_Charge__c!=null?tr.Recurring_Charge__c:0)*remitmqty;
                            remitmqty=0;
                            break;
                        }
                        else if(remitmqty>=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1) && trc<trlist.size())
                        {
                            itm_TotalPrice+=(tr.Recurring_Charge__c!=null?tr.Recurring_Charge__c:0)*((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                            remitmqty-=((tr.Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                        }
                        else if(trc==trlist.size())//remitmqty>=tr.Upper_Limit__c && 
                        {
                            itm_TotalPrice+=(tr.Recurring_Charge__c!=null?tr.Recurring_Charge__c:0)*remitmqty;
                            remitmqty=0;
                        }                                                   
                        trc++;
                    }
                    returnval=itm_TotalPrice/qty;                    
                }
                else
                    returnval=(reccharge!=null?reccharge:0);
            }
            else if(pricingtype.equalsignorecase('Volume'))
            {    
                integer vpc=1;
                
                if(vplist!=null && vplist.size()>0)
                {
                    decimal itm_TotalPrice=0;
                    for(Volume_Pricing__c vp:vplist)
                    {
                        if((qty<=vp.Lower_Limit__c && vpc==1) || (qty>=vp.Lower_Limit__c && qty<=vp.Upper_Limit__c)||(vpc==vplist.size() && qty>=vp.Upper_Limit__c))
                        {
                            itm_TotalPrice=(vp.Recurring_Charge__c!=null?vp.Recurring_Charge__c:0);
                            returnval=itm_TotalPrice/qty;
                            break;
                        }
                        vpc++;
                    }
                }
                else
                    returnval=(reccharge!=null?reccharge:0);

            }       
            //#### ====== PRICE CALCULATION RECURRING [END] ===== ####//     
        } // recurring item end  
        else if(itemtype.equalsignorecase('Post-Paid Usage'))
        {
            returnval=(reccharge!=null?reccharge:0);
        }
        return returnval;
    }
    integer j=0;
    for(Subscription__c s : trigger.new)
    {
        Subscription__c os=trigger.old[j++];
        if(s.ChikPeaO2B__Quantity__c!=null && s.ChikPeaO2B__Quantity__c!=os.ChikPeaO2B__Quantity__c){
            System.debug('#****ItemMap.get(s.ChikPeaO2B__Item__c)='+ItemMap.get(s.ChikPeaO2B__Item__c));
            System.debug('#****TieredMap='+TieredMap);
            //s.ChikPeaO2B__Recurring_Charge__c=PriceReCalculation(TieredMap.get(s.ChikPeaO2B__Rate_Plan__c),VolumeMap.get(s.ChikPeaO2B__Rate_Plan__c),ItemMap.get(s.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c,RPMAP.get(s.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c,s.ChikPeaO2B__Recurring_Charge__c,s.ChikPeaO2B__Quantity__c);
        }
    }
}