trigger UsageRateUpdate on Usage__c (before update) {
    
    List<ChikPeaO2B__O2B_Setting__c>o2bsetList=new List<ChikPeaO2B__O2B_Setting__c>();
    set<id>ItmID=new Set<id>();
    set<id>RPID=new set<id>();
    set<id>Accid=new set<id>();
    set<id>Purchaseid=new set<id>();
    set<id>Subscriptionid=new set<id>();
    Map<id,ChikPeaO2B__Item__c>ItemMap=new Map<id,ChikPeaO2B__Item__c>();
    Map<id,ChikPeaO2B__Rate_Plan__c>RPMap=new Map<id,ChikPeaO2B__Rate_Plan__c>();
    Map<string,List<ChikPeaO2B__Account_based_tiered_pricing__c>>AccBasedTieredMap=new Map<string,List<ChikPeaO2B__Account_based_tiered_pricing__c>>();
    Map<String,List<ChikPeaO2B__Account_based_volume_pricing__c>>AccBasedVolumeMap=new Map<String,List<ChikPeaO2B__Account_based_volume_pricing__c>>();
    Map<string,List<ChikPeaO2B__Account_Based_Tiered_Flat_pricing__c>>AccBasedTieredFlatMap=new Map<string,List<ChikPeaO2B__Account_Based_Tiered_Flat_pricing__c>>();
    Map<String,List<ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c>>AccBasedVolumeTieredMap=new Map<String,List<ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c>>();    
    Map<id,ChikPeaO2B__Purchase__c>purchaseMap=new Map<id,ChikPeaO2B__Purchase__c>();
    Map<id,ChikPeaO2B__Subscription__c>subscriptionMap=new Map<id,ChikPeaO2B__Subscription__c>();
    integer j=0;
    for(ChikPeaO2B__Usage__c usg : trigger.new)
    {
        ChikPeaO2B__Usage__c ousg=trigger.old[j++];
        if(usg.ChikPeaO2B__Total_Usage__c!=ousg.ChikPeaO2B__Total_Usage__c)//Total Usage change
        {
            ItmID.add(usg.ChikPeaO2B__Item__c);
            RPID.add(usg.ChikPeaO2B__Rate_Plan__c);
            Accid.add(usg.ChikPeaO2B__Account__c);
            if(usg.ChikPeaO2B__Subscription__c!=null)
                Subscriptionid.add(usg.ChikPeaO2B__Subscription__c);
            if(usg.ChikPeaO2B__Purchase__c!=null)
                Purchaseid.add(usg.ChikPeaO2B__Purchase__c);    
        }
    }
    if(ItmID!=null && ItmID.size()>0){
    for(ChikPeaO2B__Item__c itm : [SELECT id,name,ChikPeaO2B__Item_Type__c,ChikPeaO2B__Is_aggregation__c from ChikPeaO2B__Item__c where id in : ItmID])
    {
        ItemMap.put(itm.id,itm);
    }
        o2bsetList=[SELECT id,name,ChikPeaO2B__Account_based_tiering__c from ChikPeaO2B__O2B_Setting__c limit1];
    }
    if(RPID!=null && RPID.size()>0){
    for(ChikPeaO2B__Rate_Plan__c rp : [SELECT id,name,ChikPeaO2B__Bill_Cycle__c,ChikPeaO2B__Pricing_Type__c,(SELECT id,name,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Tiered_Pricing__r order by ChikPeaO2B__Lower_Limit__c asc),(SELECT id,name,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Volume_Pricings__r order by ChikPeaO2B__Lower_Limit__c asc),(SELECT id,name,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Tiered_Flat_Pricings__r order by ChikPeaO2B__Lower_Limit__c asc),(SELECT id,name,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Volume_Tiereds__r order by ChikPeaO2B__Lower_Limit__c asc) from ChikPeaO2B__Rate_Plan__c where id in : RPID])
    {
        RPMap.put(rp.id,rp);
    }
    }
    if(Accid!=null && Accid.size()>0){
    for(Account acc : [SELECT id,(SELECT id,ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Account_based_tiered_pricings__r order by ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c asc),(SELECT id,ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Account_based_volume_pricings__r order by ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c asc),(SELECT id,ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Account_Based_Tiered_Flat_pricings__r order by ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c asc),(SELECT id,ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c,ChikPeaO2B__Upper_Limit__c,ChikPeaO2B__Usage_Rate__c from Account_Based_Volume_Tiered_pricings__r order by ChikPeaO2B__Item__c,ChikPeaO2B__Rate_Plan__c,ChikPeaO2B__Lower_Limit__c asc) from Account where id in : Accid])
    {
        if(acc.Account_based_tiered_pricings__r!=null && acc.Account_based_tiered_pricings__r.size()>0)
        {   
            for(Account_based_tiered_pricing__c abtp : acc.Account_based_tiered_pricings__r)
            {
                if(!AccBasedTieredMap.containskey(string.valueof(acc.id)+abtp.ChikPeaO2B__Item__c+abtp.ChikPeaO2B__Rate_Plan__c))
                {
                    List<ChikPeaO2B__Account_based_tiered_pricing__c>tmplist=new List<ChikPeaO2B__Account_based_tiered_pricing__c>();
                    tmplist.add(abtp);
                    AccBasedTieredMap.put(string.valueof(acc.id)+abtp.ChikPeaO2B__Item__c+abtp.ChikPeaO2B__Rate_Plan__c,tmplist);
                }
                else
                {
                    List<ChikPeaO2B__Account_based_tiered_pricing__c>tmplist=AccBasedTieredMap.get(string.valueof(acc.id)+abtp.ChikPeaO2B__Item__c+abtp.ChikPeaO2B__Rate_Plan__c);
                    tmplist.add(abtp);
                    AccBasedTieredMap.put(string.valueof(acc.id)+abtp.ChikPeaO2B__Item__c+abtp.ChikPeaO2B__Rate_Plan__c,tmplist);
                }               
            }
        }
        if(acc.Account_based_volume_pricings__r!=null && acc.Account_based_volume_pricings__r.size()>0)
        {
            for(ChikPeaO2B__Account_based_volume_pricing__c abvp : acc.Account_based_volume_pricings__r)
            {
                if(!AccBasedVolumeMap.containskey(string.valueof(acc.id)+abvp.ChikPeaO2B__Item__c+abvp.ChikPeaO2B__Rate_Plan__c))
                {
                    List<ChikPeaO2B__Account_based_volume_pricing__c>tmplist=new List<ChikPeaO2B__Account_based_volume_pricing__c>();
                    tmplist.add(abvp);
                    AccBasedVolumeMap.put(string.valueof(acc.id)+abvp.ChikPeaO2B__Item__c+abvp.ChikPeaO2B__Rate_Plan__c,tmplist);
                }
                else
                {
                    List<ChikPeaO2B__Account_based_volume_pricing__c>tmplist=AccBasedVolumeMap.get(string.valueof(acc.id)+abvp.ChikPeaO2B__Item__c+abvp.ChikPeaO2B__Rate_Plan__c);
                    tmplist.add(abvp);
                    AccBasedVolumeMap.put(string.valueof(acc.id)+abvp.ChikPeaO2B__Item__c+abvp.ChikPeaO2B__Rate_Plan__c,tmplist);
                }
            }
        }
        if(acc.Account_Based_Tiered_Flat_pricings__r!=null && acc.Account_Based_Tiered_Flat_pricings__r.size()>0)
        {
            for(Account_Based_Tiered_Flat_pricing__c abtf : acc.Account_Based_Tiered_Flat_pricings__r)
            {
                if(!AccBasedTieredFlatMap.containskey(string.valueof(acc.id)+abtf.ChikPeaO2B__Item__c+abtf.ChikPeaO2B__Rate_Plan__c))
                {
                    List<Account_Based_Tiered_Flat_pricing__c>tmplist=new List<Account_Based_Tiered_Flat_pricing__c>();
                    tmplist.add(abtf);
                    AccBasedTieredFlatMap.put(string.valueof(acc.id)+abtf.ChikPeaO2B__Item__c+abtf.ChikPeaO2B__Rate_Plan__c,tmplist);
                }
                else
                {
                    List<Account_Based_Tiered_Flat_pricing__c>tmplist=AccBasedTieredFlatMap.get(string.valueof(acc.id)+abtf.ChikPeaO2B__Item__c+abtf.ChikPeaO2B__Rate_Plan__c);
                    tmplist.add(abtf);
                    AccBasedTieredFlatMap.put(string.valueof(acc.id)+abtf.ChikPeaO2B__Item__c+abtf.ChikPeaO2B__Rate_Plan__c,tmplist);
                }
            }
        }
        if(acc.Account_Based_Volume_Tiered_pricings__r!=null && acc.Account_Based_Volume_Tiered_pricings__r.size()>0)
        {
            for(ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c abtv : acc.Account_Based_Volume_Tiered_pricings__r)
            {
                if(!AccBasedVolumeTieredMap.containskey(string.valueof(acc.id)+abtv.ChikPeaO2B__Item__c+abtv.ChikPeaO2B__Rate_Plan__c))
                {
                    List<ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c>tmplist=new List<ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c>();
                    tmplist.add(abtv);
                    AccBasedVolumeTieredMap.put(string.valueof(acc.id)+abtv.ChikPeaO2B__Item__c+abtv.ChikPeaO2B__Rate_Plan__c,tmplist);
                }
                else
                {
                    List<ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c>tmplist=AccBasedVolumeTieredMap.get(string.valueof(acc.id)+abtv.ChikPeaO2B__Item__c+abtv.ChikPeaO2B__Rate_Plan__c);
                    tmplist.add(abtv);
                    AccBasedVolumeTieredMap.put(string.valueof(acc.id)+abtv.ChikPeaO2B__Item__c+abtV.ChikPeaO2B__Rate_Plan__c,tmplist);
                }
            }
        } 
    }
    system.debug('~~~'+AccBasedVolumeMap.size()+'~~~'+AccBasedVolumeMap);
    }
    if(Purchaseid!=null && Purchaseid.size()>0){
    for(ChikPeaO2B__Purchase__c pur : [SELECT id,ChikPeaO2B__Aggregate_Usage__c from ChikPeaO2B__Purchase__c where id in : Purchaseid])
    {
        purchaseMap.put(pur.id,pur);    
    }
    }
    if(Subscriptionid!=null && Subscriptionid.size()>0){
    for(ChikPeaO2B__Subscription__c sub : [SELECT id,ChikPeaO2B__Aggregate_Usage__c from ChikPeaO2B__Subscription__c where id in : Subscriptionid])
    {
        subscriptionMap.put(sub.id,sub);    
    }
    }
    integer i=0;
    for(ChikPeaO2B__Usage__c usg : trigger.new)
    {
        ChikPeaO2B__Usage__c ousg=trigger.old[i++];
        if(usg.ChikPeaO2B__Total_Usage__c!=ousg.ChikPeaO2B__Total_Usage__c)//Total Usage change
        {
            if(o2bsetList.size()>0 && o2bsetList[0].ChikPeaO2B__Account_based_tiering__c)//Account Based tiering
            {
                if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Is_aggregation__c)//Aggregation Item, then total usage calculation at purchase/subscription level
                {
                    if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Pre-Paid Usage')//PrePaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(AccBasedTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringRateCalculation(AccBasedTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r);                                                                                          
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            if(AccBasedVolumeMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeRateCalculation(AccBasedVolumeMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);    
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(AccBasedTieredFlatMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringFlatRateCalculation(AccBasedTieredFlatMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(AccBasedVolumeTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeTieredRateCalculation(AccBasedVolumeTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r);
                            else
                                usg.ChikPeaO2B__Rate__c=VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                        if(usg.ChikPeaO2B__Purchase__c!=null){
                        decimal val = (purchaseMap.get(usg.ChikPeaO2B__Purchase__c)!=null?purchaseMap.get(usg.ChikPeaO2B__Purchase__c).ChikPeaO2B__Aggregate_Usage__c :0);
                        if(val==null){
                          val=0;
                        }
                        ChikPeaO2B__Purchase__c pur=purchaseMap.get(usg.ChikPeaO2B__Purchase__c);
                        
                        if(usg.ChikPeaO2B__Total_Usage__c!=null)//0
                        pur.ChikPeaO2B__Aggregate_Usage__c =val-((usg.ChikPeaO2B__Total_Usage__c!=null?usg.ChikPeaO2B__Total_Usage__c:0)-(ousg.ChikPeaO2B__Total_Usage__c!=null?ousg.ChikPeaO2B__Total_Usage__c:0));//reducing remaining usage from purchase bucket
                        purchaseMap.put(usg.ChikPeaO2B__Purchase__c,pur);
                        }
                        else
                        {
                            usg.adderror(' No purchase found for aggregation');
                        }
                    }
                    else if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Post-Paid Usage')//PostPaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(AccBasedTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringRateCalculation(AccBasedTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r);                                                              
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);    
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            system.debug('@@@'+(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c)+'@@@'+AccBasedVolumeMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c));
                            if(AccBasedVolumeMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeRateCalculation(AccBasedVolumeMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);    
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(AccBasedTieredFlatMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringFlatRateCalculation(AccBasedTieredFlatMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(AccBasedVolumeTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeTieredRateCalculation(AccBasedVolumeTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r);
                            else
                                usg.ChikPeaO2B__Rate__c=VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                        if(usg.ChikPeaO2B__Subscription__c!=null){
                        decimal val = (subscriptionMap.get(usg.ChikPeaO2B__Subscription__c)!=null?subscriptionMap.get(usg.ChikPeaO2B__Subscription__c).ChikPeaO2B__Aggregate_Usage__c :0);
                        if(val==null){
                          val=0;
                        }
                        ChikPeaO2B__Subscription__c sub=subscriptionMap.get(usg.ChikPeaO2B__Subscription__c);
                        if(usg.ChikPeaO2B__Total_Usage__c!=null)//0
                        sub.ChikPeaO2B__Aggregate_Usage__c =val+((usg.ChikPeaO2B__Total_Usage__c!=null?usg.ChikPeaO2B__Total_Usage__c:0)-(ousg.ChikPeaO2B__Total_Usage__c!=null?ousg.ChikPeaO2B__Total_Usage__c:0));
                        subscriptionMap.put(usg.ChikPeaO2B__Subscription__c,sub);
                        }
                        else
                        {
                            usg.adderror(' No subscription found for aggregation');
                        }
                    }
                }
                else//Not an Aggregation Item, then total usage calculation at usage level
                {
                    if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Pre-Paid Usage')//PrePaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(AccBasedTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringRateCalculation(AccBasedTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r);                                                              
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);    
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            if(AccBasedVolumeMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeRateCalculation(AccBasedVolumeMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);    
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(AccBasedTieredFlatMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringFlatRateCalculation(AccBasedTieredFlatMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(AccBasedVolumeTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeTieredRateCalculation(AccBasedVolumeTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r);
                            else
                                usg.ChikPeaO2B__Rate__c=VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                        if(usg.ChikPeaO2B__Total_Usage__c!=0 && usg.ChikPeaO2B__Total_Usage__c!=null && usg.ChikPeaO2B__Remaining_Usage__c!=null)
                        usg.ChikPeaO2B__Remaining_Usage__c-=(usg.ChikPeaO2B__Total_Usage__c-(ousg.ChikPeaO2B__Total_Usage__c!=null?ousg.ChikPeaO2B__Total_Usage__c:0));//reducing remaining usage
                    }
                    else if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Post-Paid Usage')//PostPaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(AccBasedTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringRateCalculation(AccBasedTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r);                                                              
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);    
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            if(AccBasedVolumeMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeRateCalculation(AccBasedVolumeMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(AccBasedTieredFlatMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedTieringFlatRateCalculation(AccBasedTieredFlatMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r);
                            else
                                usg.ChikPeaO2B__Rate__c=RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(AccBasedVolumeTieredMap.containskey(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c))
                                usg.ChikPeaO2B__Rate__c= AccBasedVolumeTieredRateCalculation(AccBasedVolumeTieredMap.get(string.valueof(usg.ChikPeaO2B__Account__c)+usg.ChikPeaO2B__Item__c+usg.ChikPeaO2B__Rate_Plan__c),usg.ChikPeaO2B__Total_Usage__c,RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r);
                            else
                                usg.ChikPeaO2B__Rate__c=VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);        
                        }
                    }
                }
            }
            else//Not Account based tiering
            {
                if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Is_aggregation__c)//Aggregation Item, then total usage calculation at purchase/subscription level
                {
                    if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Pre-Paid Usage')//PrePaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        if(usg.ChikPeaO2B__Purchase__c!=null){
                        decimal val = (purchaseMap.get(usg.ChikPeaO2B__Purchase__c)!=null?purchaseMap.get(usg.ChikPeaO2B__Purchase__c).ChikPeaO2B__Aggregate_Usage__c :0);
                        ChikPeaO2B__Purchase__c pur=purchaseMap.get(usg.ChikPeaO2B__Purchase__c);
                        if(usg.ChikPeaO2B__Total_Usage__c!=null)//0
                        pur.ChikPeaO2B__Aggregate_Usage__c =val-((usg.ChikPeaO2B__Total_Usage__c!=null?usg.ChikPeaO2B__Total_Usage__c:0)-(ousg.ChikPeaO2B__Total_Usage__c!=null?ousg.ChikPeaO2B__Total_Usage__c:0));//reducing remaining usage from purchase bucket
                        purchaseMap.put(usg.ChikPeaO2B__Purchase__c,pur);
                        }
                        else
                        {
                            usg.adderror(' No purchase found for aggregation');
                        }
                    }
                    else if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Post-Paid Usage')//PostPaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);                                                              
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        if(usg.ChikPeaO2B__Subscription__c!=null){
                        decimal val = (subscriptionMap.get(usg.ChikPeaO2B__Subscription__c)!=null?subscriptionMap.get(usg.ChikPeaO2B__Subscription__c).ChikPeaO2B__Aggregate_Usage__c :0);
                        ChikPeaO2B__Subscription__c sub=subscriptionMap.get(usg.ChikPeaO2B__Subscription__c);
                        if(usg.ChikPeaO2B__Total_Usage__c!=null)//0
                        sub.ChikPeaO2B__Aggregate_Usage__c =val+((usg.ChikPeaO2B__Total_Usage__c!=null?usg.ChikPeaO2B__Total_Usage__c:0)-(ousg.ChikPeaO2B__Total_Usage__c!=null?ousg.ChikPeaO2B__Total_Usage__c:0));
                        subscriptionMap.put(usg.ChikPeaO2B__Subscription__c,sub);
                        }
                        else
                        {
                            usg.adderror(' No subscription found for aggregation');
                        }
                    }
                }
                else//Not a Aggregation Item, then total usage calculation at usage level
                {
                    if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Pre-Paid Usage')//PrePaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);                                                              
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        if(usg.ChikPeaO2B__Total_Usage__c!=0 && usg.ChikPeaO2B__Total_Usage__c!=null && usg.ChikPeaO2B__Remaining_Usage__c!=null)
                        usg.ChikPeaO2B__Remaining_Usage__c-=(usg.ChikPeaO2B__Total_Usage__c-ousg.ChikPeaO2B__Total_Usage__c);//reducing remaining usage
                    }
                    else if(ItemMap.get(usg.ChikPeaO2B__Item__c).ChikPeaO2B__Item_Type__c=='Post-Paid Usage')//PostPaid type item
                    {
                        if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered')//Usage's rate Plan is Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Pricing__r,usg.ChikPeaO2B__Total_Usage__c);                                                              
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume')//Usage's rate Plan is Volume type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationVolume(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Tiered Flat')//Usage's rate Plan is Tiered Flat type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= RateCalculationTieredFlat(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Tiered_Flat_Pricings__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                        else if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).ChikPeaO2B__Pricing_Type__c=='Volume Tiered')//Usage's rate Plan is Volume Tiered type
                        {
                            if(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c)!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r!=null && RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r.size()>0)
                                usg.ChikPeaO2B__Rate__c= VolumeTieredRateCalculation(RPMap.get(usg.ChikPeaO2B__Rate_Plan__c).Volume_Tiereds__r,usg.ChikPeaO2B__Total_Usage__c);                                                                  
                        }
                    }
                }    
            }
        }
    }
    if(subscriptionMap!=null && subscriptionMap.values().size()>0)
        update(subscriptionMap.values());
    if(purchaseMap!=null && purchaseMap.values().size()>0)
        update(purchaseMap.values());    
    public decimal AccBasedTieringRateCalculation(List<ChikPeaO2B__Account_based_tiered_pricing__c> tieredlist,decimal totalusage,List<ChikPeaO2B__Tiered_Pricing__c> tieredlist1)
    {
        if(totalusage>0){
            integer trc=1;
            if(tieredlist!=null && tieredlist.size()>0)
            {
                decimal remusg=totalusage;
                decimal totalprice=0;
                for(ChikPeaO2B__Account_based_tiered_pricing__c abt : tieredlist)
                {
                    if(remusg<=((abt.ChikPeaO2B__Upper_Limit__c-abt.ChikPeaO2B__Lower_Limit__c)+1))
                    {
                        totalprice+=(abt.ChikPeaO2B__Usage_Rate__c!=null?abt.ChikPeaO2B__Usage_Rate__c:0)*remusg;
                        remusg=0;
                        break;
                    }
                    else if(remusg>=((abt.ChikPeaO2B__Upper_Limit__c-abt.ChikPeaO2B__Lower_Limit__c)+1) && trc<tieredlist.size())
                    {
                        totalprice+=(abt.ChikPeaO2B__Usage_Rate__c!=null?abt.ChikPeaO2B__Usage_Rate__c:0)*((abt.ChikPeaO2B__Upper_Limit__c-abt.ChikPeaO2B__Lower_Limit__c)+1);
                        remusg-=((abt.ChikPeaO2B__Upper_Limit__c-abt.ChikPeaO2B__Lower_Limit__c)+1);
                    }
                    else if(trc==tieredlist.size())//remusg>=abt.ChikPeaO2B__Upper_Limit__c && 
                    {
                        totalprice+=(abt.ChikPeaO2B__Usage_Rate__c!=null?abt.ChikPeaO2B__Usage_Rate__c:0)*remusg;
                        remusg=0;
                    }                                           
                    trc++;
                }
                return (totalprice/totalusage);
            }
            else
            {
                return RateCalculation(tieredlist1,totalusage);
            }
        }
        else
            return 0;
        
    }
    public decimal RateCalculation(List<ChikPeaO2B__Tiered_Pricing__c> tieredlist,decimal totalusage)
    {
        if(totalusage>0)
        {
            integer trc=1;
            
            if(tieredlist!=null && tieredlist.size()>0)
            {
                decimal remusg=totalusage;
                decimal totalprice=0;
                for(ChikPeaO2B__Tiered_Pricing__c tr : tieredlist)
                {
                    system.debug('Lower==>'+tr.ChikPeaO2B__Lower_Limit__c+'Upper==>'+tr.ChikPeaO2B__Upper_Limit__c);
                    if(remusg<=((tr.ChikPeaO2B__Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1))
                    {
                        totalprice+=(tr.ChikPeaO2B__Usage_Rate__c!=null?tr.ChikPeaO2B__Usage_Rate__c:0)*remusg;
                        remusg=0;
                        break;
                    }
                    else if(remusg>=((tr.ChikPeaO2B__Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1) && trc<tieredlist.size())
                    {
                        totalprice+=(tr.ChikPeaO2B__Usage_Rate__c!=null?tr.ChikPeaO2B__Usage_Rate__c:0)*((tr.ChikPeaO2B__Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                        remusg-=((tr.ChikPeaO2B__Upper_Limit__c-tr.ChikPeaO2B__Lower_Limit__c)+1);
                    }
                    else if(trc==tieredlist.size())//remusg>=tr.ChikPeaO2B__Upper_Limit__c && 
                    {
                        totalprice+=(tr.ChikPeaO2B__Usage_Rate__c!=null?tr.ChikPeaO2B__Usage_Rate__c:0)*remusg;
                        remusg=0;
                    }
                    system.debug('Total==>'+totalprice);                                           
                    trc++;
                }
                system.debug('totalprice==>'+totalprice+'totalusage==>'+totalusage);
                return (totalprice/totalusage);
            }
            else
            {
                return 0;
            }
        }
        else
            return 0;
        
    }
    public decimal AccBasedVolumeRateCalculation(List<ChikPeaO2B__Account_based_volume_pricing__c> volumelist,decimal totalusage,List<ChikPeaO2B__Volume_Pricing__c> volumelist1)
    {
        if(totalusage>0)
        {
            integer vpc=1;
            decimal totalprice=0;
            if(volumelist!=null && volumelist.size()>0)
            {
                for(ChikPeaO2B__Account_based_volume_pricing__c abvp:volumelist)
                {
                    if((totalusage<=abvp.Lower_Limit__c && vpc==1) || (totalusage>=abvp.Lower_Limit__c && totalusage<=abvp.Upper_Limit__c)||(totalusage>=abvp.Upper_Limit__c && vpc==volumelist.size()))
                    {
                        totalprice=(abvp.ChikPeaO2B__Usage_Rate__c!=null?abvp.ChikPeaO2B__Usage_Rate__c:0);
                        break;
                    }
                    vpc++;
                }
                return (totalprice/totalusage);
            }
            else
                return RateCalculationVolume(volumelist1,totalusage);
        }
        else
            return 0;
    }
    public decimal RateCalculationVolume(List<ChikPeaO2B__Volume_Pricing__c> volumelist,decimal totalusage)
    {
        if(totalusage>0)
        {
            integer vpc=1;
            decimal totalprice=0;
            if(volumelist!=null && volumelist.size()>0)
            {
                for(ChikPeaO2B__Volume_Pricing__c vp:volumelist)
                {
                    if((totalusage<=vp.Lower_Limit__c && vpc==1) || (totalusage>=vp.Lower_Limit__c && totalusage<=vp.Upper_Limit__c)||(totalusage>=vp.Upper_Limit__c && vpc==volumelist.size()))
                    {
                        totalprice=(vp.ChikPeaO2B__Usage_Rate__c!=null?vp.ChikPeaO2B__Usage_Rate__c:0);
                        break;
                    }
                    vpc++;
                }
                return (totalprice/totalusage);
            }
            else
                return 0;
        }
        else
            return 0;
    }
    public decimal AccBasedTieringFlatRateCalculation(List<ChikPeaO2B__Account_Based_Tiered_Flat_pricing__c> tieredlist,decimal totalusage,List<ChikPeaO2B__Tiered_Flat_Pricing__c> tieredlist1)
    {
        if(totalusage>0){
            integer trc=1;
            decimal flatrate=0;
            if(tieredlist!=null && tieredlist.size()>0)
            {
                for(ChikPeaO2B__Account_Based_Tiered_Flat_pricing__c atf : tieredlist)
                {
                    if((totalusage<=atf.Lower_Limit__c && trc==1) || (totalusage>=atf.Lower_Limit__c && totalusage<=atf.Upper_Limit__c)||(totalusage>=atf.Upper_Limit__c && trc==tieredlist.size()))
                    {
                        flatrate=(atf.ChikPeaO2B__Usage_Rate__c!=null?atf.ChikPeaO2B__Usage_Rate__c:0);
                        break;
                    }
                    trc++;
                }
                return (flatrate);
            }
            else
            {
                return RateCalculationTieredFlat(tieredlist1,totalusage);
            }
        }
        else
            return 0;   
    }
    public decimal RateCalculationTieredFlat(List<ChikPeaO2B__Tiered_Flat_Pricing__c> TieredFlatlist,decimal totalusage)
    {
        if(totalusage>0){
            integer trc=1;
            decimal flatrate=0;
            if(TieredFlatlist!=null && TieredFlatlist.size()>0)
            {
                for(ChikPeaO2B__Tiered_Flat_Pricing__c tf : TieredFlatlist)
                {
                    if((totalusage<=tf.Lower_Limit__c && trc==1) || (totalusage>=tf.Lower_Limit__c && totalusage<=tf.Upper_Limit__c)||(totalusage>=tf.Upper_Limit__c && trc==TieredFlatlist.size()))
                    {
                        flatrate=(tf.ChikPeaO2B__Usage_Rate__c!=null?tf.ChikPeaO2B__Usage_Rate__c:0);
                        break;
                    }
                    trc++;
                }
                return (flatrate);
            }
            else
                return 0;   
        }
        else
            return 0;   
    }
    public decimal AccBasedVolumeTieredRateCalculation(List<ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c> VolumeTieredlist,decimal totalusage,List<ChikPeaO2B__Volume_Tiered_pricing__c> VolumeTieredlist1)
    {
        if(totalusage>0)
        {
            integer vtpc=1;
            decimal totalprice=0;
            if(VolumeTieredlist!=null && VolumeTieredlist.size()>0)
            {
                decimal remTrdQty=0;
                for(ChikPeaO2B__Account_Based_Volume_Tiered_pricing__c avt: VolumeTieredlist)
                {
                    if((totalusage<=avt.Lower_Limit__c && vtpc==1) || (totalusage>=avt.Lower_Limit__c && totalusage<=avt.Upper_Limit__c && vtpc<VolumeTieredlist.size())||(totalusage>=avt.Upper_Limit__c && vtpc==(VolumeTieredlist.size()-1)))
                    {
                        totalprice=(avt.ChikPeaO2B__Usage_Rate__c!=null?avt.ChikPeaO2B__Usage_Rate__c:0);
                        system.debug('~~~~'+avt.ChikPeaO2B__Usage_Rate__c);
                        if(totalusage>=avt.Upper_Limit__c && vtpc==(VolumeTieredlist.size()-1))
                        {
                            remTrdQty=totalusage-avt.Upper_Limit__c;
                            system.debug('==>'+totalusage+'-'+avt.Upper_Limit__c);
                        }
                        else
                            break;
                    }
                    else if(remTrdQty>0 && (vtpc==VolumeTieredlist.size()))
                    {
                        totalprice+=(remTrdQty*(avt.ChikPeaO2B__Usage_Rate__c!=null?avt.ChikPeaO2B__Usage_Rate__c:0));
                        system.debug('&&&&'+avt.ChikPeaO2B__Usage_Rate__c+'*'+remTrdQty);
                    }
                    vtpc++;
                }
                system.debug('##'+totalprice);
                return (totalprice/totalusage);
            }
            else
                return VolumeTieredRateCalculation(VolumeTieredlist1,totalusage);    
        }
        else
            return 0;
    }
    public decimal VolumeTieredRateCalculation(List<ChikPeaO2B__Volume_Tiered_pricing__c> VolumeTieredlist,decimal totalusage)
    {
        if(totalusage>0)
        {
            integer vtpc=1;
            decimal totalprice=0;
            if(VolumeTieredlist!=null && VolumeTieredlist.size()>0)
            {
                decimal remTrdQty=0;
                for(ChikPeaO2B__Volume_Tiered_pricing__c vt: VolumeTieredlist)
                {
                    if((totalusage<=vt.Lower_Limit__c && vtpc==1) || (totalusage>=vt.Lower_Limit__c && totalusage<=vt.Upper_Limit__c && vtpc<VolumeTieredlist.size())||(totalusage>=vt.Upper_Limit__c && vtpc==(VolumeTieredlist.size()-1)))
                    {
                        totalprice=(vt.ChikPeaO2B__Usage_Rate__c!=null?vt.ChikPeaO2B__Usage_Rate__c:0);
                        if(totalusage>=vt.Upper_Limit__c && vtpc==(VolumeTieredlist.size()-1))
                        {
                            remTrdQty=totalusage-vt.Upper_Limit__c;
                        }
                        else
                            break;
                    }
                    else if(remTrdQty>0 && (vtpc==VolumeTieredlist.size()))
                    {
                        totalprice+=(remTrdQty*(vt.ChikPeaO2B__Usage_Rate__c!=null?vt.ChikPeaO2B__Usage_Rate__c:0));
                    }
                    vtpc++;
                }
                return (totalprice/totalusage);
            }
            else
                return 0;    
        }
        else
            return 0;
    }
}